#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

check_lint_usage()
{
    echo "Check project for Coding Rules compliance."
    echo
    echo "Usage: $(basename ${0}) [options] [MODULE]"
    echo
    echo "By default, checks all modules, except those which are ignored"
    echo "(see options for details). Optionally, modules to check can be"
    echo "explicitly specified via arguments."
    echo
    echo "Options:"
    echo
    echo "  --help (-h)             Print this help information and exit."
    echo
    echo "  --full-check (-f)       Perform complete check (all modules)."
    echo "                          Default: OFF."
    echo
    echo "  --check-code-aster (-c) Additionally check code_aster catalogue."
    echo "                          Ignored if --full-check is ON. Default: OFF."
    echo
    echo "  --check-test (-t)       Additionally check test scripts."
    echo "                          Ignored if --full-check is ON. Default: OFF."
    echo
    echo "  --full-report (-u)      Display full report. Default: OFF."
    echo
    echo "  --parallel (-j/-p)      Run in parallel. Default: OFF."
    echo "                          Warning: running in parallel can give unstable"
    echo "                          results."
    echo
    exit 0
}

check_lint_main()
{
    if [ "${SUBMIT_FORCE}" = "LINT" ]; then
        return 0
    fi

    local full_check=0
    local check_codeaster=0
    local check_test=0
    local run_parallel=0
    local full_report=0

    local option
    while getopts ":-:hfctjpu" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) check_lint_usage ;;
                full-check ) full_check=1 ;;
                check-code-aster ) check_codeaster=1 ;;
                check-test ) check_test=1 ;;
                parallel ) run_parallel=1 ;;
                full-report ) full_report=1 ;;
                * ) echo "Wrong option: --${OPTARG}" ; exit 1 ;;
            esac
        else
            case ${option} in
                h ) check_lint_usage ;;
                f ) full_check=1 ;;
                c ) check_codeaster=1 ;;
                t ) check_test=1 ;;
                j | p ) run_parallel=1 ;;
                u ) full_report=1 ;;
                ? ) echo "Wrong option" ; exit 1 ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    local here=$(readlink -f $(dirname "${0}"))
    local pylintrc=${here}/.pylintrc

    # default ignore files are defined in pylintrc file
    local ignored=$(grep -E "^ignore=" ${pylintrc} | sed -e "s%^ignore *= *\(\)%\1%g")

    if [ "${full_check}" = "1" ]
    then
        ignored=$(grep -E "^never_check=" ${pylintrc} | sed -e "s%^never_check *= *\(\)%\1%g")
    else
        test "${check_codeaster}" = "0" && ignored="${ignored},code_aster_version"
        test "${check_test}" = "0" && ignored="${ignored},test"
    fi

    local bin_scripts=(${ASTERSTUDYDIR}/bin/{ajs2apb,ajs2ast,apb2ajs,apb2ast,ast2ajs,ast2apb,asterstudy,asterstudy_assistant,comm2code,comm2study,debug_ajs})
    local packages=(${ASTERSTUDYDIR}/asterstudy)
    local other=(${ASTERSTUDYDIR}/salome/{ASTERSTUDYGUI.py,asterstudy_setenv.py})

    local options
    options="${options} --rcfile=${pylintrc}"
    options="${options} --ignore=${ignored}"
    options="${options} --disable=locally-disabled,file-ignored,cyclic-import --output-format=parseable"
    test "${run_parallel}" = 1 && options="${options} --jobs=$(nproc)"
    test "${full_report}" = 1 && options="${options} --reports=y" || options="${options} --reports=n --disable=I"

    local report=${ASTERSTUDYDIR}/pylint_output.txt
    rm -f ${report}

    pylint --version || return 2

    local ret=0
    (
        set -x
        find ${ASTERSTUDYDIR} -name '*.pyc' -delete
        pylint ${options} ${*:-${bin_scripts[*]} ${packages[*]} ${other[*]}} > ${report}
    )
    ret=${?}

    cat ${report}
    printf "\npylint checkings ended: "
    if [ ${ret} -ne 0 ]
    then
        printf "FAILED\n\n"
    else
        printf "ok\n\n"
    fi

    return ${ret}
}

check_lint_main "${@}"
exit ${?}
