#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

check_persistence_main()
{
    local dir=${ASTERSTUDYDIR}/data/persistence
    set -x
    make --directory=${dir} --makefile=Makefile.unittest ${*} all
    local iret=${?}

    if [ ${iret} -eq 0 ]
    then
        printf "\nReverting changes in 'data/persistence'...\n"
        if [ -d .git ]
        then
            git checkout -- data/persistence
        fi
        make --directory=${dir} --makefile=Makefile.unittest clean
    fi

    return ${iret}
}

check_persistence_main "${@}"
exit ${?}
