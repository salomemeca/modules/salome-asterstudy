# Additional environment for Calibre9
# Add here what is not done by 'salome shell'
where_am_i=$(dirname ${BASH_SOURCE[0]})

# dependencies are listed in appli_XXXX/salome
module load "boost/1.58.0" \
            "cminpack/1.3.6" \
            "gl2ps/1.4.0.1" \
            "hdf5/1.10.3" \
            "lapack/3.8.0" \
            "melissa/0.0.0" \
            "numpy/1.15.1" \
            "occ/7.3.2" \
            "omniorb/4.2.2" \
            "omniorbpy/4.2.2" \
            "opencv/3.2.0" \
            "openturns/1.13.0" \
            "pv/5.6.1" \
            "py36-nlopt/2.4.2" \
            "python/3.6.5" \
            "python3-docutils/0.12" \
            "python3-jinja/2.7.3" \
            "python3-matplotlib/2.2.2" \
            "python3-pygments/2.0.2" \
            "python3-pyqt/5.9.0" \
            "python3-scipy/0.19.1" \
            "python3-sip/4.19.3" \
            "python36-cython3/0.25.2" \
            "python36-h5py/2.8.0" \
            "python36-six/1.12.0" \
            "qt/5.9.1" \
            "swig/3.0.12" \
            "zeromq/4.3.1"

# more dependencies (provided by salome-prerequisites-9.4.0)
module load "python3-sphinx/1.7.6"

# tools needed for development:
# - protoc just compiled from source.
# - devtools for mercurial hooks
export PATH=${HOME}/inst/bin:${HOME}/dev/codeaster/devtools/bin:${PATH}
export PYTHONPATH=${PYTHONPATH}:${HOME}/dev/codeaster/devtools/lib

# load virtualenv built with Python from SALOME
# + pip install -r .requirements.txt
test -e ${where_am_i}/.env_virtualenv.sh && . ${where_am_i}/.env_virtualenv.sh

# For example .env_virtualenv.sh contains:
# . ${HOME}/env/.virtualenvs/asterstudydev/bin/activate

# Re-prepend ASTERSTUDY paths after sourcing SALOME environment
export PATH=${ASTERSTUDYDIR}/bin:${ASTERSTUDYDIR}/dev:${PATH}
export PYTHONPATH=${ASTERSTUDYDIR}:${ASTERSTUDYDIR}/test:${PYTHONPATH}

# to force code_aster execution to set PYTHONHOME to the SALOME one
unset ABSOLUTE_APPLI_PATH
