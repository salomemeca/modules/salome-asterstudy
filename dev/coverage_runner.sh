#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

coverage_runner_main()
{
    local root_dir=${ASTERSTUDYDIR}
    local test_dir=${root_dir}/test

    local files=${test_dir}/${1}
    local packages=${2}
    local min_percentage=${3}
    local branches=0
    local parallel=0

    local nose_options="-c ${test_dir}/.noserc"
    processes=$(($(nproc)-1)) && if ((processes<2)) ; then processes=1 ; fi
    test "${parallel}" = "1" && nose_options="${nose_options} --processes=${processes} --process-timeout=1000"
    local cover_options="--rcfile=${test_dir}/.coveragerc"
    local run_options=""
    test "${parallel}" = "1" && run_options+="${run_options} --parallel-mode --concurrency=multiprocessing"
    if [ "${branches}" = "1" ]
    then
        test "${parallel}" = "1" && echo "Warning: --branches option cannot be combined with --parallel option" \
            || run_options="${run_options} --branch"
    fi

    local result=0

    echo

    # erase previous results
    (set -x && coverage erase ${cover_options})
    rm -f .coverage .coverage.*

    (set -x && coverage run ${cover_options} ${run_options} $(which nosetests) ${nose_options} ${files})
    result=$((result+${?}))

    # combine results
    test "${parallel}" = "1" && (set -x && coverage combine ${cover_options} .coverage.*)

    # generate XML output
    (set -x coverage xml ${cover_options})

    # show report
    (set -x && coverage report ${cover_options} --include=${packages} --fail-under=${min_percentage})
    result=$((result+${?}))

    return ${result}
}

coverage_runner_main "${@}"
exit ${?}
