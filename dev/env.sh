# Copyright 2016-2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

where_am_i=$(dirname ${BASH_SOURCE[0]})

if [ "${CMAKE_ROOT_DIR}x" = "x" ]; then
    test -e /dn25/salome/vsr/ASTER/env_9x.sh && . /dn25/salome/vsr/ASTER/env_9x.sh
    test -e /PRODUCTS/env.sh && . /PRODUCTS/env.sh
    if [ -z "${SINGULARITY_NAME}" ]; then
        test -e ${where_am_i}/.env_salome.sh && . ${where_am_i}/.env_salome.sh
        [ $(egrep -c 'Calibre|Scibian' /etc/issue) -eq 1 ] && [ -e ${where_am_i}/.env_instc9.sh ] && . ${where_am_i}/.env_instc9.sh
    else
        [ -e ${where_am_i}/.env_sif.sh ] && . ${where_am_i}/.env_sif.sh
    fi
fi

export ASTERSTUDYDIR=$(readlink -f ${where_am_i}/..)
export PATH=${ASTERSTUDYDIR}/bin:${ASTERSTUDYDIR}/dev:${PATH}
export PYTHONPATH=${ASTERSTUDYDIR}:${ASTERSTUDYDIR}/test:${PYTHONPATH}

unset where_am_i
