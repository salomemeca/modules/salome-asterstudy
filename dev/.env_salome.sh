# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

remove_path()
{
    if [ ${#} -eq 0 ]
    then
        echo "usage: remove_path value pattern1 [pattern2 [...]]"
        echo
        echo "    Returns the 'value' with excluding the given patterns."
        echo
        echo "    Example of use: export PATH=\`remove_path \"\${PATH}\" ${HOME}/local\`"
        exit
    fi

    if [ ${#} -lt 2 ]
    then
        echo ${1}
        return
    fi

    local values=${1}
    shift

    local i
    for i in ${@}
    do
        values=$(echo ${values} | tr ":" "\n" | grep -v -F ${i} | tr "\n" ":" | sed -e "s%:\+%:%g;s%^:%%g;s%:$%%g")
    done

    echo ${values}
}

if [ -z "$SALOME_VERSION" ]
then
    SALOME_VERSION=V9_INTEGR
fi
APPLI=${HOME}/salome_meca_integr/appli_${SALOME_VERSION}
envSalome=${HOME}/salome_meca_integr/${SALOME_VERSION}/salome_prerequisites.sh

# salome installation with embedded prerequisites
if [ -z "${ROOT_SALOME}" ] && [ -e ${envSalome} ]
then
    . ${envSalome}

    # need for SalomePyQt, setenv...
    export PYTHONPATH=${PYTHONPATH}:${APPLI}/bin/salome:${APPLI}/lib/python3.6/site-packages/salome:${APPLI}/lib/salome
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${APPLI}/lib/salome

    # environment variable probably set in a SALOME session, needed by smeshBuilder
    [ -z "${SMESH_MeshersList}" ] && export SMESH_MeshersList=""

    # keep current directory first to avoid sourcing a env.sh in $PATH (prerequisites/Occ-691)
    export PATH=.:${PATH}
fi

export LD_LIBRARY_PATH=$(remove_path ${LD_LIBRARY_PATH} ASTER_default Eficas_nouveau InterfaceQT4)
export PYTHONPATH=$(remove_path ${PYTHONPATH} ASTER_default Eficas_nouveau InterfaceQT4 Paraview)
