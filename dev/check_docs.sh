#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

check_docs_cov()
{
    # list modules, which don't need to be documented, here:
    local exceptions="asterstudy.common.debug asterstudy.gui.guitest asterstudy.gui.widgets.dirwidget asterstudy.datamodel.asterstudy_pb2 asterstudy.datamodel.test.__init__ asterstudy.datamodel.test.dict_categories_test asterstudy.datamodel.test.dict_categories_fake"

    # get rst files
    local doc_files=$(find ${doc_dir} -name "*.rst")
    local doc_modules

    # parse rst files and check what modules are documented
    local doc_file
    for doc_file in ${doc_files}
    do
        local files=$(grep "automodule" ${doc_file} | sed -e "s%.*\s*::\s*\([A-Za-z._]\+\)%\1%g")
        doc_modules="${doc_modules} ${files}"
    done

    # get python modules
    local python_modules
    local package
    for package in asterstudy/{common,datamodel,gui,assistant,api}
    do
        local files=$(find ${root_dir}/${package} -name "*.py" | grep -v ".#" | sed -e "s%^${root_dir}/%%g;s%/%.%g;s%\.py$%%g")
        python_modules="${python_modules} ${files}"
    done

    local ok=0
    for package in ${python_modules}
    do
        echo ${exceptions} | grep -Eq "\<${package}\>" && continue
        echo ${doc_modules} | grep -Evq "\<${package}\>" && \
            echo "${package} is not documented" > /dev/stderr && ok=$((ok+1))
    done
    return ${ok}
}

check_docs_main()
{
    local root_dir=${ASTERSTUDYDIR}
    local doc_dir=${root_dir}/docs
    local steps="${*:-doctest html coverage cov}"

    echo "--- Cleaning docs" && make clean --directory=${doc_dir} --file=Makefile.sphinx

    local result=0
    local step
    for step in ${steps}
    do
        if [ "${step}" = "cov" ]
        then
            echo "--- Run ${step}" && check_docs_cov || result=$((result+1))
        else
            echo "--- Run ${step}" && make ${step} --directory=${doc_dir} --file=Makefile.sphinx \
                || result=$((result+1))
        fi
    done

    return ${result}
}

check_docs_main "${@}"
exit ${?}
