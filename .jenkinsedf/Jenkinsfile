#!groovy
pipeline {
    options {
        timestamps()
        timeout(time: 2, unit: 'HOURS')
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '20'))
    }
    agent {
        docker {
            image "localhost:5000/intg-asterstudy:V9_INTEGR-20200525"
        }
    }

    environment {
        ADM = credentials("SCM_ADMINASTER")
    }

    stages {
        stage('Init') {
            steps {
                sh """#!/bin/bash -l
                cat << EOF | curl --url smtp://mailhost.der.edf.fr \
                    --mail-from "no-reply@edf.fr" \
                    --mail-rcpt "CI asterstudy <afaa4694.edfonline.onmicrosoft.com@emea.teams.ms>" \
                    --upload-file -
From: "CI Robot" <no-reply@edf.fr>
To: "CI asterstudy " <afaa4694.edfonline.onmicrosoft.com@emea.teams.ms>
Content-Type: text/html; charset=utf-8
Mime-version: 1.0
Subject: [asterstudy] Starting ${env.JOB_NAME} #${env.BUILD_NUMBER}

<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head><body>
<p>Job started -
<a href="${env.BUILD_URL}/display/redirect">click here for details</a>
</p>
</body></html>

EOF
                """
            }
        }
        stage('CheckTools') {
            // Check for development tools
            steps {
                sh '''#!/bin/bash -l
                find . -name '*.pyc' -delete
                cp ${HOME}/env/.env_virtualenv.sh ./dev/
                source ./dev/env.sh

                which pylint || exit 1
                pylint --version
                which nosetests || exit 1
                nosetests --version
                which protoc || exit 1
                protoc --version
                which salome || exit 1
                salome --version

                exit 0
                '''
            }
        }
        stage('PreCheck') {
            steps {
                sh '''#!/bin/bash -l
                ./dev/check_automerge_edf.sh
                '''
            }
        }
        stage('Test') {
            steps {
                sh '''#!/bin/bash -l
                source ./dev/env.sh
                source $HOME/env/env_init_check.sh
                ./dev/check.sh --edfci --stop
                '''
            }
        }
        stage('Merge') {
            when {
                expression { return currentBuild.currentResult == "SUCCESS" }
            }
            steps {
                sh """#!/bin/bash -l

                # install may update '.ts' file, ignore it
                hg revert -C resources/AsterStudy_msg_fr.ts
                # may appear with different version of protobuf
                hg revert -C asterstudy/datamodel/asterstudy_pb2.py

                cp  .hg/hgrc .hg/hgrc.bck
                cat << EOF >> .hg/hgrc
[ui]
username = mergerobot <no-reply@edf.fr>

[extensions]
strip =

[auth]
robot.prefix = http://aster-repo.der.edf.fr/scm/
robot.username = ${ADM_USR}
robot.password = ${ADM_PSW}
EOF
                ./dev/check_automerge_edf.sh --verbose --commit
                ret=\$?

                mv .hg/hgrc.bck .hg/hgrc

                exit \${ret}
                """
            }
        }
    }
    post {
        success {
            sh """#!/bin/bash -l
            cat << EOF | curl --url smtp://mailhost.der.edf.fr \
                --mail-from "no-reply@edf.fr" \
                --mail-rcpt "CI asterstudy <afaa4694.edfonline.onmicrosoft.com@emea.teams.ms>" \
                --upload-file -
From: "CI Robot" <no-reply@edf.fr>
To: "CI asterstudy " <afaa4694.edfonline.onmicrosoft.com@emea.teams.ms>
Content-Type: text/html; charset=utf-8
Mime-version: 1.0
Subject: [asterstudy] Completed - ${env.JOB_NAME} #${env.BUILD_NUMBER}

<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head><body>
<p><a href="${env.BUILD_URL}/display/redirect">Job ${env.JOB_NAME} (#${env.BUILD_NUMBER})</a>
ended successfully.</p>
<p>Branch name: ${env.MERCURIAL_REVISION_BRANCH}</p>
<p>Revision: ${env.MERCURIAL_REVISION_SHORT}</p>
<p>The changes have been integrated in the main branch.</p>
</body></html>

EOF
                """
        }
        failure {
            sh """#!/bin/bash -l
            cat << EOF | curl --url smtp://mailhost.der.edf.fr \
                --mail-from "no-reply@edf.fr" \
                --mail-rcpt "CI asterstudy <afaa4694.edfonline.onmicrosoft.com@emea.teams.ms>" \
                --upload-file -
From: "CI Robot" <no-reply@edf.fr>
To: "CI asterstudy " <afaa4694.edfonline.onmicrosoft.com@emea.teams.ms>
Content-Type: text/html; charset=utf-8
Mime-version: 1.0
Subject: [asterstudy] Failed - ${env.JOB_NAME} #${env.BUILD_NUMBER}

<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head><body>
<p><a href="${env.BUILD_URL}/display/redirect">Job ${env.JOB_NAME} (#${env.BUILD_NUMBER})</a>
ended with failure.</p>
<p>Branch name: ${env.MERCURIAL_REVISION_BRANCH}</p>
<p>Revision: ${env.MERCURIAL_REVISION_SHORT}</p>
<p>The changes have been rejected and won't be integrated in the main branch.</p>
</body></html>

EOF
                """
        }
    }
}
