<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>Commands</name>
    <message>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>Read a mesh</source>
        <translation>Lire un maillage</translation>
    </message>
    <message>
        <source>Input mesh file</source>
        <translation>Fichier de maillage en entrée</translation>
    </message>
</context>
</TS>
