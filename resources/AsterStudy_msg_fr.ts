<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutDlg</name>
    <message>
        <source>About {}</source>
        <translation>A propos de {}</translation>
    </message>
    <message>
        <source>Version: {}</source>
        <translation>Version {}</translation>
    </message>
    <message>
        <source>License Information</source>
        <translation>Détail de la licence</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Crédits</translation>
    </message>
    <message>
        <source>GUI framework for {code_aster}</source>
        <translation>IHM pour {code_aster}</translation>
    </message>
</context>
<context>
    <name>Assistant</name>
    <message>
        <source>untitled</source>
        <translation>sans titre</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Parameter &apos;{}&apos; is not properly defined.</source>
        <translation>Le paramètre &apos;{}&apos; n&apos;est pas correctement défini.</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <source>Choose File</source>
        <translation>Choisir le fichier</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <source>Med files</source>
        <translation>Fichiers Med</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <source>Unselect</source>
        <translation>Désélectionner</translation>
    </message>
    <message>
        <source>Edit...</source>
        <translation>Editer...</translation>
    </message>
    <message>
        <source>Calculation Assistant</source>
        <translation>Assistant de calcul</translation>
    </message>
    <message>
        <source>No choice is made between:</source>
        <translation>Aucun choix n&apos;est fait entre :</translation>
    </message>
</context>
<context>
    <name>AsterStudy</name>
    <message>
        <source>Aster workspace</source>
        <translation>Espace de travail Aster</translation>
    </message>
    <message>
        <source>Null study</source>
        <translation>Etude vide</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Undo last operation</source>
        <translation>Annuler la dernière opération</translation>
    </message>
    <message>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <source>No operations to undo</source>
        <translation>Aucune opération à annuler</translation>
    </message>
    <message>
        <source>Undo %s</source>
        <translation>Annuler %s</translation>
    </message>
    <message>
        <source>Undo %d actions</source>
        <translation>Annuler %d actions</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;Refaire</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <source>Redo last undone operation</source>
        <translation>Refaire la dernière opération</translation>
    </message>
    <message>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <source>No operations to redo</source>
        <translation>Aucune opération à répéter</translation>
    </message>
    <message>
        <source>Redo %s</source>
        <translation>Répéter %s</translation>
    </message>
    <message>
        <source>Redo %d actions</source>
        <translation>Répéter %d actions</translation>
    </message>
    <message>
        <source>Dup&amp;licate</source>
        <translation>Du&amp;pliquer</translation>
    </message>
    <message>
        <source>Duplicate</source>
        <translation>Dupliquer</translation>
    </message>
    <message>
        <source>Create a copy of selected object</source>
        <translation>Créer une copie de l&apos;objet sélectionné</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Remove selected objects</source>
        <translation>Supprimer les objets sélectionnés</translation>
    </message>
    <message>
        <source>Del</source>
        <translation>Suppr</translation>
    </message>
    <message>
        <source>New stage</source>
        <translation>Nouvelle étape</translation>
    </message>
    <message>
        <source>Add an empty stage to the case</source>
        <translation>Ajouter une nouvelle étape au cas</translation>
    </message>
    <message>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <source>Add a stage by importing a command file</source>
        <translation>Ajouter une étape en important un fichier de commande</translation>
    </message>
    <message>
        <source>Add a command from the catalogue</source>
        <translation>Ajouter une commande depuis le catalogue</translation>
    </message>
    <message>
        <source>Rena&amp;me</source>
        <translation>Reno&amp;mmer</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <source>Rename selected object</source>
        <translation>Renommer l&apos;objet</translation>
    </message>
    <message>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <source>&amp;Graphical Mode</source>
        <translation>Mode &amp;graphique</translation>
    </message>
    <message>
        <source>Graphical mode</source>
        <translation>Mode graphique</translation>
    </message>
    <message>
        <source>Switch stage to the graphical mode</source>
        <translation>Passer l&apos;étape en mode graphique</translation>
    </message>
    <message>
        <source>&amp;Text Mode</source>
        <translation>Mode &amp;texte</translation>
    </message>
    <message>
        <source>Text mode</source>
        <translation>Mode texte</translation>
    </message>
    <message>
        <source>Switch stage to the text mode</source>
        <translation>Passer l&apos;étape en mode texte</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <source>&amp;Operations</source>
        <translation>&amp;Opérations</translation>
    </message>
    <message>
        <source>&amp;Commands</source>
        <translation>&amp;Commandes</translation>
    </message>
    <message>
        <source>Text editor error.</source>
        <translation>Erreur de l&apos;éditeur de texte.</translation>
    </message>
    <message>
        <source>Cannot edit stage.</source>
        <translation>Impossible d&apos;éditer l&apos;étape.</translation>
    </message>
    <message>
        <source>Text editor is not set.</source>
        <translation>Editeur de texte non défini.</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation>Ouvrir fichier</translation>
    </message>
    <message>
        <source>Study files</source>
        <translation>Fichiers de l&apos;étude</translation>
    </message>
    <message>
        <source>Cannot load study.</source>
        <translation>Impossible de charger l&apos;étude.</translation>
    </message>
    <message>
        <source>Cannot save null study.</source>
        <translation>Impossible d&apos;enregistrer une étude vide.</translation>
    </message>
    <message>
        <source>Cannot save not modified study.</source>
        <translation>Impossible d&apos;enregistrer une étude non modifiée.</translation>
    </message>
    <message>
        <source>Save File</source>
        <translation>Enregistrer fichier</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>New study</source>
        <translation>Nouvelle étude</translation>
    </message>
    <message>
        <source>Create new study</source>
        <translation>Créer une nouvelle étude</translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <source>Open study</source>
        <translation>Ouvrir étude</translation>
    </message>
    <message>
        <source>Open study from the the file on a disk</source>
        <translation>Ouvrir une étude</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>Save study</source>
        <translation>Sauvegarder l&apos;étude</translation>
    </message>
    <message>
        <source>Save study to the file on a disk</source>
        <translation>Sauvegarder l&apos;étude dans un fichier</translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>Sauveg&amp;arder sous...</translation>
    </message>
    <message>
        <source>Save study with new name</source>
        <translation>Sauvegarder l&apos;étude dans un nouveau fichier</translation>
    </message>
    <message>
        <source>Save study to the alternative location on a disk</source>
        <translation>Sauvegarder l&apos;étude à un nouvel emplacement</translation>
    </message>
    <message>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Close study</source>
        <translation>Fermer l&apos;étude</translation>
    </message>
    <message>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <source>Quit application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <source>&amp;Preferences...</source>
        <translation>&amp;Préférences...</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Edit application&apos;s preferences</source>
        <translation>Editer les préférences de l&apos;application</translation>
    </message>
    <message>
        <source>&amp;User&apos;s Guide</source>
        <translation>Guide &amp;utilisateur</translation>
    </message>
    <message>
        <source>User&apos;s Guide</source>
        <translation>Guide utilisateur</translation>
    </message>
    <message>
        <source>Show user&apos;s manual</source>
        <translation>Voir le manuel utilisateur</translation>
    </message>
    <message>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <source>&amp;About...</source>
        <translation>&amp;A propos...</translation>
    </message>
    <message>
        <source>About application</source>
        <translation>A propos de l&apos;application</translation>
    </message>
    <message>
        <source>Display information about this application</source>
        <translation>Affiche des informations à propos de cette application</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <source>&amp;Toolbars</source>
        <translation>&amp;Barres d&apos;outils</translation>
    </message>
    <message>
        <source>&amp;Panels</source>
        <translation>&amp;Panneaux</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <source>Do you want to save changes to %s?</source>
        <translation>Voulez-vous enregistrer les modifications vers %s ?</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <source>Cannot duplicate node</source>
        <translation>Impossible de dupliquer le noeud</translation>
    </message>
    <message>
        <source>Null case</source>
        <translation>Cas vide</translation>
    </message>
    <message>
        <source>Import File</source>
        <translation>Importer fichier</translation>
    </message>
    <message>
        <source>Command files</source>
        <translation>Fichiers de commandes</translation>
    </message>
    <message>
        <source>Cannot import stage</source>
        <translation>Impossible d&apos;importer l&apos;étape</translation>
    </message>
    <message>
        <source>The name is not valid. The old name is restored.</source>
        <translation>Le nom n&apos;est pas valide. L&apos;ancien nom est utilisé.</translation>
    </message>
    <message>
        <source>Add command</source>
        <translation>Ajouter commande</translation>
    </message>
    <message>
        <source>Cannot add command</source>
        <translation>Impossible d&apos;ajouter une commande</translation>
    </message>
    <message>
        <source>Switch to graphical mode</source>
        <translation>Passer en mode graphique</translation>
    </message>
    <message>
        <source>The item is not a stage</source>
        <translation>L&apos;élément n&apos;est pas une étape</translation>
    </message>
    <message>
        <source>Cannot convert the stage to graphic mode</source>
        <translation>Impossible de convertir l&apos;étape en mode graphique</translation>
    </message>
    <message>
        <source>Switch to text mode</source>
        <translation>Passer en mode texte</translation>
    </message>
    <message>
        <source>Cannot convert the stage to text mode</source>
        <translation>Impossible de convertir l&apos;étape en mode texte</translation>
    </message>
    <message>
        <source>Edit stage</source>
        <translation>Editer l&apos;étape</translation>
    </message>
    <message>
        <source>Stage is not in text mode</source>
        <translation>L&apos;étape n&apos;est pas en mode texte</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Export File</source>
        <translation>Exporter fichier</translation>
    </message>
    <message>
        <source>Unexpected error</source>
        <translation>Erreur inattendue</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <source>Value:</source>
        <translation>Valeur :</translation>
    </message>
    <message>
        <source>Traceback:</source>
        <translation>Trace :</translation>
    </message>
    <message>
        <source>Edit selected object</source>
        <translation>Editer l&apos;objet</translation>
    </message>
    <message>
        <source>Category: {}</source>
        <translation>Catégorie : {}</translation>
    </message>
    <message>
        <source>Add command from &apos;{}&apos; category</source>
        <translation>Ajouter une commande de la catégorie &apos;{}&apos;</translation>
    </message>
    <message>
        <source>Not implemented yet</source>
        <translation>Pas encore implémenté</translation>
    </message>
    <message>
        <source>Selected objects will be deleted. Are you sure?</source>
        <translation>Les objets sélectionnés seront supprimés. Etes-vous sûr ?</translation>
    </message>
    <message>
        <source>Show/hide &apos;{}&apos; view</source>
        <translation>Afficher/Masquer la vue &apos;{}&apos;</translation>
    </message>
    <message>
        <source>Python console</source>
        <translation>Console Python</translation>
    </message>
    <message>
        <source>&amp;Console</source>
        <translation>&amp;Console</translation>
    </message>
    <message>
        <source>Command with type &apos;{}&apos; successfully added</source>
        <translation>La commande de type &apos;{}&apos; a été ajoutée avec succès</translation>
    </message>
    <message>
        <source>Load script</source>
        <translation>Charger script</translation>
    </message>
    <message>
        <source>Python scripts</source>
        <translation>Scripts Python</translation>
    </message>
    <message>
        <source>&amp;Load Script...</source>
        <translation>&amp;Charger script...</translation>
    </message>
    <message>
        <source>Execute script in the embedded Python console</source>
        <translation>Exécuter un script dans la console embarquée</translation>
    </message>
    <message>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <source>&amp;New Case</source>
        <translation>&amp;Nouveau cas</translation>
    </message>
    <message>
        <source>New case</source>
        <translation>Nouveau cas</translation>
    </message>
    <message>
        <source>Add an empty case to the study</source>
        <translation>Ajouter un cas vide à l&apos;étude</translation>
    </message>
    <message>
        <source>&amp;Copy As Current</source>
        <translation>&amp;Copier comme cas courant</translation>
    </message>
    <message>
        <source>Copy as current</source>
        <translation>Copier comme cas courant</translation>
    </message>
    <message>
        <source>Copy content of the selected case into the Current case</source>
        <translation>Copier le contenu du cas sélectionné dans le cas courant</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <source>&amp;View Case (read-only)</source>
        <translation>&amp;Vue cas (lecture seule)</translation>
    </message>
    <message>
        <source>View case (read-only)</source>
        <translation>Vue cas (lecture seule)</translation>
    </message>
    <message>
        <source>View selected case (read-only)</source>
        <translation>Voir le cas sélectionné (lecture seule)</translation>
    </message>
    <message>
        <source>&amp;Edit Case</source>
        <translation>&amp;Editer cas</translation>
    </message>
    <message>
        <source>Edit case</source>
        <translation>Editer le cas</translation>
    </message>
    <message>
        <source>Edit selected Case</source>
        <translation>Editer le cas sélectionné</translation>
    </message>
    <message>
        <source>View selected object</source>
        <translation>Vue objet</translation>
    </message>
    <message>
        <source>Current case is not empty. All changes will be discarded.</source>
        <translation>Le cas courant n&apos;est pas vide. Tous les changements seront annulés.</translation>
    </message>
    <message>
        <source>Cannot remove case used by other case(s).</source>
        <translation>Impossible de supprimer un cas utilisé par d&apos;autres cas.</translation>
    </message>
    <message>
        <source>Do you want to remove case used by other case(s)?</source>
        <translation>Voulez-vous supprimer les cas utilisés par d&apos;autres cas ?</translation>
    </message>
    <message>
        <source>There are data files with undefined file name.</source>
        <translation>Il existe des fichiers sans nom.</translation>
    </message>
    <message>
        <source>Undefined files</source>
        <translation>Fichiers non définis</translation>
    </message>
    <message>
        <source>Choose code_aster version</source>
        <translation>Choisir la version de code_aster</translation>
    </message>
    <message>
        <source>&amp;Import Case</source>
        <translation>&amp;Importer un Cas</translation>
    </message>
    <message>
        <source>Import case</source>
        <translation>Import un cas</translation>
    </message>
    <message>
        <source>Add a case by importing an export file</source>
        <translation>Ajoute un cas à partir d&apos;un fichier export</translation>
    </message>
    <message>
        <source>Import an export file from ASTK</source>
        <translation>Importe un fichier export de ASTK</translation>
    </message>
    <message>
        <source>Cannot import case</source>
        <translation>Impossible d&apos;importer un cas</translation>
    </message>
    <message>
        <source>Ctrl+Shift+I</source>
        <translation>Ctrl+Shift+I</translation>
    </message>
    <message>
        <source>Document&amp;ation</source>
        <translation>Document&amp;ation</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <source>Show documentation of selected command</source>
        <translation>Ouvre la documentation de la commande</translation>
    </message>
    <message>
        <source>Missing study directory</source>
        <translation>Répertoire d&apos;étude manquant</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Inconsistent study directory</source>
        <translation>Répertoire d&apos;étude incohérent</translation>
    </message>
    <message>
        <source>Execution error</source>
        <translation>Erreur à l&apos;exécution</translation>
    </message>
    <message>
        <source>Conversion error</source>
        <translation>Erreur à la conversion</translation>
    </message>
    <message>
        <source>Existing embedded files</source>
        <translation>Fichiers embarqués existants</translation>
    </message>
    <message>
        <source>[noname]</source>
        <translation>[sans-nom]</translation>
    </message>
    <message>
        <source>Create &amp;Variable</source>
        <translation>Créer &amp;variable</translation>
    </message>
    <message>
        <source>Create Variable</source>
        <translation>Créer variable</translation>
    </message>
    <message>
        <source>Create a python variable</source>
        <translation>Créer une nouvelle variable python</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <source>Copy selected objects to the clipboard</source>
        <translation>Copier la commande ou la variable sélectionnée</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Co&amp;uper</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <source>Cut selected objects to the clipboard</source>
        <translation>Couper la commande ou la variable sélectionnée</translation>
    </message>
    <message>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>C&amp;oller</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Coller la commande</translation>
    </message>
    <message>
        <source>Paste objects from the clipboard</source>
        <translation>Coller la commande ou variable sélectionnée</translation>
    </message>
    <message>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <source>Cannot cut node(s)</source>
        <translation>Impossible de couper le(s) noeud(s)</translation>
    </message>
    <message>
        <source>Cannot paste node(s)</source>
        <translation>Impossible de coller le(s) noeud(s)</translation>
    </message>
    <message>
        <source>This operation will remove selected stage(s) and all subsequent ones.
Continue?</source>
        <translation>Cette opération va supprimer la ou les étapes sélectionnées et tous les descendants.
Continuer ?</translation>
    </message>
    <message>
        <source>Med files</source>
        <translation>Fichiers Med</translation>
    </message>
    <message>
        <source>Other mesh files</source>
        <translation>Autres fichiers de maillages</translation>
    </message>
    <message>
        <source>Text files</source>
        <translation>Fichiers texte</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <source>0D elements</source>
        <translation>Eléments 0D</translation>
    </message>
    <message>
        <source>1D elements</source>
        <translation>Eléments 1D</translation>
    </message>
    <message>
        <source>2D elements</source>
        <translation>Eléments 2D</translation>
    </message>
    <message>
        <source>3D elements</source>
        <translation>Eléments 3D</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>The stage is syntactically invalid.
Come back to graphical mode may be not completely possible.

Do you want to continue?</source>
        <translation>La syntaxe de l&apos;étape n&apos;est pas valide.
Certaines informations pourraient ne pas être conservées dans le mode graphique.

Voulez-vous continuer ?</translation>
    </message>
    <message>
        <source>Ctrl+Shift+E</source>
        <translation>Ctrl+Shift+E</translation>
    </message>
    <message>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Shift+A</translation>
    </message>
    <message>
        <source>Ctrl+Shift+G</source>
        <translation>Ctrl+Shift+G</translation>
    </message>
    <message>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <source>Ctrl+Shift+V</source>
        <translation>Ctrl+Shift+V</translation>
    </message>
    <message>
        <source>Ctrl+Shift+F1</source>
        <translation>Ctrl+Shift+F1</translation>
    </message>
    <message>
        <source>Edit &amp;Comment</source>
        <translation>Editer &amp;commentaire</translation>
    </message>
    <message>
        <source>Edit comment</source>
        <translation>Editer le commentaire</translation>
    </message>
    <message>
        <source>Edit comment for the selected object</source>
        <translation>Editer le commentaire d&apos;un objet</translation>
    </message>
    <message>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <source>Hide unused</source>
        <translation>Masquer non utilisé</translation>
    </message>
    <message>
        <source>Hide Unused</source>
        <translation>Masquer non utilisé</translation>
    </message>
    <message>
        <source>Hide unused keywords</source>
        <translation>Masquer les mots-clés non utilisés</translation>
    </message>
    <message>
        <source>What&apos;s This?</source>
        <translation>Qu&apos;est-ce que c&apos;est ?</translation>
    </message>
    <message>
        <source>What&apos;s this?</source>
        <translation>Qu&apos;est-ce que c&apos;est ?</translation>
    </message>
    <message>
        <source>Show element&apos;s description</source>
        <translation>Afficher la description d&apos;un élément</translation>
    </message>
    <message>
        <source>Existing embedded files have been detected in directory {0}.
This could be due to Salome stopping unexpectedly.
Loading will override them.
Do you wish to continue?</source>
        <translation>Des fichiers embarqués ont été détectés dans le répertoire {0}.
Cela peut être dû à un arrêt brutal de Salome.
Charger l&apos;étude va les écraser.
Souhaitez-vous continuer ?</translation>
    </message>
    <message>
        <source>&amp;Add File</source>
        <translation>Ajouter &amp;fichier</translation>
    </message>
    <message>
        <source>Add file</source>
        <translation>Ajouter un fichier</translation>
    </message>
    <message>
        <source>Add a data file to the stage</source>
        <translation>Ajouter un fichier à l&apos;étape</translation>
    </message>
    <message>
        <source>&amp;Go To</source>
        <translation>&amp;Aller à</translation>
    </message>
    <message>
        <source>Go to</source>
        <translation>Aller à</translation>
    </message>
    <message>
        <source>Go to the selected command</source>
        <translation>Aller à la commande sélectionnée</translation>
    </message>
    <message>
        <source>&amp;Embedded</source>
        <translation>E&amp;mbarquer</translation>
    </message>
    <message>
        <source>Embedded</source>
        <translation>Embarquer un fichier</translation>
    </message>
    <message>
        <source>Embed/Unembed selected data file</source>
        <translation>Embarquer/Détacher un fichier</translation>
    </message>
    <message>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <source>Embed file</source>
        <translation>Fichier embarqué</translation>
    </message>
    <message>
        <source>Unembed file</source>
        <translation>Fichier détaché</translation>
    </message>
    <message>
        <source>&amp;Open In Editor</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <source>Open in editor</source>
        <translation>Ouvrir avec un éditeur</translation>
    </message>
    <message>
        <source>View selected data file in the text editor</source>
        <translation>Ouvrir le fichier avec un éditeur de texte</translation>
    </message>
    <message>
        <source>Open in ParaVis</source>
        <translation>Ouvrir dans ParaVis</translation>
    </message>
    <message>
        <source>View selected file in SALOME ParaVis module</source>
        <translation>Ouvrir le fichier avec le module ParaVis de SALOME</translation>
    </message>
    <message>
        <source>Edit file</source>
        <translation>Editer le fichier</translation>
    </message>
    <message>
        <source>Cannot write file.</source>
        <translation>Impossible d&apos;écrire le fichier.</translation>
    </message>
    <message>
        <source>File &apos;{}&apos; does not exist.</source>
        <translation>Le fichier &apos;{}&apos; n&apos;existe pas.</translation>
    </message>
    <message>
        <source>{} bytes</source>
        <translation>{} octets</translation>
    </message>
    <message>
        <source>File &apos;{}&apos; is quite big ({}).

Do you confirm opening it in a text editor?</source>
        <translation>La taille du fichier &apos;{}&apos; est très importante ({}).

Souhaitez-vous quand même, l&apos;ouvrir dans l&apos;éditeur de texte ?</translation>
    </message>
    <message>
        <source>&amp;Back Up</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>Back up</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <source>Create backup copy of the Current case</source>
        <translation>Créer une copie de sauvegarde du cas courant</translation>
    </message>
    <message>
        <source>Edit Desc&amp;ription</source>
        <translation>Editer &amp;description</translation>
    </message>
    <message>
        <source>Edit description</source>
        <translation>Editer la description</translation>
    </message>
    <message>
        <source>Edit description of the selected case</source>
        <translation>Editer la description du cas</translation>
    </message>
    <message>
        <source>Re-&amp;run</source>
        <translation>Ré-e&amp;xécuter</translation>
    </message>
    <message>
        <source>Re-run</source>
        <translation>Ré-exécuter</translation>
    </message>
    <message>
        <source>Execute selected case with the previous parameters</source>
        <translation>Exécuter le cas sélectionné avec les paramètres précédents</translation>
    </message>
    <message>
        <source>&amp;Delete Results</source>
        <translation>Supprimer &amp;résultats</translation>
    </message>
    <message>
        <source>Delete results</source>
        <translation>Supprimer les résultats</translation>
    </message>
    <message>
        <source>Remove results from the selected Run case</source>
        <translation>Supprimer les résultats du cas</translation>
    </message>
    <message>
        <source>Read-only (click here to switch back to edit {})</source>
        <translation>Lecture seule (cliquer ici pour revenir au mode édition {})</translation>
    </message>
    <message>
        <source>Cyclic dependency detected</source>
        <translation>Dépendance cyclique détectée</translation>
    </message>
    <message>
        <source>Name &apos;{}&apos; is already in use</source>
        <translation>Le nome &apos;{}&apos; est déjà utilisé</translation>
    </message>
    <message>
        <source>Elements</source>
        <translation>Eléments</translation>
    </message>
    <message>
        <source>Export files</source>
        <translation>Fichiers d&apos;export</translation>
    </message>
    <message>
        <source>Astk files</source>
        <translation>Fichiers Astk</translation>
    </message>
    <message>
        <source>From flasheur directory</source>
        <translation>Depuis le répertoire flasheur</translation>
    </message>
    <message>
        <source>Edit properties of selected data file</source>
        <translation>Editer les propriétés du fichier</translation>
    </message>
    <message>
        <source>View file</source>
        <translation>Voir le fichier</translation>
    </message>
    <message>
        <source>View properties of selected data file</source>
        <translation>Voir les propriétés du fichier</translation>
    </message>
    <message>
        <source>Remove selected data file from the stage</source>
        <translation>Supprimer le fichier de l&apos;étape</translation>
    </message>
    <message>
        <source>Syntax problem</source>
        <translation>Problème de syntaxe</translation>
    </message>
    <message>
        <source>Broken dependencies</source>
        <translation>Dépendances cassées</translation>
    </message>
    <message>
        <source>Naming conflict</source>
        <translation>Conflit sur le nom</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>Rec&amp;hercher</translation>
    </message>
    <message>
        <source>Find</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Find objects in the study</source>
        <translation>Rechercher des objets</translation>
    </message>
    <message>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <source>&amp;Set-up Directories</source>
        <translation>&amp;Configurer répertoires</translation>
    </message>
    <message>
        <source>Set-up directories</source>
        <translation>Configurer les répertoires</translation>
    </message>
    <message>
        <source>Set-up input and output directories of the case</source>
        <translation>Configurer les répertoires d&apos;entrée et sortie du cas</translation>
    </message>
    <message>
        <source>Show selected file in the explorer</source>
        <translation>Voir le fichier dans l&apos;explorateur</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <source>Case</source>
        <translation>Cas</translation>
    </message>
    <message>
        <source>Stage</source>
        <translation>Etape</translation>
    </message>
    <message>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>Macro</source>
        <translation>Macro</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <source>From stage: {}</source>
        <translation>De l&apos;étape : {}</translation>
    </message>
    <message>
        <source>&amp;Remove Directory</source>
        <translation>S&amp;upprimer répertoire</translation>
    </message>
    <message>
        <source>Remove directory</source>
        <translation>Supprimer le répertoire</translation>
    </message>
    <message>
        <source>Remove directory and all enclosed files from the disk</source>
        <translation>Supprime le répertoire et tous ses fichiers du disque</translation>
    </message>
    <message>
        <source>You are removing one or more directories from study. Remove also all related data files?</source>
        <translation>Vous allez supprimer un ou plusieurs répertoires de l&apos;étude. Voulez-vous également supprimer les fichiers de données associés ?</translation>
    </message>
    <message>
        <source>Warning: Files belonging to graphical stages will not be removed!</source>
        <translation>Attention : les fichiers relatifs aux étapes en mode graphique ne seront pas supprimés !</translation>
    </message>
    <message>
        <source>Cannot remove directory</source>
        <translation>Impossible de supprimer le répertoire</translation>
    </message>
    <message>
        <source>Errors occurred during checking the study directory</source>
        <translation>Erreurs lors de la vérification du répertoire de l&apos;étude</translation>
    </message>
    <message>
        <source>Unexpected error during operation {0!r}:</source>
        <translation>Erreur inattendue lors de l&apos;opération {0!r} :</translation>
    </message>
    <message>
        <source>Add stage</source>
        <translation>Ajoute une étape</translation>
    </message>
    <message>
        <source>Add Stage from File</source>
        <translation>Ajoute une étape à partir d&apos;un fichier</translation>
    </message>
    <message>
        <source>The stage cannot be converted to graphical mode.

Do you want to create a stage with the first commands and a second with the last part in text mode?

See the details for the unsupported features.</source>
        <translation>L&apos;étape ne peut pas être convertie en mode graphique.

Voulez-vous créer une étape avec les premières commandes et une seconde avec la dernière partie en mode texte ?

Voir les détails pour les fonctionnalités non supportées.</translation>
    </message>
    <message>
        <source>Export Command File</source>
        <translation>Exporte le fichier de commandes</translation>
    </message>
    <message>
        <source>&amp;Add Stage</source>
        <translation>&amp;Ajoute une étape</translation>
    </message>
    <message>
        <source>Add stage from file</source>
        <translation>Ajoute une étape à partir d&apos;un fichier</translation>
    </message>
    <message>
        <source>Export command file</source>
        <translation>Exporte le fichier de commandes</translation>
    </message>
    <message>
        <source>Syntax error when looking for the command &apos;{0}&apos; in a python file.</source>
        <translation>Erreur de syntaxe en cherchant la commande &apos;{0}&apos; dans un fichier python.</translation>
    </message>
    <message>
        <source>Sorry it failed again!

The study will be loaded in failsafe mode (all stages in text mode).

See the details for the unsupported features.</source>
        <translation>Désolé, cela a de nouveau échoué !

L&apos;étude sera lue en mode sans échec (toutes les étapes en mode texte).

Voir les détails pour les fonctionnalités non supportées.</translation>
    </message>
    <message>
        <source>&amp;Add Stage from File</source>
        <translation>&amp;Ajoute une étape à partir d&apos;un fichier</translation>
    </message>
    <message>
        <source>&amp;Add Text Stage from File</source>
        <translation>&amp;Ajoute une étape texte à partir d&apos;un fichier</translation>
    </message>
    <message>
        <source>Add a text stage by importing a command file</source>
        <translation>Ajouter une étape texte en important un fichier de commande</translation>
    </message>
    <message>
        <source>&amp;Export Command File</source>
        <translation>&amp;Exporte le fichier de commandes</translation>
    </message>
    <message>
        <source>Export stage contents into a command file</source>
        <translation>Exporter le contenu de l&apos;étape vers un fichier de commandes</translation>
    </message>
    <message>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <source>Catalog error</source>
        <translation>Erreur de catalogue</translation>
    </message>
    <message>
        <source>Cannot import catalog of version {0!r}.</source>
        <translation>On ne peut pas importer le catalogue de la version {0!r}.</translation>
    </message>
    <message>
        <source>Cannot create history for code_aster version {0!r}
Reason: {1}</source>
        <translation>On ne peut pas créer l&apos;historique pour la version {0!r} de code_aster
Raison : {1}</translation>
    </message>
    <message>
        <source>Export a case for a testcase</source>
        <translation>Exporte un cas pour un cas-test</translation>
    </message>
    <message>
        <source>Choose new export file name</source>
        <translation>Choisir le nom du nouveau fichier export</translation>
    </message>
    <message>
        <source>Cannot import the testcase</source>
        <translation>On ne peut pas importer le cas-test</translation>
    </message>
    <message>
        <source>Add a case by importing a testcase</source>
        <translation>Ajoute un cas en important un cas-test</translation>
    </message>
    <message>
        <source>List of code_aster results is not supported.</source>
        <translation>Les listes de résultats code_aster ne sont pas supportées.</translation>
    </message>
    <message>
        <source>The commands file contains only comments.
They will be lost in graphical mode.

Do you want to continue?</source>
        <translation>Le fichier de commandes contient uniquement des commentaires.
Ils seront perdus en mode graphique.

Souhaitez-vous continuer ?</translation>
    </message>
    <message>
        <source>Command DEFI_FICHIER is not supported in graphical mode.</source>
        <translation>La commande DEFI_FICHIER n&apos;est pas supportée en mode graphique.</translation>
    </message>
    <message>
        <source>Business-oriented language helper</source>
        <translation>Outil pour le langage orienté métier</translation>
    </message>
    <message>
        <source>Open tool to suggest business-oriented translations</source>
        <translation>Ouvre l&apos;outil pour proposer des traductions orientées métier</translation>
    </message>
    <message>
        <source>The selected file is {0} lines long.

Importing it in graphical mode may take a while.
Later modifications may be very long too.

Do you want to import it in text mode?</source>
        <translation>Le fichier sélectionné contient {0} lignes.

L&apos;importer en mode graphique peut prendre un certain temps.
Les modifications ultérieurs peuvent aussi être très longues.

Voulez-vous l&apos;importer en mode texte ?</translation>
    </message>
    <message>
        <source>Python statements can not be edited in graphical mode.
You can only edit this commands file in text mode.</source>
        <translation>Les instructions Python ne peuvent pas être éditées en mode graphique.
Vous pouvez uniquement modifier ce fichier de commandes en mode texte.</translation>
    </message>
    <message>
        <source>Activate case</source>
        <translation>Activer cas</translation>
    </message>
    <message>
        <source>Uncompleted docstring or expression starting *after* line {0} and not yet completed at line {1}</source>
        <translation>Chaîne doc ou expression incomplète démarrant *après* la ligne {0} et pas encore complétée à la ligne {1}</translation>
    </message>
    <message>
        <source>Export case</source>
        <translation>Exporter le cas</translation>
    </message>
    <message>
        <source># sequences have been limited to the first {} occurrences.</source>
        <translation># les séquences ont été limitées aux {} premières occurrences.</translation>
    </message>
    <message>
        <source>Please select graphical stage to add a variable</source>
        <translation>Choisissez une étape graphique pour ajouter une variable</translation>
    </message>
    <message>
        <source>Please select graphical stage to add a command</source>
        <translation>Choisissez une étape graphique pour ajouter une commande</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>&amp;Show Normals</source>
        <translation>&amp;Voir les normales</translation>
    </message>
    <message>
        <source>Show faces orientation vectors</source>
        <translation>Afficher les vecteurs d&apos;orientation des faces</translation>
    </message>
    <message>
        <source>&amp;Validity Report</source>
        <translation>Rapport de &amp;Validité</translation>
    </message>
    <message>
        <source>Validity report</source>
        <translation>Rapport de validité</translation>
    </message>
    <message>
        <source>Show validity report for stage</source>
        <translation>Affiche le rapport de validité pour l&apos;étape</translation>
    </message>
    <message>
        <source>&amp;Add Stage from Template</source>
        <translation>&amp;Ajouter une étape depuis un template</translation>
    </message>
    <message>
        <source>Add stage from template</source>
        <translation>Ajouter une étape depuis un template</translation>
    </message>
    <message>
        <source>Add a stage by importing a template command file</source>
        <translation>Ajoute une étape en important un template</translation>
    </message>
    <message>
        <source>Add text stage from file</source>
        <translation>Ajouter une étape texte depuis un fichier</translation>
    </message>
    <message>
        <source>&amp;Add Text Stage from Template</source>
        <translation>&amp;Ajouter une étape texte depuis un template</translation>
    </message>
    <message>
        <source>Add text stage from template</source>
        <translation>Ajoute une étape en mode texte en important un template</translation>
    </message>
    <message>
        <source>Add a text stage by importing a template command file</source>
        <translation>Ajouter une étape texte en important un template d&apos;un fichier de commande</translation>
    </message>
    <message>
        <source>Show Mesh View</source>
        <translation>Afficher la vue de maillage</translation>
    </message>
    <message>
        <source>Show mesh view</source>
        <translation>Afficher la vue de maillage</translation>
    </message>
    <message>
        <source>Show / hide mesh view</source>
        <translation>Afficher / masquer la vue de maillage</translation>
    </message>
    <message>
        <source>&amp;Activate</source>
        <translation>&amp;Activer</translation>
    </message>
    <message>
        <source>Activate</source>
        <translation>Activer</translation>
    </message>
    <message>
        <source>Activate selected command</source>
        <translation>Activer la commande sélectionnée</translation>
    </message>
    <message>
        <source>&amp;Deactivate</source>
        <translation>&amp;Désactiver</translation>
    </message>
    <message>
        <source>Deactivate</source>
        <translation>Désactiver</translation>
    </message>
    <message>
        <source>Deactivate selected command</source>
        <translation>Désactiver la commande sélectionnée</translation>
    </message>
    <message>
        <source>deactivated</source>
        <translation>désactivée</translation>
    </message>
    <message>
        <source>Activate command</source>
        <translation>Activer la commande</translation>
    </message>
    <message>
        <source>Deactivate command</source>
        <translation>Désactiver la commande</translation>
    </message>
    <message>
        <source>Edit concepts</source>
        <translation>Editer les concepts</translation>
    </message>
    <message>
        <source>Edit concepts of the selected stage</source>
        <translation>Editer les concepts de l&apos;étape sélectionnée</translation>
    </message>
    <message>
        <source>INCLUDE detected</source>
        <translation>INCLUDE détecté</translation>
    </message>
    <message>
        <source>Directory path with study files contains unsupported symbols. You will not be able to run &lt;b&gt;&lt;i&gt;code_aster&lt;/i&gt;&lt;/b&gt;. Please resave the study with valid path.</source>
        <translation>Le chemin de répertoire avec les fichiers d&apos;étude contient des symboles non pris en charge. Vous ne pourrez pas exécuter &lt;b&gt;&lt;i&gt;code_aster&lt;/i&gt;&lt;/b&gt;. Veuillez réenregistrer l&apos;étude avec un chemin valide.</translation>
    </message>
    <message>
        <source>Saved study has stages which can&apos;t be restored in graphical mode.</source>
        <translation>L&apos;étude sauvegardée a des étapes qui ne peuvent pas être restaurées en mode graphique.</translation>
    </message>
    <message>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <source>Shift+F4</source>
        <translation>Shift+F4</translation>
    </message>
    <message>
        <source>automatic</source>
        <translation>automatique</translation>
    </message>
    <message>
        <source>or reuse the input object</source>
        <translation>ou réutilisez l&apos;objet d&apos;entrée</translation>
    </message>
    <message>
        <source>MissingInclude: INCLUDE needs an additional input file</source>
        <translation>MissingInclude: INCLUDE a besoin d&apos;un fichier d&apos;entrée supplémentaire</translation>
    </message>
    <message>
        <source>The imported file contains NOEUD/MAILLE in {0} which are now deprecated. You cannot change their value. If you edit this command you will need to select a group.</source>
        <translation>Le fichier importé contient NOEUD/MAILLE dans {0}, ils sont dorénavant dépréciés. Vous ne pourrez pas changer leur valeur. Si vous éditez la commande vous devrez sélectionner un groupe.</translation>
    </message>
    <message>
        <source>Only first {0} warnings out of {1} have been reported.</source>
        <translation>Seuls les {0} premières alarmes sur {1} ont été rapportées.</translation>
    </message>
    <message>
        <source>&apos;{name}&apos; statements are ignored during import.</source>
        <translation>Les instructions &apos;{name}&apos; sont ignorées pendant l&apos;import.</translation>
    </message>
    <message>
        <source>Chaining with other codes</source>
        <translation>Chainage avec d&apos;autres codes</translation>
    </message>
    <message>
        <source>Dynamics</source>
        <translation>Dynamique</translation>
    </message>
    <message>
        <source>Statics</source>
        <translation>Statique</translation>
    </message>
    <message>
        <source>Thermics</source>
        <translation>Thermique</translation>
    </message>
    <message>
        <source>Use Business-Oriented Translations</source>
        <translation>Utiliser le langage &apos;Métier&apos;</translation>
    </message>
    <message>
        <source>Use Business-Oriented Translations, this parameter may be defined globally in Preferences</source>
        <translation>Utiliser le langage &apos;Métier&apos;, ce paramètre peut être réglé globalement dans les préférences</translation>
    </message>
    <message>
        <source>Invalid file path</source>
        <translation>Chemin non valide</translation>
    </message>
    <message>
        <source>File path contains unsupported symbols.&lt;br&gt;You will not be able to run &lt;b&gt;&lt;i&gt;code_aster&lt;/i&gt;&lt;/b&gt;.&lt;br&gt;&lt;br&gt;Continue?</source>
        <translation>Le chemin du fichier contient des symboles non pris en charge.&lt;br&gt;Vous ne pourrez pas exécuter &lt;b&gt;&lt;i&gt;code_aster&lt;/i&gt;&lt;/b&gt;.&lt;br&gt;&lt;br&gt;Continuer ?</translation>
    </message>
    <message>
        <source>Mounting filesystems for {0}...</source>
        <translation>Montage des systèmes de fichiers pour {0}...</translation>
    </message>
    <message>
        <source>The new study contains no run cases.&lt;br&gt;If you wanted to move the Study, you had to move the HDF file and its &lt;i&gt;_Files&lt;/i&gt; directory manually.</source>
        <translation>La nouvelle étude ne contient aucun cas exécuté.&lt;br&gt;Si vous vouliez déplacer l&apos;Etude, vous devez déplacer le fichier HDF et son répertoire &lt;i&gt;_Files&lt;/i&gt; manuellement.</translation>
    </message>
    <message>
        <source>on unit {0}</source>
        <translation>sur l&apos;unité {0}</translation>
    </message>
    <message>
        <source>named {0}</source>
        <translation>nommé {0}</translation>
    </message>
    <message>
        <source>Please select a file for INCLUDE {0}</source>
        <translation>Veuillez sélectionner un fichier pour INCLUDE</translation>
    </message>
    <message>
        <source>Save current state</source>
        <translation>Sauvegarde de l&apos;état courant</translation>
    </message>
    <message>
        <source>Connect Remote Servers</source>
        <translation>Connexion aux serveurs distants</translation>
    </message>
    <message>
        <source>Connect remote servers</source>
        <translation>Connexion aux serveurs distants</translation>
    </message>
    <message>
        <source>Connect and mount filesystems from remote servers</source>
        <translation>Connecte les répertoires des serveurs distants</translation>
    </message>
    <message>
        <source>Please wait while AsterStudy finishes loading...</source>
        <translation>Veuillez patienter pendant le chargement d&apos;AsterStudy...</translation>
    </message>
    <message>
        <source>Groups involved</source>
        <translation>Groupes concernés</translation>
    </message>
    <message>
        <source>View mesh groups involved in the command</source>
        <translation>Voir les groupes de mailles concernés par la commande</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Null wizard data in {}</source>
        <translation>Pas de données d&apos;assistant dans {}</translation>
    </message>
    <message>
        <source>Cannot generate wizard from {}</source>
        <translation>Impossible de générer l&apos;assistant depuis {}</translation>
    </message>
    <message>
        <source>Null template data in {}</source>
        <translation>Données de modèle nulles dans le {}</translation>
    </message>
    <message>
        <source>Empty stage is generated by assistant.</source>
        <translation>Une étape vide est générée par l&apos;assistant.</translation>
    </message>
    <message>
        <source>Business-Oriented Language &amp;Helper</source>
        <translation>&amp;Outil pour le langage orienté métier</translation>
    </message>
    <message>
        <source>Import &amp;Testcase</source>
        <translation>Importe un Cas-&amp;Test</translation>
    </message>
    <message>
        <source>Import testcase</source>
        <translation>Importe un cas-test</translation>
    </message>
    <message>
        <source>E&amp;xport Case</source>
        <translation>E&amp;xporte un Cas</translation>
    </message>
    <message>
        <source>&amp;Add Stage with Assistant</source>
        <translation>&amp;Ajouter une étape avec l&apos;assistant</translation>
    </message>
    <message>
        <source>Add Stage with calculation assistant</source>
        <translation>Ajouter une étape avec l&apos;assistant de calcul</translation>
    </message>
    <message>
        <source>Add a stage using the calculation assistant</source>
        <translation>Ajouter une étape en utilisant l&apos;assistant de calcul</translation>
    </message>
    <message>
        <source>&amp;Insert Stage Above</source>
        <translation>&amp;Insérer une étape avant</translation>
    </message>
    <message>
        <source>Insert stage above</source>
        <translation>Insérer une étape avant</translation>
    </message>
    <message>
        <source>Insert a new stage before the selected one</source>
        <translation>Insérer une étape avant l&apos;étape sélectionnée</translation>
    </message>
    <message>
        <source>Edit Concept&amp;s</source>
        <translation>Editer les concept&amp;s</translation>
    </message>
    <message>
        <source>Analysis summary</source>
        <translation>Résumé de l&apos;analyse</translation>
    </message>
    <message>
        <source>&amp;Browse Directory</source>
        <translation>&amp;Parcourir le répertoire</translation>
    </message>
    <message>
        <source>Browse directory</source>
        <translation>Parcourir le répertoire</translation>
    </message>
    <message>
        <source>Add Stage from Text</source>
        <translation>Ajouter une étape texte</translation>
    </message>
    <message>
        <source>Copy data</source>
        <translation>Copier les données</translation>
    </message>
    <message>
        <source>Cut data</source>
        <translation>Couper les données</translation>
    </message>
    <message>
        <source>Paste data</source>
        <translation>Coller les données</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation>Effacer les données</translation>
    </message>
    <message>
        <source>&amp;Fit Selection</source>
        <translation>Adapter à la &amp;Sélection</translation>
    </message>
    <message>
        <source>Fit selection</source>
        <translation>Adapter à la sélection</translation>
    </message>
    <message>
        <source>Fit view contents to selected groups</source>
        <translation>Adapter la vue pour les groupes sélectionnés</translation>
    </message>
    <message>
        <source>&amp;Select Groups</source>
        <translation>&amp;Sélectionner des groupes</translation>
    </message>
    <message>
        <source>Select groups from central view</source>
        <translation>Sélectionner des groupes depuis la vue principale</translation>
    </message>
    <message>
        <source>New Case from Database</source>
        <translation>Nouveau cas à partir d&apos;une base</translation>
    </message>
    <message>
        <source>New case from database</source>
        <translation>Créer un nouveau cas à partir d&apos;une base</translation>
    </message>
    <message>
        <source>Start a new case from a database</source>
        <translation>Démarre un nouveau cas à partir d&apos;une base</translation>
    </message>
    <message>
        <source>Convert as Mesh Object</source>
        <translation>Convertit en tant qu&apos;objet Mesh</translation>
    </message>
    <message>
        <source>Convert as Mesh object</source>
        <translation>Convertit en tant qu&apos;objet Mesh</translation>
    </message>
    <message>
        <source>Convert mesh file as a Mesh object</source>
        <translation>Convertit un fichier de maillage en objet Mesh</translation>
    </message>
    <message>
        <source>Found meshes: {0}</source>
        <translation>Maillages présents : {0}</translation>
    </message>
    <message>
        <source>Input mesh format</source>
        <translation>Format du maillage en entrée</translation>
    </message>
    <message>
        <source>Ouput mesh name</source>
        <translation>Nom du maillage produit</translation>
    </message>
    <message>
        <source>It will be the SMESH object name</source>
        <translation>Ce sera le nom de l&apos;objet SMESH</translation>
    </message>
    <message>
        <source>Convert</source>
        <translation>Convertir</translation>
    </message>
    <message>
        <source>Please select the input file format.</source>
        <translation>Veuillez sélectionner le format du maillage en entrée.</translation>
    </message>
    <message>
        <source>Invalid mesh object name.</source>
        <translation>Nom d&apos;objet maillage invalide.</translation>
    </message>
    <message>
        <source>Converting mesh file. Please wait...</source>
        <translation>Conversion du fichier maillage. Patienter...</translation>
    </message>
    <message>
        <source>Restart from a database</source>
        <translation>Démarrer depuis une base</translation>
    </message>
    <message>
        <source>Database path</source>
        <translation>Chemin de la base</translation>
    </message>
    <message>
        <source>Extract available objects</source>
        <translation>Extraire les objets disponibles</translation>
    </message>
    <message>
        <source>code_aster will be executed to extract the database content.</source>
        <translation>code_aster sera exécuté pour extraire le contenu de la base.</translation>
    </message>
    <message>
        <source>Unknown mesh format for: {0}</source>
        <translation>Format de maillage inconnu pour : {0}</translation>
    </message>
    <message>
        <source>Starting mesh conversion in directory {0}...</source>
        <translation>Démarrage de la conversion de maillage dans le répertoire {0}...</translation>
    </message>
    <message>
        <source>Conversion ended with state {0}</source>
        <translation>Conversion terminée avec le statut {0}</translation>
    </message>
    <message>
        <source>The current study is using the catalog of the version &apos;{0}&apos;.&lt;br&gt;If the database to be reloaded has been created by a different version, you should create a new study and select the same version.&lt;br&gt;</source>
        <translation>L&apos;étude actuelle utilise le catalogue de la version &apos;{0}&apos;.&lt;br&gt;Si la base à relire a été créée avec une version différente, vous devez créer une nouvelle étude et sélectionner la même version.&lt;br&gt;</translation>
    </message>
    <message>
        <source>Mesh converter ({0})</source>
        <translation>Conversion de maillage ({0})</translation>
    </message>
    <message>
        <source>Replace input file usage by the new Mesh object in commands.</source>
        <translation>Remplace l&apos;utilisation du fichier par le nouvel objet Mesh dans les commandes.</translation>
    </message>
    <message>
        <source>Updating commands using the input file...</source>
        <translation>Mise à jour des commandes utilisant le fichier...</translation>
    </message>
    <message>
        <source>More than one mesh: commands will not be changed!</source>
        <translation>Plus d&apos;un maillage : les commandes ne seront pas modifiées !</translation>
    </message>
    <message>
        <source>Can not update commands!</source>
        <translation>On ne peut pas modifier les commandes !</translation>
    </message>
    <message>
        <source>&amp;Unused Concepts</source>
        <translation>Concepts non &amp;utilisés</translation>
    </message>
    <message>
        <source>Unused concepts</source>
        <translation>Concepts non utilisés</translation>
    </message>
    <message>
        <source>Detect unused concepts</source>
        <translation>Détecter les concepts non utilisés</translation>
    </message>
    <message>
        <source>Persalys study file is &apos;{0}&apos;</source>
        <translation>Le fichier d&apos;étude Persalys est &apos;{0}&apos;</translation>
    </message>
    <message>
        <source>The study is empty.

If it should not, click the &apos;No&apos; button and try to recover its content: select &apos;Operations/Recover&apos; from the menu.

By choosing &apos;Yes&apos; you may loose the previous content.

Do you want to continue?</source>
        <translation>L&apos;étude est vide.

Si elle ne devrait pas, cliquez sur le bouton &apos;Non&apos; et essayer de récupérer son contenu : sélectionner &apos;Opérations/Récupération&apos; dans le menu.

En choisissant &apos;Oui&apos;, vous pouvez perdre le contenu précédent.

Voulez-vous continuer ?</translation>
    </message>
    <message>
        <source>Recovery Files</source>
        <translation>Fichiers de récupération</translation>
    </message>
    <message>
        <source>Your study file can not be reloaded.
</source>
        <translation>Votre fichier d&apos;étude ne peut pas être relu.
</translation>
    </message>
    <message>
        <source>The version name was not found from the recovery file.
Use version &apos;{0}&apos; instead.
</source>
        <translation>Le nom de la version n&apos;a pas été trouvé dans le fichier de récupération.
On utilise &apos;{0}&apos; à la place.
</translation>
    </message>
    <message>
        <source>Recovery failed from &apos;{0}&apos;.

A new empty study will be created.</source>
        <translation>La récupération a échoué depuis &apos;{0}&apos;.

Une nouvelle étude sera créée.</translation>
    </message>
    <message>
        <source>
The DataSettings of the CurrentCase has been successfully restored from the recovery file.
The Stages have been restored but you have to define the DataFiles manually.

It is recommended to save your study in a different place not to erase the files of the previous RunCases.
Use &apos;File/Save As...&apos; to save the study.
</source>
        <translation>
La mise en données du Cas Courant a été restaurée avec succès depuis le fichier de récupération.
Les étapes ont été restaurées mais vous devez définir les fichiers de l&apos;étude manuellement.

Il est recommandé de sauvegarder votre étude à un endroit différent pour ne pas effacer les fichiers des précédents RunCases.
Utilisez &apos;Fichier/Enregistrer sous...&apos; pour enregistrer l&apos;étude.
</translation>
    </message>
    <message>
        <source>Recover from a &apos;data.ajs&apos; file</source>
        <translation>Récupération depuis un fichier &apos;data.ajs&apos;</translation>
    </message>
    <message>
        <source>Recovery Mode</source>
        <translation>Mode de Récupération</translation>
    </message>
    <message>
        <source>Recovery mode</source>
        <translation>Mode de récupération</translation>
    </message>
    <message>
        <source>Recover the CurrentCase of a Study</source>
        <translation>Récupérer le Cas Courant d&apos;une étude</translation>
    </message>
    <message>
        <source>At least one stage that was in graphical mode is syntactically invalid.

Choose &apos;Ok&apos; to restore the stage with some errors.
Choose &apos;Cancel&apos; to split the stage with the first commands in graphical mode and a second one with the last part in text mode.

See the details for the unsupported features.</source>
        <translation>Au moins une étape qui était en mode graphique est syntaxiquement invalide.

Choisissez &apos;Ok&apos; pour restaurer l&apos;étape avec des erreurs.
Choisissez &apos;Annuler&apos; pour découper l&apos;étape avec les premières commandes en mode graphique et un second avec la deuxième partie en mode texte.

Voir les détails pour les fonctionnalités non supportées.</translation>
    </message>
    <message>
        <source>Repair commands dependencies</source>
        <translation>Réparer les dépendances des commandes</translation>
    </message>
    <message>
        <source>The case is still invalid.
This feature can only fix dependencies problems when a result with the same name exists.

Please check the validity report of invalid stages for remaining errors.</source>
        <translation>Le cas est toujours invalide.
Cette fonctionnalité ne peut corriger que les problèmes de dépendances quand un résultat de même name existe.

Vérifiez le rapport de validation des étapes pour les erreurs restantes.</translation>
    </message>
    <message>
        <source>Repair Dependencies</source>
        <translation>Correction des dépendances</translation>
    </message>
    <message>
        <source>Try to repair broken dependencies</source>
        <translation>Essaie de corriger les dépendances cassées</translation>
    </message>
    <message>
        <source>The study seems incomplete with regard to the study directory.
For instance, subdirectory &apos;{0}&apos; has no corresponding stage.</source>
        <translation>L&apos;étude semble incomplète par rapport au répertoire de l&apos;étude.
Par exemple, le sous-répertoire &apos;{0}&apos; n&apos;a pas d&apos;étape correspondant.</translation>
    </message>
    <message>
        <source>There seems to be empty cases in the study.

For instance, case {0} is empty.

Continuing will delete them.</source>
        <translation>Il semble qu&apos;il y ait des cas vides dans l&apos;étude.

Par exemple, le case &apos;{0}&apos; est vide.

Continuer les supprimera.</translation>
    </message>
    <message>
        <source>There seems to be empty cases directories in the study.

For instance, directory &apos;{0}&apos; is empty.

Continuing will remove them.</source>
        <translation>Il semble qu&apos;il y ait des répertoires de cas vides dans l&apos;étude.

Par exemple, le répertoire &apos;{0}&apos; est vide.

Continuer les supprimera.</translation>
    </message>
    <message>
        <source>The following stages will not be loaded ({0}/{1}):
</source>
        <translation>Les étapes suivantes ne seront pas rechargées ({0}/{1}):
</translation>
    </message>
    <message>
        <source>The commands files will be saved into the directory {0}
</source>
        <translation>Les fichiers de commandes seront sauvegardés dans le répertoire {0}
</translation>
    </message>
    <message>
        <source>The following subfolders will be deleted ({0}/{1}):
</source>
        <translation>Les sous-répertoires suivants seront supprimés  ({0}/{1}):
</translation>
    </message>
    <message>
        <source>Continue anyway?</source>
        <translation>Voulez-vous continuer malgré tout ?</translation>
    </message>
    <message>
        <source>The study directory &apos;{0}&apos; did not exist and had to be created empty.</source>
        <translation>Le répertoire de l&apos;étude &apos;{0}&apos; n&apos;existait pas et a été créé vide.</translation>
    </message>
    <message>
        <source>If you moved the HDF study &apos;{0}&apos; from another location, please make sure to move the study directory as well.

Click &apos;Cancel&apos;, move the study directory then try again or click &apos;OK&apos; to continue without study directory.</source>
        <translation>Si vous avez déplacé l&apos;étude HDF &apos;{0}&apos; depuis un autre emplacement, assurez-vous de déplacer aussi le répertoire de l&apos;étude.

Cliquez &apos;Annuler&apos;, déplacer l&apos;étude HDF et essayez de nouveau ou bien cliquez &apos;Ok&apos; pour continuer sans le répertoire de l&apos;étude.</translation>
    </message>
    <message>
        <source>The study directory &apos;{0}&apos; does not match the HDF study &apos;{1}&apos;.
For instance, subdirectory &apos;{2}&apos; does not exist.</source>
        <translation>Le répertoire de l&apos;étude &apos;{0}&apos; ne correspond pas à l&apos;étude HDF &apos;{1}&apos;.
Par exemple, le sous-répertoire &apos;{2}&apos; n&apos;existe pas.</translation>
    </message>
    <message>
        <source>Run cases will not be loaded.</source>
        <translation>Les cas exécutés ne seront pas rechargés.</translation>
    </message>
    <message>
        <source>Template files are not found</source>
        <translation>Fichiers templates non trouvés</translation>
    </message>
    <message>
        <source>Assistant files are not found</source>
        <translation>Fichiers de l&apos;assistant non trouvés</translation>
    </message>
    <message>
        <source>Groups &amp;Involved</source>
        <translation>Groupes &amp;concernés</translation>
    </message>
    <message>
        <source>Analysis &amp;Summary</source>
        <translation>Ré&amp;sumé de l&apos;analyse</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>&amp;Show Groups</source>
        <translation>Voir les groupe&amp;s</translation>
    </message>
    <message>
        <source>Show groups</source>
        <translation>Voir les groupes</translation>
    </message>
    <message>
        <source>Show groups associated with selected usage</source>
        <translation>Voir les groupes associés à l&apos;utilisation sélectionnée</translation>
    </message>
    <message>
        <source>&amp;Hide Groups</source>
        <translation>Cac&amp;her les groupes</translation>
    </message>
    <message>
        <source>Hide groups</source>
        <translation>Cacher les groupes</translation>
    </message>
    <message>
        <source>Hide groups associated with selected usage</source>
        <translation>Cacher les groupes associés à l&apos;utilisation sélectionnée</translation>
    </message>
    <message>
        <source>Show description of usage</source>
        <translation>Voir la description de l&apos;utilisation</translation>
    </message>
    <message>
        <source>Analysis summary for {}</source>
        <translation>Résumé de l&apos;analyse pour {}</translation>
    </message>
    <message>
        <source>Groups involved in {}</source>
        <translation>Groupes concernés par {}</translation>
    </message>
    <message>
        <source>Set-up Directories</source>
        <translation>Configurer répertoires</translation>
    </message>
    <message>
        <source>Delete file</source>
        <translation>Supprimer le fichier</translation>
    </message>
    <message>
        <source>Loading Persalys module...</source>
        <translation>Chargement du module Persalys...</translation>
    </message>
    <message>
        <source>Show &amp;Results</source>
        <translation>Afficher les &amp;Résultats</translation>
    </message>
    <message>
        <source>Assign Default Filenames</source>
        <translation>Définir des noms de fichiers par défaut</translation>
    </message>
    <message>
        <source>Assign default filenames</source>
        <translation>Définir des noms de fichiers par défaut</translation>
    </message>
    <message>
        <source>Please save the study first.</source>
        <translation>Veuillez enregistrer l&apos;étude.</translation>
    </message>
    <message>
        <source>Assign a default filename to all unnamed files</source>
        <translation>Affecte un nom de fichier à tous les fichiers non nommés</translation>
    </message>
    <message>
        <source>Loading result file ({0:.1f} MB)</source>
        <translation>Chargement du fichier résultat ({0:.1f} Mo)</translation>
    </message>
    <message>
        <source>Initializing post-processor and loading result file ({0:.1f} MB)</source>
        <translation>Initialisation du post-traitement et chargement du fichier résultat ({0:.1f} Mo)</translation>
    </message>
    <message>
        <source>Show results</source>
        <translation>Afficher les résultats</translation>
    </message>
    <message>
        <source>Show results from selected file in the Results tab</source>
        <translation>Afficher les résultats du fichier sélectionné dans l&apos;onglet Résultats</translation>
    </message>
    <message>
        <source>&amp;Post-process</source>
        <translation>&amp;Post-traitement</translation>
    </message>
    <message>
        <source>Post-process</source>
        <translation>Post-traitement</translation>
    </message>
    <message>
        <source>Open In Para&amp;Vis</source>
        <translation>Ouvrir dans Para&amp;Vis</translation>
    </message>
    <message>
        <source>Post-process the result the Results tab</source>
        <translation>Post-traitement des résultats dans l&apos;onglet Résultats</translation>
    </message>
    <message>
        <source>&apos;Save As...&apos; automatically renamed result files that were under &lt;i&gt;_Files&lt;/i&gt; directory. Other files should be manually renamed in Datafiles tab.</source>
        <translation>&apos;Enregistrer Sous...&apos; a automatiquement renommé les fichiers résultats qui étaient sous le répertoire &lt;i&gt;_Files&lt;/i&gt;. Les autres fichiers doivent être renommés manuellement dans l&apos;onglet Fichiers.</translation>
    </message>
    <message>
        <source>`{0.name}(FICHIER=...)` is not supported.
Workaround: insert a stage above in text mode and add an input file selecting &apos;{1}&apos;.

Then type &apos;Ctrl+&lt;F&gt;&apos; to search &apos;{1}&apos; and enter FICHIER=&apos;fort.N&apos; (with the used number).</source>
        <translation>`{0.name}(FICHIER=...)` n&apos;est pas supporté.
Contournement : insérer un stage avant en mode texte et ajouter un fichier en sélectionnant &apos;{1}&apos;.

Ensuite taper &apos;Ctrl+&lt;F&gt;&apos; pour chercher &apos;{1}&apos; et entrer FICHIER=&apos;fort.N&apos; (avec le numéro utilisé).</translation>
    </message>
    <message>
        <source>`{0.title}(reuse={0.name}, ...)` without setting the result object is tolerated and should work in most cases.

Type &apos;Ctrl+&lt;F&gt;&apos; to search &apos;{0.name}&apos; and check the reused object is used.</source>
        <translation>`{0.title}(reuse={0.name}, ...)` sans définir le résultat est toléré et devrait fonctionner dans la plupart des cas.

Taper &apos;Ctrl+&lt;F&gt;&apos; pour chercher &apos;{0.name}&apos; et vérifier que l&apos;objet réentrant est utilisé.</translation>
    </message>
    <message>
        <source>The object &apos;{0.name}&apos; is changed by {0.title}.
The following commands are using a  previous instance of &apos;{0.name}&apos;:
{1}

Type &apos;Ctrl+&lt;F&gt;&apos; to search &apos;{0.name}&apos; and check that these commands are using the right instance of this object.</source>
        <translation>L&apos;objet &apos;{0.name}&apos; est modifié par {0.title}.
Les commandes suivantes utilisent une autre instance de &apos;{0.name}&apos; :
{1}

Taper &apos;Ctrl+&lt;F&gt;&apos; pour chercher &apos;{0.name}&apos; et vérifier que ces commandes utilisent la bonne instance de l&apos;objet.</translation>
    </message>
    <message>
        <source>The result file must be in the MED format and created by code_aster.
Otherwise the post-processing will fail!</source>
        <translation>Le fichier de résultats doit être au format MED et créé par code_aster.
Sinon le post-traitement va échouer !</translation>
    </message>
    <message>
        <source>The selected file is not in MED format.</source>
        <translation>Le fichier sélectionné n&apos;est pas au format MED.</translation>
    </message>
    <message>
        <source>Post-process an external MED Results File</source>
        <translation>Post-traitement d&apos;un fichier de résultats MED externe</translation>
    </message>
    <message>
        <source>Post-process an external MED results file</source>
        <translation>Post-traitement d&apos;un fichier de résultats MED externe</translation>
    </message>
    <message>
        <source>Post-process an external results file in MED format in the Results tab</source>
        <translation>Post-traitement d&apos;un fichier de résultats MED externe dans l&apos;onglet Résultats</translation>
    </message>
    <message>
        <source>Post-process MED Results File</source>
        <translation>Post-traitement d&apos;un fichier de résultats MED</translation>
    </message>
    <message>
        <source>Post-process a MED results file</source>
        <translation>Post-traitement d&apos;un fichier de résultats MED</translation>
    </message>
    <message>
        <source>Post-process a results file in MED format</source>
        <translation>Post-traitement d&apos;un fichier de résultats MED</translation>
    </message>
    <message>
        <source>The provided MED file contains no result concepts or fields.
</source>
        <translation>Le fichier MED fourni ne contient pas d&apos;objet résultat ou de champ.
</translation>
    </message>
    <message>
        <source>&amp;Show All Commands</source>
        <translation>&amp;Voir toutes les commandes</translation>
    </message>
    <message>
        <source>Show All Commands</source>
        <translation>Voir toutes les commandes</translation>
    </message>
    <message>
        <source>Post-process the results in the Results tab</source>
        <translation>Post-traitement des résultats dans l&apos;onglet Résultats</translation>
    </message>
    <message>
        <source>It seems that your study used a version that is not available anymore.

Do you want to try using another version?

The stages may be imported in text mode in case of syntaxic incompatibility.</source>
        <translation>Il semble que votre étude utilise une version qui n&apos;est plus disponible.

Voulez-vous essayer d&apos;utiliser une autre version ?

Les étapes seront peut-être importés en mode texte en cas d&apos;incompatibilité de syntaxe.</translation>
    </message>
    <message>
        <source>Can not import &apos;persalys&apos;. Please check your installation.</source>
        <translation>Impossible d&apos;importer &apos;persalys&apos;. Vérifiez votre installation.</translation>
    </message>
    <message>
        <source>Writing to existing file error</source>
        <translation>Ecriture dans le fichier d&apos;erreur existant</translation>
    </message>
    <message>
        <source>Load a new results file in MED format</source>
        <translation>Charger un nouveau résultat code_aster en format MED</translation>
    </message>
    <message>
        <source>Group &apos;{}&apos; mixes different topological dimensions, a situation likely to cause confusion in Code_Aster commands. 
All elements of the group will be displayed on central view</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CasesView</name>
    <message>
        <source>Run Cases</source>
        <translation>Cas exécutés</translation>
    </message>
    <message>
        <source>Backup Cases</source>
        <translation>Cas sauvegardés</translation>
    </message>
</context>
<context>
    <name>CatalogsView</name>
    <message>
        <source>Add catalog</source>
        <translation>Ajouter un catalogue</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Set-up catalogue</source>
        <translation>Configurer le catalogue</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <source>Path &apos;{}&apos; is already in use</source>
        <translation>Le chemin &apos;{}&apos; est déjà utilisé</translation>
    </message>
    <message>
        <source>Select directory</source>
        <translation>Choisir un répertoire</translation>
    </message>
    <message>
        <source>Path does not exist:
{}

You must select a directory that contains a &apos;code_aster&apos; subdirectory.</source>
        <translation>Chemin inexistant :
{}

Vous devez sélectionner un chemin contenant un sous-répertoire &apos;code_aster&apos;.</translation>
    </message>
    <message>
        <source>Label &apos;{}&apos; is reserved for internal use (case-insensitive)</source>
        <translation>Le libellé &apos;{}&apos; est réservé pour usage interne (insensible à la casse)</translation>
    </message>
    <message>
        <source>Label &apos;{}&apos; is already in use (case-insensitive)</source>
        <translation>Le libellé &apos;{}&apos; est déjà utilisé (insensible à la casse)</translation>
    </message>
</context>
<context>
    <name>Categories</name>
    <message>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <source>Material</source>
        <translation>Matériaux</translation>
    </message>
    <message>
        <source>Material Assignment</source>
        <translation>Affectation matériaux</translation>
    </message>
    <message>
        <source>Functions and Lists</source>
        <translation>Fonctions et listes</translation>
    </message>
    <message>
        <source>Finite Element</source>
        <translation>Elément fini</translation>
    </message>
    <message>
        <source>BC and Load</source>
        <translation>BC et chargement</translation>
    </message>
    <message>
        <source>Pre Processing</source>
        <translation>Prétraitement</translation>
    </message>
    <message>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <source>Post Processing</source>
        <translation>Post-traitement</translation>
    </message>
    <message>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Autre</translation>
    </message>
    <message>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <source>Deprecated</source>
        <translation>Obsolète</translation>
    </message>
    <message>
        <source>Fracture and Fatigue</source>
        <translation>Fracture et fatigue</translation>
    </message>
    <message>
        <source>Hidden</source>
        <translation>Caché</translation>
    </message>
    <message>
        <source>Model Definition</source>
        <translation>Définition du modèle</translation>
    </message>
    <message>
        <source>Pre Analysis</source>
        <translation>Pré-analyse</translation>
    </message>
</context>
<context>
    <name>CategoryView</name>
    <message>
        <source>{visible} of {total} items shown</source>
        <translation>{visible} éléments affichés sur {total}</translation>
    </message>
    <message>
        <source>{total} items</source>
        <translation>{total} éléments</translation>
    </message>
</context>
<context>
    <name>CmdTextEditor</name>
    <message>
        <source>Edit command</source>
        <translation>Editer la commande</translation>
    </message>
</context>
<context>
    <name>ConceptsEditor</name>
    <message>
        <source>Add concept</source>
        <translation>Ajouter concept</translation>
    </message>
    <message>
        <source>Remove selected concepts</source>
        <translation>Supprimer les concepts sélectionnés</translation>
    </message>
    <message>
        <source>Concepts to add</source>
        <translation>Concepts à ajouter</translation>
    </message>
    <message>
        <source>Existing concepts to delete</source>
        <translation>Concepts existants à supprimer</translation>
    </message>
    <message>
        <source>Concept</source>
        <translation>Concept</translation>
    </message>
    <message>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Integer</source>
        <translation>Entière</translation>
    </message>
    <message>
        <source>Real</source>
        <translation>Réel</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>&amp;Add Concept</source>
        <translation>&amp;Ajouter un concept</translation>
    </message>
    <message>
        <source>&amp;Remove Concepts</source>
        <translation>Supp&amp;rimer des concepts</translation>
    </message>
    <message>
        <source>Remove concepts</source>
        <translation>Supprimer des concepts</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <source>Operation performed</source>
        <translation>Opération réussie</translation>
    </message>
    <message>
        <source>There is executed operation. Do you want to break it and lose all input data?</source>
        <translation>Une commande est en cours d&apos;édition. Voulez-vous l&apos;interrompre et perdre les données ?</translation>
    </message>
</context>
<context>
    <name>Dashboard</name>
    <message>
        <source>Run</source>
        <translation>Exécuter</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Trace</translation>
    </message>
    <message>
        <source>Create Run case</source>
        <translation>Créer un cas</translation>
    </message>
    <message>
        <source>Accept Run results</source>
        <translation>Accepter les résultats</translation>
    </message>
    <message>
        <source>Batch</source>
        <translation>Batch</translation>
    </message>
    <message>
        <source>Interactive</source>
        <translation>Interactif</translation>
    </message>
    <message>
        <source>Resume</source>
        <translation>Reprendre</translation>
    </message>
    <message>
        <source>Reusable</source>
        <translation>Réutilisable</translation>
    </message>
    <message>
        <source>Show message file</source>
        <translation>Voir le fichier des messages</translation>
    </message>
    <message>
        <source>&apos;message&apos;</source>
        <translation>&apos;message&apos;</translation>
    </message>
    <message>
        <source>&apos;export&apos;</source>
        <translation>&apos;export&apos;</translation>
    </message>
    <message>
        <source>&apos;log&apos;</source>
        <translation>&apos;log&apos;</translation>
    </message>
    <message>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
    <message>
        <source>All sources</source>
        <translation>Toutes les sources</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Voir</translation>
    </message>
    <message>
        <source>All stages</source>
        <translation>Toutes les étapes</translation>
    </message>
    <message>
        <source>Use optimized version</source>
        <translation>Version optimisée</translation>
    </message>
    <message>
        <source>Use debug version</source>
        <translation>Version debug</translation>
    </message>
    <message>
        <source>Run under Debugger</source>
        <translation>Exécute un débogueur</translation>
    </message>
    <message>
        <source>Prepare the environment</source>
        <translation>Prépare l&apos;environnement</translation>
    </message>
    <message>
        <source>Go To</source>
        <translation>Aller à</translation>
    </message>
    <message>
        <source>Open the log file at the position of the message.&lt;br&gt;&lt;i&gt;If your editor is already opened, it may not jump the right line.&lt;br&gt;See Preferences to configure the editor.&lt;/i&gt;</source>
        <translation>Ouvre le fichier de log à la position du message.&lt;br&gt;&lt;i&gt;Si votre éditeur est déjà ouvert, il se peut qu&apos;il n&apos;aille pas à la bonne ligne.&lt;/i&gt;Voir les Préférences pour configurer l&apos;éditeur.&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Go to the DataSettings at the origin of the message.&lt;br&gt;The Command will be edited for a graphical Stage,&lt;br&gt;The text editor will be opened at the line of the command.&lt;br&gt;Same as &lt;i&gt;Show&lt;/i&gt; for runner messages.</source>
        <translation>Aller à la Mise en Données à l&apos;origine du message.&lt;br&gt;La Commande sera éditée pour une Etape graphique.&lt;br&gt;L&apos;éditeur sera ouvert à la ligne de la Commande.&lt;br&gt;Pareil que &lt;i&gt;Voir&lt;/i&gt; pour les messages du lanceur.</translation>
    </message>
    <message>
        <source>Warning: You won&apos;t be able to restart from the last executed stage if &quot;Reusable&quot; is not checked.</source>
        <translation>Avertissement : Vous ne pourrez pas redémarrer depuis la dernière exécution if &quot;Réutilisable&quot; n&apos;est pas coché.</translation>
    </message>
    <message>
        <source>Local Run lost</source>
        <translation>Exécution locale perdue</translation>
    </message>
    <message>
        <source>Run case started</source>
        <translation>Exécution démarrée</translation>
    </message>
    <message>
        <source>Calculation is running locally. Do not close the application or you won&apos;t be able to get the results.</source>
        <translation>Un calcul tourne localement. Ne fermer pas l&apos;application sinon vous ne pourrez pas récupérer les résultats.</translation>
    </message>
    <message>
        <source>Console</source>
        <translation>Console</translation>
    </message>
    <message>
        <source>Completion</source>
        <translation>Avancement</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <source>Iterations</source>
        <translation>Itérations</translation>
    </message>
    <message>
        <source>Relative residue</source>
        <translation>Résidu relatif</translation>
    </message>
    <message>
        <source>Absolute residue</source>
        <translation>Résidu absolu</translation>
    </message>
    <message>
        <source>Value of a degree of freedom</source>
        <translation>Valeur d&apos;un degré de liberté</translation>
    </message>
    <message>
        <source>The application has been closed before the end of &apos;{0}&apos;. Its state has been extracted from its output file if it exists.</source>
        <translation>L&apos;application a été fermée avant la fin de &apos;{0}&apos;. Son état a été extrait du fichier de sortie s&apos;il existe.</translation>
    </message>
    <message>
        <source>Starting calculation...&lt;br&gt;Saving study and copying data files, please wait...</source>
        <translation>Démarrage du calcul...&lt;br&gt;Sauvegarde de l&apos;étude et copie des fichiers de données, merci de patienter...</translation>
    </message>
    <message>
        <source>Graph progress</source>
        <translation>Progression du graphe</translation>
    </message>
    <message>
        <source>Graph size</source>
        <translation>Taille du graphe</translation>
    </message>
    <message>
        <source>Hide graph</source>
        <translation>Caché le graphe</translation>
    </message>
    <message>
        <source>This operation needs the Study directory. So, you should save the study before executing this operation.</source>
        <translation>Cette opération a besoin de connaître le répertoire de l&apos;Etude. Vous devez donc sauvegarder l&apos;étude avant d&apos;exécuter cette opération.</translation>
    </message>
    <message>
        <source>Skip</source>
        <translation>Passer</translation>
    </message>
    <message>
        <source>Reuse</source>
        <translation>Réutiliser</translation>
    </message>
    <message>
        <source>Keep results</source>
        <translation>Garder les résultats</translation>
    </message>
    <message>
        <source>Submission parameters:</source>
        <translation>Paramètres de soumission :</translation>
    </message>
    <message>
        <source>Parallel parameters:</source>
        <translation>Paramètres pour le parallélisme :</translation>
    </message>
    <message>
        <source>The calculation will be stopped and the files deleted.
Do you want to continue?</source>
        <translation>Le calcul va être arrêté et les fichiers seront supprimés.
Voulez-vous continuer ?</translation>
    </message>
    <message>
        <source>Stop a job</source>
        <translation>Arrêter un job</translation>
    </message>
    <message>
        <source>Auto Refresh: {0}</source>
        <translation>Rafraichissement auto : {0}</translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation>Fréquence</translation>
    </message>
    <message>
        <source>Open in Persalys</source>
        <translation>Ouvrir dans Persalys</translation>
    </message>
    <message>
        <source>Run with Adao</source>
        <translation>Exécuter avec Adao</translation>
    </message>
    <message>
        <source>&apos;Yacs log&apos;</source>
        <translation>&apos;Yacs&apos; log</translation>
    </message>
    <message>
        <source>Start time: {0}</source>
        <translation>Heure de début : {0}</translation>
    </message>
    <message>
        <source>End time: {0}</source>
        <translation>Heure de fin : {0}</translation>
    </message>
    <message>
        <source>Server name: {0}</source>
        <translation>Nom du serveur : {0}</translation>
    </message>
    <message>
        <source>Version: {0}</source>
        <translation>Version : {0}</translation>
    </message>
    <message>
        <source>Memory limit: {0} MB</source>
        <translation>Limite mémoire : {0} Mo</translation>
    </message>
    <message>
        <source>Time limit: {0}</source>
        <translation>Temps maximum d&apos;exécution : {0}</translation>
    </message>
    <message>
        <source>Partition: {0}</source>
        <translation>Partition : {0}</translation>
    </message>
    <message>
        <source>QOS: {0}</source>
        <translation>QOS : {0}</translation>
    </message>
    <message>
        <source>Job identifier: {0}</source>
        <translation>Identifiant du job : {0}</translation>
    </message>
    <message>
        <source>Number of nodes: {0}</source>
        <translation>Nombre de noeuds : {0}</translation>
    </message>
    <message>
        <source>Number of processors: {0}</source>
        <translation>Nombre de processeurs : {0}</translation>
    </message>
    <message>
        <source>Number of threads: {0}</source>
        <translation>Nombre de threads : {0}</translation>
    </message>
    <message>
        <source>Case executes remotely, automatic updates are disabled. Use &apos;Refresh&apos; or &apos;Auto Refresh&apos; to update</source>
        <translation>Cas exécuté à distance et les mises à jour automatiques sont désactivées. Utilisez &apos;Actualiser&apos; ou &apos;Rafraichissement auto&apos; pour mettre à jour</translation>
    </message>
</context>
<context>
    <name>DataFiles</name>
    <message>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>Nom fichier</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Exists</source>
        <translation>Existe</translation>
    </message>
    <message>
        <source>Embedded</source>
        <translation>Embarqué</translation>
    </message>
    <message>
        <source>Mode</source>
        <translation>Mode</translation>
    </message>
    <message>
        <source>undefined</source>
        <translation>non défini</translation>
    </message>
    <message>
        <source>embedded</source>
        <translation>embarqué</translation>
    </message>
    <message>
        <source>File {0} is already in use elsewhere in the study. It cannot be reused under a different file entry.</source>
        <translation>Le fichier {0} est déjà utilisé ailleurs dans l&apos;étude. Il ne peut pas être réutilisé dans une autre entrée.</translation>
    </message>
    <message>
        <source>Input directory</source>
        <translation>Répertoire d&apos;entrée</translation>
    </message>
    <message>
        <source>Output directory</source>
        <translation>Répertoire de sortie</translation>
    </message>
    <message>
        <source>Data Files Summary</source>
        <translation>Résumé des fichiers de données</translation>
    </message>
    <message>
        <source>Edit...</source>
        <translation>Editer...</translation>
    </message>
    <message>
        <source>Browse Directory</source>
        <translation>Parcourir le répertoire</translation>
    </message>
    <message>
        <source>Browse directory</source>
        <translation>Parcourir le répertoire</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation>Choisir fichier</translation>
    </message>
    <message>
        <source>Change file path</source>
        <translation>Changer le chemin</translation>
    </message>
    <message>
        <source>Stage</source>
        <translation>Etape</translation>
    </message>
    <message>
        <source>Data Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Edit file path</source>
        <translation>Editer le chemin</translation>
    </message>
    <message>
        <source>Show selected file in the explorer</source>
        <translation>Voir le fichier dans l&apos;explorateur</translation>
    </message>
    <message>
        <source>Data Files of {}</source>
        <translation>Fichiers du {}</translation>
    </message>
    <message>
        <source>Server</source>
        <translation>Serveur</translation>
    </message>
</context>
<context>
    <name>DataSettings</name>
    <message>
        <source>Expand All</source>
        <translation>Déplier tout</translation>
    </message>
    <message>
        <source>Collapse All</source>
        <translation>Replier tout</translation>
    </message>
    <message>
        <source>Data Settings</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>Cases</source>
        <translation>Cas</translation>
    </message>
</context>
<context>
    <name>DirsPanel</name>
    <message>
        <source>Input directory</source>
        <translation>Répertoire d&apos;entrée</translation>
    </message>
    <message>
        <source>Output directory</source>
        <translation>Répertoire de sortie</translation>
    </message>
    <message>
        <source>Input directory &apos;{}&apos; does not exist</source>
        <translation>Le répertoire d&apos;entrée &apos;{}&apos; n&apos;existe pas</translation>
    </message>
    <message>
        <source>Choose input directory</source>
        <translation>Choisir le répertoire d&apos;entrée</translation>
    </message>
    <message>
        <source>Choose output directory</source>
        <translation>Choisir le répertoire de sortie</translation>
    </message>
    <message>
        <source>Input and output directories cannot be the same</source>
        <translation>Les répertoires d&apos;entrée et de sortie ne peuvent pas être identiques</translation>
    </message>
    <message>
        <source>Input and output directories cannot be sub-path of each other</source>
        <translation>Les répertoires d&apos;entrée et de sortie ne peuvent pas être des sous-répertoires de l&apos;autre</translation>
    </message>
</context>
<context>
    <name>GroupPanel</name>
    <message>
        <source>&lt; Previous</source>
        <translation>&lt; Recherche précédent</translation>
    </message>
    <message>
        <source>Next &gt;</source>
        <translation>Recherche suivant &gt;</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Show previous {} usages</source>
        <translation>Voir les utilisations précédentes de {}</translation>
    </message>
    <message>
        <source>Show next {} usages</source>
        <translation>Voir les utilisations suivantes de {}</translation>
    </message>
    <message>
        <source>... more usages in this category</source>
        <translation>... plus d&apos;utilisations dans cette catégorie</translation>
    </message>
    <message>
        <source>Usage</source>
        <translation>Utilisation</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Groupes</translation>
    </message>
    <message>
        <source>Keywords</source>
        <translation>Mots-clés</translation>
    </message>
    <message>
        <source>Everywhere</source>
        <translation>Partout</translation>
    </message>
    <message>
        <source>Some groups are used multiple times.</source>
        <translation>Certains groupes sont utilisés plusieurs fois.</translation>
    </message>
</context>
<context>
    <name>InfoView</name>
    <message>
        <source>{} items selected</source>
        <translation>{} éléments sélectionnés</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Informations</translation>
    </message>
    <message>
        <source>Edit Text</source>
        <translation>Editer le texte</translation>
    </message>
    <message>
        <source>Edit selected command in text mode</source>
        <translation>Editer la commande sélectionnée en mode texte</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Edit selected command</source>
        <translation>Editer la commande sélectionnée</translation>
    </message>
    <message>
        <source>Edit text</source>
        <translation>Editer le texte</translation>
    </message>
</context>
<context>
    <name>IntervalTable</name>
    <message>
        <source>Bar plot</source>
        <translation>Histogramme</translation>
    </message>
    <message>
        <source>Line plot</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <source>Show labels</source>
        <translation>Afficher les libellés</translation>
    </message>
    <message>
        <source>Interval range</source>
        <translation>Taille de l&apos;intervalle</translation>
    </message>
    <message>
        <source>Number of steps</source>
        <translation>Nombre de pas</translation>
    </message>
    <message>
        <source>Interval index</source>
        <translation>Index d&apos;intervalle</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <source>DEBUG</source>
        <translation>DEBUG</translation>
    </message>
    <message>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <source>WARNING</source>
        <translation>ALERTE</translation>
    </message>
    <message>
        <source>ERROR</source>
        <translation>ERREUR</translation>
    </message>
    <message>
        <source>Runner</source>
        <translation>Lanceur</translation>
    </message>
    <message>
        <source>Stage</source>
        <translation>Etape</translation>
    </message>
    <message>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
    <message>
        <source> during initialization</source>
        <translation> à l&apos;initialisation</translation>
    </message>
    <message>
        <source> after run #{0}</source>
        <translation> après l&apos;exécution n°{0}</translation>
    </message>
    <message>
        <source> from line {0}</source>
        <translation> depuis la ligne {0}</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>Don&apos;t show this message anymore.</source>
        <translation>Ne plus afficher ce message.</translation>
    </message>
</context>
<context>
    <name>Navigator Tree</name>
    <message>
        <source>&amp;Warp with amplification</source>
        <translation>&amp;Déformée avec amplification</translation>
    </message>
    <message>
        <source>&amp;Color by</source>
        <translation>&amp;Colorier avec</translation>
    </message>
    <message>
        <source>Color by &amp;another field</source>
        <translation>Colorier avec un &amp;autre champ</translation>
    </message>
    <message>
        <source>&amp;Iso-surfaces</source>
        <translation>&amp;Iso-surfaces</translation>
    </message>
    <message>
        <source>&amp;Vector representation</source>
        <translation>&amp;Vecteur</translation>
    </message>
    <message>
        <source>&amp;Show as</source>
        <translation>&amp;Afficher en</translation>
    </message>
    <message>
        <source>Linearize quadratic elements</source>
        <translation>Linéariser les eléments quadratiques</translation>
    </message>
    <message>
        <source>Visualize local frame</source>
        <translation>Visualiser les repères locaux</translation>
    </message>
    <message>
        <source>&amp;Opacity</source>
        <translation>&amp;Opacité</translation>
    </message>
    <message>
        <source>&amp;Ambient Light</source>
        <translation>Lumière &amp;Ambiante</translation>
    </message>
</context>
<context>
    <name>OpenWithAction</name>
    <message>
        <source>Open With</source>
        <translation>Ouvrir avec</translation>
    </message>
    <message>
        <source>Choose Program...</source>
        <translation>Choisir le programme...</translation>
    </message>
    <message>
        <source>Choose program</source>
        <translation>Choisir un programme</translation>
    </message>
</context>
<context>
    <name>ParameterPanel</name>
    <message>
        <source>Edit...</source>
        <translation>Editer...</translation>
    </message>
    <message>
        <source>Add item</source>
        <translation>Ajouter élément</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Parameter</source>
        <translation>Paramètre</translation>
    </message>
    <message>
        <source>Function</source>
        <translation>Fonction</translation>
    </message>
    <message>
        <source>Real</source>
        <translation>Réel</translation>
    </message>
    <message>
        <source>Imaginary</source>
        <translation>Imaginaire</translation>
    </message>
    <message>
        <source>Import table</source>
        <translation>Importer un tableau</translation>
    </message>
    <message>
        <source>Insert row</source>
        <translation>Insérer une ligne</translation>
    </message>
    <message>
        <source>Number of columns in the file does not correspond to the table dimension.
Do you want to read this file anyway?</source>
        <translation>Le nombre de colonne du fichier ne correspond pas à la dimension du tableau.
Voulez-vous continuer à lire ce fichier ?</translation>
    </message>
    <message>
        <source>Invalid input.</source>
        <translation>Saisie non valide.</translation>
    </message>
    <message>
        <source>Do you want to save the changes any way?</source>
        <translation>Voulez-vous sauvegarder les changements ?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Choisir fichier</translation>
    </message>
    <message>
        <source>Edit command</source>
        <translation>Editer la commande</translation>
    </message>
    <message>
        <source>Could not find available file descriptors in the range [%d, %d].
The given file would not be set.</source>
        <translation>Impossible de trouver l&apos;indice du fichier dans [%d, %d].
Le fichier indiqué ne pourra pas être utilisé.</translation>
    </message>
    <message>
        <source>Manual selection</source>
        <translation>Sélection manuelle</translation>
    </message>
    <message>
        <source>More than one mesh found</source>
        <translation>Plusieurs maillages trouvés</translation>
    </message>
    <message>
        <source>No mesh found</source>
        <translation>Aucun maillage trouvé</translation>
    </message>
    <message>
        <source>Command &apos;{}&apos; successfully stored</source>
        <translation>La commande &apos;{}&apos; a été stockée avec succès</translation>
    </message>
    <message>
        <source>View command</source>
        <translation>Voir la commande</translation>
    </message>
    <message>
        <source>Invalid input in {0}.</source>
        <translation>Saisie non valide in {0}.</translation>
    </message>
    <message>
        <source>Not respected the rule: {0} with keywords: {1}.</source>
        <translation>Ne respecte pas la règle : {0} avec les mots-clés : {1}.</translation>
    </message>
    <message>
        <source>&lt;no object selected&gt;</source>
        <translation>&lt;aucun objet sélectionné&gt;</translation>
    </message>
    <message>
        <source>Switch on/off</source>
        <translation>Commutateur on/off</translation>
    </message>
    <message>
        <source>Select value</source>
        <translation>Choisir une valeur</translation>
    </message>
    <message>
        <source>Select python variable</source>
        <translation>Choisir une variable Python</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>item</source>
        <translation>élément</translation>
    </message>
    <message>
        <source>items</source>
        <translation>éléments</translation>
    </message>
    <message>
        <source>Reset to default</source>
        <translation>Revenir aux valeurs par défaut</translation>
    </message>
    <message>
        <source>There are some unsaved modifications will be lost. Do you want to continue?</source>
        <translation>Des modifications n&apos;ont pas été sauvegardées et seront perdues. Voulez-vous continuer ?</translation>
    </message>
    <message>
        <source>Select mesh</source>
        <translation>Choisir maillage</translation>
    </message>
    <message>
        <source>Select existing result</source>
        <translation>Choisir un résultat existant</translation>
    </message>
    <message>
        <source>Enter new result name</source>
        <translation>Entrer un nouveau nom de résultat</translation>
    </message>
    <message>
        <source>Command edition will be aborted and all made changes will be lost. Do you want to continue?</source>
        <translation>L&apos;édition de la commande sera interrompue et tous les changements seront perdus. Voulez-vous continuer ?</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Interrompre</translation>
    </message>
    <message>
        <source>Enter manually the wanted groups if not present in the list</source>
        <translation>Saisir manuellement les groupes s&apos;ils sont absents de la liste</translation>
    </message>
    <message>
        <source>Remove item</source>
        <translation>Supprimer l&apos;élément</translation>
    </message>
    <message>
        <source>Change item position in list</source>
        <translation>Changer la position de l&apos;élément dans la liste</translation>
    </message>
    <message>
        <source>Append row</source>
        <translation>Ajouter une ligne</translation>
    </message>
    <message>
        <source>Remove rows</source>
        <translation>Supprimer des lignes</translation>
    </message>
    <message>
        <source>Append row to the end of table</source>
        <translation>Ajouter une ligne en dernier</translation>
    </message>
    <message>
        <source>Insert row at current position</source>
        <translation>Ajouter une ligne à la position courante</translation>
    </message>
    <message>
        <source>Remove selected rows</source>
        <translation>Supprimer les lignes sélectionnées</translation>
    </message>
    <message>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <source>Move selected rows up</source>
        <translation>Monter les lignes sélectionnées</translation>
    </message>
    <message>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <source>Move selected rows down</source>
        <translation>Descendre les lignes sélectionnées</translation>
    </message>
    <message>
        <source>Plot function</source>
        <translation>Dessiner la fonction</translation>
    </message>
    <message>
        <source>Show/hide plot view</source>
        <translation>Montrer/Cacher la fonction</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>integer</source>
        <translation>entier</translation>
    </message>
    <message>
        <source>float</source>
        <translation>réel</translation>
    </message>
    <message>
        <source>string</source>
        <translation>chaîne</translation>
    </message>
    <message>
        <source>macro name</source>
        <translation>nom de la macro</translation>
    </message>
    <message>
        <source>object with type</source>
        <translation>objet de type</translation>
    </message>
    <message>
        <source>enumerated</source>
        <translation>énumération</translation>
    </message>
    <message>
        <source>List with types</source>
        <translation>Liste de type</translation>
    </message>
    <message>
        <source>Value types</source>
        <translation>Types de valeur</translation>
    </message>
    <message>
        <source>List length should be</source>
        <translation>La taille liste doit être</translation>
    </message>
    <message>
        <source>in range</source>
        <translation>dans la plage</translation>
    </message>
    <message>
        <source>not less than</source>
        <translation>pas moins que</translation>
    </message>
    <message>
        <source>greater than</source>
        <translation>plus grand que</translation>
    </message>
    <message>
        <source>and</source>
        <translation>et</translation>
    </message>
    <message>
        <source>not greater than</source>
        <translation>moins grand que</translation>
    </message>
    <message>
        <source>less than</source>
        <translation>moins que</translation>
    </message>
    <message>
        <source>Default value: </source>
        <translation>Valeur par défaut : </translation>
    </message>
    <message>
        <source>Not Supported</source>
        <translation>Non supporté</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Show Normals</source>
        <translation>Afficher les normales</translation>
    </message>
    <message>
        <source>Hide Normals</source>
        <translation>Cacher les normales</translation>
    </message>
    <message>
        <source>&lt;no {} available&gt;</source>
        <translation>&lt;aucun {} disponible&gt;</translation>
    </message>
    <message>
        <source>&lt;Add variable...&gt;</source>
        <translation>&lt;Ajouter variable...&gt;</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <source>Unselect</source>
        <translation>Désélectionner</translation>
    </message>
    <message>
        <source>Invalid group name: &apos;{}&apos;. Please rename the group.</source>
        <translation>Nom de groupe non valide : &apos;{}&apos;. Veuillez renommer le groupe s&apos;il vous plaît.</translation>
    </message>
    <message>
        <source>Invalid names of groups: {}.</source>
        <translation>Noms de groupe non valides : {}.</translation>
    </message>
    <message>
        <source>Import table (separators: &apos;.&apos;  for decimals and &apos;,&apos; for columns)</source>
        <translation>Importer un tableau</translation>
    </message>
    <message>
        <source>Import function data from file (separators: &apos;.&apos; for decimals and &apos;,&apos; for columns)</source>
        <translation>Importer les données de la fonction depuis un fichier</translation>
    </message>
    <message>
        <source>Number of steps</source>
        <translation>Nombre de pas</translation>
    </message>
    <message>
        <source>Step length</source>
        <translation>Taille du pas</translation>
    </message>
    <message>
        <source>Until</source>
        <translation>Jusqu&apos;à</translation>
    </message>
    <message>
        <source>Interval type</source>
        <translation>Type d&apos;intervalle</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Enter integer number</source>
        <translation>Saisir un nombre entier</translation>
    </message>
    <message>
        <source>Enter real number</source>
        <translation>Saisir un nombre réel</translation>
    </message>
    <message>
        <source>Enter complex number</source>
        <translation>Saisir un nombre complexe</translation>
    </message>
    <message>
        <source>Enter string</source>
        <translation>Saisir une chaîne de caractères</translation>
    </message>
    <message>
        <source>Insert into multiple selection not possible</source>
        <translation>Impossible d&apos;insérer dans une sélection multiple</translation>
    </message>
    <message>
        <source>Before checking the box to reuse an input object, please fill one of these keywords:
{0}</source>
        <translation>Avant de cocher la case pour réutiliser un objet en entrée, veuillez renseigner un de ces mots-clés :
{0}</translation>
    </message>
    <message>
        <source>Select reusable object</source>
        <translation>Sélectionner un objet réutilisable</translation>
    </message>
    <message>
        <source>Add an occurrence</source>
        <translation>Ajoute une occurrence</translation>
    </message>
    <message>
        <source>Enter a number or a string</source>
        <translation>Saisir un nombre ou une chaîne de caractères</translation>
    </message>
</context>
<context>
    <name>ParametricPanel</name>
    <message>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <source>No Python variable found. You must add one or more Python variables in a graphical stage of your study.</source>
        <translation>Aucune variable Python trouvée. Vous devez ajouter au moins une variable Python dans une étape graphique de l&apos;étude.</translation>
    </message>
    <message>
        <source>No output command found. You must add a &lt;i&gt;IMPR_TABLE&lt;/i&gt; command using &lt;i&gt;FORMAT=&apos;NUMPY&apos;&lt;/i&gt; and &lt;i&gt;NOM_PARA&lt;/i&gt; to define an explicit list of output parameters.</source>
        <translation>Aucune commande de sortie trouvée. Vous devez ajouter la commande &lt;i&gt;IMPR_TABLE&lt;/i&gt; utilisant &lt;i&gt;FORMAT=&apos;NUMPY&apos;&lt;/i&gt; et &lt;i&gt;NOM_PARA&lt;/i&gt; pour définir explicitement la liste des paramètres de sortie.</translation>
    </message>
    <message>
        <source>Parametric and uncertainty analysis (Persalys)</source>
        <translation>Analyses paramétriques ou d&apos;incertitudes (Persalys)</translation>
    </message>
    <message>
        <source>Parameters identification, models adjustment (Adao)</source>
        <translation>Identification de paramètres, ajustement de modèles (Adao)</translation>
    </message>
    <message>
        <source>Select a CSV, Text or numpy file...</source>
        <translation>Sélectionnez un fichier CSV, texte ou numpy...</translation>
    </message>
    <message>
        <source>no file selected</source>
        <translation>pas de fichier sélectionné</translation>
    </message>
    <message>
        <source>{0} rows, {1} columns {2}</source>
        <translation>{0} lignes, {1} colonnes {2}</translation>
    </message>
    <message>
        <source>{0} rows, {1} columns</source>
        <translation>{0} lignes, {1} colonnes</translation>
    </message>
    <message>
        <source>can not read file content</source>
        <translation>on ne peut pas lire le contenu du fichier</translation>
    </message>
    <message>
        <source>Observations file and code_aster numpy file&lt;br/&gt;must have the same shape!</source>
        <translation>Le fichier d&apos;observations et le fichier numpy de code_aster&lt;br/&gt;doivent avoir la même dimension !</translation>
    </message>
    <message>
        <source>Nominal value</source>
        <translation>Valeur nominale</translation>
    </message>
    <message>
        <source>Initial value</source>
        <translation>Valeur initiale</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <source>the file does not exist, can not check the array shape</source>
        <translation>Le fichier n&apos;existe pas, on ne peut pas vérifier la dimension du tableau</translation>
    </message>
    <message>
        <source>Output file created by IMPR_TABLE</source>
        <translation>Fichier produit par IMPR_TABLE</translation>
    </message>
    <message>
        <source>Select the numpy file</source>
        <translation>Sélectionnez le fichier numpy</translation>
    </message>
    <message>
        <source>Select a text or numpy file</source>
        <translation>Sélectionnez un fichier texte ou numpy</translation>
    </message>
    <message>
        <source>please check Persalys or Adao box</source>
        <translation>veuillez cocher la case Persalys ou Adao</translation>
    </message>
    <message>
        <source>The existing file seems inconsistent with current NOM_PARA keyword.</source>
        <translation>Le fichier existant semble incohérent avec le mot-clé NOM_PARA actuel.</translation>
    </message>
    <message>
        <source>Observations file</source>
        <translation>Fichier des observations</translation>
    </message>
    <message>
        <source>only generate the execution directory, do not start</source>
        <translation>génère seulement le répertoire d&apos;exécution, ne démarre pas</translation>
    </message>
    <message>
        <source>Warning: Selected resources (time limit, number of processors) are used for the global optimization analysis and for each evaluation!</source>
        <translation>Attention : Les ressources (temps, nombre de processeurs) sont utilisées pour l&amp;apos;analyse globale d&amp;apos;optimisation et pour chaque évaluation !</translation>
    </message>
</context>
<context>
    <name>PrefDlg</name>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>North</source>
        <translation>Nord</translation>
    </message>
    <message>
        <source>South</source>
        <translation>Sud</translation>
    </message>
    <message>
        <source>West</source>
        <translation>Ouest</translation>
    </message>
    <message>
        <source>East</source>
        <translation>Est</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Only display icon</source>
        <translation>Afficher seulement l&apos;icone</translation>
    </message>
    <message>
        <source>Only display text</source>
        <translation>Afficher seulement le texte</translation>
    </message>
    <message>
        <source>Text appears beside icon</source>
        <translation>Texte à côté de l&apos;icone</translation>
    </message>
    <message>
        <source>Text appears under icon</source>
        <translation>Texte sous l&apos;icone</translation>
    </message>
    <message>
        <source>Follow style</source>
        <translation>Suivre le style</translation>
    </message>
    <message>
        <source>Some changes will take effect after application restart.</source>
        <translation>Certains changements prendront effet après avoir redémarré l&apos;application.</translation>
    </message>
    <message>
        <source>Strict import mode</source>
        <translation>Mode d&apos;import strict</translation>
    </message>
    <message>
        <source>Confirmations</source>
        <translation>Confirmations</translation>
    </message>
    <message>
        <source>Delete object</source>
        <translation>Supprimer l&apos;objet</translation>
    </message>
    <message>
        <source>External editor</source>
        <translation>Editeur externe</translation>
    </message>
    <message>
        <source>Use default</source>
        <translation>Utiliser défaut</translation>
    </message>
    <message>
        <source>Ask</source>
        <translation>Demander</translation>
    </message>
    <message>
        <source>Show identifier for selector items</source>
        <translation>Afficher l&apos;identifiant des éléments du sélecteur</translation>
    </message>
    <message>
        <source>Delete case used by other case(s)</source>
        <translation>Supprimer les cas utilisés par d&apos;autres cas</translation>
    </message>
    <message>
        <source>Allow deleting cases used by other case(s)</source>
        <translation>Autoriser la suppression de cas utilisés par d&apos;autres cas</translation>
    </message>
    <message>
        <source>Undefined files</source>
        <translation>Fichiers non définis</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <source>Version of code_aster</source>
        <translation>Version de code_aster</translation>
    </message>
    <message>
        <source>Toolbar button style</source>
        <translation>Style des boutons</translation>
    </message>
    <message>
        <source>Workspace tab pages position</source>
        <translation>Position des onglets dans l&apos;espace de travail</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation>Editeur</translation>
    </message>
    <message>
        <source>Use business-oriented translations</source>
        <translation>Utiliser traduction orientée métier</translation>
    </message>
    <message>
        <source>Sort stages</source>
        <translation>Trier les étapes</translation>
    </message>
    <message>
        <source>Show catalogue name</source>
        <translation>Voir le nom du catalogue</translation>
    </message>
    <message>
        <source>Automatically activate command edition</source>
        <translation>Activer l&apos;édition automatique des commandes</translation>
    </message>
    <message>
        <source>Show related concepts</source>
        <translation>Monter les concepts liés</translation>
    </message>
    <message>
        <source>Delete child stages</source>
        <translation>Supprimer les sous-étapes</translation>
    </message>
    <message>
        <source>Show read-only banner</source>
        <translation>Afficher la bannière &quot;lecture seule&quot;</translation>
    </message>
    <message>
        <source>Sort selector items</source>
        <translation>Trier les éléments</translation>
    </message>
    <message>
        <source>Show comments</source>
        <translation>Afficher les commentaires</translation>
    </message>
    <message>
        <source>Close parameter panel with modifications</source>
        <translation>Fermer le panneau avec les modifications</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Keywords</source>
        <translation>Mots-clés</translation>
    </message>
    <message>
        <source>Values</source>
        <translation>Valeurs</translation>
    </message>
    <message>
        <source>Parameter content display mode</source>
        <translation>Mode d&apos;affichage des paramètres</translation>
    </message>
    <message>
        <source>Convert invalid graphical stage</source>
        <translation>Convertir les étapes graphiques non valides</translation>
    </message>
    <message>
        <source>Python editor</source>
        <translation>Editeur Python</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>Manuel</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Toujours</translation>
    </message>
    <message>
        <source>Completion mode</source>
        <translation>Mode de complétion</translation>
    </message>
    <message>
        <source>Abort command edition</source>
        <translation>Annuler l&apos;édition de commande</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <source>Auto-hide search panel</source>
        <translation>Masquer automatiquement le panneau de recherche</translation>
    </message>
    <message>
        <source>Catalogs</source>
        <translation>Catalogues</translation>
    </message>
    <message>
        <source>User&apos;s catalogs</source>
        <translation>Catalogues utilisateur</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <source>Enable current line highlight</source>
        <translation>Met en surbrillance de la ligne actuelle</translation>
    </message>
    <message>
        <source>Enable text wrapping</source>
        <translation>Retour à la ligne dynamique</translation>
    </message>
    <message>
        <source>Center cursor on scroll</source>
        <translation>Centre le curseur lors du scroll</translation>
    </message>
    <message>
        <source>Display line numbers area</source>
        <translation>Affiche les numéros de ligne</translation>
    </message>
    <message>
        <source>Display tab delimiters</source>
        <translation>Affiche les guides d&apos;indentation</translation>
    </message>
    <message>
        <source>Tab size</source>
        <translation>Largeur d&apos;indentation</translation>
    </message>
    <message>
        <source>Display vertical edge</source>
        <translation>Afficher la ligne verticale</translation>
    </message>
    <message>
        <source>Number of columns</source>
        <translation>Nombre de colonnes</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation>Défauts</translation>
    </message>
    <message>
        <source>Do you want to restore default preferences?</source>
        <translation>Voulez-vous restaurer les préférences par défaut ?</translation>
    </message>
    <message>
        <source>Use external editor for text stage edition</source>
        <translation>Utiliser l&apos;éditeur de texte externe pour l&apos;édition des étapes</translation>
    </message>
    <message>
        <source>Use external editor for data files edition</source>
        <translation>Utiliser l&apos;éditeur de texte externe pour l&apos;édition des fichiers de données</translation>
    </message>
    <message>
        <source>Use external editor for message files viewing</source>
        <translation>Utiliser l&apos;éditeur de texte externe pour visualiser les fichiers de message</translation>
    </message>
    <message>
        <source>Warn when viewing file larger than</source>
        <translation>Avertissement pour les fichiers plus grand que</translation>
    </message>
    <message>
        <source>Join similar data files</source>
        <translation>Fusionner les fichiers identiques</translation>
    </message>
    <message>
        <source>Edit list-like keywords in sub-panel</source>
        <translation>Editer les mots-clés listes dans un sous-panneau</translation>
    </message>
    <message>
        <source>Show catalogue name in command selector items</source>
        <translation>Montrer le nom du catalogue dans le sélecteur de commandes</translation>
    </message>
    <message>
        <source>Limit of number of lines for graphical mode</source>
        <translation>Nombre de lignes limite pour le mode graphique</translation>
    </message>
    <message>
        <source>Disable Undo/Redo feature</source>
        <translation>Désactive la fonctionnalité Annuler/Refaire</translation>
    </message>
    <message>
        <source>External browser</source>
        <translation>Navigateur externe</translation>
    </message>
    <message>
        <source>Choose program</source>
        <translation>Choisir un programme</translation>
    </message>
    <message>
        <source>Data Files panel</source>
        <translation>Panneau Fichiers</translation>
    </message>
    <message>
        <source>Data Settings panel</source>
        <translation>Panneau Paramètres</translation>
    </message>
    <message>
        <source>Parameters panel</source>
        <translation>Panneau Commande</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Site internet</translation>
    </message>
    <message>
        <source>Open workspace in</source>
        <translation>Vue par défaut</translation>
    </message>
    <message>
        <source>History view</source>
        <translation>Vue Historique</translation>
    </message>
    <message>
        <source>Case view</source>
        <translation>Vue Cas</translation>
    </message>
    <message>
        <source>Connect remote servers at startup</source>
        <translation>Connecte les serveurs distants au démarrage</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <source>Groups selection</source>
        <translation>Sélection de groupes</translation>
    </message>
    <message>
        <source>Selected groups</source>
        <translation>Groupes sélectionnés</translation>
    </message>
    <message>
        <source>Unselected groups</source>
        <translation>Groupes non sélectionnés</translation>
    </message>
    <message>
        <source>Groups highlight color is specified in the preferences of Mesh module.</source>
        <translation>La couleur de sélection des groupes est définie dans les préférences du module Mesh.</translation>
    </message>
    <message>
        <source>Show categories</source>
        <translation>Voir les catégories</translation>
    </message>
    <message>
        <source>Automatically expand items on import operations</source>
        <translation>Déplier automatiquement les éléments lors des opérations d&apos;import</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation>Tableau de bord</translation>
    </message>
    <message>
        <source>Toogle buttons for Reusable option</source>
        <translation>Boutons poussoirs pour les options de réutilisation</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Number of groups in chunk</source>
        <translation>Nombre de groupes dans le tronçon</translation>
    </message>
    <message>
        <source>WARNING: can not yet restore setting {0}.</source>
        <translation>ATTENTION : impossible de restaurer le paramètre {0}.</translation>
    </message>
    <message>
        <source>Use default language (fr)</source>
        <translation>Utiliser la langue par défaut (fr)</translation>
    </message>
</context>
<context>
    <name>Result</name>
    <message>
        <source>Output messages for stage {0}</source>
        <translation>Messages pour l&apos;étape {0}</translation>
    </message>
    <message>
        <source>top</source>
        <translation>haut</translation>
    </message>
    <message>
        <source>bottom</source>
        <translation>bas</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>back to top</source>
        <translation>retour en haut</translation>
    </message>
    <message>
        <source>Execution grouped with the following stage.</source>
        <translation>Exécution groupée avec les étapes suivantes.</translation>
    </message>
    <message>
        <source>Full job output should be available in the directory {0!r}.</source>
        <translation>La sortie complète du calcul devrait être disponible dans le répertoire {0!r}.</translation>
    </message>
    <message>
        <source>Show only the last {0} lines</source>
        <translation>Affiche seulement les {0} dernières lignes</translation>
    </message>
</context>
<context>
    <name>Rules</name>
    <message>
        <source>At Least One</source>
        <translation>Au moins une</translation>
    </message>
    <message>
        <source>Exactly One</source>
        <translation>Exactement une</translation>
    </message>
    <message>
        <source>At Most One</source>
        <translation>Au plus une</translation>
    </message>
    <message>
        <source>If First All Present</source>
        <translation>Si premier tous présent</translation>
    </message>
    <message>
        <source>Only First Present</source>
        <translation>Seulement premier présent</translation>
    </message>
    <message>
        <source>All Together</source>
        <translation>Tout ensemble</translation>
    </message>
</context>
<context>
    <name>RunPanel</name>
    <message>
        <source>Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <source>Case name</source>
        <translation>Nom du cas</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation>Mémoire</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <source>Run servers</source>
        <translation>Serveurs</translation>
    </message>
    <message>
        <source>Version of code_aster</source>
        <translation>Version de code_aster</translation>
    </message>
    <message>
        <source>Number of MPI CPU</source>
        <translation>Nombre de CPU MPI</translation>
    </message>
    <message>
        <source>User description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Run mode</source>
        <translation>Mode d&apos;exécution</translation>
    </message>
    <message>
        <source>Execution mode</source>
        <translation>Mode d&apos;exécution</translation>
    </message>
    <message>
        <source>History folder</source>
        <translation>Répertoire de l&apos;historique</translation>
    </message>
    <message>
        <source>Remote databases</source>
        <translation>Bases distantes</translation>
    </message>
    <message>
        <source>Number of MPI nodes</source>
        <translation>Nombre de noeuds MPI</translation>
    </message>
    <message>
        <source>Number of threads</source>
        <translation>Nombre de threads</translation>
    </message>
    <message>
        <source>Server partition</source>
        <translation>Partition du serveur</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>Basique</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>Run parameters</source>
        <translation>Paramètres d&apos;exécution</translation>
    </message>
    <message>
        <source>QOS</source>
        <translation>QOS</translation>
    </message>
    <message>
        <source>Compress databases</source>
        <translation>Compresser les bases de données</translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation>Arguments</translation>
    </message>
    <message>
        <source>WcKey</source>
        <translation>WcKey</translation>
    </message>
    <message>
        <source>Extra parameters</source>
        <translation>Paramètres supplémentaires</translation>
    </message>
    <message>
        <source>No available versions found on &apos;{0}&apos;.
Previous server is selected: &apos;{1}&apos;.</source>
        <translation>Aucune version disponible sur &apos;{0}&apos;.
Serveur précédent sélectionné : &apos;{1}&apos;.</translation>
    </message>
    <message>
        <source>Parametric</source>
        <translation>Paramétrique</translation>
    </message>
</context>
<context>
    <name>RunPanelModel</name>
    <message>
        <source>please select at least a variable</source>
        <translation>sélectionnez au moins une variable</translation>
    </message>
    <message>
        <source>please select the numpy file from IMPR_TABLE</source>
        <translation>sélectionnez le fichier numpy de IMPR_TABLE</translation>
    </message>
    <message>
        <source>NOM_PARA not defined in the selected IMPR_TABLE command</source>
        <translation>NOM_PARA non défini dans la commande IMPR_TABLE sélectionnée</translation>
    </message>
    <message>
        <source>please select the observations file</source>
        <translation>sélectionnez le fichier des observations</translation>
    </message>
    <message>
        <source>please define the initial value of each variable</source>
        <translation>il faut définir la valeur initiale de chaque variable</translation>
    </message>
</context>
<context>
    <name>Runner</name>
    <message>
        <source>Server {0!r} is not available.</source>
        <translation>Serveur {0!r} non disponible.</translation>
    </message>
    <message>
        <source>Unknown engine type: {0}</source>
        <translation>Type de moteur inconnu : {0}</translation>
    </message>
    <message>
        <source>filename not defined for unit {0}</source>
        <translation>fichier non défini pour l&apos;unité {0}</translation>
    </message>
    <message>
        <source>Run case &quot;{0}&quot; calculations process started</source>
        <translation>Cas &quot;{0}&quot; démarrage des calculs</translation>
    </message>
    <message>
        <source>Run case &quot;{0}&quot; calculations process stopped</source>
        <translation>Cas &quot;{0}&quot; arrêt des calculs</translation>
    </message>
    <message>
        <source>Unknown server: {0!r}</source>
        <translation>Serveur inconnu : {0!r}</translation>
    </message>
    <message>
        <source>Run case &quot;{0}&quot; calculations process finished</source>
        <translation>Cas &quot;{0}&quot; calculs terminés</translation>
    </message>
    <message>
        <source>Starting &quot;{0}&quot;...</source>
        <translation>Démarrage &quot;{0}&quot;...</translation>
    </message>
    <message>
        <source>Error during submission of &quot;{0}&quot;</source>
        <translation>Erreur lors de la soumission de &quot;{0}&quot;</translation>
    </message>
    <message>
        <source>Stage &quot;{0}&quot; start calculation (jobid={1}, queue={2})</source>
        <translation>Etape &quot;{0}&quot; démarrage des calculs (jobid={1}, queue={2})</translation>
    </message>
    <message>
        <source>Stage &quot;{0}&quot; calculation failed. Interruption</source>
        <translation>Etape &quot;{0}&quot; échec du calcul. Abandon</translation>
    </message>
    <message>
        <source>Stage &quot;{0}&quot; calculation succeeded</source>
        <translation>Etape &quot;{0}&quot; calcul effectué</translation>
    </message>
    <message>
        <source>Stage &quot;{0}&quot; start calculation (jobid={1})</source>
        <translation>Etape &quot;{0}&quot; démarrage du calcul (jobid={1})</translation>
    </message>
    <message>
        <source>Stage &quot;{0}&quot; calculation succeeded with state {1} ({2})</source>
        <translation>Etape &quot;{0}&quot; calcul terminé avec l&apos;état {1} ({2})</translation>
    </message>
    <message>
        <source>Copying result {0!r}</source>
        <translation>Copie des résultats {0!r}</translation>
    </message>
    <message>
        <source>ERROR: Copy failed: {0}</source>
        <translation>ERREUR: Echec de la copie: {0}</translation>
    </message>
    <message>
        <source>Cannot read output file or empty file.</source>
        <translation>On ne peut pas lire le fichier de sortie ou bien il est vide.</translation>
    </message>
    <message>
        <source>Run case &quot;{0}&quot; is already stopped.</source>
        <translation>Cas &quot;{0}&quot; est déjà stoppé.</translation>
    </message>
    <message>
        <source>Copying result directory {0!r}</source>
        <translation>Copier le répertoire des résultats</translation>
    </message>
    <message>
        <source>{0} result file(s) has(have) not been copied to its(their) destination. Following stages will probably fail.</source>
        <translation>{0} fichier(s) résultat n&apos;a(ont) pas été copié(s) vers leur destination. Les étapes suivantes vont probablement échouer.</translation>
    </message>
    <message>
        <source>No such file: {0!r}. Please check the Case View/DataFiles tab.</source>
        <translation>Fichier inexistant : {0!r}. Vérifiez l&apos;onglet Vue Cas / Fichiers.</translation>
    </message>
    <message>
        <source>The mesh object does not exist anymore. Please check objects in the Mesh module.</source>
        <translation>L&apos;objet maillage n&apos;existe plus. Vérifiez les objets du module Mesh.</translation>
    </message>
    <message>
        <source>Execution of &quot;{0}&quot; failed</source>
        <translation>L&apos;exécution de &quot;{0}&quot; a échoué</translation>
    </message>
    <message>
        <source>Terminating job {0} on {1}...</source>
        <translation>Arrêt du job {0} sur {1}...</translation>
    </message>
    <message>
        <source>Refreshing configuration on {0}...</source>
        <translation>Actualisation de la configuration de {0}...</translation>
    </message>
    <message>
        <source>Getting job output on {0}...</source>
        <translation>Récupération de la sortie du job sur {0}...</translation>
    </message>
    <message>
        <source>Copy of {0}@{1}:{2} to {3} exited with code {4[0]}.
Error:
{4[2]}</source>
        <translation>La copie de {0}@{1}:{2} vers {3} s&apos;est terminée avec le code {4[0]}.
Erreur:
{4[2]}</translation>
    </message>
    <message>
        <source>Calculation not started, execution directory prepared</source>
        <translation>Le calcul n&apos;a pas été lancé, le répertoire d&apos;exécution a été préparé</translation>
    </message>
    <message>
        <source>Asking the server for the job state...</source>
        <translation>Interroge le serveur pour l&apos;état du calcul...</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <source>Search... (press Esc to clear search)</source>
        <translation>Rechercher... (Tapez Esc pour vider la recherche)</translation>
    </message>
</context>
<context>
    <name>Searcher</name>
    <message>
        <source>Search criteria</source>
        <translation>Critère de recherche</translation>
    </message>
    <message>
        <source>Concept</source>
        <translation>Concept</translation>
    </message>
    <message>
        <source>Keyword</source>
        <translation>Mot-clé</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Type of search</source>
        <translation>Type de recherche</translation>
    </message>
    <message>
        <source>Find previous item</source>
        <translation>Recherche précédent</translation>
    </message>
    <message>
        <source>Find next item</source>
        <translation>Recherche suivant</translation>
    </message>
    <message>
        <source>Close search panel</source>
        <translation>Fermer le panneau de recherche</translation>
    </message>
    <message>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>ShowAllPanel</name>
    <message>
        <source>Command description</source>
        <translation>Description de la commande</translation>
    </message>
    <message>
        <source>Add command</source>
        <translation>Ajouter commande</translation>
    </message>
    <message>
        <source>Expand All</source>
        <translation>Déplier tout</translation>
    </message>
    <message>
        <source>Collapse All</source>
        <translation>Replier tout</translation>
    </message>
    <message>
        <source>Show all commands</source>
        <translation>Voir toutes les commandes</translation>
    </message>
    <message>
        <source>Remove from Favorites</source>
        <translation>Supprimer des favoris</translation>
    </message>
    <message>
        <source>Add to Favorites</source>
        <translation>Ajouter aux favoris</translation>
    </message>
    <message>
        <source>Add to Study</source>
        <translation>Ajouter à l&apos;étude</translation>
    </message>
</context>
<context>
    <name>StageTextEditor</name>
    <message>
        <source>Edit stage</source>
        <translation>Editer l&apos;étape</translation>
    </message>
    <message>
        <source>Edit stage &apos;{}&apos;</source>
        <translation>Editer l&apos;étape &apos;{}&apos;</translation>
    </message>
</context>
<context>
    <name>TextFileDialog</name>
    <message>
        <source>View file</source>
        <translation>Voir le fichier</translation>
    </message>
    <message>
        <source>Edit file</source>
        <translation>Editer le fichier</translation>
    </message>
</context>
<context>
    <name>UndoAction</name>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>UnitPanel</name>
    <message>
        <source>Add data file</source>
        <translation>Ajouter fichier</translation>
    </message>
    <message>
        <source>Edit data file</source>
        <translation>Editer fichier</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Choisir fichier</translation>
    </message>
    <message>
        <source>You should save the study before embedding the file</source>
        <translation>Vous devez enregistrer l&apos;étude avant d&apos;embarquer le fichier</translation>
    </message>
    <message>
        <source>There is already a file in this stage whose basename is &apos;{0}&apos;.
Please rename the file before you add it to the study.</source>
        <translation>Il y a déjà dans ce stage un fichier dont le nom est &apos;{0}&apos;.
Veuillez renommer the fichier avant de l&apos;ajouter à l&apos;étude.</translation>
    </message>
    <message>
        <source>Please select a value for the `Mode` property above before browsing for the file path.</source>
        <translation>Veuillez sélectionner la propriété `Mode` au-dessus avant de choisir un fichier.</translation>
    </message>
    <message>
        <source>Your modifications lead to file {0} being written after a first use,
a situation that will almost always result to overriden or unexploitable files.
Are you sure that you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnusedConceptsDialog</name>
    <message>
        <source>Detect unused concepts for {}</source>
        <translation>Détecter les concepts non utilisés pour {}</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Sélectionner tout</translation>
    </message>
    <message>
        <source>Unselect All</source>
        <translation>Désélectionner tout</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <source>Unselect</source>
        <translation>Désélectionner</translation>
    </message>
</context>
<context>
    <name>ValidityReportDialog</name>
    <message>
        <source>Validity report for {}</source>
        <translation>Rapport de validité pour {}</translation>
    </message>
    <message>
        <source>Show valid commands</source>
        <translation>Afficher les commandes valides</translation>
    </message>
    <message>
        <source>Syntaxic</source>
        <translation>Syntaxique</translation>
    </message>
    <message>
        <source>Dependency</source>
        <translation>Dépendance</translation>
    </message>
    <message>
        <source>Naming</source>
        <translation>Nommage</translation>
    </message>
    <message>
        <source>Concept</source>
        <translation>Concept</translation>
    </message>
    <message>
        <source>Used</source>
        <translation>Utilisés</translation>
    </message>
</context>
<context>
    <name>VariablePanel</name>
    <message>
        <source>Create variable</source>
        <translation>Créer une variable</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Expression</source>
        <translation>Expression</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Edit variable</source>
        <translation>Editer variable</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
</context>
<context>
    <name>Workspace</name>
    <message>
        <source>History View</source>
        <translation>Historique</translation>
    </message>
    <message>
        <source>Case View</source>
        <translation>Vue Cas</translation>
    </message>
    <message>
        <source>Data Settings</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>Data Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Cases</source>
        <translation>Cas</translation>
    </message>
    <message>
        <source>Run parameters</source>
        <translation>Paramètres d&apos;exécution</translation>
    </message>
    <message>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
</context>
</TS>
