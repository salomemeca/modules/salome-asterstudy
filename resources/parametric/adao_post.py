import os.path as osp
from glob import glob

import matplotlib.pyplot as plt
import numpy

from asterstudy.common import valid_file_name

import adao_data as DATA


def plot(ax, dirname, col, *args, **kwargs):
    """Plot a column from a directory results on the given figure.

    Arguments:
        ax (Axes): Axes instance to be extended.
        dirname (str): directory path containing the 'outputs.npy' file.
        col (int): index of the column to be plotted.
    """
    result = numpy.load(osp.join(dirname, "outputs.npy"))
    ax.plot(result[:, 0], result[:, col], *args, **kwargs)


def plot_results(all=False, show=False):
    """Plot results and observations.

    Arguments:
        all (bool): plot all intermediate results if *True*.
        show (bool): show the figure in interactive mode if *True*.
    """
    # Observations are supposed to be : x, y0, y1, ...
    fig, axe = plt.subplots(DATA.obs_nb - 1)

    # analysis
    param = numpy.load("analysis.npy")
    print("Results of the analysis:", param)
    diropt = ".run_param_" + valid_file_name(repr(list(param)))

    for i in range(DATA.obs_nb - 1):
        col = i + 1
        axe[i].set_xlabel(DATA.obs_names[0])
        axe[i].set_ylabel(DATA.obs_names[col])

        # plot observations and results for optimized variables
        obs = DATA.obs_values
        axe[i].plot(obs[:, 0], obs[:, col], label="observation")
        if osp.exists(diropt):
            plot(axe[i], diropt, col, label="analysis")
        else:
            print("Results for optimal variables not available")

        # plot all all results
        if all:
            dirs = glob(".run_param_*")
            for subdir in dirs:
                if subdir in (diropt, ):
                    continue
                plot(axe[i], subdir, col, "+")
        axe[i].legend()
        axe[i].grid(True)

    plt.tight_layout()
    plt.savefig('analysis.png')
    if show:
        plt.show()
    plt.close(fig)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--all", action="store_true")
    parser.add_argument("--show", action="store_true")
    args = parser.parse_args()
    plot_results(**vars(args))
