# -*- coding: utf-8 -*-

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""
Interface with Parametric modules
---------------------------------

This module implements the links with the Adao or Persalys modules.

"""

import getpass
import os
import os.path as osp

from ..common import (CFG, debug_message, info_message, translate,
                      valid_file_name)
from ..datamodel.engine.salome_runner import (resource_parameters,
                                              unique_directory)
from ..datamodel.parametric import export_to_parametric
from ..datamodel.job_informations import JobInfos
from .salomegui_utils import get_salome_gui, get_salome_pyqt

HAS_OTGUI = False
try:
    import persalys
    from salome_ot import getYacsPyStudy
    HAS_OTGUI = True
except ImportError:
    info_message(translate("AsterStudy", "Can not import 'persalys'. "
                           "Please check your installation."))


def create_otstudy(case):
    """Create the Persalys study.

    TODO:
        - set nice job name (idefix_job) : salome_parameters
        - files/directories

    Arguments:
        case (Case): Case object.

    Returns:
        OTStudy: Persalys study.
    """
    otdata = export_to_parametric(case, case.folder)
    debug_message("Case name: {0}".format(case.name))
    debug_message("Python function for Persalys:\n{0}".format(otdata.code))
    debug_message("Input files and directories:\n{0}".format(otdata.files))
    debug_message("Job parameters:\n{0}".format(otdata.parameters))

    # see `salome_ot.py` (in PERSALYS module) for examples of API usage
    otstudy, model = getYacsPyStudy(otdata.code, model_name=case.name)

    # set nominal value and variables description
    lvar = [var for name, var in case.variables.items()
            if name in case.job_infos.get("input_vars")]
    for var in lvar:
        model.setInputValue(var.name, var.update())
        model.setInputDescription(var.name, str(var.comment or ""))

    jinf = case.job_infos.copy()
    salome_job = model.jobParameters().salome_parameters
    salome_job.job_name = valid_file_name(case.name)
    salome_job.wckey = jinf.get('wckey') or CFG.get_wckey() or ''
    prefix = osp.join(os.getenv('HOME', '/tmp'),
                      'salome_workdir_' + getpass.getuser())
    salome_job.work_directory = unique_directory(prefix, 'from_ot')
    salome_job.maximum_duration = jinf.get('time')
    salome_job.in_files = otdata.files

    jinf.set('mode', JobInfos.Batch)
    salome_job.resource_required = resource_parameters(jinf)

    return otstudy


def open_persalys(case):
    """Open Persalys module.

    Arguments:
        case (Case): Case object.
    """
    if not HAS_OTGUI:
        return

    otstudy = create_otstudy(case)
    xmlfile = osp.join(case.folder, otstudy.getName() + ".xml")
    otstudy.save(xmlfile)
    info_message(translate("AsterStudy", "Persalys study file is '{0}'"
                           .format(xmlfile)))

    info_message(translate("AsterStudy", "Loading Persalys module..."))
    otm = get_salome_gui().getComponentUserName(str('OPENTURNS'))
    get_salome_pyqt().activateModule(str(otm))

    studies = persalys.Study.GetInstances()
    if otstudy not in studies:
        persalys.Study.Open(xmlfile)
