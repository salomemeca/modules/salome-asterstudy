# -*- coding: utf-8 -*-

# Copyright 2016-2018 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.
"""
Parametric widget
-----------------

This module implements `Export As Parametric` panel.

"""

import os.path as osp
import traceback
from functools import partial

import numpy
from PyQt5 import Qt as Q

from ..common import debug_message, load_pixmap, translate
from ..datamodel.job_informations import JobInfos

__all__ = ["ParametricPanel"]

# note: the following pragma is added to prevent pylint complaining
#       about functions that follow Qt naming conventions;
#       it should go after all global functions
# pragma pylint: disable=invalid-name

Persalys = JobInfos.Persalys
Adao = JobInfos.Adao


class ParametricPanel(Q.QWidget):
    """OpenTuns Export panel.

    Signal:
        useParametric(bool): Send the kind of parametric study is selected.
    """
    # pragma pylint: disable=too-many-instance-attributes
    useParametric = Q.pyqtSignal(int)

    def __init__(self, model, parent=None):
        """
        Create panel.

        Arguments:
            parent (Optional[QWidget]): Parent widget. Defaults to *None*.
        """
        super().__init__(parent=parent)
        self._model = model
        self._titles = {
            "vars": translate("ParametricPanel", "Variables"),
            "npy": translate("ParametricPanel",
                             "Output file created by IMPR_TABLE"),
            "obs": translate("ParametricPanel", "Observations file"),
        }
        self._group = {}

        main = Q.QVBoxLayout(self)
        main.setContentsMargins(0, 0, 0, 0)

        module = Q.QWidget(self)
        layout = Q.QVBoxLayout(module)
        self._cbox = {}
        self._cbox[Persalys] = Q.QCheckBox(
            translate("ParametricPanel",
                      "Parametric and uncertainty analysis (Persalys)"),
            module)
        adct = Q.QWidget(self)
        adctlay = Q.QHBoxLayout(adct)
        self._cbox[Adao] = Q.QCheckBox(
            translate("ParametricPanel",
                      "Parameters identification, models adjustment (Adao)"),
            module)
        layout.addWidget(self._cbox[Persalys])
        adctlay.addWidget(self._cbox[Adao])
        adctlay.setContentsMargins(0, 0, 0, 0)
        self._beta = Q.QLabel(adct)
        self._beta.setPixmap(load_pixmap("as_beta.png"))
        adctlay.addWidget(self._beta)
        layout.addWidget(adct)
        env = Q.QWidget(module)
        sublayout = Q.QHBoxLayout(env)
        sublayout.setContentsMargins(40, 0, 0, 0)
        self._makeenv = Q.QCheckBox(
            translate("ParametricPanel",
                      "only generate the execution directory, do not start"))
        sublayout.addWidget(self._makeenv)
        layout.addWidget(env)
        main.addWidget(module)

        self._tbox = Q.QToolBox(self)
        main.addWidget(self._tbox)

        wid = Q.QWidget(self)
        wid.setSizePolicy(Q.QSizePolicy.Expanding, Q.QSizePolicy.Expanding)
        layout = Q.QVBoxLayout(wid)
        layout.setContentsMargins(0, 0, 0, 0)
        self._vars = VariablesWidget(wid)
        self._vars.setObjectName("variables_table")
        layout.addWidget(self._vars)
        self._group["vars"] = self._tbox.count()
        self._tbox.addItem(wid, self._titles["vars"])

        wid = Q.QWidget(self)
        layout = Q.QGridLayout(wid)
        self._cmd_label = Q.QLabel(
            translate("ParametricPanel", "Select the numpy file"), wid)
        layout.addWidget(self._cmd_label, 0, 0)
        self._cmd_path = Q.QComboBox(wid)
        self._cmd_path.setObjectName("output_npy")
        layout.addWidget(self._cmd_path, 0, 1)
        self._cmd_info = Q.QLabel("", wid)
        layout.addWidget(self._cmd_info, 1, 0, 1, 2)
        self._group["npy"] = self._tbox.count()
        self._tbox.addItem(wid, self._titles["npy"])

        wid = Q.QWidget(self)
        layout = Q.QGridLayout(wid)
        self._obs_label = Q.QLabel(
            translate("ParametricPanel", "Select a text or numpy file"), wid)
        layout.addWidget(self._obs_label, 0, 0)
        self._obs_select = translate("ParametricPanel",
                                     "Select a CSV, Text or numpy file...")
        self._obs = Q.QPushButton(self._obs_select, wid)
        # self._obs.setStyleSheet("text-align:left;")
        layout.addWidget(self._obs, 0, 1)
        self._obs_info = Q.QLabel("", wid)
        layout.addWidget(self._obs_info, 1, 0, 1, 2)
        layout.setRowStretch(2, 0)
        self._group["obs"] = self._tbox.count()
        self._tbox.addItem(wid, self._titles["obs"])

        self._status = Q.QLabel("", self)
        self._status.setWordWrap(True)
        main.addWidget(self._status)

        for idx, wid in self._cbox.items():
            wid.stateChanged.connect(partial(self.toggleModule, idx))
        self._makeenv.stateChanged.connect(self.toggleMakeEnv)
        self._vars.setModel(self._model)
        self._model.dataChanged.connect(self._dataChanged)
        self._obs.clicked.connect(self.browseObsFile)
        self._cmd_path.setModel(self._model)
        self._cmd_path.setRootModelIndex(self._model.commands.index())
        self._cmd_path.currentIndexChanged.connect(self.selectNpyFile)

    def _dataChanged(self, topleft, bottomright):
        """Called on model changes"""
        # for widgets that don't use model/view pattern
        if topleft.parent() == self._model.param.index():
            for row in range(topleft.row(), bottomright.row() + 1):
                data = self._model.param.child(row).data(Q.Qt.UserRole)
                if row == self._model.Key2Row['job_type']:
                    if data in self._cbox:
                        self._cbox[data].setChecked(True)
                    self.useParametric.emit(data)
                    pers = self._cbox[Persalys].isChecked()
                    adao = self._cbox[Adao].isChecked()
                    self._vars.setEnabledBounds(adao)
                    self._tbox.setItemEnabled(self._group["vars"], pers
                                              or adao)
                    self._tbox.setItemEnabled(self._group["npy"], pers or adao)
                    self._cmd_label.setEnabled(pers or adao)
                    self._cmd_path.setEnabled(pers or adao)
                    self._tbox.setItemEnabled(self._group["obs"], adao)
                    self._obs_label.setEnabled(adao)
                    self._obs.setEnabled(adao)
                    self._makeenv.setEnabled(adao)
                    self._beta.setEnabled(adao)
                    self.check_consistency()
                elif row == self._model.Key2Row['make_env']:
                    self._makeenv.setChecked(data)
                elif row == self._model.Key2Row['observations_path']:
                    self._obs.setText(osp.basename(data)
                                      if data else self._obs_select)
                    self.check_obs_array()
                elif row in (self._model.Key2Row['npypath'],
                             self._model.Key2Row['output_vars']):
                    self.check_result_array()
                elif row in (self._model.Key2Row['obsshape'],
                             self._model.Key2Row['npyshape']):
                    self.check_consistency()
                elif row == self._model.Key2Row['output_unit']:
                    selected = self._model.findCommand(data)
                    if selected < 0 and self._model.commands.rowCount() == 1:
                        selected = 0
                    self._cmd_path.setCurrentIndex(selected)
        self.updateState()

    def updateState(self):
        """Update widget state"""
        self._status.setText("")
        if not self._model.get_param("job_type"):
            warn = translate("ParametricPanel",
                             "please check Persalys or Adao box")
            self._status.setText(format_msg(warn, "warning"))
            return True
        error = []
        used = self._model.used_variables()
        if self._model.table.rowCount() == 0:
            error.append(
                translate(
                    "ParametricPanel", "No Python variable found. "
                    "You must add one or more Python variables "
                    "in a graphical stage of your study."))
        if not used:
            error.append(
                translate("RunPanelModel",
                          "please select at least a variable"))
        if self._cmd_path.count() == 0:
            error.append(
                translate(
                    "ParametricPanel", "No output command found. "
                    "You must add a <i>IMPR_TABLE</i> command "
                    "using <i>FORMAT='NUMPY'</i> and <i>NOM_PARA</i> "
                    "to define an explicit list of output parameters."))
        if not self._model.get_param("output_unit"):
            error.append(
                translate("RunPanelModel", "please select the numpy file "
                          "from IMPR_TABLE"))
        if not self._model.get_param("output_vars"):
            error.append(
                translate(
                    "RunPanelModel",
                    "NOM_PARA not defined in the selected IMPR_TABLE command"))
        if self._cbox[Adao].isChecked():
            if not self._model.get_param('observations_path'):
                error.append(
                    translate("RunPanelModel",
                              "please select the observations file"))
            if None in [info[1] for info in used]:
                error.append(
                    translate(
                        "RunPanelModel",
                        "please define the initial value of each variable"))
            if not self._model.get_param("okshape"):
                warn = translate(
                    "ParametricPanel",
                    "Observations file and code_aster numpy file<br/>"
                    "must have the same shape!")
                self._status.setText(format_msg(warn, "warning"))
            else:
                warn = translate(
                    "ParametricPanel",
                    "Warning: Selected resources (time limit, number of "
                    "processors) are used for the global optimization "
                    "analysis and for each evaluation!")
                self._status.setText(format_msg(warn, "warning"))
        debug_message("\n".join(error))
        if error:
            self._status.setText(format_msg(error[0], "error"))
        return not error

    def refresh(self):
        """Refresh state after model reset.

        For parameters that are only set by a widget selection."""
        self.selectNpyFile()

    def toggleModule(self, idx, state):
        """Called on selection of the destination module"""
        if state != Q.Qt.Unchecked:
            # uncheck other: exclusive + allow no selection
            for but in self._cbox:
                if but != idx:
                    self._cbox[but].setChecked(False)
            self._model.set_param('job_type', idx)
        else:
            self._model.set_param('job_type', JobInfos.Aster)

    def toggleMakeEnv(self, state):
        """Called state of make_env changed"""
        self._model.set_param('make_env', state != Q.Qt.Unchecked)

    def browseObsFile(self):
        """Select Observations file."""
        dlg = Q.QFileDialog(self)
        dlg.setWindowTitle(self._obs.text())
        dlg.setFileMode(Q.QFileDialog.ExistingFile)
        filters = [
            'CSV (*.csv)', 'Text (*.txt)', 'numpy (*.npy)', 'All files (*)'
        ]
        dlg.setNameFilters(filters)
        path = dlg.selectedFiles()[0] if dlg.exec_() else None
        if path:
            self._model.set_param('observations_path', path)

    def selectNpyFile(self):
        """Selected numpy file changed"""
        data = self._cmd_path.currentData()
        if not data:
            self._model.set_param('output_unit', 0)
        else:
            self._model.set_param('npypath', data[0])
            self._model.set_param('output_unit', data[1])
            self._model.set_param('output_vars', data[2])

    @staticmethod
    def _check_array(path):
        shape = None
        nbr, nbc = 0, 0
        try:
            shape = numpy.loadtxt(path).shape
        except Exception:  # pragma pylint: disable=broad-except
            debug_message("with numpy.loadtxt:", traceback.format_exc())
            try:
                shape = numpy.load(path).shape
            except Exception:  # pragma pylint: disable=broad-except
                debug_message("with numpy.load:", traceback.format_exc())
        if shape is not None:
            nbr, nbc = (shape if len(shape) == 2 else
                        (1, shape[0]) if len(shape) == 1 else
                        (1, 1))
        return nbr, nbc

    def check_result_array(self):
        """Check the output *numpy* file"""
        nbr, nbc = 0, 0
        npy_path = self._model.get_param('npypath')
        if not npy_path:
            npy_cmt = translate("ParametricPanel", "no file selected")
        elif not osp.exists(npy_path):
            npy_cmt = translate(
                "ParametricPanel", "the file does not exist, can not check the "
                "array shape")
        else:
            npy_cmt = ""
            para = self._model.get_param('output_vars')
            nbr, nbc = self._check_array(npy_path)
            if nbr * nbc:
                npy_cmt = (translate("ParametricPanel",
                                     "{0} rows, {1} columns {2}").format(
                                         nbr, nbc, repr(para)))
                if para and nbc != len(para):
                    npy_cmt += "<br/>" + translate(
                        "ParametricPanel",
                        "The existing file seems inconsistent with current "
                        "NOM_PARA keyword.")
        self._model.set_param('npyshape', (nbr, nbc))
        self._cmd_info.setText(format_msg(npy_cmt, "info"))

    def check_obs_array(self):
        """Check the observations *numpy* file"""
        nbr, nbc = 0, 0
        obs_path = self._model.get_param('observations_path')
        if not obs_path:
            obs_cmt = translate("ParametricPanel", "no file selected")
        else:
            nbr, nbc = self._check_array(obs_path)
            if nbr * nbc:
                obs_cmt = translate("ParametricPanel",
                                    "{0} rows, {1} columns").format(nbr, nbc)
            else:
                self._model.set_param('observations_path', None)
                obs_cmt = translate("ParametricPanel",
                                    "can not read file content")
        self._model.set_param('obsshape', (nbr, nbc))
        self._obs_info.setText(format_msg(obs_cmt, "info"))

    def check_consistency(self):
        """Check the shapes consistency between two arrays"""
        nbr1, nbc1 = self._model.get_param("npyshape") or (0, 0)
        nbr2, nbc2 = self._model.get_param("obsshape") or (0, 0)
        if self._model.get_param('job_type') == Persalys:
            nbr2 = 0
        ok = True
        if nbr1 * nbc1 * nbr2 * nbc2 and (nbr1, nbc1) != (nbr2, nbc2):
            ok = False
        self._model.set_param("okshape", ok)


class DoubleOrEmptyValidator(Q.QValidator):
    """Validator that accepts double numbers or an empty string"""

    @staticmethod
    def validate(input, pos):
        """Check input value"""
        if not str(input).strip():
            return (Q.QValidator.Acceptable, str(input), pos)

        state = Q.QValidator.Acceptable
        try:
            input = input.replace(",", ".")
            str(float(input))
        except ValueError:
            state = Q.QValidator.Intermediate
        return (state, str(input), pos)


class VariablesItemDelegate(Q.QStyledItemDelegate):
    """Item delegate for DA Table view"""

    def __init__(self, parent):
        self._valid = DoubleOrEmptyValidator()
        super().__init__(parent)

    def createEditor(self, parent, _option, index):
        """Create cell editor"""
        if index.column() == 0:
            return None
        editor = Q.QLineEdit(parent)
        editor.setValidator(self._valid)
        return editor

    @staticmethod
    def setEditorData(editor, index):
        """Adjust input value"""
        value = index.model().data(index, Q.Qt.DisplayRole)
        if value:
            # pragma pylint: disable=eval-used
            editor.setText(str(eval(value) * 1.0))

    def setModelData(self, editor, model, index):
        """Store value to model"""
        value = editor.text().strip()
        if not value:
            value = None
        model.setData(index, value)
        super().setModelData(editor, model, index)


class VariablesTableView(Q.QTableView):
    """DA Table view"""

    def __init__(self, parent):
        super().__init__(parent=parent)

    def keyPressEvent(self, event):
        """Support deletion of values by <Delete> key"""
        if event.key() == Q.Qt.Key_Delete:
            for idx in self.selectedIndexes():
                if idx.column() == 0:
                    continue
                self.model().setData(idx, None)
        else:
            super().keyPressEvent(event)


class VariablesWidget(Q.QWidget):
    """Widget for DA datas"""

    def __init__(self, parent=None):
        super().__init__(parent=parent)

        table = VariablesTableView(self)
        self._nomi = translate("ParametricPanel", 'Nominal value')
        self._init = translate("ParametricPanel", 'Initial value')
        table.verticalHeader().setVisible(False)
        table.setItemDelegate(VariablesItemDelegate(table))
        layout = Q.QVBoxLayout(self)
        layout.addWidget(table)
        layout.setContentsMargins(0, 0, 0, 0)
        self._table = table
        self._model = None
        self.setLayout(layout)

    def setModel(self, model):
        """Assign the support model"""
        model.setHorizontalHeaderLabels([
            translate("ParametricPanel", 'Variable'), self._nomi,
            translate("ParametricPanel", 'Min'),
            translate("ParametricPanel", 'Max')
        ])
        self._table.setModel(model)
        self._table.setRootIndex(model.table.index())
        self._table.horizontalHeader().setSectionResizeMode(Q.QHeaderView.Stretch)
        self._model = model

    def setEnabledBounds(self, enabled):
        """Enable or disable bounds columns"""

        def _setFlags(item):
            if not item:
                return
            flags = item.flags()
            changes = (Q.Qt.ItemIsEnabled | Q.Qt.ItemIsEditable
                       | Q.Qt.ItemIsSelectable)
            if enabled:
                flags = flags | changes
            else:
                flags = flags ^ changes
            item.setFlags(flags)

        self._table.clearSelection()
        self._model.setHeaderData(1, Q.Qt.Horizontal,
                                  self._init if enabled else self._nomi)
        tab = self._model.table
        for j in range(2, 4):
            for i in range(tab.rowCount()):
                _setFlags(tab.child(i, j))


def format_msg(txt, style="normal"):
    """Format a message according to the chosen style"""
    fmt = {
        "normal":
        "<p align='center'>{0}</p>",
        "info":
        "<p align='center'><i>{0}</i></p>",
        "warning":
        "<p align='center'><i><b>{0}</b></i></p>",
        "error": ("<p align='center'><font color='#ff0000'>"
                  "<i><b>{0}</b></i></font></p>"),
    }
    return fmt.get(style, fmt["normal"]).format(txt)
