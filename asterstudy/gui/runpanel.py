# -*- coding: utf-8 -*-

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""
Run panel
---------

The module implements panel with run parameters for AsterStudy GUI.
See `RunPanel` class for more details.

"""


import os.path as osp
from functools import partial

from PyQt5 import Qt as Q

from ..common import (hms2s, is_localhost, load_icon, secs2hms, translate,
                      wait_cursor)
from ..datamodel.engine import (nearest_versions, serverinfos_factory,
                                version_ismpi)
from ..datamodel.job_informations import JobInfos
from . import Context, Entity, NodeType, check_selection, get_node_type
from .parametricpanel import ParametricPanel
from .runpanel_model import RunPanelModel
from .widgets import MessageBox, TitleWidget

# note: the following pragma is added to prevent pylint complaining
#       about functions that follow Qt naming conventions;
#       it should go after all global functions
# pragma pylint: disable=invalid-name

# pragma pylint: disable=too-many-instance-attributes
class RunPanel(Q.QWidget):
    """
    Panel with run parameters.
    """
    runPanelEnabled = Q.pyqtSignal()
    parametersChanged = Q.pyqtSignal()
    useParametric = Q.pyqtSignal(int)

    class TimeEdit(Q.QWidget):
        """
        Class that represents the input time field as tree
        separate parts - hours, minutes and seconds
        """
        valueChanged = Q.pyqtSignal(int)

        def __init__(self, parent=None):
            super().__init__(parent)

            base = Q.QHBoxLayout(self)
            base.setContentsMargins(0, 0, 0, 0)

            self._hour = Q.QSpinBox(self)
            self._hour.setMinimum(0)
            self._hour.setMaximum(10000)
            self._hour.setSizePolicy(Q.QSizePolicy(Q.QSizePolicy.Expanding,
                                                   Q.QSizePolicy.Preferred))
            base.addWidget(self._hour)

            base.addWidget(Q.QLabel(':', self))

            self._minute = Q.QSpinBox(self)
            self._minute.setMinimum(0)
            self._minute.setMaximum(59)
            self._minute.setSizePolicy(Q.QSizePolicy(Q.QSizePolicy.Expanding,
                                                     Q.QSizePolicy.Preferred))
            base.addWidget(self._minute)

            base.addWidget(Q.QLabel(':', self))

            self._second = Q.QSpinBox(self)
            self._second.setMinimum(0)
            self._second.setMaximum(59)
            self._second.setSizePolicy(Q.QSizePolicy(Q.QSizePolicy.Expanding,
                                                     Q.QSizePolicy.Preferred))
            base.addWidget(self._second)
            self._hour.valueChanged.connect(self.valueChanged)
            self._minute.valueChanged.connect(self.valueChanged)
            self._second.valueChanged.connect(self.valueChanged)

        def time(self):
            """
            Gets the current time

            Returns:
                str: String in format hh:mm:ss
            """
            return "{0:02d}:{1:02d}:{2:02d}".format(self._hour.value(),
                                                    self._minute.value(),
                                                    self._second.value())

        def setTime(self, timestr):
            """
            Sets the time into edit

            Arguments:
                timestr (str): String with time in format [[hh:]mm:]ss
            """
            if not timestr:
                return
            times = secs2hms(hms2s(timestr)).split(':')
            self._hour.setValue(int(times[0]))
            self._minute.setValue(int(times[1]))
            self._second.setValue(int(times[2]))

        def isReadOnly(self):
            """
            Gets the read only state
            """
            return self._hour.isReadOnly() and self._minute.isReadOnly() and \
                self._second.isReadOnly()

        def setReadOnly(self, state):
            """
            Sets the read only state
            """
            self._hour.setReadOnly(state)
            self._minute.setReadOnly(state)
            self._second.setReadOnly(state)


    class PathEdit(Q.QWidget):
        """
        Class that represents the input path field with browse button.
        """
        pathChanged = Q.pyqtSignal(str)

        def __init__(self, parent=None):
            super().__init__(parent)

            self._path = Q.QLineEdit(self)
            self._path.setObjectName('RunPanel.path')
            self._path.setCompleter(Q.QCompleter(Q.QDirModel(self._path),
                                                 self._path))
            self._path.textChanged.connect(self.pathChanged)

            self._browse = Q.QToolButton(self)
            self._browse.setText("...")
            self._browse.clicked.connect(self._browseClicked)

            base = Q.QHBoxLayout(self)
            base.setContentsMargins(0, 0, 0, 0)
            base.addWidget(self._path)
            base.addWidget(self._browse)

        def path(self):
            """
            Gets the current path

            Returns:
                str: Path string
            """
            return self._path.text()

        def setPath(self, value):
            """
            Sets the current path

            Arguments:
                value (str): Path string
            """
            self._path.setText(value)

        def isReadOnly(self):
            """
            Gets the read only state.
            """
            return self._path.isReadOnly()

        def setReadOnly(self, state):
            """
            Sets the read only state.
            """
            self._path.setReadOnly(state)

        def _browseClicked(self):
            """
            Invoked when 'Browse' button clicked.
            """
            if not self.isReadOnly():
                path = self.path()
                caption = translate("RunPanel", "Directory")
                sel_path = Q.QFileDialog.getExistingDirectory(self,
                                                              caption, path)
                if sel_path:
                    self.setPath(sel_path)


    class RemotePathEdit(PathEdit):
        """
        Input path field with browse button and checkbox.
        """
        remoteStateChanged = Q.pyqtSignal(bool)

        def __init__(self, parent=None):
            """
            Calls parent initializer and inserts the checkbox.
            """
            super().__init__(parent)

            self._checkbox = Q.QCheckBox(self)
            self._checkbox.stateChanged.connect(self._checkboxStateChanged)

            base = self.layout()
            base.insertWidget(0, self._checkbox)

            # Waiting for a portable option to browse remote files,
            # Hide the browse button
            base.removeWidget(self._browse)
            self._browse.hide()

            self._checkbox.stateChanged.emit(self._checkbox.checkState())

        def state(self):
            """
            State of the checkbox.
            """
            return self._checkbox.checkState() == Q.Qt.Checked

        def setEnabled(self, onbox, onpath):
            """"Set enabled state for each widget."""
            if not onbox:
                self.setChecked(False)
            self._checkbox.setEnabled(onbox)
            self._path.setEnabled(onpath)

        def setChecked(self, checked):
            """"Set the boolean value for remote databases."""
            self._checkbox.setChecked(checked)

        def _checkboxStateChanged(self, state):
            """
            Called when checkbox is ticked or unticked.

            Note:
                Ticking enables text edit and browsing.
                Wrapper that emits `remoteStateChanged`.
            """
            bool_state = (state == Q.Qt.Checked)
            self._path.setEnabled(bool_state)
            self.remoteStateChanged.emit(bool_state)


    class TextEdit(Q.QTextEdit):
        """A TextEdit widget that emits *editingFinished* on focus out."""

        editingFinished = Q.pyqtSignal()

        def focusOutEvent(self, event):
            """On focus out."""
            self.editingFinished.emit()
            super().focusOutEvent(event)


    def __init__(self, astergui, parent=None, server_infos=None):
        super().__init__(parent)
        self._astergui = astergui
        self._case = None
        # use not to update case.job_infos until model is completely set
        self._model_set = False
        self._last_server = ''
        self._model = RunPanelModel(parent=self)

        base = Q.QVBoxLayout(self)
        base.setContentsMargins(0, 0, 0, 0)
        base.setSpacing(0)
        base.addWidget(TitleWidget(translate("RunPanel", "Run parameters"), self))

        self.tab_widget = Q.QTabWidget(self)
        base.addWidget(self.tab_widget)

        basic_tab = Q.QWidget(self)
        basic_grid = Q.QGridLayout(basic_tab)
        advan_tab = Q.QWidget(self)
        advan_grid = Q.QGridLayout(advan_tab)
        self.param_tab = ParametricPanel(self._model)
        self.param_tab.useParametric.connect(self.useParametric)

        # interface to server informations
        self._infos = server_infos or serverinfos_factory()

        # Basic tab
        self._getter = {}
        self._setter = {}

        # Run case name
        self._casename = Q.QLineEdit(self)
        self._addWidget(basic_grid, "casename",
                        translate("RunPanel", "Case name"), self._casename)

        # Memory
        self._memory = Q.QLineEdit(self)
        self._memory.setValidator(Q.QIntValidator(self._memory))
        self._memory.validator().setBottom(0)
        self._addWidget(basic_grid, "memory",
                        translate("RunPanel", "Memory"), self._memory)

        # Time
        self._time = RunPanel.TimeEdit(self)
        self._addWidget(basic_grid, "time",
                        translate("RunPanel", "Time"), self._time)

        # Server
        self._server = Q.QComboBox(self)
        self._server.addItems(self._infos.available_servers)
        updbutton = Q.QToolButton(self)
        updbutton.setIcon(load_icon("as_pic_update.png"))

        srvcont = Q.QWidget(self)
        srvbase = Q.QHBoxLayout(srvcont)
        srvbase.setContentsMargins(0, 0, 0, 0)
        srvbase.addWidget(self._server)
        srvbase.addWidget(updbutton)

        self._addWidget(basic_grid, "server",
                        translate("RunPanel", "Run servers"), srvcont,
                        widget=self._server)

        # code_aster version
        self._history_version = None
        self._version = Q.QComboBox(self)
        self._addWidget(basic_grid, "version",
                        translate("RunPanel",
                                  "Version of code_aster"), self._version,
                        self.codeAsterVersion, self.setCodeAsterVersion)

        # MPI CPU
        self._mpicpu = Q.QLineEdit(self)
        self._mpicpu.setValidator(Q.QIntValidator(1, 8192, self._mpicpu))
        self._addWidget(basic_grid, "mpicpu",
                        translate("RunPanel",
                                  "Number of MPI CPU"), self._mpicpu)

        # User description
        self._descr = RunPanel.TextEdit(self)
        self._descr.setAcceptRichText(False)
        self._addWidget(basic_grid, "description",
                        translate("RunPanel",
                                  "User description"), self._descr)

        # Advanced tab

        # Run mode
        self._mode = Q.QComboBox(self)
        self._addWidget(advan_grid, "mode",
                        translate("RunPanel", "Run mode"), self._mode,
                        self.mode, self.setMode)

        # Exec mode
        self._execmode = Q.QComboBox(self)
        self._addWidget(advan_grid, "execmode",
                        translate("RunPanel", "Execution mode"),
                        self._execmode)

        # History folder
        self._folder = RunPanel.PathEdit(self)
        self._addWidget(advan_grid, "folder",
                        translate("RunPanel",
                                  "History folder"), self._folder)

        # Remote folder (where to store result databases)
        self._remote_folder = RunPanel.RemotePathEdit(self)
        self._addWidget(advan_grid, "remote_folder",
                        translate("RunPanel",
                                  "Remote databases"), self._remote_folder)
        self._remote_folder.remoteStateChanged.connect(self.parametersChanged)
        self._remote_folder.remoteStateChanged.connect(self.remoteFolderChecked)

        # Databases compression
        self._compress = Q.QCheckBox(self)
        self._addWidget(advan_grid, "compress",
                        translate("RunPanel",
                                  "Compress databases"), self._compress)

        # Number of nodes
        self._nodes = Q.QLineEdit(self)
        self._nodes.setValidator(Q.QIntValidator(1, 8192, self._nodes))
        self._addWidget(advan_grid, "nodes",
                        translate("RunPanel",
                                  "Number of MPI nodes"), self._nodes)

        # Number of threads
        self._threads = Q.QLineEdit(self)
        self._threads.setValidator(Q.QIntValidator(0, 8192, self._threads))
        self._addWidget(advan_grid, "threads",
                        translate("RunPanel",
                                  "Number of threads"), self._threads)

        # Server partition
        self._srvpart = Q.QLineEdit(self)
        self._addWidget(advan_grid, "partition",
                        translate("RunPanel", "Server partition"),
                        self._srvpart)

        # Priority QOS
        self._qos = Q.QLineEdit(self)
        self._addWidget(advan_grid, "qos",
                        translate("RunPanel", "QOS"), self._qos)

        # WCKEY
        self._wckey = Q.QLineEdit(self)
        self._addWidget(advan_grid, "wckey",
                        translate("RunPanel", "WcKey"), self._wckey)

        # Command line arguments
        self._args = Q.QLineEdit(self)
        self._addWidget(advan_grid, "args",
                        translate("RunPanel", "Arguments"), self._args)

        # Extra parameters
        self._extra = RunPanel.TextEdit(self)
        self._addWidget(advan_grid, "extra",
                        translate("RunPanel",
                                  "Extra parameters"), self._extra)
        self._extra.setMaximumHeight(48)

        #---------------------------------------------------------------------
        self.tab_widget.addTab(basic_tab, translate("RunPanel", "Basic"))
        self.tab_widget.addTab(advan_tab, translate("RunPanel", "Advanced"))
        self.tab_widget.addTab(self.param_tab,
                               translate("RunPanel", "Parametric"))

        basic_grid.setRowStretch(basic_grid.rowCount(), 1)
        advan_grid.setRowStretch(advan_grid.rowCount(), 1)

        #---------------------------------------------------------------------
        self._model.dataChanged.connect(self._dataChanged)
        self.runPanelEnabled.connect(self._modeChanged)

        updbutton.clicked.connect(self._serverUpdate)
        self._server.activated.connect(self._serverActivated)
        self._version.activated.connect(self._versionActivated)

        self._serverActivated()
        self._versionActivated()

        self.setWindowTitle(translate("RunPanel", "Run parameters"))
        self._updateState()

    def _useParametric(self, value):
        """Emit that the parametric panel is enabled"""
        self.useParametric.emit(value)

    def _setData(self, key):
        """Called when a widget value changed to update the model."""
        if not self._getter[key]:
            return
        value = self._getter[key]()
        # skip if unchanged to avoid recursive calls
        if self._model.get_param(key) != value:
            if key in ("memory", "mpicpu", "nodes", "threads"):
                self._model.set_param(key, int(value))
            else: # direct (text, bool)
                self._model.set_param(key, value)

    def _dataChanged(self, topleft, bottomright):
        """Called on model changes to update widgets"""
        if topleft.parent() == self._model.param.index():
            for row in range(topleft.row(), bottomright.row() + 1):
                data = self._model.param.child(row).data(Q.Qt.UserRole)
                for key, idx in self._model.Key2Row.items():
                    if row == idx and self._setter.get(key):
                        self._setter[key](data)
                        break
        self.parametersChanged.emit()
        self.updateJobInfos()

    def _modeChanged(self):
        """Called when the working mode Case/History changed"""
        curcase = (self._astergui.study().history.current_case
                   if self._astergui.study() is not None else None)
        if curcase:
            self.setCase(curcase, force=True)

    def selection(self):
        """
        Gets the currently selected objects.

        Returns:
            list: List of selected objects: cases.
        """
        case = self.case()
        return [Entity(case.uid, get_node_type(case))] \
            if case is not None else []

    def setSelection(self, objs):
        """
        Sets the selection, i.e. select given objects.
        Other objects will be unselected.

        Arguments:
            objs (list): List of objects that should be selected.
        """
        case = None
        if check_selection(objs, size=1, typeid=NodeType.Case):
            case = self._astergui.study().node(objs[0])

        self.setCase(case)

    def update(self):
        """
        Updates the panel
        """
        study = self._astergui.study()
        hasfolder = bool(study and study.history.folder)

        path = ''
        if hasfolder:
            path = study.history.folder
        else:
            path = study.url()
            if path:
                path = osp.splitext(path)[0] + '_Files'

        self._model.set_param('folder', path)
        self._model.set_param('casename', self._generateName())

        self.setFolderEnabled(False)
        self.setRemoteFolderEnabled(not self.isLocal())
        self.setHistoryVersion(study.history.version_number)

    def case(self):
        """
        Gets the case
        """
        return self._case

    def setCase(self, case, force=False):
        """
        Sets the case
        """
        if force or self._case != case:
            if self._case:
                # save current GUI state
                self._case.job_infos = self._model.export_as_jobinfos()
            self._case = case
            self._model_set = False
            self._model.import_from(case)
            self._model_set = True
            self.param_tab.refresh()
            self._updateState()

    def setReadOnly(self, state):
        """
        Sets the read only state to all controls.
        """
        for index in range(self.tab_widget.count()):
            self.tab_widget.widget(index).setDisabled(state)

    # all setters must accept 'empty' values as initialized by 'JobInfos()'
    def caseName(self):
        """
        Gets the run case name.

        Returns:
            str: Run case name.
        """
        return self._casename.text()

    def setCaseName(self, name):
        """
        Sets the run case name.

        Arguments:
            name (str): Run case name.
        """
        self._casename.setText(name)

    def memory(self):
        """
        Gets the memory quantity.

        Returns:
            int: Memory quantity.
        """
        val = self._memory.text()
        return int(val) if val else 0

    def setMemory(self, mem):
        """
        Sets the memory quantity.

        Arguments:
            mem (int): Memory quantity.
        """
        self._memory.setText(str(int(float(mem))))

    def time(self):
        """
        Gets the running time.

        Returns:
            str: Running time.
        """
        return self._time.time()

    def setTime(self, timeval):
        """
        Sets the running time.

        Arguments:
            timeval (str): Running time.
        """
        self._time.setTime(str(timeval))

    def server(self):
        """
        Gets the currently selected run server.

        Returns:
            str: Run server name.
        """
        return self._server.currentText()

    def setServer(self, serv):
        """
        Sets the current run server.

        Arguments:
            serv (str): Run server name.
        """
        if not serv:
            return
        self._server.setCurrentIndex(self._server.findText(serv))
        self._serverActivated()

    def codeAsterVersion(self):
        """
        Gets the current code_aster version.

        Returns:
            str: code_aster version.
        """
        return self._version.currentData()

    def setCodeAsterVersion(self, ver):
        """
        Sets the current code_aster version.

        Arguments:
            ver (str): code_aster version.
        """
        idx = self._version.findData(ver)
        if idx >= 0:
            self._version.setCurrentIndex(idx)
        self._versionActivated()

    def execMode(self):
        """
        Gets the execution mode.

        Returns:
            str: Debug mode.
        """
        return self._execmode.currentText()

    def setExecMode(self, value):
        """
        Sets the execution mode.

        Arguments:
            value (str): Debug mode.
        """
        self._execmode.setCurrentIndex(self._execmode.findText(value))

    def mode(self):
        """
        Gets the running mode.

        Returns:
            int: Running mode.
        """
        return self._mode.currentData()

    def setMode(self, mode):
        """
        Sets the running mode.

        Arguments:
            mode (str): Running mode as text.
        """
        idx = self._mode.findData(mode)
        if idx >= 0:
            self._mode.setCurrentIndex(idx)

    def mpiCpu(self):
        """
        Gets the number of MPI CPU's.

        Returns:
            int: number of MPI CPU's.
        """
        val = self._mpicpu.text()
        return int(val) if val else 0

    def setMpiCpu(self, cpu):
        """
        Sets the number of MPI CPU's.

        Arguments:
            cpu (int): Number of MPI CPU's.
        """
        self._mpicpu.setText(str(cpu))

    def threads(self):
        """
        Gets the number of threads.

        Returns:
            int: number of threads.
        """
        val = self._threads.text()
        return int(val) if val else 0

    def setThreads(self, num):
        """
        Sets the number of threads.

        Arguments:
            num (int): Number of threads.
        """
        self._threads.setText(str(num))

    def nodes(self):
        """
        Gets the number of nodes.

        Returns:
            int: number of nodes.
        """
        val = self._nodes.text()
        return int(val) if val else 0

    def setNodes(self, num):
        """
        Sets the number of nodes.

        Arguments:
            num (int): Number of nodes.
        """
        self._nodes.setText(str(num))

    def userDescription(self):
        """
        Gets the user description.

        Returns:
            str: User description text.
        """
        return self._descr.toPlainText()

    def setUserDescription(self, txt):
        """
        Sets the user description.

        Arguments:
            txt (str): User description text.
        """
        self._descr.setPlainText(txt)

    def folder(self):
        """
        Gets the history folder.

        Returns:
            str: Folder path string
        """
        return self._folder.path()

    def setFolder(self, path):
        """
        Sets the history folder.

        Arguments:
            path (str): Folder path string
        """
        self._folder.setPath(path)

    def remoteFolder(self):
        """
        Gets the history folder.

        Returns:
            str: Folder path string
        """
        return self._remote_folder.path() if self._remote_folder.state() else ''

    def setRemoteFolder(self, path):
        """
        Sets the history folder.

        Arguments:
            path (str): Folder path string
        """
        self._remote_folder.setPath(path)

    def isFolderEnabled(self):
        """
        Gets the history folder enabled state.

        Returns:
            bool: Enabled state
        """
        return self._folder.isEnabled()

    def setFolderEnabled(self, on):
        """
        Sets the history folder enabled state.

        Arguments:
            on (bool): Enabled state
        """
        return self._folder.setEnabled(on)

    def setRemoteFolderEnabled(self, on):
        """
        Sets the history folder enabled state.

        Arguments:
            on (bool): Enabled state
        """
        study = self._astergui.study()
        hasremote = bool(study and study.history.remote_folder)
        return self._remote_folder.setEnabled(on, on and not hasremote)

    def remoteFolderChecked(self, state):
        """Slot called when the checkbox state is changed.

        Arguments:
            state (bool): New checked state.
        """
        if state:
            self.setRemoteFolderEnabled(state)

    def compression(self):
        """Gets the compression choice."""
        return self._compress.checkState() == Q.Qt.Checked

    def setCompression(self, value):
        """Sets the boolean value for databases compression."""
        self._compress.setChecked(value)

    def setHistoryVersion(self, version):
        """Sets the version name used in history."""
        self._history_version = version

    def serverPartition(self):
        """
        Gets the server partition
        """
        return self._srvpart.text()

    def setServerPartition(self, srv):
        """
        Sets the server partition
        """
        self._srvpart.setText(srv)

    def qos(self):
        """
        Gets the QOS value
        """
        return self._qos.text()

    def setQos(self, qos):
        """
        Sets the QOS value
        """
        self._qos.setText(qos)

    def wckey(self):
        """
        Gets the WcKey value
        """
        return self._wckey.text()

    def setWcKey(self, wckey):
        """
        Sets the WcKey value
        """
        self._wckey.setText(wckey)

    def args(self):
        """
        Gets the command line arguments
        """
        return self._args.text()

    def setArgs(self, args):
        """
        Sets the command line arguments
        """
        self._args.setText(args)

    def extra(self):
        """
        Gets the extra submission parameters.
        """
        return self._extra.toPlainText()

    def setExtra(self, txt):
        """
        Sets the extra submission parameters.

        Arguments:
            txt (str): Extra parameters text.
        """
        self._extra.setPlainText(txt)

    def updateJobInfos(self):
        """Update JobInfos of the current case with the dialogs content."""
        if self._case and self._model_set:
            # save GUI state in job_infos
            self._case.job_infos = self._model.export_as_jobinfos()

    def isLocal(self):
        """
        Returns *True* if the selected server is the local machine.
        """
        return is_localhost(self.server())

    def isValid(self):
        """
        Returns the run parameters validity state
        """
        hasfolder = self.folder() is not None and len(self.folder()) > 0
        hasrfolder = self.remoteFolder() is not None \
                     and len(self.remoteFolder()) > 0
        needsremote = self._remote_folder.state()
        isvalid = hasfolder and (hasrfolder or not needsremote)
        # ensure a version is selected (in case of error when refeshing server)
        return isvalid and self.codeAsterVersion() != ''

    def _updateState(self):
        """
        Updates the internal controls state depending on assigned case
        """
        case = self.case()
        curcase = (self._astergui.study().history.current_case
                   if self._astergui.study() is not None else None)

        if case is not None:
            if case == curcase:
                self._model.set_param('casename', self._generateName())
                self.setReadOnly(False)
                self._updateServerInfo()
            else:
                hasremote = bool(self._model.get_param('remote_folder'))
                self.setRemoteFolderEnabled(hasremote)
                self._remote_folder.setChecked(hasremote)
                self.setReadOnly(True)
        self.setEnabled(case is not None)

    def _generateName(self):
        """
        Generates the new run case name

        Returns:
            str: String with probably run case name
        """
        return self._astergui.study().history.new_run_case_name()

    def _serverUpdate(self):
        """
        Invoked when server update button clicked
        """
        self._updateServerInfo(True)

    def _serverActivated(self):
        """
        Invoked when current server changed
        """
        self._updateServerInfo(False)
        serv = self.server()
        # refresh failed?
        if self._version.count() == 0 and self._last_server != serv:
            msg = translate("RunPanel",
                            "No available versions found on '{0}'.\n"
                            "Previous server is selected: '{1}'.")
            MessageBox.critical(self, "AsterStudy",
                                msg.format(serv, self._last_server))
            self.setServer(self._last_server)
        else:
            self._last_server = serv

    def _updateServerInfo(self, force=False):
        """
        Updates the code aster versions list dependant from current server.
        """
        self._model_set = False
        wait_cursor(True)
        self._infos.refresh_once(self.server(), force)
        wait_cursor(False)
        self._version.clear()
        numbers = self._infos.server_versions(self.server())
        _, sortvers = nearest_versions(self._history_version or (0, 0, 0),
                                       numbers)
        for vers in sortvers:
            num = numbers[vers]
            label = "{0} ({1})".format(vers, num) if num else vers
            self._version.addItem(label, vers)
        if sortvers and self._case:
            # print("nearest versions", sortvers,
            #       "current", self._case.job_infos.get("version"))
            if (not self._case.job_infos.is_defined("version")
                    or self._case.job_infos.get("version") not in sortvers):
                selected = sortvers[0]
            if self._case.job_infos.get("version") in sortvers:
                selected = self._case.job_infos.get("version")
            self._model.set_param('version', selected)

        self._mode.clear()
        for textmode in self._infos.server_modes(self.server()):
            mode = JobInfos.text_to_mode(textmode)
            self._mode.addItem(textmode, mode)
        self.setRemoteFolderEnabled(not self.isLocal())
        # Add default value only if the box is checked
        if self._remote_folder.state() and not self._remote_folder.path():
            user = self._infos.server_username(self.server())
            self._remote_folder.setPath("/scratch/{0}".format(user))

        # # update buttons
        # self.parametersChanged.emit()
        self._model_set = True

    def _versionActivated(self):
        """
        Enables/disables the MPI input fields dependant from current
        code_aster version (fields are enabled when version tagged by '_mpi').
        """
        ismpi = version_ismpi(self.codeAsterVersion())
        self._nodes.setEnabled(ismpi)
        self._mpicpu.setEnabled(ismpi)
        self.setNodes(self._model.get_param("nodes") or 1)
        self.setMpiCpu(self._model.get_param("mpicpu") or 1)
        # execution modes may depend on the version later
        self._execmode.clear()
        self._execmode.addItems(self._infos.exec_modes())

    def _addWidget(self, grid, key, label, edit, getter=None, setter=None,
                   widget=None):
        """Added labeled control into the last grid row.

        Arguments:
            grid (*QGridLayout*): Grid host.
            key (str): Parameter name in model (and widget key).
            edit (*QWidget*): Widget to be added.
            getter (*func*): Function that returns the widget content.
            setter (*func*): Function that sets the widget content.
            widget (*QWidget*): Base widget if *edit* is a container.
        """
        row = grid.rowCount()
        grid.addWidget(Q.QLabel(label, self), row, 0)
        grid.addWidget(edit, row, 1)
        # register widgets getters
        widget = widget or edit
        slot = partial(self._setData, key)
        default_get = default_set = None
        if isinstance(widget, Q.QLineEdit):
            default_get = widget.text
            default_set = lambda x: widget.setText(str(x))
            widget.editingFinished.connect(slot)
        elif isinstance(widget, RunPanel.TextEdit):
            default_get = widget.toPlainText
            default_set = widget.setText
            widget.editingFinished.connect(slot)
        elif isinstance(widget, Q.QComboBox):
            default_get = widget.currentText
            default_set = widget.setCurrentText
            widget.currentIndexChanged.connect(slot)
        elif isinstance(widget, Q.QCheckBox):
            default_get = widget.isChecked
            default_set = lambda x: widget.setChecked(bool(x))
            widget.stateChanged.connect(slot)
        elif isinstance(widget, RunPanel.TimeEdit):
            default_get = widget.time
            default_set = widget.setTime
            widget.valueChanged.connect(slot)
        elif isinstance(widget, (RunPanel.PathEdit, RunPanel.RemotePathEdit)):
            default_get = widget.path
            default_set = widget.setPath
            widget.pathChanged.connect(slot)
        self._getter[key] = getter or default_get
        self._setter[key] = setter or default_set
        assert self._getter[key] and self._setter[key], key

    def _lastStage(self):
        dsbrd = self._astergui.workSpace().view(Context.Dashboard) \
            if self._astergui.workSpace() is not None else None
        return dsbrd.lastStage() if dsbrd is not None else None
