# -*- coding: utf-8 -*-

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Issue #29154 in EDF REX: check for dangerous use of out files"""
import unittest
from hamcrest import *
from testutils import attr

from asterstudy.datamodel.history import History
from asterstudy.datamodel.comm2study import comm2study
from asterstudy.datamodel.file_descriptors import Info

from asterstudy.common import WritingToExistingFileError

def test_issue_29154_1():
    """Check dangerous 'out then out' use of file within graphical stage"""
    hist = History()
    cc = hist.current_case
    st = cc.create_stage('st')
    text = """
mesh=LIRE_MAILLAGE(UNITE=20, FORMAT='MED')

mesh2=CREA_MAILLAGE(MAILLAGE=mesh, LINE_QUAD=_F(TOUT='OUI'))

fonc = DEFI_FONCTION(NOM_PARA='INST', VALE=(0, 10, 1, 20))

IMPR_RESU(FORMAT='MED', UNITE=80, RESU=_F(MAILLAGE=mesh2))

IMPR_FONCTION(COURBE=_F(FONCTION=fonc), UNITE=80)
"""
    comm2study(text, st)
    st['mesh']['UNITE'].value = {20: '/dummy/file/A'}
    st[3]['UNITE'].value = st[4]['UNITE'].value = {80: '/dummy/file/B'}

    # Check that dangerous situation is detected
    assert_that(calling(st.check_reused_out_file_in_cmd).with_args(st[3]), raises(WritingToExistingFileError))
    assert_that(calling(st.check_reused_out_file_in_cmd).with_args(st[4]), raises(WritingToExistingFileError))
    assert_that(calling(st.check_reused_out_file).with_args('/dummy/file/B'), raises(WritingToExistingFileError))
    assert_that(calling(st.check_reused_out_file).with_args(80), raises(WritingToExistingFileError))

def test_issue_29154_2():
    """Check dangerous 'in then out' use of file within graphical stage"""
    hist = History()
    cc = hist.current_case
    st = cc.create_stage('st')
    text = """
mesh=LIRE_MAILLAGE(UNITE=20, FORMAT='MED')

mesh2=CREA_MAILLAGE(MAILLAGE=mesh, LINE_QUAD=_F(TOUT='OUI'))

IMPR_RESU(FORMAT='MED', UNITE=20, RESU=_F(MAILLAGE=mesh2))
"""
    comm2study(text, st)
    st['mesh']['UNITE'].value = st[2]['UNITE'].value = {20: '/dummy/file/A'}

    # Check that dangerous situation is detected
    assert_that(calling(st.check_reused_out_file_in_cmd).with_args(st[0]), raises(WritingToExistingFileError))
    assert_that(calling(st.check_reused_out_file_in_cmd).with_args(st[2]), raises(WritingToExistingFileError))
    assert_that(calling(st.check_reused_out_file).with_args('/dummy/file/A'), raises(WritingToExistingFileError))
    assert_that(calling(st.check_reused_out_file).with_args(20), raises(WritingToExistingFileError))

def test_issue_29154_3():
    """Check dangerous 'out then out' use of file between stages"""

    hist = History()
    cc = hist.current_case
    st1 = cc.create_stage('st1')
    st2 = cc.create_stage('st2')
    text1 = """
mesh=LIRE_MAILLAGE(UNITE=20, FORMAT='MED')

mesh2=CREA_MAILLAGE(MAILLAGE=mesh, LINE_QUAD=_F(TOUT='OUI'))

IMPR_RESU(FORMAT='MED', UNITE=80, RESU=_F(MAILLAGE=mesh2))
"""
    text2 = """
fonc = DEFI_FONCTION(NOM_PARA='INST', VALE=(0, 10, 1, 20))

IMPR_FONCTION(COURBE=_F(FONCTION=fonc), UNITE=80)
"""
    comm2study(text1, st1)
    comm2study(text2, st2)
    st1['mesh']['UNITE'].value = {20: '/dummy/file/A'}
    st1[2]['UNITE'].value = st2[1]['UNITE'].value = {80: '/dummy/file/B'}

    assert_that(calling(st1.check_reused_out_file_in_cmd).with_args(st1[2]), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file_in_cmd).with_args(st2[1]), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args('/dummy/file/B'), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args(80), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args('/dummy/file/B'), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args(80), raises(WritingToExistingFileError))

    # Converting second stage to text, checking if this still works
    st2.use_text_mode()

    assert_that(calling(st1.check_reused_out_file_in_cmd).with_args(st1[2]), raises(WritingToExistingFileError))
    cmd3 = st1('IMPR_RESU')
    storage = {'FORMAT': 'MED', 'UNITE': 80, 'RESU':{'MAILLAGE': st1[1]}}
    assert_that(calling(cmd3.init).with_args(storage, file_check=True), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args('/dummy/file/B'), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args(80), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args('/dummy/file/B'), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args(80), raises(WritingToExistingFileError))

    # Converting first stage to text, checking if this still works
    st1.use_text_mode()

    assert_that(calling(st1.check_reused_out_file).with_args(80), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args('/dummy/file/B'), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args(80), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args('/dummy/file/B'), raises(WritingToExistingFileError))


def test_issue_29154_4():
    """Check dangerous 'in then out' use of file between stages"""
    hist = History()
    cc = hist.current_case
    st1 = cc.create_stage('st1')
    st2 = cc.create_stage('st2')

    text1 = """
mesh=LIRE_MAILLAGE(UNITE=20, FORMAT='MED')

mesh2=CREA_MAILLAGE(MAILLAGE=mesh, LINE_QUAD=_F(TOUT='OUI'))

mesh3=LIRE_MAILLAGE(UNITE=21, FORMAT='MED')
"""
    text2 = """
IMPR_RESU(FORMAT='MED', UNITE=20, RESU=_F(MAILLAGE=mesh2))
"""
    comm2study(text1, st1)
    comm2study(text2, st2)

    st1['mesh']['UNITE'].value = st2[0]['UNITE'].value = {20: '/dummy/file/A'}
    st1['mesh3']['UNITE'].value = {21: '/dummy/file/B'}

    # Check that dangerous situation is detected
    assert_that(calling(st1.check_reused_out_file_in_cmd).with_args(st1[0]), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file_in_cmd).with_args(st2[0]), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args('/dummy/file/A'), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args(20), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args('/dummy/file/A'), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args(20), raises(WritingToExistingFileError))
    st2._handle2info[22] = Info(st2)
    st2._handle2info[22].filename = "/a/name"
    st2.check_reused_out_file('/a/name')

    # Converting second stage to text, checking if this still works
    st2.use_text_mode()
    assert_that(calling(st1.check_reused_out_file_in_cmd).with_args(st1[0]), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args('/dummy/file/A'), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args(20), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args('/dummy/file/A'), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args(20), raises(WritingToExistingFileError))

    # Converting first stage to text, checking if this still works
    st1.use_text_mode()
    assert_that(calling(st1.check_reused_out_file).with_args('/dummy/file/A'), raises(WritingToExistingFileError))
    assert_that(calling(st1.check_reused_out_file).with_args(20), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args('/dummy/file/A'), raises(WritingToExistingFileError))
    assert_that(calling(st2.check_reused_out_file).with_args(20), raises(WritingToExistingFileError))

    # For full coverage
    assert_that(st1.child_info_out(st1.handle2info[21]), none())
    st1.check_reused_out_file('/never/used/before')
    st2._handle2info[23] = Info(st2)
    st2._handle2info[23].filename = "/an/other/name"
    st2.check_reused_out_file('/an/other/name')

def test_issue_31571():
    """Check error on undefined files"""
    try:
        raise WritingToExistingFileError(None)
    except WritingToExistingFileError as err:
        assert str(err), "must not fail"


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
