# coding=utf-8

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for unit model - sequential ones."""


import unittest
from unittest.mock import Mock

import os
import os.path as osp

from PyQt5.Qt import Qt
from PyQt5 import Qt as Q

from asterstudy.gui.unit_model import UnitModel, NoSalomeProxyModel
from asterstudy.gui import Role
from asterstudy.datamodel.comm2study import comm2study
from asterstudy.common import external_files_callback

from hamcrest import *
from testutils import tempdir
import testutils.gui_utils


@tempdir
def test_issue_27535(tmpdir):
    """Test proxy models filtering out Salome objects"""
    from asterstudy.gui.study import Study
    study = Study(None)

    # mock a Salome reference
    inref = "0:1:2:3"

    ext_files = Mock()
    ext_files.files = Mock(return_value=[inref])
    ext_files.filename = Mock(return_value='dummy')
    external_files_callback(ext_files, True)

    # get the Qt model, interface to access data in `datamodel.file_descriptors`
    # this is a Stage hierarchy of tables
    # rows are Info objects, column properties of Info objects
    model = study.dataFilesModel()

    # populate the history with new objects, including files
    history = study.history
    history.folder = tmpdir

    cc = history.current_case
    cc.name = 'cc'
    st1 = cc.create_stage('st1')

    # LIRE_MAILLAGE : IN and UnitMed type     => Salome references should be included
    # LIRE_FONCTION : IN but not UnitMed type => --------------------------- excluded
    # IMPR_RESU     : OUT                     => --------------------------- excluded
    text1 = \
"""
function=LIRE_FONCTION(UNITE=19,
                       NOM_PARA='INST',
                       INDIC_PARA=[1, 1],
                       INDIC_RESU=[1, 2]
                       )

mesh = LIRE_MAILLAGE(UNITE=20, FORMAT='MED')
"""

    comm2study(text1, st1)

    # create a standard file
    infile = osp.join(tmpdir, 'dummy_in.txt')
    open(infile, 'a').close()

    st1['function']['UNITE'].value = {19: infile}
    st1['mesh']['UNITE'].value = {20: inref}

    model.update()

    # create a UnitModel
    file_model = UnitModel(st1)
    proxy_model = NoSalomeProxyModel(file_model)

    # Test data returned by the file model
    assert_that(file_model.rowCount(), equal_to(2))
    assert_that(file_model.columnCount(), equal_to(1))
    in_index = file_model.index(1, 0)
    basename = file_model.data(in_index, Qt.DisplayRole)
    assert_that(basename, equal_to('dummy'))

    # test data returned by the proxy model
    # There should be no reference (only 1 row)
    assert_that(proxy_model.rowCount(), equal_to(1))
    assert_that(proxy_model.columnCount(), equal_to(1))
    in_index = proxy_model.index(0, 0)
    basename = proxy_model.data(in_index, Qt.DisplayRole)
    assert_that(basename, equal_to(osp.basename(infile)))

    # Test helper functions
    ind = proxy_model.findDataHelper(basename, Qt.DisplayRole)
    assert_that(ind, equal_to(0))
    ind = proxy_model.findDataHelper("not_in_model", Qt.DisplayRole)
    assert_that(ind, equal_to(-1))
    bname = proxy_model.itemDataHelper(0, Qt.DisplayRole)
    assert_that(bname, equal_to(basename))
    invalid = proxy_model.itemDataHelper(-1, Qt.DisplayRole)
    assert_that(invalid, is_(None))
    invalid = proxy_model.itemDataHelper(1, Qt.DisplayRole)
    assert_that(invalid, is_(None))

    # Datamodel test of UnitMed type
    from asterstudy.datamodel.catalogs import CATA
    umed = CATA.package('DataStructure').UnitMed
    assert_that(st1['function']['UNITE'].gettype(), is_not(same_instance(umed)))
    assert_that(st1['mesh']['UNITE'].gettype(), same_instance(umed))

    # Test refresh
    proxy_model.refreshFromSource(file_model)
    assert_that(proxy_model.sourceModel(), same_instance(file_model))

    # remove ext_files
    external_files_callback(ext_files, False)


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
