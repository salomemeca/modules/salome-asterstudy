# coding=utf-8

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Script used manually to check installation/configuration."""


import unittest

from asterstudy.common import CFG
from asterstudy.datamodel.catalogs import CATA
from asterstudy.datamodel.history import History
from hamcrest import *


def test_show_versions():
    print()
    versions = CFG.options("Versions")
    print("Versions", versions)
    for vers in versions:
        if vers == "fake":
            continue
        History.reset_catalog()
        history = History(vers)
        print("-", history.version, CATA.version_number)
        print(" ", CATA.version_path(vers))
        print(" ", history.extern_data_path)


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
