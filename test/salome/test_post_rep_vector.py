# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ResultFile, VectorRep
from asterstudy.post.utils import (get_array_range, nb_points_cells,
                                   show_min_max)
from hamcrest import *
from testutils import paraview_post

# def force_print(*args, **kwargs):
#     import sys
#     print(*args, **kwargs, file=sys.__stdout__)

# def assert_that_debug(*args, **kwargs):
#     force_print('Assert > {}'.format(args))
#     return assert_that(*args, **kwargs)


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_vectorrep():
    """
    Test the VectorRep representation.
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, '1d_quad.med')
    # This file contains 1 concept : reslin, and 2 fields
    # SIEF_ELGA (on integration pts), DEPL (on points)
    result = ResultFile(medf)
    result.toggle_quadratic()

    depl_field = result.lookup('reslin').lookup('DEPL')

    #------------------------ VectorRep ----------------------------------------

    # ------------------------------------------
    # VectorRep on a displacement field (DEPL)
    # ------------------------------------------
    rep = VectorRep(depl_field)
    pvs.Render()
    # Note that nbp = 4141 is due to the glyph creation, it is much
    # larger than the 41 points in the mesh
    assert_that(nb_points_cells(rep.source), equal_to((4141, 1763)))
    assert_that(rep.opts['Component'], equal_to('Magnitude'))
    assert_that(rep.opts['MaxArrowSize'], close_to(0.05, 1.e-7))

    # >>> VectorRep should automatically calculate a translational vector
    #     (XXX:TRAN) for the representation, and colors by another calculated
    #     magnitude scalar field (XXX:MAGTRAN), let's do the check
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':MAGTRAN'))
    mag_range = get_array_range(rep.source, rep.array, 0, atype='point')
    assert_that(mag_range[1], close_to(34.0354, 1.e-4))
    assert_that(rep.source.OrientationArray[1],\
                equal_to(depl_field.info['pv-aident']+':TRAN'))

    assert_that(mag_range[0], close_to(rep.opts['ColorBarMin'], 1.e-8))
    assert_that(mag_range[1], close_to(rep.opts['ColorBarMax'], 1.e-8))

    # >>> Let's update the representation coloring by using a non translational component
    rep.update_({'Component': 'DRX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((4141, 1763)))
    # It should be colored by an scalar (extracted) point field
    mag_range = get_array_range(rep.source, rep.array, 'DRX', atype='point')
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']))
    assert_that(mag_range[0], close_to(-47.2336, 1e-4))

    # >>> Let's go back to the magnitude coloring
    rep.update_({'Component': 'Magnitude'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((4141, 1763)))
    # It should be colored by an scalar (extracted) point field
    mag_range = get_array_range(rep.source, rep.array, 'Magnitude', atype='point')
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':MAGTRAN'))
    assert_that(rep.source.OrientationArray[1],\
                equal_to(depl_field.info['pv-aident']+':TRAN'))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
