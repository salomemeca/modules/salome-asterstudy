# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import BaseRep, LocalFrameRep, ResultFile
from asterstudy.post.utils import (get_array_range, nb_points_cells,
                                   show_min_max)
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_localframerep():
    """
    Test the LocalFrameRep representation for med files containing
    cara_elem print outs relative to local frames with 2 options:
    - ELEM (local frame on elements) : <concept>.REPLO_1/2/3
    - ELNO (local frame on each node): RepLocal_X/Y/Z
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, 'local_frames.med')
    # This file must be interpreted as containing 3 concepts:
    # cara_elem / named as 00000005 by code_aster v15
    # RepLocal (Local frame / ELNO with IMPR_CONCEPT)
    # STATIC : result of a static calculation
    result = ResultFile(medf)

    elem_lframe_field = result.lookup('00000005').lookup('.REPLO_1')
    elno_lframe_field = result.lookup('RepLocal').lookup('_X')

    # Check that the fields which are read are indeed ELEM and ELNO types
    assert_that(elem_lframe_field.info['support'], equal_to('CELLS'))
    assert_that(elno_lframe_field.info['support'], equal_to('POINTS'))
    assert_that(elno_lframe_field.info['disc'], equal_to('GSSNE'))

    # Check that both fields have 'X', 'Y', 'Z' components
    assert_that(elem_lframe_field.info['components'], equal_to(['X', 'Y', 'Z']))
    assert_that(elno_lframe_field.info['components'], equal_to(['X', 'Y', 'Z']))


    #------------------------ LocalFrameRep ---------------------------------------

    # ELEM field
    # ----------

    rep = LocalFrameRep(elem_lframe_field)
    pvs.Render()

    # Check that 3 vector representations were indeed created and are valid
    assert_that(len(rep.axes_reps), equal_to(3))
    assert_that(rep.opts['ScaleFactor'], close_to(0.072, 1e-6))
    assert_that(rep.axes_reps[0].source.ScaleFactor, close_to(0.072, 1e-6))
    assert_that(rep.axes_reps[1].source.ScaleFactor, close_to(0.072, 1e-6))
    assert_that(rep.axes_reps[2].source.ScaleFactor, close_to(0.072, 1e-6))
    assert_that(nb_points_cells(rep.axes_reps[0].source), equal_to((415312, 176816)))
    assert_that(nb_points_cells(rep.axes_reps[1].source), equal_to((415312, 176816)))
    assert_that(nb_points_cells(rep.axes_reps[2].source), equal_to((415312, 176816)))


    # ELNO field
    # ----------
    rep = LocalFrameRep(elno_lframe_field)
    pvs.Render()

    # Check that 3 vector representations were indeed created and are valid
    assert_that(len(rep.axes_reps), equal_to(3))
    assert_that(rep.opts['ScaleFactor'], close_to(0.072, 1e-6))
    assert_that(rep.axes_reps[0].source.ScaleFactor, close_to(0.072, 1e-6))
    assert_that(rep.axes_reps[1].source.ScaleFactor, close_to(0.072, 1e-6))
    assert_that(rep.axes_reps[2].source.ScaleFactor, close_to(0.072, 1e-6))
    assert_that(nb_points_cells(rep.axes_reps[0].source), equal_to((1729120, 736160)))
    assert_that(nb_points_cells(rep.axes_reps[1].source), equal_to((1729120, 736160)))
    assert_that(nb_points_cells(rep.axes_reps[2].source), equal_to((1729120, 736160)))

    # Update the ScaleFactor parameter
    # --------------------------------
    rep.update_({'ScaleFactor': 0.1})
    pvs.Render()
    assert_that(rep.opts['ScaleFactor'], close_to(0.1, 1e-12))
    assert_that(rep.axes_reps[0].source.ScaleFactor, close_to(0.1, 1e-12))
    assert_that(rep.axes_reps[1].source.ScaleFactor, close_to(0.1, 1e-12))
    assert_that(rep.axes_reps[2].source.ScaleFactor, close_to(0.1, 1e-12))
    assert_that(nb_points_cells(rep.axes_reps[0].source), equal_to((1729120, 736160)))
    assert_that(nb_points_cells(rep.axes_reps[1].source), equal_to((1729120, 736160)))
    assert_that(nb_points_cells(rep.axes_reps[2].source), equal_to((1729120, 736160)))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
