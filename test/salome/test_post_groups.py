# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing feature of group filtering"""
import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ColorRep, ContourRep, ResultFile, WarpRep
from asterstudy.post.utils import (get_array_range, nb_points_cells,
                                   show_min_max)
from hamcrest import *
from testutils import paraview_post

# -----------------------------------------------------------------------------
# The following is commented but can be useful for graphical debugging
# by loading salome_meca, opening Paravis module and loading this script
# - Replace assert_that with assert_that_debug
# - Comment the last line of the main function to avoid salome closing
#   at the end of the test
# - Comment the hamcrest and testutils imports
# - Adapt the developper source directory (sys.path, and os.environ)
# -----------------------------------------------------------------------------

# if True:
#     import sys
#     sys.path.append('/home/J64371/dev/asterstudy/src/test')
#     os.environ['ASTERSTUDYDIR'] ='/home/J64371/dev/asterstudy/src/'
#     from hamcrest import *
#     from testutils import paraview_post

# def force_print(*args, **kwargs):
#     import sys
#     print(*args, **kwargs, file=sys.__stdout__)

# def assert_that_debug(*args, **kwargs):
#     force_print('Assert > {}'.format(args))
#     return assert_that(*args, **kwargs)

def select_groups(result, group_names):
    """
    Generates a group selector based on the result file and the given
    group_names
    """
    output = []
    for gr, supp in result.groups:
        if gr in group_names:
            output.append([gr, True, supp])
        else:
            output.append([gr, False, supp])
    return output

@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_rep_groups_filtering():
    """
    Test group-filtered result representations for a typical thermo-mechanical
    analysis with code_aster
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, 'groups.med')
    # This file contains a main concept [COMBI] containing multiple
    # mechanical fields like displacement, stresses, internal forces, etc.

    result = ResultFile(medf)

    # These are the reference groups in the result med file
    ref_groups = [
        ['LEVEL_0', '-30'], ['LEVEL_0_PANEL_1', '-30'],
        ['LEVEL_0_PANEL_1_CONT', '-6'], ['LEVEL_0_PANEL_2', '-31'],
        ['LEVEL_0_PANEL_2_CONT', '-6'], ['LEVEL_0_PANEL_3', '-32'],
        ['LEVEL_0_PANEL_3_CONT', '-9'], ['LEVEL_0_PANEL_4', '-33'],
        ['LEVEL_0_PANEL_4_CONT', '-5'], ['LEVEL_0_SLAB_1', '-38'],
        ['LEVEL_0_SLAB_1_CONT', '-1'], ['LEVEL_1', '-7'],
        ['LEVEL_1_PANEL_5', '-7'], ['LEVEL_1_PANEL_5_CONT', '-7'],
        ['LEVEL_1_PANEL_6', '-10'], ['LEVEL_1_PANEL_6_CONT', '-10'],
        ['LEVEL_1_PANEL_7', '-13'], ['LEVEL_1_PANEL_7_CONT', '-13'],
        ['LEVEL_1_PANEL_8', '-15'], ['LEVEL_1_PANEL_8_CONT', '-15'],
        ['LEVEL_1_SLAB_2', '-7'], ['LEVEL_1_SLAB_2_CONT', '-7'],
        ['LEVEL_2', '-19'], ['LEVEL_2_SLAB_3', '-19'],
        ['LEVEL_2_SLAB_3_CONT', '-19'], ['PANEL_1_OPENING_1', '7'],
        ['PANEL_1_OPENING_1_CONT', '7'], ['PANEL_2_OPENING_1', '9'],
        ['PANEL_2_OPENING_1_CONT', '9'], ['PANEL_3_OPENING_1', '11'],
        ['PANEL_3_OPENING_1_CONT', '11'], ['PANEL_4_OPENING_1', '12'],
        ['PANEL_4_OPENING_1_CONT', '12'], ['PANEL_5_OPENING_1', '15'],
        ['PANEL_5_OPENING_1_CONT', '15'], ['PANEL_6_OPENING_1', '19'],
        ['PANEL_6_OPENING_1_CONT', '19'], ['PANEL_7_OPENING_1', '21'],
        ['PANEL_7_OPENING_1_CONT', '21'], ['PANEL_8_OPENING_1', '17'],
        ['PANEL_8_OPENING_1_CONT', '17'], ['SLAB_3_OPENING_1', '20'],
        ['SLAB_3_OPENING_1_CONT', '20'], ['__ALL_PANELS', '-30'],
        ['__ALL_SLABS', '-38']]

    assert_that(result.groups, equal_to(ref_groups))

    depl_field = result.lookup('COMBI').lookup('DEPL')
    reac_noda_field = result.lookup('COMBI').lookup('REAC_NODA')
    sief_elga_field = result.lookup('COMBI').lookup('SIEF_ELGA')
    ferr_elem_field = result.lookup('COMBI').lookup('FERR_ELEM')
    efge_elno_field = result.lookup('COMBI').lookup('EFGE_ELNO')

    # ----------------------------------------------------------------------
    # Simple coloring of each of the preceding fields on the group LEVEL_1
    # ----------------------------------------------------------------------
    rep = ColorRep(depl_field)
    filter_opts = {'GroupsFilter': True,
                   'GroupsFilterOptions': select_groups(result, 'LEVEL_1')}
    rep = rep.update_(filter_opts)
    pvs.Render()
    rep.update_(filter_opts)
    rep.update_(filter_opts) # Done on purpose twice, for coverage

    assert_that(nb_points_cells(rep.source), equal_to((2277, 2204)))

    rep.animate()
    rep.scene.GoToFirst()

    # >>> ColorRep calculates a translational magnitude to allow easy picking
    #     of the results, even if those are 3D and displacement only has 3 components
    #     Let's do the check
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':MAGTRAN'))

    mag_range1 = get_array_range(rep.source, rep.array, 'Magnitude', atype='point')
    mag_range2 = get_array_range(rep.source, rep.array, -1, atype='point')

    assert_that(mag_range1[0], close_to(0.0, 1e-4))
    assert_that(mag_range1[1], close_to(5.55614e-4, 1.e-7))
    assert_that(mag_range2[1], close_to(5.55614e-4, 1.e-7))

    # >>> Let's update the representation by changing the coloring component
    rep.update_({'Component': 'DX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((2277, 2204)))
    # It should be colored by the original array in the field
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']))

    mag_range = get_array_range(rep.source, rep.array, 'DX', atype='point')
    assert_that(mag_range[0], close_to(-2.568524e-05, 1e-7))
    assert_that(mag_range[1], close_to(2.5685281e-05, 1e-7))

    rep = ColorRep(sief_elga_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((2277, 2204)))
    assert_that(rep.opts['ColorBarMax'], close_to(3.8980e4, 1.))

    rep = ColorRep(efge_elno_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((2277, 2204)))

    rep = ColorRep(reac_noda_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((2277, 2204)))

    rep.animate()

    # --------------------------------------------------
    # Contour representation
    # --------------------------------------------------

    rep = ContourRep(depl_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((887, 882)))
    assert_that(rep.opts['ColorBarMax'], close_to(5.55614e-4, 1.e-7))

    rep.update_({'Component': 'DX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((555, 530)))

    # The following test may cause problems in script mode...
    # -------------------------------------------------------
    rep = ContourRep(sief_elga_field, Component='SIXX')
    pvs.Render()
    nbpts, nbcells = nb_points_cells(rep.source)
    assert_that(nbpts, greater_than(0))
    assert_that(nbcells, greater_than(0))
    # assert_that(nb_points_cells(rep.source), equal_to((955, 914)))
    '''remove assert check discussion on MR21 asterstudy git
    try :
        assert_that(nb_points_cells(rep.source), equal_to((950, 909)))
    except :
        print(f"Failed to apply assert_that(nb_points_cells(rep.source), equal_to((18071, 3995)))")
        print(f"Try new values for singularity")
        assert_that(nb_points_cells(rep.source), equal_to((955, 914)))
    '''

    rep = ContourRep(efge_elno_field, Component='NXX')
    pvs.Render()
    nbpts, nbcells = nb_points_cells(rep.source)
    assert_that(nbpts, greater_than(0))
    assert_that(nbcells, greater_than(0))
    # assert_that(nb_points_cells(rep.source), equal_to((1116, 1051)))

    rep.animate()

    # --------------------------------------------------
    # Back to unfiltered representation
    # --------------------------------------------------
    rep.update_({'GroupsFilterOptions': []})
    pvs.Render()
    assert_that(rep.opts['GroupsFilter'], equal_to(False))
    assert_that(nb_points_cells(rep.source), equal_to((2079, 1039)))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
