# coding=utf-8

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for unit model."""


import unittest

from PyQt5 import Qt as Q

from asterstudy.gui.unit_model import FileServerModel
from asterstudy.datamodel.engine.salome_runner import has_salome

from hamcrest import *
import testutils.gui_utils


def xtest_issue_27871(server):
    """Test the model for the file server"""
    import os.path
    # There is no need for history
    model = FileServerModel()
    assert_that(server, is_in(model._infos.available_servers))
    assert_that(model.rowCount()%2, equal_to(1))

    idx0 = model.index(0, 0)
    assert_that(model.data(idx0, Q.Qt.DisplayRole), equal_to("localhost"))
    assert_that(model.data(idx0, Q.Qt.ToolTipRole), equal_to(os.path.expanduser("~")))

    # find server in available servers
    assert_that(model._infos.available_servers, has_item(server))
    i_serv = model._infos.available_servers.index(server)
    idx1 = model.index((i_serv-1)*2+1)
    assert_that(model.data(idx1, Q.Qt.DisplayRole), equal_to(server + " (home)"))

    user = model._infos.server_username(server)
    host = model._infos.server_hostname(server)
    assert_that(model.data(idx1, Q.Qt.ToolTipRole), equal_to("sftp://" + user + "@" + host + "/home/" + user + "/dummy"))

    idx2 = model.index(i_serv*2)
    assert_that(model.data(idx2, Q.Qt.DisplayRole), equal_to(server + " (scratch)"))
    assert_that(model.data(idx2, Q.Qt.ToolTipRole), equal_to("sftp://" + user + "@" + host + "/scratch/" + user + "/dummy"))

    theurl = model.data(idx2, Q.Qt.ToolTipRole)

    # For full coverage, test invalid index
    assert_that(model.data(Q.QModelIndex(), Q.Qt.DisplayRole), none())

    assert_that(model.find_server_by_url(theurl), equal_to(server))
    assert_that(model.find_partition_by_url(theurl), equal_to("scratch"))

    assert_that(model.find_server_by_url("/local00/home/NNI"), equal_to("localhost"))
    # partition is None by default for local paths
    assert_that(model.find_partition_by_url("/local00/home/NNI"), none())


@unittest.skipIf(not has_salome(), "salome is required")
def test_issue_27871():
    xtest_issue_27871("gaia")

if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
