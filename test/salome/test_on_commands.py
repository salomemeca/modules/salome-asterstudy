# -*- coding: utf-8 -*-

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for commands that need SALOME."""


import unittest

from hamcrest import *

from asterstudy.common import get_cmd_mesh, is_medfile
from asterstudy.datamodel.history import History
from asterstudy.datamodel.comm2study import comm2study
from asterstudy.datamodel.engine.salome_runner import has_salome


@unittest.skipIf(not has_salome(), "salome is required")
def test_get_cmd_mesh():
    """Test getting meshfile from a mesh command"""

    import os
    hist = History()
    cc = hist.current_case

    st = cc.create_stage('st')
    text1 = """
mesh = LIRE_MAILLAGE(UNITE=20, FORMAT='MED')

mesh2 = LIRE_MAILLAGE(UNITE=38, FORMAT='ASTER')

mesh3 = ASSE_MAILLAGE(MAILLAGE_1=mesh, MAILLAGE_2=mesh2, OPERATION='SUPERPOSE')
"""
    comm2study(text1, st)

    # test standard LIRE_MAILLAGE with MED format
    filename = os.path.join(os.getenv('ASTERSTUDYDIR'),
                            'data', 'export', 'adlv100a.mmed')
    assert_that(is_medfile(filename), equal_to(True))
    st['mesh']['UNITE'].value = {20: filename}

    filepath, medname = get_cmd_mesh(st['mesh'])
    assert_that(os.path.samefile(filename, filepath), equal_to(True))
    assert_that(medname, equal_to("MA"))

    # test a file not in MED format
    filename = os.path.join(os.getenv('ASTERSTUDYDIR'),
                        'data', 'export', 'fileB.38')
    st['mesh2']['UNITE'].value = {38: filename}

    filepath, medname = get_cmd_mesh(st['mesh2'])
    assert_that(filepath, equal_to(None))
    assert_that(medname, equal_to(None))

    # test another command than LIRE_MAILLAGE
    filepath, medname = get_cmd_mesh(st['mesh3'])
    assert_that(filepath, equal_to(None))
    assert_that(medname, equal_to(None))

    # after that the command is deleted
    cmd = st['mesh']
    cmd.delete()
    filepath, medname = get_cmd_mesh(cmd)
    assert_that(filepath, equal_to(None))
    assert_that(medname, equal_to(None))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
