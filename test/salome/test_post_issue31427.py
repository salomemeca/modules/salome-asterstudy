# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic test for handling of code_aster v15 named concepts"""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ColorRep, ResultFile
from asterstudy.post.utils import nb_points_cells
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_code_aster_v15_concept_naming():
    """
    Test the right handling of code_aster v15 named concepts, especially
    for the Calculator filter for translational magnitude.
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, 'issue31427.med')
    # This file contains 1 concept called 0000000e
    # issue31427 shows that Salome Meca V2021.0.0 either
    # - crashes with a segmentation fault upon loading the file
    # - provides no visualisation for the displacement magnitude

    result = ResultFile(medf)

    depl_noeu_field = result.lookup('0000000e').lookup('DEPL')

    rep = ColorRep(depl_noeu_field)
    pvs.Render()

    assert_that(rep.opts['Component'], equal_to('Magnitude'))
    assert_that(rep.opts['ColorBarMax'], close_to(1.414376e-05, 1.e-8))
    assert_that(rep.opts['ColorBarMin'], close_to(0., 1.e-10))
    assert_that(nb_points_cells(rep.source), equal_to((1032, 648)))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
