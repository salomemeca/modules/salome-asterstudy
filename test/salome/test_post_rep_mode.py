# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ModesRep, ResultFile
from asterstudy.post.utils import get_array_range, nb_points_cells
from hamcrest import *
from testutils import paraview_post

# def force_print(*args, **kwargs):
#     import sys
#     print(*args, **kwargs, file=sys.__stdout__)

# def assert_that_debug(*args, **kwargs):
#     force_print('Assert > {}'.format(args))
#     return assert_that(*args, **kwargs)

@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_modesrep():
    """
    Test the ModesRep representation.
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, '1d_quad.med')
    # This file contains 1 concept : reslin, and 2 fields
    # SIEF_ELGA (on integration pts), DEPL (on points)
    result = ResultFile(medf)

    assert_that(result.quadratic, equal_to(1))
    result.toggle_quadratic()

    depl_field = result.lookup('reslin').lookup('DEPL')

    #------------------------ ModesRep ----------------------------------------

    # ------------------------------------------
    # ModesRep on a displacement field (DEPL)
    # ------------------------------------------
    rep = ModesRep(depl_field)
    pvs.Render()

    assert_that(nb_points_cells(rep.source)[0], equal_to(41))
    assert_that(rep.opts['Component'], equal_to('Magnitude'))
    assert_that(rep.opts['ScaleFactor'], close_to(1.46905e-3, 1.e-7))

    # >>> ModesRep should be based on a translational vector (Generate Vectors)
    #     upon loading.
    assert_that(rep.array, contains_string(depl_field.info['pv-aident']))
    assert_that(rep.array, contains_string('_Vector'))
    mag_range = get_array_range(rep.source, rep.array, 'Magnitude', atype='point')
    assert_that(mag_range[0], close_to(rep.opts['ColorBarMin'], 1.e-8))
    assert_that(mag_range[1], close_to(rep.opts['ColorBarMax'], 1.e-8))

    # >>> Let's update the representation coloring by using a non translational component
    rep.update_({'Component': 'DRX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source)[0], equal_to(41))
    # It should be colored by the original array (not the generated vector)
    assert_that(rep.array, contains_string(depl_field.info['pv-aident']))
    assert_that(rep.array, not_(contains_string('_Vector')))

    # >>> Let's go back to the magnitude coloring
    rep.update_({'Component': 'Magnitude'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source)[0], equal_to(41))
    # It should be re-colored by the generated vector (translational) array
    assert_that(rep.array, contains_string(depl_field.info['pv-aident']))
    assert_that(rep.array, contains_string('_Vector'))

    # >>> Let's color a translational component like DY
    rep.update_({'Component': 'DY'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source)[0], equal_to(41))
    # It should be re-colored by the generated vector (translational) array
    assert_that(rep.array, contains_string(depl_field.info['pv-aident']))
    assert_that(rep.array, contains_string('_Vector'))

    # Check that the animation works
    rep.update_({'NbPeriods':3})
    rep.animate()
    assert_that(rep.opts['NbPeriods'], equal_to(3))
    assert_that(rep.scene.Duration, equal_to(rep.opts['NbPeriods']))

    # >>> Create new ModesRep with user set scale factor
    rep = ModesRep(depl_field, ScaleFactor=5.e-3)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    assert_that(rep.opts['ScaleFactor'], close_to(5.e-3, 1.e-7))
    assert_that(rep.opts['ScaleFactorAuto'], equal_to(False))
    rep.update_({'ScaleFactorAuto': True})

    # >>> Create new ModesRep with a scale factor of zero (should be set to auto)
    rep = ModesRep(depl_field, ScaleFactor=0.0)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    assert_that(rep.opts['ScaleFactor'], close_to(1.46905e-3, 1.e-7))
    assert_that(rep.opts['ScaleFactorAuto'], equal_to(True))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
