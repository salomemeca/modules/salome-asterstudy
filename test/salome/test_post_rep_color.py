# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import BaseRep, ColorRep, ResultFile
from asterstudy.post.utils import (get_array_range, nb_points_cells,
                                   show_min_max)
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_colorrep():
    """
    Test the BaseRep and ColorRep representations, including updates
    min_max identifying, slicing, and finally clearing PV pipeline
    and insured coherence with the internal BaseRep sources register.
    Covers around 50% of representation.py
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, '1d_quad.med')
    # This file contains 1 concept : reslin, and 2 fields
    # SIEF_ELGA (on integration pts), DEPL (on points)
    result = ResultFile(medf)

    assert_that(result.quadratic, equal_to(1))
    result.toggle_quadratic()

    sief_elga_field = result.lookup('reslin').lookup('SIEF_ELGA')
    depl_field = result.lookup('reslin').lookup('DEPL')
    #------------------------ ColorRep ----------------------------------------

    # ---------------------------------
    # ColorRep on a point field (DEPL)
    # ---------------------------------
    rep = ColorRep(depl_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))

    # >>> ColorRep should automatically calculate a translational magnitude
    #     and then color by that, let's do the check
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':MAGTRAN'))

    mag_range1 = get_array_range(rep.source, rep.array, 'Magnitude', atype='point')
    mag_range2 = get_array_range(rep.source, rep.array, -1, atype='point')

    assert_that(mag_range1[0], close_to(0.0, 1e-4))
    assert_that(mag_range1[1], close_to(34.0354, 1.e-4))
    assert_that(mag_range2[1], close_to(34.0354, 1.e-4))

    # Check min_max, values and localisation
    min_sphere, min_text, max_sphere, max_text = show_min_max(rep)
    min_center = min_sphere.Center
    assert_that(min_center[0], close_to(0.0, 1e-4))
    assert_that(min_center[1], close_to(0.0, 1e-4))
    assert_that(min_center[2], close_to(0.0, 1e-4))
    assert_that(min_text.Text, contains_string('E-17'))

    max_center = max_sphere.Center
    assert_that(max_center[0], close_to(0.0, 1e-4))
    assert_that(max_center[1], close_to(1.0, 1e-4))
    assert_that(max_center[2], close_to(0.0, 1e-4))
    assert_that(max_text.Text, contains_string('3.403'))

    # >>> Let's update the representation by changing the coloring component
    rep.update_({'Component': 'DZ'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    # It should be colored by the original array in the field
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']))

    mag_range = get_array_range(rep.source, rep.array, 'DZ', atype='point')
    assert_that(mag_range[0], close_to(-34.0353, 1e-4))
    assert_that(mag_range[1], close_to(0.0, 1e-8))

    # >>> Let's update the representation by using a non translational component
    rep.update_({'Component': 'DRX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    # It should be colored by the original array in the field
    mag_range = get_array_range(rep.source, rep.array, 'DRX', atype='point')
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']))
    assert_that(mag_range[0], close_to(-47.2336, 1e-4))

    # ---------------------------------------------
    # ColorRep on an integration point field (ELGA)
    # ---------------------------------------------
    # >>> Define the component from the representation initialisation
    rep = ColorRep(sief_elga_field, Component='SIXY')
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))

    # ELGA fields for 1D fail unless they are represented in their cell averaged form,
    # the color array is automatically adjusted to reflect that, let's do the check
    assert_that(rep.array, equal_to(sief_elga_field.info['pv-aident']+'_avg'))

    mag_range = get_array_range(rep.source, rep.array, 'SIXY', atype='cell')
    assert_that(mag_range[0], close_to(3.4246, 1e-4))
    assert_that(mag_range[1], close_to(6.9217, 1e-4))

    # Check min_max, values and localisation
    min_sphere, min_text, max_sphere, max_text = show_min_max(rep)
    min_center = min_sphere.Center
    assert_that(min_center[0], close_to(0.0, 1e-4))
    assert_that(min_center[1], close_to(0.025, 1e-4))
    assert_that(min_center[2], close_to(0.0, 1e-4))
    assert_that(min_text.Text, contains_string('3.424'))

    max_center = max_sphere.Center
    assert_that(max_center[0], close_to(0.0, 1e-4))
    assert_that(max_center[1], close_to(0.525, 1e-4))
    assert_that(max_center[2], close_to(0.0, 1e-4))
    assert_that(max_text.Text, contains_string('6.921'))

    # Test deleting all items from the paraview proxy, then update
    # the sources in the BaseRep register

    # Before deleting, the sources keys are either the loaded med file
    # or a symbolic string called 'root' for adding the min max locators and texts
    # It is thus of size 2

    # Disabled because it depends on the order of test launching
    # print(BaseRep._sources.keys())
    # assert_that(BaseRep._sources, has_length(2))


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_colorrep_2():
    " Test using the slice with a simple displacement magnitude coloring"
    import pvsimple as pvs
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, '1d_quad.med')

    # Test using the slice with a simple displacement magnitude coloring
    # Note : need to reload the file and create a view because we closed everything
    # by clearing the proxy manager !
    result = ResultFile(medf)
    assert_that(result.quadratic, equal_to(1))
    result.toggle_quadratic()

    depl_field = result.lookup('reslin').lookup('DEPL')

    rep = ColorRep(depl_field, Slice='Normal to y-axis', SlicePosition=0.5)
    pvs.Render()
    # The slice of a 1d segment produces a single point (and a containing cell)
    assert_that(nb_points_cells(rep.source), equal_to((1, 1)))

    # Verify that the slice is centered in the middle of the box and also that the
    # slicing plane has a normal along Y
    orig = rep.source.SliceType.Origin
    normal = rep.source.SliceType.Normal
    assert_that(orig[1], close_to(0.5, 1.e-4))
    assert_that(normal[0], close_to(0., 1.e-8))
    assert_that(normal[1], close_to(1., 1.e-4))
    assert_that(normal[2], close_to(0., 1.e-8))

    # Finally test how the paraview sources base can be cleared
    # from within the baserep register
    slice_source = rep.source
    # Remove the slice, simply by updating the representation
    rep.update_({'Slice': 'None', 'Representation': 'Surface'})
    pvs.Render()

    # The old slice source is still known by paraview. Let's verify
    pm = pvs.servermanager.ProxyManager()
    all_sources = list(pm.GetProxiesInGroup('sources').values())
    assert_that(slice_source, is_in(all_sources))

    # >>> Add some unusable options (for coverage testing)
    rep.update_({'ColorField': depl_field, 'ScaleFactor': 1.0, 'Unit': 'SOME UNIT'})
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))

    # Now ask the BaseRep to clear up all sources
    rep.clear_sources()

    # Check that no more sources are present in the BaseRep registry
    # and that no slice source is to be find in the paraview pipeline
    all_sources = list(pm.GetProxiesInGroup('sources').values())
    assert_that(BaseRep._sources, empty())
    assert_that(slice_source, is_not(is_in(all_sources)))



#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
