# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the MEDReader stability in post-processing"""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ColorRep, ResultFile
from asterstudy.post.utils import nb_points_cells
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_medreader_stability():
    """
    Test MEDReader stability upon selection and deselection of several
    fields from the same concept
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, 'issue29257.med')
    # This file contains 1 concept called MODES
    # issue29257 shows that Salome Meca V2019.0.1 crashes with a
    # malloc error upon visualizing the SIEF_NOEU field followed
    # by visualizing SIPO_NOEU. The paraview mecanism for selecting
    # fields for MEDReader proxies has been identified as unstable/faulty
    # and a new approach has been deployed which limits the number of
    # selection changes of <proxy>.FieldsStatus = [xxx]

    result = ResultFile(medf)

    depl_noeu_field = result.lookup('MODES').lookup('DEPL')
    sief_noeu_field = result.lookup('MODES').lookup('SIEF_NOEU')
    sipo_noeu_field = result.lookup('MODES').lookup('SIPO_NOEU')

    rep = ColorRep(depl_noeu_field)
    pvs.Render()

    # For the sake of testing let's go back on forth 5 times between
    # the incriminated fields
    for i in range(5):
        # ----------------------------------------------------------------------
        # Simple coloring and max. checking for each of the 2 fields
        # ----------------------------------------------------------------------
        rep = ColorRep(sief_noeu_field)
        pvs.Render()
        assert_that(nb_points_cells(rep.source), equal_to((279, 278)))
        assert_that(rep.opts['ColorBarMax'], close_to(1288.31, 1.e0))

        rep = ColorRep(sipo_noeu_field)
        pvs.Render()
        assert_that(nb_points_cells(rep.source), equal_to((279, 278)))
        assert_that(rep.opts['ColorBarMax'], close_to(8.4344e6, 1.e3))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
