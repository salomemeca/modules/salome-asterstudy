# -*- coding: utf-8 -*-

# Copyright 2016-2018 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.
"""
Automatic tests for ParametricPanel
"""

import os
import os.path as osp
import sys
import traceback

import unittest

import numpy
from PyQt5 import Qt as Q

import testutils.gui_utils
from asterstudy.datamodel.comm2study import comm2study
from asterstudy.datamodel.history import History
from asterstudy.datamodel.job_informations import JobInfos
from asterstudy.gui.runpanel_model import RunPanelModel
from asterstudy.gui.runpanel import RunPanel
from testcommon.gui_functions import get_application
from hamcrest import *
from testutils import tempdir


def _setup_zzzz159b(tmpdir):
    history = History()
    if not history.support["parametric"]:
        return None

    history.folder = tmpdir
    history.current_case.create_stage("unused")
    runcase = history.create_run_case(name="RunCase_N")
    runcase.job_infos.set("casename", "RunCase_N")
    runcase.job_infos.set("server", "supercomputer")
    runcase.job_infos.set("version", "custom")

    case = history.current_case
    case.name = 'zzzz159b'

    stage = case.create_stage('zzzz159b')
    cmd_file = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                        'zzzz159b.3')
    with open(cmd_file) as comm:
        comm2study(comm.read(), stage)

    info = stage.handle2info[2]
    info.filename = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                             'zzzz159a.mail')

    info = stage.handle2info[3]
    info.filename = osp.join(tmpdir, 'result_file.npy')

    info = stage.handle2info[8]
    info.filename = osp.join(tmpdir, 'result_file.txt')

    cmd = stage.add_command("IMPR_TABLE")
    cmd.init(
        dict(
            FORMAT='NUMPY',
            NOM_PARA=('COOR_X', 'DZ'),
            #   TABLE=table0,
            UNITE=35))

    info = stage.handle2info[35]
    info.filename = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                             'zzzz159b.npy')
    return case

def _setup_job_infos(case):
    """Add JobInfos to *case*"""
    job_infos = case.job_infos
    job_infos.set_parameters_from({
        "job_type": JobInfos.Aster,
        "assigned": True,
        "jobid": "123456",
        "dump_string": "xml dump",
        "name": "RunCase_17",
        "server": "eole",
        "mode": JobInfos.Batch,
        "start_time": "01:02:03",
        "end_time": "02:03:04",
        "description": "a detailed description",
        "studyid": "123456-host",
        "memory": 2048,
        "time": "00:15:00",
        "version": "stable",
        "mpicpu": 16,
        "nodes": 2,
        "threads": 0,
        "folder": "/path/to/Study_Files",
        "compress": False,
        "partition": "bm",
        "qos": "",
        "args": "",
        "wckey": "P11YB:ASTER",
        "extra": "",
        "input_vars": ['DSDE__', 'SIGY__', 'YOUN__'],
        "input_init": [200., 1., 8.E4],
        "input_bounds": [[None, None], [0., None], [50.e3, 250.e3]],
        "output_unit": 3,
        "output_vars": ['INST', 'DX', 'DY', 'DZ'],
        "observations_path": "/path/to/csv",
    })


@tempdir
def test_runpanel(tmpdir, interactive=False):
    from unittest.mock import MagicMock
    app = get_application()
    case = _setup_zzzz159b(tmpdir)
    if case is None:
        return

    _setup_job_infos(case)
    assert_that(case.job_infos.get("job_type"), equal_to(JobInfos.Aster))
    # init with a command #123 that does not exist anymore
    case.job_infos.set_parameters_from({
        "input_vars": ['YOUN__', 'SIGY__'],
        "output_unit": 1234,
        "output_vars": ['INST', 'DX', 'DZ'],
        "input_init": [8.E4, None],
        "input_bounds": [[None, None], [0., 1000.]],
    })

    widget = Q.QWidget()
    vbox = Q.QVBoxLayout(widget)

    study = MagicMock(name="study")
    study.history = case.model
    astergui = MagicMock(name="astergui")
    astergui.study.return_value = study
    infos = MagicMock(name="server_infos")
    infos.available_servers = ["localhost", "hpc1", "hpc2"]
    infos.server_versions.return_value = {
        "stable": "14.6.0",
        "testing": "15.2.0",
        "DEV_MPI": "15.2.8",
    }
    infos.server_modes.return_value = ["Batch", "Interactive"]
    infos.server_username.return_value = "user"

    panel = RunPanel(astergui, server_infos=infos)
    # use 'case' by clicking on "Select Case" button

    vbox.addWidget(panel)

    # add Ok button
    buttons = Q.QWidget()
    butts = Q.QHBoxLayout(buttons)
    select = Q.QPushButton('Select Case', widget)
    ok = Q.QPushButton('Run', widget)
    butts.addWidget(select)
    butts.addWidget(ok)
    vbox.addWidget(buttons)

    app.main.setCentralWidget(widget)
    app.main.setGeometry(100, 200, 500, 400)

    ok.setEnabled(False)
    cases = study.history.cases[:]
    cases.insert(0, None)
    def loop():
        sel = cases.pop(-1)
        cases.insert(0, sel)
        print("select case:", sel.name if sel else None)
        panel.setCase(sel)

    select.clicked.connect(loop)
    panel.useParametric.connect(ok.setEnabled)

    def _apply():
        print("updateState() returns:", panel.updateState())

    ok.clicked.connect(_apply)

    if interactive:
        print()
        return

    panel.setCase(case)
    parpan = panel.param_tab
    assert_that(parpan.updateState(), equal_to(True))
    assert_that(parpan._status.text(), contains_string("please check"))

    parpan._cbox[JobInfos.Persalys].setChecked(True)
    assert_that(parpan._model.get_param('job_type'),
                equal_to(JobInfos.Persalys))
    assert_that(parpan.updateState(), equal_to(False))
    assert_that(parpan._status.text(), contains_string("file from IMPR_TABLE"))

    parpan._cmd_path.setCurrentIndex(1)
    assert_that(parpan.updateState(), equal_to(True))
    assert_that(parpan._status.text(), empty())

    parpan._cbox[JobInfos.Adao].setChecked(True)
    assert_that(parpan._model.get_param('job_type'),
                equal_to(JobInfos.Adao))
    assert_that(parpan.updateState(), equal_to(False))
    assert_that(parpan._status.text(), contains_string("observations file"))

    parpan._model.set_param('observations_path', "/path/file.csv")
    assert_that(parpan.updateState(), equal_to(False))
    parpan._model.set_param('observations_path',
                           osp.join(os.getenv('ASTERSTUDYDIR'), 'data',
                                    'export', 'zzzz159b_short.csv'))
    assert_that(parpan.updateState(), equal_to(True))
    assert_that(parpan._status.text(),
                contains_string("must have the same shape"))

    parpan._model.set_param('observations_path',
                           osp.join(os.getenv('ASTERSTUDYDIR'), 'data',
                                    'export', 'zzzz159b.csv'))
    assert_that(parpan.updateState(), equal_to(True))
    assert_that(parpan._status.text(),
                contains_string("Warning: Selected resources"))

    nbr, nbc = parpan._check_array(osp.join(os.getenv('ASTERSTUDYDIR'), 'data',
                                            'export', 'siyy200.csv'))
    assert_that(nbr, equal_to(1))
    assert_that(nbc, equal_to(1))


def show():
    """Execute `check_test.sh test_export_ot.py:show` to test the GUI."""
    try:
        test_runpanel(interactive=True)
    except:
        traceback.print_exc()
    get_application().exec_()


@tempdir
def test_runpanel_model(tmpdir):
    case = _setup_zzzz159b(tmpdir)
    _setup_job_infos(case)

    model = RunPanelModel(None)
    novalues = model.export_as_dict()
    assert_that(novalues, has_length(0))

    model.import_from(case)
    assert_that(model.commands.rowCount(), equal_to(2))
    # 0: UNITE=3 and 1: UNITE=35
    assert_that(model.findCommand(case.job_infos.get("output_unit")), equal_to(0))

    used = model.used_variables()
    assert_that(used, has_length(3))
    chk_vars = []
    chk_init = []
    chk_bounds = []
    for name, init, bounds in used:
        chk_vars.append(name)
        chk_init.append(init)
        chk_bounds.append(bounds)
    assert_that(chk_vars, contains('DSDE__', 'SIGY__', 'YOUN__'))
    assert_that(chk_init, contains(200., 1., 80.e3))
    assert_that(chk_bounds[0], contains(None, None))
    assert_that(chk_bounds[1], contains(0., None))
    assert_that(chk_bounds[2], contains(50.e3, 250.e3))

    exported = model.export_as_jobinfos()
    assert_that(exported == case.job_infos)



if __name__ == "__main__":
    # show()
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
