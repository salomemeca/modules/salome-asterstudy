#!/usr/bin/env python

"""Performance check"""


import os
import sys
import cProfile
import subprocess

from asterstudy.datamodel import *
from hamcrest import *

if sys.version_info >= (3, 3):
    from time import perf_counter as time
else:
    from time import clock as time

from gc import disable as disable_garbage_collection
disable_garbage_collection()

def test_import(basename, tref, nmax):
    history = History()
    case = history.current_case

    filename = os.path.join(os.getenv('ASTERSTUDYDIR'), 'data', 'performance',
                            basename)
    with open(filename) as file:
        text = file.read()

    start_time = time()
    stage = case.create_stage(':memory:')
    comm2study(text, stage)
    elap = time() - start_time
    print('import of {1} = {0:.6f} (secs)'.format(elap, basename))

    nb_parrots = elap / tref
    assert_that(nb_parrots, less_than_or_equal_to(nmax))
    print('number_parrots = {0:.3f} (less than {1})'.format(nb_parrots, nmax))

    return stage


def test_export(basename, stage, tref, nmax, prof=False):
    name = "study2comm"
    start_time = time()
    if prof:
        cProfile.run("study2comm(stage)",
                     filename=name + ".prof")
    else:
        text = study2comm(stage)
        # print text
    elap = time() - start_time
    print('export of {1} = {0:.6f} (secs)'.format(elap, basename))

    nb_parrots = elap / tref
    assert_that(nb_parrots, less_than_or_equal_to(nmax))
    print('number_parrots = {0:.3f} (less than {1})\n'.format(nb_parrots, nmax))

    if prof:
        print("generating dot file...")
        subprocess.call(['gprof2dot.py', '-f', 'pstats', '-o', name + ".dot",
                         name + ".prof"])
        print("generating png file...")
        subprocess.call(['dot', '-T', 'png', '-o', name + ".png",
                         name + ".dot"])

        print("cleaning...")
        os.remove(name + ".prof")
        os.remove(name + ".dot")


def object_creation(n):
    """Create and use 'n' instances."""
    from math import cos, sin
    class Dummy:
        def __init__(self, i):
            self.attr1 = i * (sin(i)**2 + cos(i)**2)
            self.attr2 = n

        def check(self, value):
            assert abs(self.attr1 + self.attr2 - value) < 1.e-6

    for i in range(n):
        obj = Dummy(i)
        obj.check(i + n)


if __name__ == "__main__":
    start_time = time()
    object_creation(35000)
    a_parrot_time = time() - start_time
    print('parrot_time = {0:.6f} (secs)\n'.format(a_parrot_time))

    filename = 'Recette_asterstudy_20161010_vsr.comm'
    stage = test_import(filename, a_parrot_time, nmax=7.0)
    test_export(filename, stage, a_parrot_time, nmax=0.1)

    filename = 'ssnp15a.comm'
    stage = test_import(filename, a_parrot_time, nmax=21)
    test_export(filename, stage, a_parrot_time, nmax=0.4)

    filename = 'sdll23a.comm'
    stage = test_import(filename, a_parrot_time, nmax=27)
    test_export(filename, stage, a_parrot_time, nmax=0.8)

    filename = 'epicu03a.comm'
    stage = test_import(filename, a_parrot_time, nmax=135)
    test_export(filename, stage, a_parrot_time, nmax=1.0)

    filename = 'sdll127b.comm'
    stage = test_import(filename, a_parrot_time, nmax=1300)
    test_export(filename, stage, a_parrot_time, nmax=7.0)
