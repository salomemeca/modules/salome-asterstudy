#------------------------------------------------------------------------------
#  LOT 6:
#  Checks creation of stages in graphical and  text modes, and by import.
#  Checks computation of stages.
#------------------------------------------------------------------------------
import filecmp

global DEBUG_FUNC
DEBUG_FUNC = "" # Set the desirable function name to debug or "ALL" or "" otherwise

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start(debug=True, noexhook=True)

def cleanup():
    global_cleanup()

def newStudy():
    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    createNewStudy()
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #--------------------------------------------------------------------------
def newStage():

    #[section] Stage creation
    test.log( "Stage creation" )

    #[step] Create a new stage by toolbar button
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )
    #[check] 'Stage' was added to tree
    checkOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
def cell(row, col):
    return "{columnIndex='%d' container=':_FunctionTable' rowIndex='%d' type='QExpandingLineEdit' unnamed='1' visible='1'}" % (col, row)

    #--------------------------------------------------------------------------
def saveStage( func_name_to_debug, stage="" ):

    file_name = 'before%s.comm' % ( func_name_to_debug )
    file_path = os.path.join( testdir(), file_name )
    selectItem( stage if stage else "Stage_1")
    clickButton( waitForObject( ":AsterStudy *.Export Command File_QToolButton" ) )
    setParameterValue(":fileNameEdit_QLineEdit", file_path)
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    waitUntilObjectExists( ":QFileDialog.Save_QPushButton" )

    ref_file = os.path.join( casedir(), "ref", file_name )
    test.verify( filecmp.cmp( ref_file, file_path, shallow=False ),
                 "Comparison of reference '%s' and current '%s' stages in text mode." % ( ref_file, file_path ) )

    #--------------------------------------------------------------------------
def loadStage( func_name_to_debug = DEBUG_FUNC ):
    file_path = os.path.join( casedir(), "ref", 'before%s.comm'%func_name_to_debug )
    importStage( file_path, [":Undefined files.OK_QPushButton"] )

    try: clickButton(waitForObject(":AsterStudy.OK_QPushButton", 1000))
    except: pass

    activateOBContextMenuItem("CurrentCase.%s\\" % os.path.splitext(os.path.basename(file_path))[0], "Rename")
    type(waitForObject(":_QExpandingLineEdit"), "Stage_1")
    type(waitForObject(":_QExpandingLineEdit"), "<Return>")

    eval(func_name_to_debug)()

    #--------------------------------------------------------------------------
def runCase():
    #[project] AsterStudy
    #[scenario] LOT6
    #[topic] Acceptance LOT6
    #[tested functionality] Stages creation. Computation.
    #[summary description] Creation of stages in graphical, text modes, and by import.
    #[expected results] No errors. No exceptions. Correct validation.

    newStudy()

    if not DEBUG_FUNC or DEBUG_FUNC == "ALL":
        newStage()
        _1_MAIL()

    else:
        ast_file_path = os.path.join( casedir(), "ref", 'before%s.ajs'%DEBUG_FUNC )
        if not salomeUnderTest() and os.path.exists( ast_file_path ):
            openFile(ast_file_path)
            eval(DEBUG_FUNC)()
        else:
            comm_file_path = os.path.join( casedir(), "ref", 'before%s.comm'%DEBUG_FUNC )
            loadStage() # provide function name to debug

    #--------------------------------------------------------------------------
def _1_MAIL():

    #[section] Creation LIRE_MAILLAGE command named 'MAIL'
    test.log( "Creation LIRE_MAILLAGE command named 'MAIL'" )

    #[step] Select 'Stage' in tree
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #[step] Click 'Mesh' combobox in toolbar and select 'LIRE_MAILLAGE' item
    addCommandByDialog("LIRE_MAILLAGE", "Mesh")

    #[step] Click 'Rename' in context menu
    activateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.LIRE_MAILLAGE", "Rename" )
    #[step] Type 'MAIL' and press <Enter>
    setParameterValue( ":_QExpandingLineEdit", "MAIL" )
    type( waitForObject( ":_QExpandingLineEdit" ), "<Return>" )
    #[check] 'LIRE_MAILLAGE' was renamed to 'MAIL'
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "MAIL" )

    #[step] Click 'Edit' in context menu
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.MAIL", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'LIRE_MAILLAGE'
    test.compare(str(waitForObjectExists(":_ParameterTitle").displayText), "LIRE_MAILLAGE")
    #[check] Command name is 'MAIL'
    test.compare(str(waitForObjectExists(":Name_QLineEdit").displayText), "MAIL")

    #[step] Check 'FORMAT' checkbox
    symbol = tr( 'FORMAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'MED' in 'FORMAT' combobox
    symbol = tr( 'FORMAT', 'QComboBox' )
    mouseClick(waitForObject(symbol), 36, 12, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(symbol, "MED"), 39, 9, 0, Qt.LeftButton)

    #[step] Click '...' browse button
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Open 'lot6.mmed' file
    med_file_path = os.path.join( casedir(), 'lot6.mmed' )
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    #[check] 'lot6.mmed' file name was added to combobox and activated
    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(med_file_path))
    #[step] Check 'NOM_MED' checkbox
    symbol = tr( 'NOM_MED', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'MeshCoude' in 'NOM_MED' combobox
    symbol = tr( 'NOM_MED', 'QComboBox' )
    mouseClick(waitForObject(symbol), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(symbol, "MeshCoude"), 70, 8, 0, Qt.LeftButton)

    #[step] Click OK button to accept the changes
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    saveStage("_2_MAIL")
    _2_MAIL()

    #--------------------------------------------------------------------------
def _2_MAIL():

    #[section] Creation MODI_MAILLAGE command named 'MAIL'
    test.log( "Creation MODI_MAILLAGE command named 'MAIL'" )

    #[step] Click 'Mesh' combobox in toolbar and select 'MODI_MAILLAGE' item
    addCommandByDialog("MODI_MAILLAGE", "Mesh")

    #[step] Double click on 'MODI_MAILLAGE' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Mesh.MODI\\_MAILLAGE", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )
    #[check] Command name is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "MODI_MAILLAGE" )

    #[step] Type 'MAIL' in 'Name' input field instead of 'MODI_MAILLAGE'
    setParameterValue(":Name_QLineEdit", "MAIL")

    #[step] Select 'MAIL' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    setParameterValue(symbol, 'MAIL')

    #[step] Check 'ORIE_PEAU_3D' checkbox
    symbol = tr( 'ORIE_PEAU_3D', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ORIE_PEAU_3D', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_3D" )
    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_3D > [0]" )
    #[step] Click 'Edit...' button of 'GROUP_MA' keyword
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] 'GROUP_MA' panel was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_3D > [0] > GROUP_MA" )
    #[check] 'MAIL' mesh is set in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'MAIL' )

    ##[step] Select 'SURFINT' group in list
    waitForObjectItem(":MESH_QTreeWidget", "2D elements.SURFINT")
    clickItem(":MESH_QTreeWidget", "2D elements.SURFINT", 12, 6, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_3D > [0]" )
    #[check] 'Content label of 'GROUP_MA' keyword was displayed the '(SURFINT)'
    test.compare( waitForObject( ":GROUP_MA_content" ).text, "('SURFINT')" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_3D" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Command's panel was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'MODI_MAILLAGE' was renamed to 'MAIL'
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "MAIL" )

    saveStage("MATER")
    MATER()

    #--------------------------------------------------------------------------
def MATER():

    #[section] Creation DEFI_MATERIAU command named 'MATER'
    test.log( "Creation DEFI_MATERIAU command named 'MATER'" )

    #[step] Add 'DEFI_MATERIAU' command by 'Show All' dialog
    addCommandByDialog("DEFI_MATERIAU", "Material")

    #[step] Double click on 'DEFI_MATERIAU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Material.DEFI\\_MATERIAU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_MATERIAU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )
    #[check] Command name is 'DEFI_MATERIAU'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_MATERIAU" )

    #[step] Type 'MATER' in 'Name' input field instead of 'DEFI_MATERIAU'
    setParameterValue(":Name_QLineEdit", "MATER")

    #[step] Check 'ELAS' checkbox
    symbol = tr( 'ELAS', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ELAS', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU > ELAS" )

    #[step] Type '204000000000.0' in 'E' field
    symbol = tr( 'E', 'QLineEdit' )
    setParameterValue( symbol, '204000000000.0' )
    #[step] Type '0.3' in 'NU' field
    symbol = tr( 'NU', 'QLineEdit' )
    setParameterValue( symbol , '0.3' )
    #[step] Check 'ALPHA' checkbox
    symbol = tr( 'ALPHA', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '1.096e-05' in 'ALPHA' field
    symbol = tr( 'ALPHA', 'QLineEdit' )
    setParameterValue( symbol, '1.096e-05' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )

    #[step] Type 'THER' in search field
    symbol = tr( 'searcher', 'QLineEdit' )
    setParameterValue( symbol, "THER" )

    #[step] Check 'THER' checkbox
    symbol = tr( 'THER', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'THER', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU > THER" )

    #[step] Type '54.6' in 'LAMBDA' field
    symbol = tr( 'LAMBDA', 'QLineEdit' )
    setParameterValue( symbol, '54.6' )
    #[step] Check 'RHO_CP' checkbox
    symbol = tr( 'RHO_CP', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '3710000.0' in 'RHO_CP' field
    symbol = tr( 'RHO_CP', 'QLineEdit' )
    setParameterValue( symbol, '3710000.0' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_MATERIAU' was renamed to 'MATER'
    checkOBItem( "CurrentCase", "Stage_1", "Material", "MATER" )

    saveStage("MODTH")
    MODTH()

    #--------------------------------------------------------------------------
def MODTH():

    #[section] Creation AFFE_MODELE command named 'MODTH'
    test.log( "Creation AFFE_MODELE command named 'MODTH'" )

    #[step] Click 'Show All' toolbar button
    #[step] Type 'AFFE_MODELE' in search field
    #[step] Select 'AFFE_MODELE' in list
    #[step] Click OK
    addCommandByDialog("AFFE_MODELE", "Model Definition")

    #[step] Double click on 'AFFE_MODELE' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Model Definition.AFFE\\_MODELE", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_MODELE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE" )
    #[check] Command name is 'AFFE_MODELE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "AFFE_MODELE" )

    #[step] Type 'MODTH' in 'Name' input field instead of 'AFFE_MODELE'
    setParameterValue(":Name_QLineEdit", "MODTH")

    #[step] Select 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    #[step] Check 'AFFE' checkbox
    symbol = tr( 'AFFE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE" )
    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0]" )
    #[step] Check 'TOUT' radiobutton
    symbol = tr( 'TOUT', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Select 'OUI' in 'TOUT' combobox
    symbol = tr( 'TOUT', 'QComboBox' )
    setParameterValue(symbol, "OUI")
    #[check] Select 'THERMIQUE' in 'PHENOMENE' combobox
    setParameterValue(tr( 'PHENOMENE', 'QComboBox' ), "THERMIQUE")
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0] > MODELISATION" )
    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with combobox and 'Remove' buttons was added to list
    symbol = tr( '0', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ) )
    #[step] Select '3D' in the combobox
    setParameterValue(symbol, '3D')

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE[0]' list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0]" )

    #[check] Select 'OUI' in 'TOUT' combobox
    symbol = tr( 'TOUT', 'QComboBox' )
    setParameterValue(symbol, "OUI")

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE_MODELE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'AFFE_MODELE' was renamed to 'MODTH'
    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "MODTH" )

    saveStage("CHMATER")
    CHMATER()

    #--------------------------------------------------------------------------
def CHMATER():

    #[section] Creation AFFE_MATERIAU command named 'CHMATER'
    test.log( "Creation AFFE_MATERIAU command named 'CHMATER'" )

    #[step] Add 'AFFE_MATERIAU' command by 'Show All' dialog
    addCommandByDialog("AFFE_MATERIAU", "Material")

    #[step] Double click on 'AFFE_MATERIAU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Material.AFFE\\_MATERIAU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_MATERIAU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU" )
    #[check] Command name is 'AFFE_MATERIAU'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "AFFE_MATERIAU" )

    #[step] Type 'CHMATER' in 'Name' input field instead of 'AFFE_MATERIAU'
    setParameterValue(":Name_QLineEdit", "CHMATER")

    #[step] Check 'MAILLAGE' checkbox
    symbol = tr( 'MAILLAGE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    #[step] Click 'Edit...' button of 'AFFE' keyword
    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'AFFE' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[step] Check 'TOUT' radiobutton
    symbol = tr( 'TOUT', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Select 'OUI' in 'TOUT' combobox
    symbol = tr( 'TOUT', 'QComboBox' )
    setParameterValue(symbol, "OUI")

    #[step] Click 'Edit...' button of 'MATER' keyword
    symbol = tr( 'MATER', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0] > MATER" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with combobox and 'Remove' buttons was added to list
    symbol = tr( '0', 'QComboBox' )
    test.verify( waitForObject( symbol ) )
    #[step] Select 'MATER' in the combobox
    setParameterValue(symbol, 'MATER')

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'AFFE_MATERIAU' was renamed to 'CHMATER'
    checkOBItem( "CurrentCase", "Stage_1", "Material", "CHMATER" )

    saveStage("F_TEMP")
    F_TEMP()

    #--------------------------------------------------------------------------
def F_TEMP():

    #[section] Creation DEFI_FONCTION command named 'F_TEMP'
    test.log( "Creation DEFI_FONCTION command named 'F_TEMP'" )

    #[step] Add 'DEFI_FONCTION' command by 'Show All' dialog
    addCommandByDialog("DEFI_FONCTION", "Functions and Lists")

    #[step] Call context menu on 'DEFI_FONCTION' item in tree
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Functions and Lists.DEFI_FONCTION", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_FONCTION'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )
    #[check] Command name is 'DEFI_FONCTION'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_FONCTION" )

    #[step] Type 'F_TEMP' in 'Name' input field instead of 'DEFI_FONCTION'
    setParameterValue(":Name_QLineEdit", "F_TEMP")

    #[check] 'NOM_PARA' combobox is enable
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'INST' value in 'NOM_PARA' combobox
    setParameterValue(symbol, 'INST')

    #[step] Check 'VALE' radiobutton
    symbol = tr( 'VALE', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of table was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION > VALE" )

    #[check] Tab has 1 row and 2 columns
    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")
    test.compare( tableWidget.rowCount, 1 )

    waitForObjectItem(":_FunctionTable", "0/0")
    #[step] Do double click in cell (0, 0)
    doubleClickItem(":_FunctionTable", "0/0", 39, 13, 0, Qt.LeftButton)
    #[step] Type '0' in  cell (0, 0) and press <Tab>
    type(waitForObject(cell(0, 0)), "0")
    type(waitForObject(cell(0, 0)), "<Tab>")

    #[step] Type '20' in  cell (0, 1) and press <Tab>
    type(waitForObject(cell(0, 1)), "20")
    type(waitForObject(cell(0, 1)), "<Tab>")

    #[check] A second row was added
    test.compare( tableWidget.rowCount, 2 )

    #[step] Type '10' in  cell (1, 0) and press <Tab>
    type(waitForObject(cell(1, 0)), "10")
    type(waitForObject(cell(1, 0)), "<Tab>")

    #[step] Type '70' in  cell (1, 1) and press <Tab>
    type(waitForObject(cell(1, 1)), "70")
    type(waitForObject(cell(1, 1)), "<Return>")

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_FONCTION' was renamed to 'F_TEMP'
    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "F_TEMP" )

    saveStage("CHARTH")
    CHARTH()

    #--------------------------------------------------------------------------
def CHARTH():

    #[section] Creation AFFE_CHAR_THER_F command named 'CHARTH'
    test.log( "Creation AFFE_CHAR_THER_F command named 'CHARTH'" )

    #[step] Add 'AFFE_CHAR_THER_F' command by 'Show All' dialog
    addCommandByDialog("AFFE_CHAR_THER_F", "BC and Load")

    #[step] Double click on 'AFFE_CHAR_THER_F' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.BC and Load.AFFE\\_CHAR\\_THER\\_F", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_CHAR_THER_F'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_THER_F" )
    #[check] Command name is 'AFFE_CHAR_THER_F'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "AFFE_CHAR_THER_F" )

    #[step] Type 'CHARTH' in 'Name' input field instead of 'AFFE_CHAR_THER_F'
    setParameterValue(":Name_QLineEdit", "CHARTH")

    #[step] Select 'MODTH' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    setParameterValue(symbol, 'MODTH')

    #[step] Check 'TEMP_IMPO' checkbox
    symbol = tr( 'TEMP_IMPO', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'TEMP_IMPO', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'TEMP_IMPO' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_THER_F > TEMP_IMPO" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_THER_F > TEMP_IMPO > [0]" )

    #[step] Check 'GROUP_MA' checkbox
    symbol = tr( 'GROUP_MA', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_THER_F > TEMP_IMPO > [0] > GROUP_MA" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'SURFINT' group in list
    waitForObjectItem(":More than one mesh found_QTreeWidget", "2D elements.SURFINT")
    clickItem(":More than one mesh found_QTreeWidget", "2D elements.SURFINT", 14, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_THER_F > TEMP_IMPO > [0]" )

    #[check] Check contents label of 'GROUP_MA' keyword was contains the '(SURFINT)'
    test.compare( waitForObject( ":GROUP_MA_content" ).text, "('SURFINT')" )

    #[step] Check 'TEMP' checkbox
    symbol = tr( 'TEMP', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'F_TEMP' in 'TEMP' combobox
    symbol = tr( 'TEMP', 'QComboBox' )
    setParameterValue(symbol, 'F_TEMP')

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'TEMP_IMPO' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_THER_F > TEMP_IMPO" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE_CHAR_THER_F' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_THER_F" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'AFFE_CHAR_THER_F' was renamed to 'CHARTH'
    checkOBItem( "CurrentCase", "Stage_1", "BC and Load", "CHARTH" )

    saveStage("LINST")
    LINST()

    #--------------------------------------------------------------------------
def LINST():

    #[section] Creation DEFI_LIST_REEL command named 'LINST'
    test.log( "Creation DEFI_LIST_REEL command named 'LINST'" )

    #[step] Add 'DEFI_LIST_REEL' command by 'Show All' dialog
    addCommandByDialog("DEFI_LIST_REEL", "Functions and Lists")

    #[step] Double click on 'DEFI_LIST_REEL' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Functions and Lists.DEFI\\_LIST\\_REEL", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_LIST_REEL'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL" )
    #[check] Command name is 'DEFI_LIST_REEL'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_LIST_REEL" )

    #[step] Type 'LINST' in 'Name' input field instead of 'DEFI_LIST_REEL'
    setParameterValue(":Name_QLineEdit", "LINST")

    #[step] Check 'VALE' radiobutton
    symbol = tr( 'VALE', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'VALE' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL > VALE" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with editable field and 'Remove' button was added to list
    symbol = tr( '0', 'QLineEdit' )
    test.verify( waitForObject( symbol ) )
    #[step] Type '0' in the input field
    setParameterValue( symbol, '0' )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with editable field and 'Remove' button was added to list
    symbol = tr( '1', 'QLineEdit' )
    test.verify( waitForObject( symbol ) )
    #[step] Type '5' in the input field
    setParameterValue( symbol, '5' )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with editable field and 'Remove' button was added to list
    symbol = tr( '2', 'QLineEdit' )
    test.verify( waitForObject( symbol ) )
    #[step] Type '10' in the input field
    setParameterValue( symbol, '10' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_REEL' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_LIST_REEL' was renamed to 'LINST'
    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "LINST" )

    saveStage("F_MULT")
    F_MULT()

    #--------------------------------------------------------------------------
def F_MULT():

    #[section] Creation DEFI_FONCTION command named 'F_MULT'
    test.log( "Creation DEFI_FONCTION command named 'F_MULT'" )

    #[step] Add 'DEFI_FONCTION' command by 'Show All' dialog
    addCommandByDialog("DEFI_FONCTION", "Functions and Lists")

    #[step] Call context menu on 'DEFI_FONCTION' item in tree
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Functions and Lists.DEFI_FONCTION", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_FONCTION'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )
    #[check] Command name is 'DEFI_FONCTION'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_FONCTION" )

    #[step] Type 'F_MULT' in 'Name' input field instead of 'DEFI_FONCTION'
    setParameterValue(":Name_QLineEdit", "F_MULT")

    #[check] 'NOM_PARA' combobox is enable
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'INST' value in 'NOM_PARA' combobox
    setParameterValue(symbol, 'INST')

    #[step] Check 'VALE' radiobutton
    symbol = tr( 'VALE', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of table was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION > VALE" )

    #[check] Tab has 1 row and 2 columns
    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")
    test.compare( tableWidget.rowCount, 1 )

    waitForObjectItem(":_FunctionTable", "0/0")
    #[step] Do double click in cell (0, 0)
    doubleClickItem(":_FunctionTable", "0/0", 39, 13, 0, Qt.LeftButton)
    #[step] Type '0' in  cell (0, 0) and press <Tab>
    type(waitForObject(cell(0, 0)), "0")
    type(waitForObject(cell(0, 0)), "<Tab>")

    #[step] Type '1' in  cell (0, 1) and press <Tab>
    type(waitForObject(cell(0, 1)), "1")
    type(waitForObject(cell(0, 1)), "<Tab>")

    #[check] A second row was added
    test.compare( tableWidget.rowCount, 2 )

    #[step] Type '10' in  cell (1, 0) and press <Tab>
    type(waitForObject(cell(1, 0)), "10")
    type(waitForObject(cell(1, 0)), "<Tab>")

    #[step] Type '1' in  cell (1, 1) and press <Tab>
    type(waitForObject(cell(1, 1)), "1")
    type(waitForObject(cell(1, 1)), "<Return>")

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_FONCTION' was renamed to 'F_MULT'
    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "F_MULT" )

    saveStage("TEMPE")
    TEMPE()

    #--------------------------------------------------------------------------
def TEMPE():

    #[section] Creation THER_LINEAIRE command named 'TEMPE'
    test.log( "Creation THER_LINEAIRE command named 'TEMPE'" )

    #[step] Add 'THER_LINEAIRE' command by 'Show All' dialog
    addCommandByDialog("THER_LINEAIRE", "Analysis")

    #[step] Double click on 'DEFI_LIST_INST' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Analysis.THER\\_LINEAIRE", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'THER_LINEAIRE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE" )
    #[check] Command name is 'THER_LINEAIRE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "THER_LINEAIRE" )

    #[step] Type 'TEMPE' in 'Name' input field instead of 'THER_LINEAIRE'
    setParameterValue(":Name_QLineEdit", "TEMPE")

    #[check] 'MODELE' combobox is enable
    symbol = tr( 'MODELE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'MODTH' value in 'MODELE' combobox
    setParameterValue(symbol, 'MODTH')

    #[check] 'CHAM_MATER' combobox is enable
    symbol = tr( 'CHAM_MATER', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'CHMATER' value in 'CHAM_MATER' combobox
    setParameterValue(symbol, 'CHMATER')

    #[check] 'Edit...' button of 'EXCIT' is enable
    symbol = tr( 'EXCIT', 'QPushButton' )
    test.verify( waitForObject( symbol ).enabled )
    #[step] Click the enabled 'Edit...' button
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE > EXCIT" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE > EXCIT > [0]" )

    #[check] 'CHARGE' combobox is enable
    symbol = tr( 'CHARGE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'CHARTH' value in 'CHARGE' combobox
    setParameterValue(symbol, 'CHARTH')

    #[step] Check 'FONC_MULT' checkbox
    symbol = tr( 'FONC_MULT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'F_MULT' in 'FONC_MULT' combobox
    symbol = tr( 'FONC_MULT', 'QComboBox' )
    setParameterValue(symbol, 'F_MULT')

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE > EXCIT" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'THER_LINEAIRE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE" )

    #[step] Check 'INCREMENT' checkbox
    symbol = tr( 'INCREMENT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'INCREMENT', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'INCREMENT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE > INCREMENT" )

    #[step] Check 'LIST_INST' checkbox
    symbol = tr( 'LIST_INST', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'LINST' value in 'LIST_INST' combobox
    symbol = tr( 'LIST_INST', 'QComboBox' )
    setParameterValue(symbol, 'LINST')

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'THER_LINEAIRE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE" )

    #[step] Check 'ETAT_INIT' checkbox
    symbol = tr( 'ETAT_INIT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ETAT_INIT', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'ETAT_INIT' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE > ETAT_INIT" )

    #[step] Check 'VALE' checkbox
    symbol = tr( 'VALE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '20' in the input field
    symbol = tr( 'VALE', 'QLineEdit' )
    setParameterValue( symbol, '20' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'THER_LINEAIRE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "THER_LINEAIRE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'THER_LINEAIRE' was renamed to 'TEMPE'
    checkOBItem( "CurrentCase", "Stage_1", "Analysis", "TEMPE" )

    saveStage("IMPR_RESU")
    IMPR_RESU()

    #--------------------------------------------------------------------------
def IMPR_RESU():

    #[section] Creation IMPR_RESU command named 'IMPR_RESU'
    test.log( "Creation IMPR_RESU command named 'IMPR_RESU'" )

    #[step] Add 'IMPR_RESU' command by 'Show All' dialog
    addCommandByDialog("IMPR_RESU", "Output")

    #[step] Double click on 'IMPR_RESU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Output.IMPR\\_RESU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'IMPR_RESU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU" )
    #[check] Command name is '_' (empty)
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "_" )

    #[step] Check 'FORMAT' checkbox
    symbol = tr( 'FORMAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'MED' value in 'FORMAT' combobox
    symbol = tr( 'FORMAT', 'QComboBox' )
    setParameterValue(symbol, 'MED')

    #[step] Check 'RESU' checkbox
    symbol = tr( 'RESU', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'RESU', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'RESU' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU > [0]" )

    #[step] Check 'RESULTAT' checkbox
    symbol = tr( 'RESULTAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'TEMPE' value in 'RESULTAT' combobox
    symbol = tr( 'RESULTAT', 'QComboBox' )
    setParameterValue(symbol, 'TEMPE')

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'RESU' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'IMPR_RESU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU" )

    #[step] Click '...' button to browse a file
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Type 'output.med' as file name in 'Select file' dialog
    file_name = 'output.med'
    file_path = os.path.join( testdir(), file_name )
    setParameterValue( ":fileNameEdit_QLineEdit", file_path )
    #[step] Click 'Save' in 'Select file' dialog
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Data Files" )
    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    test.log("Note: UNITE=80 according to default value in 'code_aster' for IMPR_RESU command. Stage in Graphical mode does not allow to modify it.")
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.80" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.80", 10, 10, 0, Qt.LeftButton )

    clickButton(waitForObject(":DataFilesBase.Edit_QToolButton"))

    test.compare( str( waitForObjectExists( tr( "Filename", "QComboBox" ) ).currentText ), "output.med" )
    test.compare( findObject( tr( "Unit", "QLineEdit" ) ).text, "80" )
    test.xverify( waitForObjectExists( tr( "Unit", "QLineEdit" ) ).enabled )
    test.compare( waitForObjectExists( tr( "Mode", "QComboBox" ) ).currentText, "out" )
    test.compare(waitForObjectExists( tr( "Exists", "QCheckBox" ) ).checked, False)
    test.compare(waitForObjectExists( tr( "Embedded", "QCheckBox" ) ).checked, False)

    clickButton( waitForObject( ":OK_QPushButton" ) )

    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Data Settings" )

    saveStage("ImportStage2")
    ImportStage2()

    #--------------------------------------------------------------------------
def ImportStage2():
    #[section] Import 'lot6_stage2.comm' stage
    test.log( "Import 'lot6_stage2.comm' stage" )

    stage2_path = os.path.join( casedir(), 'lot6_stage2.comm' )
    importStage(stage2_path)
#    # 'lot6_stage2.comm' can not be imported in Graphical mode
#    test.compare( str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ),
#                  "Stage is imported in text mode as it cannot be converted to graphical mode." )
#    clickButton( waitForObject( ":AsterStudy.OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "lot6_stage2" )
    clickItem( ":_QTreeWidget", item, 10, 20, 0, Qt.LeftButton )
#    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 20, 0 )
#    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    #--------------------------------------------------------------------------
#    test.compare( str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ),
#                  "Cannot convert the stage to graphic mode" )
#    clickButton( waitForObject( ":AsterStudy.OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.lot6_stage2", "Rename")
    type(waitForObject(":_QExpandingLineEdit_2"), "Stage_2")
    type(waitForObject(":_QExpandingLineEdit_2"), "<Return>")
    selectObjectBrowserItem( "CurrentCase.Stage_2" )

    saveStage("Computation1", "Stage_2")
    file_path = os.path.join( testdir(), "beforeComputation1.ajs" )
    saveFile( file_path )
    Computation1()

    #--------------------------------------------------------------------------
def Computation1():
    #[section] Computation of Stage1 and Stage2
    test.log( "Computation of Stage1 and Stage2" )

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    run_options( 'Stage_1', Skip | Execute )
    run_options( 'Stage_2', Skip | Execute )

    use_option( 'Stage_1', Execute )
    use_option( 'Stage_2', Execute )
    keep_results( 'Stage_1', True )

    run_case( 'RunCase_1' )

    snooze(3)
    test.vp("VP1")

    #--------------------------------------------------------------------------
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

#    item = showOBItem( "CurrentCase", "Stage_2" )
#    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
#    openItemContextMenu(waitForObject(":_QTreeWidget"), item, 57, 6, 0)
#    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Text Mode"))

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    file_path = os.path.join( testdir(), "beforeCreateStage3.ajs" )
    saveFile( file_path )
    CreateStage3()

    #--------------------------------------------------------------------------
def CreateStage3():
    #[section] Creation of Stage3 in TEXT mode
    test.log( "Creation of Stage3 in TEXT mode" )

    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #[step] Create a new stage by toolbar button
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )
    #[check] 'Stage' was added to tree
    checkOBItem( "CurrentCase", "Stage_3" )

    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_3", 52, 7, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Text Mode"))

    #[step] Click 'Edit' in context menu for 'CurrentCase.Stage_3'
    activateOBContextMenuItem( "CurrentCase.Stage_3", "Edit" )

    stage3_path = os.path.join( casedir(), 'lot6_stage3.comm' )
    stage3_file = open( stage3_path )

    text = stage3_file.read()

    stage3_file.close()

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    waitForObject( text_editor ).appendPlainText( text )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    item = "CurrentCase.Stage\\_3"
    waitForObjectItem( ":_QTreeWidget", item )
    clickItem( ":_QTreeWidget", item, 42, 8, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 42, 8, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton( waitForObject( ":Undefined files.OK_QPushButton" ) )

    saveStage( "Computation2", "Stage_3" )
    file_path = os.path.join( testdir(), "beforeComputation2.ajs" )
    saveFile( file_path )
    Computation2()

    #--------------------------------------------------------------------------
def Computation2():
    #[section] Computation of case using previous results
    test.log( "Computation of case using Stage1 and Stage2 results" )

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    run_options( 'Stage_1', Skip | Reuse )
    run_options( 'Stage_2', Skip | Reuse )
    run_options( 'Stage_3', Skip | Execute )

    use_option( 'Stage_3', Execute )

    run_case( 'RunCase_2' )

    snooze(3)
    test.vp("VP2")

    #--------------------------------------------------------------------------
    file_path = os.path.join( testdir(), "beforeComputation3.ajs" )
    saveFile( file_path )
    Computation3()

    #--------------------------------------------------------------------------
def Computation3():
    #[section] Computation of Stage1, Stage2 and Stage3
    test.log( "Computation of Stage1, Stage2 and Stage3" )

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    run_options( 'Stage_1', Skip | Reuse | Execute )
    run_options( 'Stage_2', Skip | Reuse | Execute )
    run_options( 'Stage_3', Skip | Execute )

    use_option( 'Stage_3', Execute )
    use_option( 'Stage_2', Execute )
    use_option( 'Stage_1', Execute )
    keep_results( 'Stage_1', True )
    keep_results( 'Stage_2', True )

    run_case( 'RunCase_3' )

    snooze(3)
    test.vp("VP3")

    #--------------------------------------------------------------------------
    file_path = os.path.join( testdir(), "beforeEND.ajs" )
    saveFile( file_path )
    END()

    #--------------------------------------------------------------------------
def END():
    pass
