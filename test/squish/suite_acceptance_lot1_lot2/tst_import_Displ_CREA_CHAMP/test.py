#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'CREA_CHAMP' command named 'Displ'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'CREA_CHAMP' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkDispl()

    pass

def checkDispl():
    #[section] Check CREA_CHAMP command named 'Displ'
    test.log( "Check CREA_CHAMP command named 'Displ'" )

    #[step] Call 'Edit' in context menu on 'Displ' item in tree
    waitForActivateOBContextMenuItem( "CurrentCase.Recette_asterstudy.Post Processing.Displ", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'CREA_CHAMP'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CREA_CHAMP" )
    #[check] Command name is 'Displ'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Displ" )

    #[check] 'TYPE_CHAM' combobox is enable
    symbol = tr( 'TYPE_CHAM', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'NOEU_DEPL_R' value in 'TYPE_CHAM' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'NOEU_DEPL_R' )

    #[check] 'OPERATION' combobox is enable
    symbol = tr( 'OPERATION', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'EXTR' value in 'OPERATION' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'EXTR' )

    #[check] 'RESULTAT' checkbox has 'checked' state
    symbol = tr( 'RESULTAT', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'NL_res' value in 'RESULTAT' combobox
    symbol = tr( 'RESULTAT', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'NL_res' )

    #[check] 'NOM_CHAM' combobox is enable
    symbol = tr( 'NOM_CHAM', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'DEPL' value in 'NOM_CHAM' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'DEPL' )

    symbol = tr( 'INST', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] '1.0' value in 'INST' combobox
    symbol = tr( 'INST', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, '1.0' )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
