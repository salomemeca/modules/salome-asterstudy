#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'TEST_RESU' command named 'TEST_RESU'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'TEST_RESU' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkTEST_RESU()

    pass

def checkTEST_RESU():
    #[section] Check TEST_RESU command named 'TEST_RESU'
    test.log( "Check TEST_RESU command named 'TEST_RESU'" )

    #[step] Double click on 'TEST_RESU' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Other", "TEST_RESU" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'TEST_RESU'
    test.compare( str( waitForObject( ":_ParameterTitle" ).displayText ), "TEST_RESU" )
    #[check] Command name is empty: '_'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "_" )

    #[check] 'RESU' checkbox has 'checked' state
    symbol = tr( 'RESU', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'RESU', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'RESU' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU" )

    #[check] Row with 'Edit...' and 'Remove' buttons is in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU > [0]" )

    #[check] 'RESULTAT' combobox is enable
    symbol = tr( 'RESULTAT', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'NL_res' value in 'RESULTAT' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'NL_res' )

    #[check] 'INST' radiobutton has 'checked' state
    symbol = tr( 'INST', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[check] '1.0' value in 'INST' input field
    symbol = tr( 'INST', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "1.0" )

    #[check] 'NOM_CHAM' radiobutton has 'checked' state
    symbol = tr( 'NOM_CHAM', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'SIEF_NOEU' value in 'NOM_CHAM' combobox
    symbol = tr( 'NOM_CHAM', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'SIEF_NOEU' )

    #[check] 'NOM_CMP' checkbox has 'checked' state
    symbol = tr( 'NOM_CMP', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'SIYY' in 'NOM_CMP' field
    symbol = tr( 'NOM_CMP', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, 'SIYY' )

    #[check] 'TYPE_TEST' checkbox has 'checked' state
    symbol = tr( 'TYPE_TEST', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'MIN' value in 'TYPE_TEST' combobox
    symbol = tr( 'TYPE_TEST', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'MIN' )

    #[check] 'VALE_CALC' radiobutton has 'checked' state
    symbol = tr( 'VALE_CALC', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'VALE_CALC', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'VALE_CALC' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU > [0] > VALE_CALC" )

    #[check] Row with editable field and 'Remove' button is in the list
    symbol = tr( '0', 'QLineEdit' )
    test.verify( object.exists( symbol ) )
    #[check] '-31.2006807436' in the input field
    test.compare( waitForObject( symbol ).text, '-31.2006807436' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'RESU[0]' item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU > [0]" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'RESU' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'TEST_RESU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
