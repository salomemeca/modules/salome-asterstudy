#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'CALC_CHAMP' command named 'NL_res'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'CALC_CHAMP' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    check_2_NL_res()

    pass

def check_2_NL_res():
    #[section] Check CALC_CHAMP command named 'NL_res'
    test.log( "Check CALC_CHAMP command named 'NL_res'" )

    #[step] Call 'Edit' in context menu on 'NL_res' item in tree
    waitForActivateOBContextMenuItem( "CurrentCase.Recette_asterstudy.Post Processing.NL_res", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'CALC_CHAMP'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CALC_CHAMP" )
    #[check] Command name is 'NL_res'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "automatic" )

    #[check] 'RESULTAT' combobox is enable
    symbol = tr( 'RESULTAT', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'NL_res' value in 'RESULTAT' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'NL_res' )

    #[check] 'CONTRAINTE' checkbox has 'checked' state
    symbol = tr( 'CONTRAINTE', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'CONTRAINTE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'CONTRAINTE' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CALC_CHAMP > CONTRAINTE" )

    #[check] Row with combobox and 'Remove' button is in the list
    symbol = tr( '0', 'QComboBox' )
    test.verify( object.exists( symbol ) )
    #[check] 'SIEF_NOEU' in the combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'SIEF_NOEU' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'CALC_CHAMP' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CALC_CHAMP" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
