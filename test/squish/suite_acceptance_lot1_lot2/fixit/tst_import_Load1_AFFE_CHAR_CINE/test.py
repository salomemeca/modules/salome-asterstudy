#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'AFFE_CHAR_CINE' command named 'Load1'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'AFFE_CHAR_CINE' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkLoad1()

    pass

def checkLoad1():
    #[section] Check AFFE_CHAR_CINE command named 'Load1'
    test.log( "Check AFFE_CHAR_CINE command named 'Load1'" )

    #[step] Double click on 'Load1' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "BC and Load", "Load1" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    waitForObject(":Edit command_QLabel")
    #[check] Command is 'AFFE_CHAR_CINE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE" )
    #[check] Command name is 'Load1'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Load1" )

    #[check] 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Model' )

    #[check] 'MECA_IMPO' radiobutton has 'checked' state
    symbol = tr( 'MECA_IMPO', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'MECA_IMPO', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'MECA_IMPO' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO" )

    #[check] 2 rows with 'Edit...' and 'Remove' buttons are in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    symbol = tr( '1', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click first 'Edit...' button in the list
    symbol = tr( '0', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of MECA_IMPO[0] item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [0]" )

    #[check] 'GROUP_MA' radiobutton has 'checked' state
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Sym_x' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [0] > GROUP_MA" )

    test.warning("As currently function to get mesh groups is not implemented yet (only a dummy stub is used), it is only possible to enter group names by manual typing in line edit field." )
    ##[check] 'Sym_x' group in list
    # TODO: check of groups should be added
    # workaround:
    #[check] 'Sym_x' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Sym_x" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [0]" )

    #[check] Button of 'GROUP_MA' keyword has name 'Sym_x'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[check] 'DX' checkbox has 'checked' state
    symbol = tr( 'DX', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] '0.0' in 'DX' field
    symbol = tr( 'DX', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, '0.0' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'MECA_IMPO' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO" )

    #[step] Click second 'Edit...' button in the list
    symbol = tr( '1', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of MECA_IMPO[1] item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [1]" )

    #[check] 'GROUP_NO' radiobutton has 'checked' state
    symbol = tr( 'GROUP_NO', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Sym_y' button
    symbol = tr( 'GROUP_NO', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_NO' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [1] > GROUP_NO" )

    test.warning("As currently function to get mesh groups is not implemented yet (only a dummy stub is used), it is only possible to enter group names by manual typing in line edit field." )
    ##[check] 'Sym_y' group in list
    # TODO: check of groups should be added
    # workaround:
    #[check] 'Sym_y' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Sym_y" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [1]" )

    #[check] Button of 'GROUP_NO' keyword has name 'Sym_y'
    symbol = tr( 'GROUP_NO', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )
    #[check] 'DY' checkbox has 'checked' state
    symbol = tr( 'DY', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] '0.0' in 'DY' field
    symbol = tr( 'DY', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, '0.0' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'MECA_IMPO' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE_CHAR_CINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
