#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'IMPR_RESU' command named 'IMPR_RESU'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'IMPR_RESU' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkIMPR_RESU()

    pass

def checkIMPR_RESU():
    #[section] Check IMPR_RESU command named 'IMPR_RESU'
    test.log( "Check IMPR_RESU command named 'IMPR_RESU'" )

    #[step] Double click on 'IMPR_RESU' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Output", "IMPR_RESU" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'IMPR_RESU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU" )
    #[check] Command name is empty: '_'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "_" )

    #[check] 'FORMAT' checkbox has 'checked' state
    symbol = tr( 'FORMAT', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'MED' value in 'FORMAT' combobox
    symbol = tr( 'FORMAT', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'MED' )

    #[check] 'RESU' checkbox has 'checked' state
    symbol = tr( 'RESU', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'RESU', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'RESU' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU" )

    #[check] Row with 'Edit...' and 'Remove' buttons is in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU > [0]" )

    #[check] 'RESULTAT' checkbox has 'checked' state
    symbol = tr( 'RESULTAT', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'NL_res' value in 'RESULTAT' combobox
    symbol = tr( 'RESULTAT', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'NL_res' )

    #[check] 'NOM_CHAM' checkbox has 'checked' state
    symbol = tr( 'NOM_CHAM', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'NOM_CHAM', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'NOM_CHAM' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU > [0] > NOM_CHAM" )

    #[check] Row with combobox and 'Remove' buttons is in the list
    symbol = tr( '0', 'QComboBox' )
    test.verify( object.exists( symbol ) )
    #[check] 'DEPL' in the combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'DEPL' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'RESU[0]' item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU > [0]" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'RESU' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'IMPR_RESU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
