#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'AFFE_MATERIAU' command named 'MatF'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'AFFE_MATERIAU' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkMatF()

    pass

def checkMatF():
    #[section] Check AFFE_MATERIAU command named 'MatF'
    test.log( "Check AFFE_MATERIAU command named 'MatF'" )

    #[step] Double click on 'MatF' item in tree   
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Material", "MatF" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_MATERIAU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU" )
    #[check] Command name is 'MatF'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "MatF" )

    #[check] 'MAILLAGE' checkbox has 'checked' state
    symbol = tr( 'MAILLAGE', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Mesh' )

    #[check] 'MODELE' checkbox has 'checked' state
    symbol = tr( 'MODELE', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Model' )

    #[step] Click 'Edit...' button of 'AFFE' keyword
    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'AFFE' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[check] 2 rows with 'Edit...' and 'Remove' buttons are in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    symbol = tr( '1', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click first 'Edit...' button in the list
    symbol = tr( '0', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[check] 'GROUP_MA' radiobutton has 'checked' state
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click the enabled 'Upper' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0] > GROUP_MA" )

    test.warning("As currently function to get mesh groups is not implemented yet (only a dummy stub is used), it is only possible to enter group names by manual typing in line edit field." )
    ##[check] 'Upper' group is checked in list
    # TODO: check of groups should be added
    # workaround:
    #[check] 'Upper' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Upper" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[check] Button of 'GROUP_MA' keyword has name 'Upper'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Click 'Edit...' button of 'MATER' keyword
    symbol = tr( 'MATER', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0] > MATER" )

    #[check] Row with combobox and 'Remove' buttons is in the list
    symbol = tr( '0', 'QComboBox' )
    test.verify( object.exists( symbol ) )
    #[check] 'Mat01' in the combobox
    test.compare( waitForObject( symbol ).currentText, 'Mat01' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[step] Click second 'Edit...' button in the list
    symbol = tr( '1', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1]" )

    #[check] 'GROUP_MA' radiobutton has 'checked' state
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click the enabled 'Lower' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1] > GROUP_MA" )

    test.warning("As currently function to get mesh groups is not implemented yet (only a dummy stub is used), it is only possible to enter group names by manual typing in line edit field." )
    ##[check] 'Lower' group is checked in list
    # TODO: check of groups should be added
    # workaround:
    #[check] 'Lower' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Lower" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1]" )

    #[check] Button of 'GROUP_MA' keyword has name 'Lower'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Click 'Edit...' button of 'MATER' keyword
    symbol = tr( 'MATER', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1] > MATER" )

    #[check] Row with combobox and 'Remove' buttons is in the list
    symbol = tr( '0', 'QComboBox' )
    test.verify( object.exists( symbol ) )
    #[check] 'Mat02' in the combobox
    test.compare( waitForObject( symbol ).currentText, 'Mat02' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1]" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
