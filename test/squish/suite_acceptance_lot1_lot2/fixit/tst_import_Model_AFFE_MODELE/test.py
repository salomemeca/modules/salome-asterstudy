#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'AFFE_MODELE' command named 'Model'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'AFFE_MODELE' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkModel()

    pass

def checkModel():
    #[section] Check AFFE_MODELE command named 'Model'
    test.log( "Check AFFE_MODELE command named 'Model'" )

    #[step] Double click on 'Model' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Model Definition", "Model" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_MODELE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE" )
    #[check] Command name is 'Model'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Model" )

    #[check] 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Mesh' )

    #[check] 'AFFE' checkbox has 'Checked' state
    symbol = tr( 'AFFE', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE" )
    #[check] Row with 'Edit...' and 'Remove' buttons is in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0]" )
    #[check] 'TOUT' radiobutton has 'checked' state
    symbol = tr( 'TOUT', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'TOUT' combobox value is 'OUI'
    symbol = tr( 'TOUT', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, "OUI" )
    #[check] 'PHENOMENE' combobox value is 'MECANIQUE'
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, "MECANIQUE" )
    #[step] Click 'Edit...' button of 'MODELISATION' keyword
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0] > MODELISATION" )
    #[check] Row with combobox and 'Remove' buttons is in the list
    symbol = tr( '0', 'QComboBox' )
    test.verify( object.exists( symbol ) )
    #[check] 'C_PLAN' in the combobox
    test.compare( waitForObjectExists( symbol ).currentText, "C_PLAN" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE[0]' list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0]" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE_MODELE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
