#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'DEFI_FONCTION' command named 'F_mult'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'DEFI_FONCTION' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkF_mult()

    pass

def cell(row, col):
    # Currently row and col are not used
    return "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"

def checkF_mult():
    #[section] Check DEFI_FONCTION command named 'F_mult'
    test.log( "Check DEFI_FONCTION command named 'F_mult'" )

    #[step] Select "F_mult" in second "Functions and Lists" category
    selectItem( "Recette_asterstudy", "Functions and Lists:-4", "F_mult" )
    #[step] Click 'Edit - Edit' in main menu
    waitForActivateMenuItem( "Edit", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_FONCTION'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )
    #[check] Command name is 'F_mult'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "F_mult" )

    #[check] 'NOM_PARA' combobox is enable
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'INST' value in 'NOM_PARA' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'INST' )

    #[check] 'VALE' radiobutton has 'checked' state
    symbol = tr( 'VALE', 'QRadioButton' )
    test.verify( waitForObjectExists( symbol ).checked )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of table was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION > VALE" )

    #[check] Tab has 2 rows and 2 columns
    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")
    test.compare( tableWidget.rowCount, 2 )
    test.compare( tableWidget.columnCount, 2 )

    waitForObjectItem(":_FunctionTable", "0/0")
    doubleClickItem(":_FunctionTable", "0/0", 39, 13, 0, Qt.LeftButton)
    #[check] '0.0' in  cell (0, 0)
    test.compare(waitForObject(cell(0, 0)).text, "0.0")
    type(waitForObject(cell(0, 0)), "<Tab>")
    #[check] '0.0' in  cell (0, 1)
    test.compare(waitForObject(cell(0, 1)).text, "0.0")
    type(waitForObject(cell(0, 1)), "<Tab>")
    #[check] '1.0' in  cell (1, 0)
    test.compare(waitForObject(cell(1, 0)).text, "1.0")
    type(waitForObject(cell(1, 0)), "<Tab>")
    #[check] '1.0' in  cell (1, 1)
    test.compare(waitForObject(cell(1, 1)).text, "1.0")
    type(waitForObject(cell(1, 1)), "<Return>")

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
