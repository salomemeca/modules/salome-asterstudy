"""
Implementation of common utilities for test suite.
"""

import datetime
import os
import pwd
import shutil
import tempfile
import types

#--------------------------------------------------------------------------
# Runners
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
class Runner:
    """Base Runner class."""

    #----------------------------------------------------------------------
    runner = None
    supported_options = {
        'debug' : '-g',
        'noexhook' : '-n',
        'basicnaming' : '-b',
        'nativenames' : '-c',
        }
    config_file = None
    config_var = None
    test_directory = None

    #----------------------------------------------------------------------
    @staticmethod
    def get_runner():
        """
        Get / create single runner instance.
        Returns:
            Runner: Single global Runner instance.
        """
        if Runner.runner is None:
            Runner.runner = SalomeRunner() if salomeUnderTest() else StdRunner()
        return Runner.runner

    #----------------------------------------------------------------------
    def __init__(self):
        """Create runner."""
        #------------------------------------------------------------------
        self.xlsfile = None
        self.case_options = []
        self.started = False

        #------------------------------------------------------------------
        # Set-up global tests behavior
        testSettings.logScreenshotOnError = True

        #------------------------------------------------------------------
        # Prepare test output directory
        try:
            # - Get path to root tmp folder
            #   Note: it can be specified via TEST_DIR environment variable.
            #   By default, system tmp directory is used.
            root_tmpdir = os.getenv("TEST_DIR")
            if root_tmpdir:
                # -- Create root tmp directory if it does not exist
                if not os.path.exists(root_tmpdir):
                    os.mkdir(root_tmpdir)
            else:
                # -- Get system tmp directory
                root_tmpdir = tempfile.gettempdir()

            # - Specify test case tmp directory by pattern:
            #    <test_suite_name>_<test_case_name>_<user_name>_<time_stamp>
            file_name = self.casedir().split(os.sep)[-2:]
            user_name = username()
            file_name.append(user_name)
            time_stamp = currentDateTime("%Y-%m-%d_%H-%M-%S")
            file_name.append(time_stamp)
            Runner.test_directory = os.path.join(root_tmpdir, '_'.join(file_name))

            # - Remove tmp directory if it is already present
            if os.path.exists(self.test_directory):
                shutil.rmtree(self.test_directory)
            # - Create tmp directory
            if not os.path.exists(self.test_directory):
                os.mkdir(self.test_directory)
        except OSError as e:
            breakTest("Cannot prepare test directory", str(e))

        #------------------------------------------------------------------
        # Process runner options specified in the configuration files
        dirs = self.cfgdirs()
        for d in reversed(dirs):
            cfg_file = os.path.join(d, "suite_cfg.py")
            if os.path.exists(cfg_file):
                ldir = {}
                with open(cfg_file) as f:
                    exec(f.read(), globals(), ldir)
                for var, val in ldir.items():
                    if var in self.supported_options:
                        if val:
                            self._add_option(self.supported_options[var], self.case_options)
                        else:
                            self._rm_option(self.supported_options[var], self.case_options)
                    else:
                        test.warning("Unknown runner option {option} in file {file}".format(option=var, file=cfg_file))

        #------------------------------------------------------------------
        # Prepare environment file
        self.prepare_envfile()

    #----------------------------------------------------------------------
    def start(self, **kwargs):
        """
        Start runner.
        Arguments:
            **kwargs: Options to run application.
        """
        #------------------------------------------------------------------
        # Process forced options specified as arguments
        for var, val in kwargs.items():
            if var in self.supported_options:
                if val:
                    self._add_option(self.supported_options[var], self.case_options)
                else:
                    self._rm_option(self.supported_options[var], self.case_options)
            else:
                test.warning("Unknown runner option {option} in initialization function".format(option=var))

        #------------------------------------------------------------------
        # Get command to run
        command = self.command2run()

        #------------------------------------------------------------------
        # Start AUT
        attempts = 10
        for attempt in range(1, attempts+1):
            try:
                #----------------------------------------------------------
                # Start application
                test.log("Start application, attempt {attempt}/{attempts}: {command}".format(attempt=attempt,
                                                                                             attempts=attempts,
                                                                                             command=command))
                app = startApplication(command)
                #----------------------------------------------------------
                # Perform additional custom actions after application start-up
                self.after_start()
                #----------------------------------------------------------
                # Wait for application's main window
                waitForObjectExists(":AsterStudy_MainWindow", 60000)
                #----------------------------------------------------------
                break # successfully started
            except:
                test.warning("Could not start application")
            self.after_start()
            snooze(5)
            self.cleanup()

        #------------------------------------------------------------------
        # Execute test case
        if not self.started:
            self.started = True
            runCase()

    #----------------------------------------------------------------------
    def finalize(self):
        """Finalize test: clean up, free resources, etc."""
        # Clean up
        self.cleanup()

        # Close XLS scenario export file if it's open
        if self.xlsfile is not None:
            self.xlsfile.close()

        # remove tmp directory
        try:
            if self.test_directory:
                # remove test case temporary directory
                if os.path.exists(self.test_directory):
                    shutil.rmtree(self.test_directory)
        except OSError as e:
            test.fatal("Error: cannot remove test directory: %s" % e)

    #----------------------------------------------------------------------
    @staticmethod
    def casedir():
        """
        Get test case's source directory.
        Returns:
            str: Path to the test case's source directory.
        """
        return squishinfo.testCase

    #----------------------------------------------------------------------
    @staticmethod
    def suitedir():
        """
        Get test suite's directory.
        Returns:
            str: Path to the test suite's directory.
        """
        return os.path.dirname(Runner.casedir())

    #----------------------------------------------------------------------
    @staticmethod
    def resultdir():
        """
        Get test case's result directory.
        Returns:
            str: Path to the test case's result directory.
        """
        return squishinfo.resultDir

    #----------------------------------------------------------------------
    @staticmethod
    def testdir():
        """
        Get test case's output directory.
        Returns:
            str: Path to the test case's output directory.
        """
        directory = Runner.test_directory
        return directory or tempfile.gettempdir() # fallback to '/tmp'

    #----------------------------------------------------------------------
    @classmethod
    def cfgdirs(cls, shared=False, envvar=False):
        # Source dirs: top-level tests directory, test suite, test case
        dirs = [os.path.abspath(os.path.join(*[cls.casedir()] + [os.pardir]*i)) for i in range(3)]
        # Shared directory
        if shared and cls.config_file:
            try:
                dirs.append(os.path.dirname(os.path.realpath(findFile('testdata', cls.config_file))))
            except LookupError:
                pass
        if envvar and cls.config_var:
            var = os.getenv(cls.config_var, '')
            if var:
                dirs.append(var)
        return dirs

    #----------------------------------------------------------------------
    @staticmethod
    def envfile():
        """
        Get test suite's settings file.
        Returns:
            str: Path to the test suite's environment file.
        """
        return os.path.join(os.path.dirname(Runner.casedir()), "envvars")

    #----------------------------------------------------------------------
    def get_env_vars(self):
        """Get base environment variables as needed by the runner."""
        dirs = self.cfgdirs()
        variables = []
        for d in reversed(dirs):
            cfg_file = os.path.join(d, "environ.cfg")
            if os.path.exists(cfg_file):
                with open(cfg_file) as f:
                    lines = [i.strip() for i in f.readlines()]
                    variables += lines
        variables += self.custom_env_vars()
        return variables        

    #----------------------------------------------------------------------
    def custom_env_vars(self):
        """Get additional environment variables as needed by the runner."""
        vars = []
        config_var = os.pathsep.join(self.cfgdirs(True, True)).strip(os.pathsep)
        if config_var:
            vars.append("{} = {}".format(self.config_var, config_var))
        return vars

    #----------------------------------------------------------------------
    def prepare_envfile(self):
        """
        Initialize additional environment needed for test case.

        Note: this method creates a file in the source repository.
        """
        env_file = self.envfile()
        env_vars = self.get_env_vars()

        if env_file and env_vars:
            try:
                with open(env_file, 'w') as f:
                    for env_var in env_vars:
                        f.write(env_var + '\n')
            except IOError:
                test.warning("Cannot update environment file: I/O error")

    #----------------------------------------------------------------------
    def command2run(self):
        """
        Get command to run.

        Default implementation returns empty string; the behavior is specified in
        subclasses.

        Returns:
            str: Command line to run the AUT.
        """
        return None

    #----------------------------------------------------------------------
    def after_start(self):
        """
        Perform post-start actions.

        Default implementation does nothing; the behavior is specified in
        subclasses.
        """
        pass

    def cleanup(self):
        """Clean-up runner."""
        try:
            currentApplicationContext().detach()
        except:
            pass
        snooze(1)

    #----------------------------------------------------------------------
    def putToXLS(status, text):
        """
        Write text data to XLS scenario export file.
        Arguments:
            status (str): Type of text data.
            text (str): Text data.
        """
        try:
            if self.xlsfile is None:
                # Initialize the file for XLS scenario export
                xls_export_file = os.path.join(self.casedir(), "xlsexport.xlexp")
                self.xlsfile = open(xls_export_file, "w")
            self.xlsfile.write("#[{status}] {text}\n".format(status=status, text=text))
        except IOError:
            test.warning("Cannot write XLS file: I/O error")

    #----------------------------------------------------------------------
    @staticmethod
    def _add_option(opt, opts):
        if opt not in opts:
            opts.append(opt)

    #----------------------------------------------------------------------
    @staticmethod
    def _rm_option(opt, opts):
        if opt in opts:
            opts.remove(opt)

#--------------------------------------------------------------------------
class StdRunner(Runner):
    """Runner for standalone AsterStudy application."""

    #----------------------------------------------------------------------
    config_file = 'AsterStudy.conf'
    config_var = 'AsterStudyConfig'

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor."""
        super().__init__()

    #----------------------------------------------------------------------
    def custom_env_vars(self):
        """Get additional environment variables as needed by the runner."""
        vars = super().custom_env_vars()
        vars.append('HOME = {}'.format(self.testdir()))
        return vars

    #----------------------------------------------------------------------
    def command2run(self):
        """Redefined from Runner class."""
        options = " ".join(self.case_options)
        return "asterstudy.sh {options}".format(options=options)

#--------------------------------------------------------------------------
class SalomeRunner(Runner):
    """ Runner for SALOME application."""

    #----------------------------------------------------------------------
    config_file = 'SalomeApp.xml'
    config_var = 'SalomeAppConfig'
    port_file = 'salomeport'

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor."""
        super().__init__()
        self.port = None

    #----------------------------------------------------------------------
    def command2run(self):
        """Redefined from Runner class."""
        options = " ".join(self.case_options)
        rcfile = os.path.join(self.testdir(), self.config_file)
        portfile = os.path.join(self.testdir(), self.port_file)
        return "raster {options} -r {rcfile} --ns-port-log={portfile}".format(options=options,
                                                                              rcfile=rcfile,
                                                                              portfile=portfile)

    #----------------------------------------------------------------------
    def after_start(self):
        """Redefined from Runner class."""
        if self.port is not None:
            return
        portfile = os.path.join(self.testdir(), self.port_file)
        try:
            with open(portfile) as fid:
                self.port = fid.readlines()[0].strip()
                test.log("SALOME is running on port %s" % self.port)
        except IOError:
            pass

    def cleanup(self):
        """Redefined from Runner class."""
        super().cleanup()
        if self.port:
            import subprocess
            test.log("Killing SALOME session on port %s" % self.port)
            process = subprocess.Popen("killSalomeWithPort.py %s" % self.port, shell=True, stdout=subprocess.PIPE)
            process.wait()
            self.port = None
        else:
            test.log('Error: there is no SALOME session running')
        snooze(5)

#--------------------------------------------------------------------------
# General purpose functions
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
def casedir():
    """Get test case's source directory. See `Runner.casedir()`."""
    return Runner.casedir()

#--------------------------------------------------------------------------
def suitedir():
    """Get test suite's source directory. See `Runner.suitedir()`."""
    return Runner.suitedir()

#--------------------------------------------------------------------------
def resultdir():
    """Get test case's resulr directory. See `Runner.resultdir()`."""
    return Runner.resultdir()

#--------------------------------------------------------------------------
def testdir():
    """Get test case's output directory. See `Runner.testdir()`."""
    return Runner.testdir()

#--------------------------------------------------------------------------
def breakTest(msg, details = ""):
    """
    Stop test case execution with fail message.
    Arguments:
        msg (str): Error message.
        details (str): Error details.
    Raises:
        RuntimeError: To immediately stop test execution.
    """
    test.fail(msg, details)
    raise RuntimeError("Test execution is forcibly aborted!")

#--------------------------------------------------------------------------
def currentDateTime(format="%d/%m/%Y %H:%M"):
    """
    Get current time stamp in given format.
    Arguments:
        format (str): Format string.
    Returns:
        str: String representation of current time stamp.
    """
    now = datetime.datetime.now()
    return str(now.strftime(format))

#--------------------------------------------------------------------------
def username():
    """
    Get user's name.
    Returns:
        str: User's ID.
    """
    return pwd.getpwuid(os.getuid()).pw_name

#--------------------------------------------------------------------------
# Menu management functions
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
def activateMenuItem(*path):
    """
    Activate main menu item.
    For example:
        activateMenuItem("File", "New")
    Arguments:
        *path: Menu item's path.
    """
    menu = ":AsterStudy_QMenuBar"
    waitForObject(menu)
    for index, item in enumerate(path):
        waitForObject(menu)
        activateItem(waitForObjectItem(menu, item))
        if index < len(path)-1:
            menu = "{title='%s' type='QMenu' unnamed='1' visible='1'}" % (item)

#--------------------------------------------------------------------------
def popupItem(*path):
    """
    Activate popup menu item.
    For example:
        openContextMenu(selectItem("Stage_1"), 10, 10, 0)
        popupItem("Edit")
    Arguments:
        *path: Popup menu items.
    """
    menu = waitForObject("{type='QMenu' unnamed='1' visible='1'}")
    parent = "{type='QMenu' unnamed='1' visible='1'}"
    for index, item in enumerate(path):
        activateItem(waitForObjectItem(menu, item))
        if index < len(path)-1:
            menu = "{title='%s' type='QMenu' unnamed='1' visible='1'}" % (item)
            parent = menu

#--------------------------------------------------------------------------
def createNewStudy():
    """
    Create a new study.
    """
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    if salomeUnderTest():
        #[step] Activate 'AsterStudy' module
        clickButton(waitForObject(":SALOME*.AsterStudy_QToolButton"))

#--------------------------------------------------------------------------
def openFile(file_path, close_active=False, save_active=False):
    """
    Open given file via menu "File" / "Open".
    If file path is relative, it is searched in the test suite's shared
    data directory.
    For example:
        openFile("/absolute/path/to/file")
    Arguments:
        file_path (str): Path (absolute or relative) to the file to open.
        close_active (bool): Tells if currently open file should be closed.
        save_active (bool):  Tells if currently open file should be saved before closing.
    """
    activateMenuItem("File", "Open...")
    if not os.path.isabs(file_path):
        file_path = os.path.realpath(os.path.join(casedir(),
                                     findFile("testdata", file_path)))
    if not os.path.exists(file_path):
        breakTest("Requested file {}' does not exist.".format(file_path))
    realName = "{buddy=':QFileDialog.fileNameLabel_QLabel' name='fileNameEdit' type='QLineEdit' visible='1'}"  # :fileNameEdit_QLineEdit
    snooze(1)
    if close_active and waitForObject(":Close active study_MessageBox"):
        clickButton(waitForObject(":Close active study.Discard_QPushButton"))
    elif save_active and waitForObject(":Close active study_MessageBox"):
        clickButton(waitForObject(":Close active study.Save_QPushButton"))
    waitForObject(realName).setText(file_path)
    type(waitForObject(realName), "<Return>")

#--------------------------------------------------------------------------
def openAnotherFile(file_path):
    """
    Open given file via menu "File" / "Open" with closing of current study.
    See also `openFile()`.
    For example:
        openAnotherFile("/absolute/path/to/file")
    Arguments:
        file_path (str): Path (absolute or relative) to the file to open.
    """
    openFile(file_path, True)

#--------------------------------------------------------------------------
def openAnotherFileWithSaving(file_path):
    """
    Open given file via menu "File" / "Open" with saving and closing of current study.
    See also `openFile()`.
    For example:
        openAnotherFileWithSaving("/absolute/path/to/file")
    Arguments:
        file_path (str): Path (absolute or relative) to the file to open.
    """
    openFile(file_path, False, True)

#--------------------------------------------------------------------------
def saveFile(file_path):
    """
    Save given file via menu "File" / "Save As".
    For example:
        saveFile("/absolute/path/to/file")
        saveFile("file_name")
    See also `openFile()`.
    Arguments:
        file_path (str): Path (absolute or relative) to the file.
    Returns:
        str: Absolute file path.
    """
    if not os.path.isabs(file_path):
        file_path = os.path.normpath(os.path.join(testdir(), file_path))
    activateMenuItem("File", "Save As...")
    lineEditRealName = "{buddy=':QFileDialog.fileNameLabel_QLabel' name='fileNameEdit' type='QLineEdit' visible='1'}"
    saveBtnRealName = "{text='Save' type='QPushButton' unnamed='1' visible='1' window=':QFileDialog_QFileDialog'}"
    waitFor("object.exists('%s')" % lineEditRealName.replace("'", "\\'"), 2000)
    waitForObject(lineEditRealName).setText(file_path)
    type(waitForObject(lineEditRealName), "<Return>")
    snooze(1)
    return file_path

#--------------------------------------------------------------------------
def closeFile():
    """
    Activate "File" / "Close" menu item.
    """
    activateMenuItem("File", "Close")

#--------------------------------------------------------------------------
def importStage(file_path, click_list=[]):
    """
    Import stage from a file.
    Arguments:
        file_path (str): Path to the stage file.
        click_list (list[str]): Symbols (objects) to click additionally (warnings, etc).
    """
    waitForActivateMenuItem("Operations", "Add Stage from File")
    waitForObject(":fileNameEdit_QLineEdit").setText(file_path)
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))

    waitUntilObjectExists(":QFileDialog.Open_QPushButton")

    for symbol in click_list:
        clickButton(waitForObject(symbol))

    waitUntilWaitCursor()

#--------------------------------------------------------------------------
# Object browser management functions
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
def checkOBItem(*parentNames):
    """
    Verify that given item exists in Object Browser.
    For example:
        checkOBItem("CurrentCase", "Stage_1")
    Arguments:
        *parentNames: Path to the Object browser item.
    """
    treeObj = waitForObject(":_QTreeWidget")
    obj = getModelIndexByName(treeObj, *parentNames)
    if (not obj is None) and obj.isValid():
        test.compare(obj.text, parentNames[-1])
    else:
        s = ""
        for name in parentNames:
            s += "/" + str(name)
        test.fail("Unexpectedly failed to find the object in OB", s)
    return obj

#--------------------------------------------------------------------------
def checkOBItemNotExists(*parentNames):
    """
    Verify that given item doesn't exist in Object Browser.
    For example:
        checkOBItemNotExists("Geometry", "Box_1")
    Arguments:
        *parentNames: Path to the Object browser item.
    """
    treeObj = waitForObject(":_QTreeWidget")
    obj = getModelIndexByName(treeObj, *parentNames)
    s = ""
    for name in parentNames:
        s += "/" + str(name)
    if (not obj is None) and obj.isValid():
        test.fail("Unexpectedly object is found in OB", s)
    else:
        test.passes("Object '%s' wasn't found in OB" % s)

#--------------------------------------------------------------------------
def selectObjectBrowserItem(name, modifier=0):
    """
    Select item in the Object browser (simulate mouse click on it).
    Arguments:
        name (str): Object browser's item.
        modifier (Qt.KeyboardModifiers): Keyboard modifier(s).
    """
    clickItem(":_QTreeWidget", getItemText(name), 10, 1, modifier, Qt.LeftButton)

#--------------------------------------------------------------------------
def activateOBContextMenuItem(item, *path):
    """
    Activate context menu item for the given item in the Object Browser.
    For example:
        activateOBContextMenuItem("CurrentCase.Stage_1.Material", "Edit")
    Arguments:
        item (str): Object browser item's path.
        *path: Popup menu items.
    """
    try:
        name = getItemText(item)
        selectObjectBrowserItem(name)
        openItemContextMenu(waitForObject(":_SettingsView"), name, 5, 5, 0)
        popupItem(*path)
    except LookupError as err:
        breakTest("Unexpectedly failed to find the object in OB", str(err))

#------------------------------------------------------------------------------
def showOBIndex(*path):
    """
    Select ObjectBrowser item unfolding all its parents if necessary.
    See also `showOBItem()`.
    Arguments:
        *path: Object browser item's path.
    Returns a tuple: Object's path and model index.
    """
    #--------------------------------------------------------------------------
    escaped_path = [ escape(item) for item in path ]
    for idx in range(1, len(escaped_path) + 1):
        full_path = '.'.join(escaped_path[ : idx ])
        index = waitForObjectItem(waitForObject(":_QTreeWidget"), full_path)
        if index.collapsed != True:
            continue

        clickItem(":_QTreeWidget", full_path, -10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    return full_path, index

#------------------------------------------------------------------------------
def showOBItem(*path):
    """
    Select ObjectBrowser item unfolding all its parents if necessary.
    See also `showOBIndex()`.
    Arguments:
        *path: Object browser item's path.
    Returns object's path.
    """
    #--------------------------------------------------------------------------
    return showOBIndex(*path)[0]

#------------------------------------------------------------------------------
def checkValidity(expected, *path):
    """
    Check item's validity status.
    Arguments:
        expected (int). Expected validity status.
        *path: Object browser item's path.
    """
    #--------------------------------------------------------------------------
    item, index = showOBIndex(*path)
    valid = index.data(Qt.UserRole + 4).toBool()
    test.compare(valid, expected)
    if valid != expected:
        pass # ???

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def isValid(expected, *path):
    """
    Check item's validity status.
    Arguments:
        expected (int). Expected validity status.
        *path: Object browser item's path.
    """
    #--------------------------------------------------------------------------
    item = selectItem(*path)
    index = waitForObject(item)
    valid = index.data(Qt.UserRole + 4).toBool()
    test.compare(valid, expected)
    if valid != expected:
        pass # ???

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def _selectChild(child, text, item, occurrence):
    #--------------------------------------------------------------------------
    if ":" in text:
        name, uid = text.split(':')
    else:
        name, uid = text, None

    item = "{column='0' container=%s occurrence='%d' text='%s' type='QModelIndex'}" % (item, occurrence, name)

    if not child.isValid():
        return -1, item, occurrence

    if child.data(Qt.DisplayRole) != name:
        return 0, item, occurrence

    occurrence += 1

    if uid is not None:
        if child.data(Qt.UserRole + 1) != int(float(uid)):
            return 0, item, occurrence

    waitForObject(item)

    return 1, item, occurrence

#------------------------------------------------------------------------------
def checkItemExists(*path):
    """
    Check that Object browser's item exists.
    Arguments:
        *path: Object browser item's path.
    """
    #--------------------------------------------------------------------------
    parent = "{column='0' container=':_QTreeWidget' text='CurrentCase' type='QModelIndex'}"
    root = waitForObject(parent)

    treeWidget = waitForObject(":_QTreeWidget")
    selectionModel = treeWidget.selectionModel()

    item_path = list(path)
    while len(item_path):
        text = item_path.pop(0)

        row = 0
        status = 0
        occurrence = 1
        while status == 0:
            child = root.child(row, 0)
            status, item, occurrence = _selectChild(child, text, parent, occurrence)

            if status == 0:
                row += 1
                continue

            if status == 1:
                parent = item
                root = child
                break

            return False, child

    selectionModel.setCurrentIndex(root, QItemSelectionModel.ClearAndSelect)

    waitForObject(parent)
    test.verify(object.exists(parent))

    return True, parent


#------------------------------------------------------------------------------
def isItemExists(*path):
    """
    Check that Object browser's item exists.
    Arguments:
        *path: Object browser item's path.
    """
    exists, item = checkItemExists(*path)
    return exists

#------------------------------------------------------------------------------
def checkItemNotExists(*path):
    """
    Check that Object browser's item does not exist.
    Arguments:
        *path: Object browser item's path.
    """
    #--------------------------------------------------------------------------
    test.verify(not isItemExists(path))

#------------------------------------------------------------------------------
def selectItem(*path):
    """
    Select Object browser's item.
    Arguments:
        *path: Object browser item's path.
    """
    exists, item = checkItemExists(*path)
    if not exists:
        test.fail("Can't find %s item" % '/'.join(path))
        raise Exception("Can't find %s item" % '/'.join(path))
    else:
        return item

#------------------------------------------------------------------------------
def isItemSelected(*path):
    """
    Check that Object browser's item is selected.
    Arguments:
        *path: Object browser item's path.
    """
    #--------------------------------------------------------------------------
    item_path = list(path)
    parent = "{column='0' container=':_QTreeWidget' text='%s' type='QModelIndex'}" % (item_path.pop(0).split(':')[0])

    while len(item_path):
        text = item_path.pop(0)

        row = 0
        status = 0
        occurrence = 1
        while status == 0:
            root = waitForObject(parent)
            child = root.child(row, 0)
            status, item, occurrence = _selectChild(child, text, parent, occurrence)

            if status < 0:
                return False

            if status == 1:
                parent = item
                root = child
                break

            row += 1
            pass

        pass

    item = waitForObject(parent)
    isSelected = item.selected
    return isSelected

#------------------------------------------------------------------------------
def isItemExpanded(*path):
    """
    Check that Object browser's item is expanded.
    Arguments:
        *path: Object browser item's path.
    """
    #--------------------------------------------------------------------------
    item_path = list(path)
    parent = "{column='0' container=':_QTreeWidget' text='%s' type='QModelIndex'}" % (item_path.pop(0).split(':')[0])
    root = waitForObject(parent)
    isExpanded = not root.collapsed

    while len(item_path):
        text = item_path.pop(0)

        row = 0
        status = 0
        occurrence = 1
        while status == 0:
            root = waitForObject(parent)
            isExpanded = isExpanded and not root.collapsed
            child = root.child(row, 0)
            status, item, occurrence = _selectChild(child, text, parent, occurrence)

            if status < 0:
                return False

            if status == 1:
                parent = item
                root = child
                break

            row += 1
            pass

        pass

    return isExpanded

#--------------------------------------------------------------------------
# Auxiliary functions
#--------------------------------------------------------------------------

#------------------------------------------------------------------------------
def waitUntilObjectExists(symbolic_name, timeoutSecs=20):
    """
    Can be used to wait for some object to disappear.
    For instance, waiting for saving of a stage or a study:
        waitUntilObjectExists(":QFileDialog.Save_QPushButton")
    """
    while object.exists(symbolic_name) and timeoutSecs > 0:
        snooze(1)
        timeoutSecs -= 1
        if timeoutSecs < 0:
            test.fail("Object '%s' still exists." % symbolic_name)
    return True

#------------------------------------------------------------------------------
def salomeUnderTest():
    """Check if we run SALOME test case."""
    return objectMap.realName(':AsterStudy_MainWindow').count("type='STD_TabDesktop'") != 0

#--------------------------------------------------------------------------
def getChildModelIndex(parentIdx, name):
    """
    Get the QModelIndex for child with given name and parent index.
    Warning: may work incorrectly for items with duplicated names.
    Arguments:
        parentIdx (QModelIndex): Parent model index.
        name (str): Item name.
    Returns:
        QModelIndex: Model index.
    """
    row = 0
    if parentIdx is None:
        return None
    childIdx = parentIdx.child(row, 0)
    while childIdx.isValid():
        if name == str(childIdx.data().toString()):
            return childIdx
        row += 1
        childIdx = childIdx.sibling(row, 0)
    return None

#--------------------------------------------------------------------------
def getModelIndexByName(treeObj, *parentNames):
    """
    Get the QModelIndex for given tree by list of hierarchical names
    For example:
        getModelIndexByName(tree, "Geometry", "Box_1")
    Arguments:
        treeObj (object): Object's symbolic name.
        *parentNames: Item name path's components.
    Returns:
        QModelIndex: Model index.
    """
    parentNodeName = parentNames[0]  # .replace('_',"\\\\_")
    # get some item
    root = treeObj.indexAt(QPoint(0, 0))
    # get it's root item
    while True:
        if root.text == parentNodeName:
            break
        obj = root.parent()
        if not obj.isValid():
            break
        root = obj
    # get requested root item
    row = 0
    while root.isValid() and root.text != parentNodeName :
        root = root.sibling(row, 0)
        row += 1
    # get requested child item
    for name in parentNames[1:]:
        idx = getChildModelIndex(root, name)
        root = idx
    return root

#--------------------------------------------------------------------------
def getItemText(item):
    """
    Convert normalized item's name (valid for Squish).
    Arguments:
        item (str): Item's name.
    Returns:
        str: Normalized item's name.
    """
    return item.replace('_', "\\\\_")

#------------------------------------------------------------------------------
def escape(name):
    """Translate a 'GUI' name into Squish one."""
    return name.replace("_", "\\_").replace('.', '\\.')

#------------------------------------------------------------------------------
def unescape(name):
    """Translate a Squish internal name into 'GUI' one."""
    return name.replace("\\", "")

#--------------------------------------------------------------------------
def setInputFieldValue(symbol, value):
    """
    Set the given value to the input field.
    Arguments:
       symbol (object): Object's symbolic name.
       value (str): Value.
    """
    waitForObject(symbol).setText(value)

#--------------------------------------------------------------------------
def setParameterValue(symbol, value):
    """
    Set given into the given input field.
    Note: this is improved version of `setInputFieldValue()`.
    Arguments:
       symbol (object): Object's symbolic name.
       value (str): Value.
    """
    obj = waitForObjectExists(symbol)
    if obj.inherits('QComboBox'):
        idx = -1
        for i in range(obj.count):
            if obj.itemText(i) == value:
                idx = i
                break
        if idx >= 0:
            obj.setCurrentIndex(idx)
        elif obj.isEditable():
            obj.setCurrentText(value)
            test.compare(str(obj.currentText), value)
    elif obj.inherits('QLineEdit'):
        obj.setText(value)
        if symbol != ":fileNameEdit_QLineEdit":
            test.compare(str(obj.text), value)

#------------------------------------------------------------------------------
def waitUntilWaitCursor(secs = 1.0):
    """Wait until override cursor is not cleared."""
    #--------------------------------------------------------------------------
    try:
        currentApplicationContext()
        waitFor("QApplication")
        snooze(secs)
        if not QApplication.overrideCursor():
            return
        waitFor("isNull(QApplication.overrideCursor()) or QApplication.overrideCursor().shape() == Qt.WaitCursor")
    except (TypeError, RuntimeError) as exc:
        test.fail("waitUntilWaitCursor - %s" % exc)
        raise exc
    finally:
        snooze(secs)

#------------------------------------------------------------------------------
def waitForActivateItem(item):
    waitUntilWaitCursor()
    activateItem(item)
    waitUntilWaitCursor()

#------------------------------------------------------------------------------
def waitForDoubleClick(objectOrName, x, y, modifierState, button):
    waitUntilWaitCursor()
    doubleClick( objectOrName, x, y, modifierState, button )
    waitUntilWaitCursor()

#------------------------------------------------------------------------------
def waitForActivateMenuItem(*path):
    waitUntilWaitCursor()
    activateMenuItem(*path)
    waitUntilWaitCursor()

#------------------------------------------------------------------------------
def waitForActivateOBContextMenuItem(item, *path):
    waitUntilWaitCursor()
    activateOBContextMenuItem(item, *path)
    waitUntilWaitCursor()

#------------------------------------------------------------------------------
def waitForPopupItem(*path):
    waitUntilWaitCursor()
    popupItem(*path)
    waitUntilWaitCursor()

#------------------------------------------------------------------------------
def to_unicode(string):
    """Convert string into unicode."""
    if isinstance(string, str):
        return string
    for encoding in ('utf-8', 'iso-8859-15', 'cp1252'):
        try:
            return string.decode(encoding)
        except UnicodeDecodeError:
            pass
    return string.decode('utf-8', 'replace')

#------------------------------------------------------------------------------
def check_text_diff(ref, cur, debug=True):
    """Compare text data."""
    ref = to_unicode(ref.strip())
    cur = to_unicode(cur.strip())
    if ref != cur:
        from difflib import context_diff
        for line in context_diff(ref.split('\n'), cur.split('\n')):
            if debug:
                print(line)
        return False

    return True

#------------------------------------------------------------------------------
def check_text_eq(ref, cur):
    """Check that text data is equal to reference one."""
    return check_text_diff(ref, cur, debug=True)

#------------------------------------------------------------------------------
def check_text_ne(ref, cur):
    """Check that text data is not equal to reference one."""
    return not check_text_diff(ref, cur, debug=False)

#--------------------------------------------------------------------------
# Test case management
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
def global_start(**kwargs):
    """
    Prepare test data and start application.
    This method is normally called at the beginning of each test case.
    Arguments:
        **kwargs: Options to run test case.
    """
    # Get / create runner
    runner = Runner.get_runner()
    # Start runner
    runner.start(**kwargs)

#--------------------------------------------------------------------------
def global_cleanup():
    """
    Finalize test case and clean-up working directory (remove temporary files and folder, etc).
    This method is normally called at the end of each test case.
    """
    try:
        Runner.runner.finalize()
    except:
        test.warning("There is no active application runner")

#--------------------------------------------------------------------------
def putToXLS(status, text):
    """
    Write text data to XLS scenario export file.
    See `Runner.putToXLS()`.
    """
    if Runner.runner is None:
        test.warning("Cannot write XLS file: there is no active runner")
    else:
        Runner.runner.putToXLS(status, text)

#--------------------------------------------------------------------------
# Dialogs management
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
def addCommandByDialog(command, group):
    """
    Add command to the study via 'Show All' dialog.
    Arguments:
        command (str): Code_aster command.
        group (str): Code_aster category.
    """
    clickButton(waitForObject(":AsterStudy *.Show All_QToolButton"))
    waitForObject(":_QLineEdit").setText(command)

    aboveWidget = "{container=':qt_tabwidget_stackedwidget_QWidget' text~='%s.*' type='QToolButton' unnamed='1' visible='1'}" % group
    grpListView = "{aboveWidget=%s container=':qt_tabwidget_stackedwidget_QWidget' type='QListWidget' unnamed='1' visible='1'}" % aboveWidget

    waitForObjectItem(grpListView, escape(command))
    clickItem(grpListView, escape(command), 10, 10, 0, Qt.LeftButton)

    clickButton(waitForObject(":OK_QPushButton"))

#------------------------------------------------------------------------------
def tr(name, type, occurrence=None):
    #--------------------------------------------------------------------------
    container = ":qt_tabwidget_stackedwidget_QWidget"
    pattern = "{container='%s' name='%s' type='%s' visible='1'}"
    res = pattern % (container, name, type)
    if occurrence:
        pattern = "{container='%s' name='%s' occurrence='%d' type='%s' visible='1'}"
        res = pattern % (container, name, occurrence, type)

    #--------------------------------------------------------------------------
    return res

#------------------------------------------------------------------------------
Skip = 'Skip'
Reuse = 'Reuse'
Execute = 'Execute'

#------------------------------------------------------------------------------
def trex(name, type, occurrence=None):
    #--------------------------------------------------------------------------
    container = ":qt_tabwidget_stackedwidget_QWidget"
    pattern = "{container='%s' name~='%s' type='%s' visible='1'}"
    res = pattern % (container, name, type)
    if occurrence:
        pattern = "{container='%s' name='%s' occurrence='%d' type='%s' visible='1'}"
        res = pattern % (container, name, occurrence, type)

    #--------------------------------------------------------------------------
    return res

#--------------------------------------------------------------------------
def run_option(stage):
    """Returns current run option for given stage."""
    #--------------------------------------------------------------------------
    buttons_layout = waitForObject("{type='QHBoxLayout' name='%s:SelectorButtons'}" % stage)
    for i in range(buttons_layout.count()):
        button = buttons_layout.itemAt(i).widget()
        if button.property('checked') == 'true':
            return str(button.objectName).replace('{}:'.format(stage), '')

#--------------------------------------------------------------------------
def run_options(stage, options, target = None):
    """Check available run options for given stage and optionally selects new one."""
    #--------------------------------------------------------------------------
    available = set()
    buttons_layout = waitForObject("{type='QHBoxLayout' name='%s:SelectorButtons'}" % stage)
    for i in range(buttons_layout.count()):
        button = buttons_layout.itemAt(i).widget()
        if button.enabled:
            option = str(button.objectName).replace('{}:'.format(stage), '')
            available.add(option)
    
    if available != options:
        breakTest("Wrong set of run options for stage %s" % stage)
    
    if target is not None:
        use_option(stage, target)

#------------------------------------------------------------------------------
def use_option(stage, target, force=False):
    """
    Selects run option for given stage. Breaks test if option is unavailable.
    
    If `force=True`, then this will click on the button anyway, even if it is already checked.
    (This can be useful for testing that nothing changes after second click on a button.)
    """
    #--------------------------------------------------------------------------
    current = run_option(stage)
    if current == target and not force:
        return

    button = waitForObject("{type='QToolButton' name='%s:%s'}" % (stage, target))
    clickButton(button)

    new_current = run_option(stage)
    if new_current == current and new_current != target:
        breakTest("State is not accessible: %s (current is %s)" % (target, current))

#------------------------------------------------------------------------------
def keep_results(stage, value):
    """Switch 'keep results' option ON/OFF."""
    #--------------------------------------------------------------------------
    waitForObject("{type='QCheckBox' name='%s:Reusable'}" % stage).setChecked(value)

#------------------------------------------------------------------------------
def open_run_dialog(path=None):
    """Prepare 'Run' panel."""
    clickTab(waitForObject(":Data Settings_QTabWidget"), "Advanced")
    if path is not None:
        setParameterValue(":DashboardRunDialog.path_QLineEdit", path)
    return str(waitForObjectExists(":DashboardRunDialog.path_QLineEdit").text)

#------------------------------------------------------------------------------
def execute_case():
    """Activate Run Case operation by pressing 'Run' button."""
    symbol = tr('Run', 'QPushButton')
    clickButton(waitForObject(symbol))

#------------------------------------------------------------------------------
def wait_for_case(case):
    """Wait for case to finish running."""
    item = "Run Cases."+escape(case)
    waitForObjectItem(":CasesTreeView_QTreeView", item)
    clickItem(":CasesTreeView_QTreeView", item, 65, 11, 0, Qt.LeftButton)

    while True:
        log = str(waitForObjectExists(":_TextEdit").plainText)
        success_line = 'Run case "%s" calculations process finished' % case
        fail_line = 'calculation failed. Interruption'
        if log.endswith(success_line):
            # SUCCESS run
            break
        elif log.endswith(fail_line):
            # Remove failed run case
            mouseClick(waitForObject(":_QGraphicsItem"), -1, 17, 0, Qt.LeftButton)
            openContextMenu(waitForObject(":_QGraphicsItem"), -1, 17, 0)
            activateItem(waitForObjectItem(":SALOME*.OperationsToolbar_QMenu", "Delete"))
            clickButton(waitForObject(":Delete.Yes_QPushButton"))
            # Start case calculation again
            run_case(case, path)
            break
        else:
            snooze(1)

    #snooze(1)

#------------------------------------------------------------------------------
def run_case(case, path=None):
    """Run case."""
    files_path = open_run_dialog(path)
    execute_case()
    wait_for_case(case)
    return files_path

#------------------------------------------------------------------------------
def create_dummy_file(filename, dirname=None):
    """Create dummy file for test purposes."""
    if not dirname:
        dirname = testdir()
    if not os.path.isabs(filename):
        filename = os.path.join(dirname, filename)
    with open(filename, 'w'):
        pass
    return filename

#------------------------------------------------------------------------------
def exec_in_console(code, commit=True):
    """Execute code in a embedded Python console"""
    # show console
    waitForActivateMenuItem("View", "Console")
    # get console object
    console = waitForObject(":AsterStudy *_PyConsole_Editor")
    # paste code into the console
    setClipboardText("from asterstudy.gui.asterstdgui import AsterStdGui\n")
    console.paste()
    setClipboardText(code + '\n')
    console.paste()
    # commit changes to study
    if commit:
        setClipboardText("AsterStdGui.gui.study().commit('Execute code')\nAsterStdGui.gui.update()\n")
        console.paste()
    # hide console
    activateMenuItem("View", "Console")
