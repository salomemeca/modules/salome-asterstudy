#------------------------------------------------------------------------------
# Issue #1675 - Subcategories management

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # create empty stage in current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # check that sub-categories are shown in ShowAll dialog 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Show All")
    test.vp("VP1")
    mouseClick(waitForObjectItem(":_QListWidget", "Sub-category 1"), 5, 5, 0, Qt.LeftButton)
    test.compare(waitForObjectExists( ":OK_QPushButton" ).enabled, False)
    test.compare(waitForObjectExists( ":Apply_QPushButton" ).enabled, False)
    mouseClick(waitForObjectItem(":_QListWidget", "LIRE\\_MAILLAGE"), 5, 5, 0, Qt.LeftButton)
    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, True )
    test.compare( waitForObjectExists( ":Apply_QPushButton" ).enabled, True )
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # check that sub-categories are shown in main menu 
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    waitForActivateMenuItem("Commands", "Category", "Sub-category 1", "DEFI_GROUP")
    
    #--------------------------------------------------------------------------
    # check that sub-categories are shown in context menu 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Category", "Sub-category 1", "DEFI_GROUP")
    
    #--------------------------------------------------------------------------
    # check that sub-categories are shown in drop-down toolbar combo-box 
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 5, 5, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "Sub-category 1"), 5, 5, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
