#------------------------------------------------------------------------------
# Issue # 1016 - L3 and L1.3 Use case around the file management
#   - crash on reading MED mesh groups

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )

    global_start(noexhook=True)
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def newStudy():
    #--------------------------------------------------------------------------
    # [section] New study creation
    test.log( "New study creation" )

    # [step] Create a new study
    createNewStudy()
    # [step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    # [check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def newStage():
    #--------------------------------------------------------------------------
    # [section] Stage creation and deletion
    test.log( "Stage creation and deletion" )

    # [step] Create a new stage by toolbar button
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )
    # [check] 'Stage' was added to tree
    checkOBItem( "CurrentCase", "Stage_1" )
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def _1_Mesh():
    #--------------------------------------------------------------------------
    # [section] Creation LIRE_MAILLAGE command named 'Mesh'
    test.log( "Creation LIRE_MAILLAGE command named 'Mesh'" )

    # [step] Select 'Stage' in tree
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    # [step] Click 'Mesh' combobox in toolbar and select 'LIRE_MAILLAGE' item
    mouseClick( waitForObject( ":AsterStudy *_ShrinkingComboBox" ), 59, 15, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( ":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE" ), 51, 11, 0, Qt.LeftButton )

    # [step] Click 'Rename' in context menu
    activateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.LIRE_MAILLAGE", "Rename" )
    # [step] Type 'Mesh' and press <Enter>
    setParameterValue( ":_QExpandingLineEdit", "Mesh" )
    type( waitForObject( ":_QExpandingLineEdit" ), "<Return>" )
    # [check] 'LIRE_MAILLAGE' was renamed to 'Mesh'
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "Mesh" )

    # [step] Click 'Edit' in context menu
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.Mesh", "Edit" )
    # [check] 'Edit command' panel was opened
    # [check] Command is 'LIRE_MAILLAGE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "LIRE_MAILLAGE" )
    # [check] Command name is 'Mesh'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Mesh" )

    # [step] Check 'FORMAT' checkbox
    symbol = tr( 'FORMAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    # [step] Select 'MED' in 'FORMAT' combobox
    mouseClick(waitForObject(":FORMAT_QComboBox"), 14, 12, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":FORMAT_QComboBox", "MED"), 14, 6, 0, Qt.LeftButton)

    # [step] Click '...' browse button
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    # [step] Open 'Mesh_recette.med' file
    med_file_path = os.path.join( casedir(), findFile( "testdata", "Mesh_recette.med" ) )
    setParameterValue( ":fileNameEdit_QLineEdit", med_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    # [check] 'Mesh_recette.med' file name was added to combobox and activated
    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( med_file_path ) )

    # [step] Click OK button to accept the changes
    clickButton( waitForObject( ":OK_QPushButton" ) )
    # [check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def _2_Mesh():
    #--------------------------------------------------------------------------
    # [section] Creation MODI_MAILLAGE command named 'Mesh'
    test.log( "Creation MODI_MAILLAGE command named 'Mesh'" )

    # [step] Click 'Mesh' combobox in toolbar and select 'MODI_MAILLAGE' item
    mouseClick( waitForObject( ":AsterStudy *_ShrinkingComboBox" ), 59, 15, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( ":AsterStudy *_ShrinkingComboBox", "MODI\\_MAILLAGE" ), 51, 11, 0, Qt.LeftButton )

    # [step] Double click on 'MODI_MAILLAGE' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Mesh.MODI\\_MAILLAGE", 59, 10, 0, Qt.LeftButton )
    # [check] 'Edit command' panel was opened
    # [check] Command is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )
    # [check] Command name is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "automatic" )

    # [step] Type 'Mesh' in 'Name' input field instead of 'MODI_MAILLAGE'
    setParameterValue( ":Name_QLineEdit", "Mesh" )

    # [step] Select 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    setParameterValue( symbol, 'Mesh' )

    # [step] Check 'ORIE_PEAU_2D' checkbox
    symbol = tr( 'ORIE_PEAU_2D', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    # [step] Click the enabled 'Edit...' button
    symbol = tr( 'ORIE_PEAU_2D', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    # [check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D" )
    # [step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    # [check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    # [step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    # [check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0]" )
    # [step] Click 'Edit...' button of 'GROUP_MA' keyword
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    # [check] 'GROUP_MA' panel was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0] > GROUP_MA" )
    # [check] 'Mesh' mesh is set in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Mesh' )

    # [step] Type 'Cont_mast, Cont_slav' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    setParameterValue( symbol, "Cont_mast,Cont_slav" )

    # [step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    # [check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0]" )
    # [check] 'Edit...' button of 'GROUP_MA' keyword was renamed to selected entities
    test.compare( waitForObject( ":GROUP_MA_content" ).text, "('Cont_mast', 'Cont_slav')" )

    # [step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    # [check] Panel of list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D" )

    # [step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    # [check] Command's panel was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )

    # [step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    # [check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    # [check] 'MODI_MAILLAGE' was renamed to 'Mesh'
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "Mesh" )
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    newStudy()
    newStage()
    _1_Mesh()
    _2_Mesh()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
