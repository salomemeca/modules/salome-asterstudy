from os import rmdir
from tempfile import mkdtemp

def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    in1 = mkdtemp()
    out1 = mkdtemp()
    in2 = mkdtemp()
    out2 = mkdtemp()
    
    try:
        waitForActivateMenuItem( "File", "New" )
        clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
        activateMenuItem( "Operations", "Add Stage" )
    
        openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 42, 5, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Create Variable"))
        type(waitForObject(":Name_QLineEdit_2"), "x")
        type(waitForObject(":Expression_QLineEdit"), "123")
        clickButton(waitForObject(":OK_QPushButton"))
        
        # leave empty fields, close
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        
        # enter paths, try closing
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), in1)
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), out1)
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
        
        # enter paths, save, close
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), in1)
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), out1)
        clickButton(waitForObject(":OK_QPushButton"))
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        
        # leave paths unmodified, close
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        test.compare(in1, waitForObject(":dirs_panel_in_dir_QLineEdit").text)
        test.compare(out1, waitForObject(":dirs_panel_out_dir_QLineEdit").text)
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        
        # modify paths, try closing
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        test.compare(in1, waitForObject(":dirs_panel_in_dir_QLineEdit").text)
        test.compare(out1, waitForObject(":dirs_panel_out_dir_QLineEdit").text)
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), in2)
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), out2)
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
        
        # modify paths, save, close
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        test.compare(in1, waitForObject(":dirs_panel_in_dir_QLineEdit").text)
        test.compare(out1, waitForObject(":dirs_panel_out_dir_QLineEdit").text)
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), in2)
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), out2)
        clickButton(waitForObject(":OK_QPushButton"))
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        
        # clear fields, try closing
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        test.compare(in2, waitForObject(":dirs_panel_in_dir_QLineEdit").text)
        test.compare(out2, waitForObject(":dirs_panel_out_dir_QLineEdit").text)
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), "<Delete>")
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), "<Delete>")
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
        
        # clear fields, save, close
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
        test.compare(in2, waitForObject(":dirs_panel_in_dir_QLineEdit").text)
        test.compare(out2, waitForObject(":dirs_panel_out_dir_QLineEdit").text)
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_in_dir_QLineEdit"), "<Delete>")
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":dirs_panel_out_dir_QLineEdit"), "<Delete>")
        clickButton(waitForObject(":OK_QPushButton"))
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
        
        # one more action, to be sure we are not seeing a dialog now
        openContextMenu(waitForObjectItem(":_SettingsView", "1"), 33, 7, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))
    
    finally:
        rmdir(in1)
        rmdir(out1)
        rmdir(in2)
        rmdir(out2)