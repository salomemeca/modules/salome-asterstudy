#------------------------------------------------------------------------------
# Issue #1152 - Edition panel: management of Python variables (CCTP 2.3.2)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1152

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )

    #--------------------------------------------------------------------------
    snippet = \
"""
vitesse = 1.e-5

t_0 = 5.e-2 / (8.0 * vitesse)

c1 = DEFI_CONSTANTE(VALE=t_0)
"""
    setClipboardText( snippet )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )
    waitForObject( ":_QTreeWidget" ).expandAll()
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "vitesse" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "vitesse" )
    setParameterValue( tr( 'Expression', 'QLineEdit' ), "'text'" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "'text'" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase", "Stage_1", "Variables", "vitesse" )
    checkValidity( False, "CurrentCase" )

    #--------------------------------------------------------------------------
    checkValidity( False, "CurrentCase", "Stage_1", "Variables", "t_0" )

    openContextMenu( selectItem( "Stage_1", "Variables", "t_0" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )
    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, False )

    test.compare( str( waitForObjectExists( tr( 'Name', 'QLineEdit' ) ).text ), "t_0" )
    test.compare( str( waitForObjectExists( tr( 'Expression', 'QLineEdit' ) ).text ), "5.e-2 / (8.0 * vitesse)" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkValidity( False, "CurrentCase", "Stage_1", "Functions and Lists", "c1" )

    openContextMenu( selectItem( "Stage_1", "Functions and Lists", "c1" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )
    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, True )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "t_0" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( tr( 'Expression', 'QLineEdit' ), "625.0" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "625.0" )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    checkValidity( True, "CurrentCase", "Stage_1", "Variables", "t_0" )
    checkValidity( True, "CurrentCase", "Stage_1", "Functions and Lists", "c1" )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_1" )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )

    snippet2 = \
"""
vitesse = 'text'

t_0 = 625.0

c1 = DEFI_CONSTANTE(VALE=t_0)
"""

    actual = str( waitForObject( text_editor ).toPlainText() )
    test.verify( check_text_eq( snippet2, actual ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
