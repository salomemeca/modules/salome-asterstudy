import re
from os.path import dirname

def main():
    source(findFile('scripts', 'common.py'))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def add_operation_with_file(stage_name, var_name, filename):
    mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 50, 14, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 56, 7, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.{}.Mesh.{}".format(stage_name, var_name)), 37, 10, 0, Qt.LeftButton)
    choose_file(filename)

def choose_file(filename):
    clickButton(waitForObject(":UNITE_QPushButton"))
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + '/' + filename)
    type(waitForObject(":_QListView"), "<Return>")
    clickButton(waitForObject(":OK_QPushButton"))

def datafilessummary_items():
    model = waitForObject(":DataFilesSummary_Tree").model()
    
    actual = []
    for row in range(model.rowCount()):
        index = model.index(row, 1)
        label = str(model.data(index, Qt.DisplayRole))
        tooltip = model.data(index, Qt.ToolTipRole)
        stage_name, file_path = re.match(r'^<pre><b>Stage</b>: (.+)<br/><b>File</b>: (.+)</pre>$', str(tooltip)).groups()
        actual.append((label, stage_name, file_path))
    
    return tuple(actual)

def runCase():
    # Create new study
    waitForActivateMenuItem('File', 'New')
    clickTab(waitForObject(":AsterStudy *.Vue Cas_QTabWidget"), "Case View")

    # Stage 1
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))
    add_operation_with_file('Stage\\_1', 'mesh', '1.txt')
    add_operation_with_file('Stage\\_1', 'mesh0', '2.txt')
    
    # Stage 2
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))
    add_operation_with_file('Stage\\_2', 'mesh1', '3.txt')
    
    # Switch to History view
    # Check that all three files are listed
    clickTab(waitForObject(":AsterStudy *.Vue Cas_QTabWidget"), "History View")
    test.compare((
        ('1.txt', 'Stage_1', dirname(__file__)+'/1.txt'),
        ('2.txt', 'Stage_1', dirname(__file__)+'/2.txt'),
        ('3.txt', 'Stage_2', dirname(__file__)+'/3.txt'),
    ), datafilessummary_items())
    
    # Replace: 1,2,3 → 1,2,4
    openContextMenu(waitForObjectItem(":DataFilesSummary_Tree", "3\\.txt"), 80, 7, 0)
    activateItem(waitForObjectItem(":AsterStudy *.StandardToolbar_QMenu", "Edit..."))
    clickButton(waitForObject(":DataFilesSummary_EditingWidget_Button"))
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + '/4.txt')
    type(waitForObject(":fileNameEdit_QLineEdit"), '<Return>')
    test.compare((
        ('1.txt', 'Stage_1', dirname(__file__)+'/1.txt'),
        ('2.txt', 'Stage_1', dirname(__file__)+'/2.txt'),
        ('4.txt', 'Stage_2', dirname(__file__)+'/4.txt'),
    ), datafilessummary_items())

    # Make a backup case
    # We shouldn't see the same files
    openContextMenu(waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase"), 94, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Back Up"))
    mouseClick(waitForObjectItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1"), 77, 7, 0, Qt.LeftButton)
    test.compare((
        ('1.txt', 'Stage_1', dirname(__file__)+'/1.txt'),
        ('2.txt', 'Stage_1', dirname(__file__)+'/2.txt'),
        ('4.txt', 'Stage_2', dirname(__file__)+'/4.txt'),
    ), datafilessummary_items())
    
    # Switch to Current Case again
    mouseClick(waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase"), 85, 5, 0, Qt.LeftButton)
    
    # Replace: 1,2,4 → 5,2,4
    doubleClick(waitForObjectItem(":DataFilesSummary_Tree", "1\\.txt"), 80, 7, 0, Qt.LeftButton)
    clickButton(waitForObject(":DataFilesSummary_EditingWidget_Button"))
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + '/5.txt')
    type(waitForObject(":fileNameEdit_QLineEdit"), '<Return>')
    test.compare((
        ('2.txt', 'Stage_1', dirname(__file__)+'/2.txt'),
        ('4.txt', 'Stage_2', dirname(__file__)+'/4.txt'),
        ('5.txt', 'Stage_1', dirname(__file__)+'/5.txt'),
    ), datafilessummary_items())
    
    # Go to Backup Case
    mouseClick(waitForObjectItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1"), 77, 7, 0, Qt.LeftButton)
    test.compare((
        ('1.txt', 'Stage_1', dirname(__file__)+'/1.txt'),
        ('2.txt', 'Stage_1', dirname(__file__)+'/2.txt'),
        ('4.txt', 'Stage_2', dirname(__file__)+'/4.txt'),
    ), datafilessummary_items())
    
    # Go to Current Case again
    mouseClick(waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase"), 85, 5, 0, Qt.LeftButton)
    test.compare((
        ('2.txt', 'Stage_1', dirname(__file__)+'/2.txt'),
        ('4.txt', 'Stage_2', dirname(__file__)+'/4.txt'),
        ('5.txt', 'Stage_1', dirname(__file__)+'/5.txt'),
    ), datafilessummary_items())
    
    # Replace: 5,2,4 → 5,2,3
    # Make the modification outside the tree view
    clickTab(waitForObject(":AsterStudy *.Vue Cas_QTabWidget"), "Case View")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2.Mesh.mesh1"), 33, 10, 0, Qt.LeftButton)
    choose_file('3.txt')
    clickTab(waitForObject(":AsterStudy *.Vue Cas_QTabWidget"), "History View")
    test.compare((
        ('2.txt', 'Stage_1', dirname(__file__)+'/2.txt'),
        ('3.txt', 'Stage_2', dirname(__file__)+'/3.txt'),
        ('5.txt', 'Stage_1', dirname(__file__)+'/5.txt'),
    ), datafilessummary_items())

    # Delete: 5,2,3 → 5,3
    # Make the modification outside the tree view
    clickTab(waitForObject(":AsterStudy *.Vue Cas_QTabWidget"), "Case View")
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Mesh.mesh0"), 25, 8, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    clickTab(waitForObject(":AsterStudy *.Vue Cas_QTabWidget"), "History View")
    test.compare((
        ('3.txt', 'Stage_2', dirname(__file__)+'/3.txt'),
        ('5.txt', 'Stage_1', dirname(__file__)+'/5.txt'),
    ), datafilessummary_items())
