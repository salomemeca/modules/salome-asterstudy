def main():
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)


def cleanup():
    global_cleanup()


def runCase():
    #--------------------------------------------------------------------------
    createNewStudy()
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    saveFile(os.path.join(testdir(), "Noname_1183.ajs"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_1', Execute )
    use_option( 'Stage_2', Execute )

    run_case( 'RunCase_1' )

    test.vp("VP1")

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    # create output file
    name = os.path.join(testdir(), "Noname_1183_Files", "RunCase_1", "Result-Stage_2", "base", "pick.1")
    test.verify( os.path.exists( name ) )

    # delete run case files
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 39, 8, 0, Qt.LeftButton)
    activateMenuItem("Operations", "Delete Results")
    snooze(1)

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)
    test.verify( not os.path.exists( name ) )

    #--------------------------------------------------------------------------
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_1', Execute )
    use_option( 'Stage_2', Execute )

    run_case( 'RunCase_2' )

    test.vp("VP2")

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_2", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Delete Results"))
    snooze(1)
