#------------------------------------------------------------------------------
# Issue #1188 - Preference option to switch on/off business-oriented translations

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(nativenames=False, basicnaming=False)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # [section] Create New study
    # [step] Menu File - New
    waitForActivateMenuItem( "File", "New" )

    #--------------------------------------------------------------------------
    # [section] Create empty stage in current case
    # [step] Switch to current case
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    # [step] Menu Operations - Add Stage
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    # [section] Check business-oriented translations in menus
    # [step] Add command from context menu with business-oriented translations switched ON
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1", "Mesh", "Read a mesh" )
    # [step] Menu Edit - Preferences
    activateMenuItem( "File", "Preferences..." )
    # [step] Switch OFF business-oriented translations
    clickButton(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    # [step] Add command from context menu with business translations switched ON
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE" )

    #--------------------------------------------------------------------------
    # [section] Check business-oriented translations in Edit Command dialog
    # [step] Open Edit Command dialog from context menu
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.mesh", "Edit" )
    # [check] "Format" keyword has no business-oriented translation
    test.compare( str(waitForObjectExists( ":FORMAT_title_ElidedLabel" ).text ), "FORMAT")
    # [check] "Format" keyword's default value has no business-oriented translation
    test.compare( str( waitForObjectExists( ":FORMAT_QComboBox" ).currentText ), "MED" )
    # [step] Menu Edit - Preferences
    activateMenuItem( "File", "Preferences..." )
    # [step] Switch ON business-oriented translations
    clickButton(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    # [step] Check that changes in Preferences dialog did not affect on 'Edit command' panel
    # [check] "Format" keyword has no business-oriented translation
    test.compare( str(waitForObjectExists( ":FORMAT_title_ElidedLabel" ).text ), "FORMAT")
    # [check] "Format" keyword's default value has no business-oriented translation
    test.compare( str( waitForObjectExists( ":FORMAT_QComboBox" ).currentText ), "MED" )
    # [step] Switch on 'Use business-oriented translations' button in 'Edit command' panel
    clickButton(waitForObject(":Use business-oriented translations_QToolButton"))
    # [check] "Format" keyword has business-oriented translation
    test.compare( str(waitForObjectExists( ":FORMAT_title_ElidedLabel" ).text ), "Mesh file format")
    # [check] "Format" keyword's default value has business-oriented translation
    test.compare( str( waitForObjectExists( ":FORMAT_QComboBox" ).currentText ), "Med" )
    # [step] Close Edit Command dialog
    clickButton(waitForObject(":Close_QPushButton"))
    # [step] Open Edit Command dialog from context menu
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.mesh", "Edit" )
    # [check] "Format" keyword has business-oriented translation
    test.compare( str(waitForObjectExists( ":FORMAT_title_ElidedLabel" ).text ), "Mesh file format")
    # [check] "Format" keyword's default value has business-oriented translation
    test.compare( str( waitForObjectExists( ":FORMAT_QComboBox" ).currentText ), "Med" )
    # [step] Menu Edit - Preferences
    activateMenuItem( "File", "Preferences..." )
    # [step] Switch OFF business-oriented translations
    clickButton(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    # [step] Check that changes in Preferences dialog did not affect on 'Edit command' panel
    # [check] "Format" keyword has business-oriented translation
    test.compare( str(waitForObjectExists( ":FORMAT_title_ElidedLabel" ).text ), "Mesh file format")
    # [check] "Format" keyword's default value has business-oriented translation
    test.compare( str( waitForObjectExists( ":FORMAT_QComboBox" ).currentText ), "Med" )
    # [step] Switch on 'Use business-oriented translations' button in 'Edit command' panel
    clickButton(waitForObject(":Use business-oriented translations_QToolButton"))
    # [check] "Format" keyword has no business-oriented translation
    test.compare( str(waitForObjectExists( ":FORMAT_title_ElidedLabel" ).text ), "FORMAT")
    # [check] "Format" keyword's default value has no business-oriented translation
    test.compare( str( waitForObjectExists( ":FORMAT_QComboBox" ).currentText ), "MED" )
    # [step] Close Edit Command dialog
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Check business-oriented translations in Show All dialog
    # [step] Open Show All dialog from context menu
    activateOBContextMenuItem( "CurrentCase.Stage_1", "Show All" )
    # [check] "LIRE_MAILLAGE" command has no business-oriented translation
    checkCommandInShowAllDialog("LIRE_MAILLAGE", "Mesh")
    # [step] Menu Edit - Preferences
    activateMenuItem( "File", "Preferences..." )
    # [step] Switch ON business-oriented translations
    clickButton(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    # [check] "LIRE_MAILLAGE" command has business-oriented translation
    checkCommandInShowAllDialog("Read a mesh (LIRE_MAILLAGE)", "Mesh")
    # [step] Menu Edit - Preferences
    activateMenuItem( "File", "Preferences..." )
    # [step] Switch ON business-oriented translations
    clickButton(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    # [check] "LIRE_MAILLAGE" command has no business-oriented translation
    checkCommandInShowAllDialog("LIRE_MAILLAGE", "Mesh")
    # [step] Close Show ALl dialog
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Quit application
    # [step] Menu File - Exit
    activateMenuItem( "File", "Exit" )
    # [step] Press "Discard" button in confirmation dialog
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def checkCommandInShowAllDialog( command, group ):
    #waitForObject( ":_QLineEdit" ).setText( command )
    aboveWidget = "{container=':qt_tabwidget_stackedwidget_QWidget' text~='%s.*' type='QToolButton' unnamed='1' visible='1'}" % group
    grpListView = "{aboveWidget=%s container=':qt_tabwidget_stackedwidget_QWidget' type='QListWidget' unnamed='1' visible='1'}" % aboveWidget
    waitForObjectItem( grpListView, escape( command ) )
    clickItem( grpListView, escape( command ), 10, 10, 0, Qt.LeftButton )
