def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def debug_widget_table():
    model = waitForObject(":BaseView_DebugWidget").model()
    return tuple(tuple(model.index(row,col).text for col in range(model.columnCount())) for row in range(model.rowCount()))

def runCase():
    waitForActivateMenuItem( "File", "New" )
    mesh_path = os.path.join(casedir(), findFile("testdata", "Mesh_recette.med"))
    exec_in_console(
        "stage = AsterStdGui.gui.study().history.current_case.create_stage()\n"
        "mesh = stage.add_command('LIRE_MAILLAGE', 'mesh').init(dict(UNITE={10:'" + mesh_path + "'}))\n"
        "stage.add_command('AFFE_MODELE', 'model1').init(dict(MAILLAGE=mesh, AFFE=("
            "dict(GROUP_MA=('Cont_mast','Cont_slav'), PHENOMENE=''),"
            "dict(GROUP_MA=('Cont_slav','Sym_x'), PHENOMENE=''),"
        ")))\n"
        "stage.add_command('AFFE_MODELE', 'model2').init(dict(MAILLAGE=mesh, AFFE=("
            "dict(GROUP_MA=('Cont_mast',), PHENOMENE=''),"
            "dict(GROUP_MA=('Cont_mast','Cont_slav'), PHENOMENE=''),"
            "dict(GROUP_MA=('Cont_mast','Cont_slav','Sym_x'), PHENOMENE=''),"
        ")))\n"
    )
    
    # Open GroupPanel
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    showOBIndex("CurrentCase", "Stage_1", "Model Definition", "model1")
    clickButton(waitForObject(":Groups Involved_QToolButton"))
    
    # Get usages' colors and make sure groups are highlighted correctly in the view
    color1 = str(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 6))
    color2 = str(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 6))
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Visible', color2),
        ('1/Sym_x', 'Visible', color2),
    ))
    
    
    #####################################################################################
    # Toggle usage by clicking on toolbutton
    
    # Hide and show first usage
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"))
    clickButton(waitForObject(":group_panel.Hide groups_QToolButton"))
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Hidden', ''),
        ('1/Cont_slav', 'Visible', color2),
        ('1/Sym_x', 'Visible', color2),
    ))
    clickButton(waitForObject(":group_panel.Show groups_QToolButton"))
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Visible', color1),
        ('1/Sym_x', 'Visible', color2),
    ))
    
    # Hide and show second usage
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''"))
    clickButton(waitForObject(":group_panel.Hide groups_QToolButton"))
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Visible', color1),
        ('1/Sym_x', 'Hidden', ''),
    ))
    clickButton(waitForObject(":group_panel.Show groups_QToolButton"))
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Visible', color2),
        ('1/Sym_x', 'Visible', color2),
    ))
    
    
    #####################################################################################
    # Toggle usage by clicking on icon
    
    # Hide and show first usage
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Hidden', ''),
        ('1/Cont_slav', 'Visible', color2),
        ('1/Sym_x', 'Visible', color2),
    ))
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Visible', color1),
        ('1/Sym_x', 'Visible', color2),
    ))
    
    # Hide and show second usage
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Visible', color1),
        ('1/Sym_x', 'Hidden', ''),
    ))
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Visible', color2),
        ('1/Sym_x', 'Visible', color2),
    ))
    
    
    #####################################################################################
    # Check that two toolbuttons can be visible at the same time
    
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"), 50, 10, Qt.NoModifier, Qt.LeftButton)
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''"), 50, 10, Qt.ShiftModifier, Qt.LeftButton)
    test.verify(waitForObjectExists(":group_panel.Show groups_QToolButton"))
    test.verify(waitForObjectExists(":group_panel.Hide groups_QToolButton"))
    
    
    #####################################################################################
    # Check use case with three usages
    
    showOBIndex("CurrentCase", "Stage_1", "Model Definition", "model2")
    clickButton(waitForObject(":Groups Involved_QToolButton"))
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 7), True)
    color1 = str(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 6))
    color2 = str(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 6))
    color3 = str(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 6))
    
    # Hide everything
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Hidden', ''),
        ('1/Cont_slav', 'Hidden', ''),
        ('1/Sym_x', 'Hidden', ''),
    ))
    
    # Display usage 1
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color1),
        ('1/Cont_slav', 'Hidden', ''),
        ('1/Sym_x', 'Hidden', ''),
    ))
    
    # Display usage 2
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color2),
        ('1/Cont_slav', 'Visible', color2),
        ('1/Sym_x', 'Hidden', ''),
    ))
    
    # Display usage 3
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color3),
        ('1/Cont_slav', 'Visible', color3),
        ('1/Sym_x', 'Visible', color3),
    ))
    
    # Hide usage 1
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color3),
        ('1/Cont_slav', 'Visible', color3),
        ('1/Sym_x', 'Visible', color3),
    ))
    
    # Hide usage 3
    mouseClick(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''"), 10, 10, Qt.NoModifier, Qt.LeftButton)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 0; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 1; PHENOMENE=''").data(Qt.UserRole + 7), True)
    test.compare(waitForObjectItem(":group_panel.view_ResizableListView", "AFFE / 2; PHENOMENE=''").data(Qt.UserRole + 7), False)
    test.compare(debug_widget_table(), (
        ('1/Cont_mast', 'Visible', color2),
        ('1/Cont_slav', 'Visible', color2),
        ('1/Sym_x', 'Hidden', ''),
    ))