def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    # Create new study
    waitForActivateMenuItem("File", "New")

    # Create a stage in the current case and add several variables
    exec_in_console("""
stage = AsterStdGui.gui.study().history.current_case.create_stage()
stage.add_variable('a', '10')
stage.add_variable('b', '20')
stage.add_variable('c', '1.23')
stage.add_variable('d', '4.56')
stage.add_variable('txt', '"Hello."')
    """)

    # Activate Case View and select Stage_1 
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    # Add DEFI_FONCTION command and open Edit panel for it
    activateOBContextMenuItem("CurrentCase.Stage_1", "Functions and Lists", "DEFI_FONCTION")
    activateOBContextMenuItem("CurrentCase.Stage_1.Functions and Lists.DEFI_FONCTION", "Edit")

    # Open table editor, for ABSCISSE mode
    clickButton(waitForObject(tr('ABSCISSE', 'QRadioButton')))
    clickButton(waitForObject(tr('ABSCISSE', 'QPushButton')))
    
    # Select variable "a" as first cell's value
    doubleClick(waitForObjectItem(":_FunctionTable", "0/0"), 10, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":ValueOrVariableEditor_Switcher_HoverPopupWidget"))
    combobox = waitForObject(":ValueOrVariableEditor_VariableSelector_QComboBox")
    visible_variables = tuple(str(combobox.itemText(i)) for i in range(1, combobox.count))
    test.compare(('a', 'b', 'c', 'd'), visible_variables)
    setParameterValue(":ValueOrVariableEditor_VariableSelector_QComboBox", "a")
    type(combobox, "<Tab>")
    
    # Select variable "b" as second cell's value
    clickButton(waitForObject(":ValueOrVariableEditor_Switcher_HoverPopupWidget"))
    combobox = waitForObject(":ValueOrVariableEditor_VariableSelector_QComboBox")
    setParameterValue(":ValueOrVariableEditor_VariableSelector_QComboBox", "b")
    type(combobox, "<Enter>")
    
    # Add new string variable "x"
    # Check that it doesn't appear in the combobox as it's of inappropriate type
    doubleClick(waitForObjectItem(":_FunctionTable", "1/0"), 10, 10, 0, Qt.LeftButton)
    combobox = waitForObject(":ValueOrVariableEditor_VariableSelector_QComboBox")
    mouseClick(combobox, 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":ValueOrVariableEditor_VariableSelector_QComboBox", "<Add variable\\.\\.\\.>"), 10, 10, 0, Qt.LeftButton)
    type(waitForObject(":Name_QLineEdit_2"), "x")
    type(waitForObject(":Expression_QLineEdit"), "\"New text\"")
    clickButton(waitForObject(":OK_QPushButton"))
    visible_variables = tuple(str(combobox.itemText(i)) for i in range(1, combobox.count))
    test.compare(('a', 'b', 'c', 'd'), visible_variables)
    
    # Add new integer variable "y"
    # Check that it appears in the combobox
    # Select it as second cell's value 
    combobox = waitForObject(":ValueOrVariableEditor_VariableSelector_QComboBox")
    mouseClick(combobox, 130, 17, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":ValueOrVariableEditor_VariableSelector_QComboBox", "<Add variable\\.\\.\\.>"), 117, 12, 0, Qt.LeftButton)
    type(waitForObject(":Name_QLineEdit_2"), "y")
    type(waitForObject(":Expression_QLineEdit"), "900")
    clickButton(waitForObject(":OK_QPushButton"))
    visible_variables = tuple(str(combobox.itemText(i)) for i in range(1, combobox.count))
    test.compare(('a', 'b', 'c', 'd', 'y'), visible_variables)
    test.compare('y', combobox.currentText)
    
    # Quit editing, save all data, ignore all warnings
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    
    # Make sure that both variables "x" and "y" are now available in the data tree
    checkOBItem("CurrentCase", "Stage_1", "Variables", "x")
    checkOBItem("CurrentCase", "Stage_1", "Variables", "y")

    # Open Edit panel for DEFI_FONCTION again
    # Check that vairables are selected as the cells values
    activateOBContextMenuItem("CurrentCase.Stage_1.Functions and Lists.DEFI_FONCTION", "Edit")
    clickButton(waitForObject(":ABSCISSE_ParameterButton"))
    table = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' type='QTableWidget' unnamed='1' visible='1'}")
    test.compare(str(table.item(0, 0).text()), "a")
    test.compare(str(table.item(1, 0).text()), "y")
    doubleClick(waitForObjectItem(":_FunctionTable", "0/0"), 10, 10, 0, Qt.LeftButton)
    combobox = waitForObject(":ValueOrVariableEditor_VariableSelector_QComboBox")
    test.compare('a', combobox.currentText)
    type(combobox, "<Cancel>")
    doubleClick(waitForObjectItem(":_FunctionTable", "1/0"), 10, 10, 0, Qt.LeftButton)
    combobox = waitForObject(":ValueOrVariableEditor_VariableSelector_QComboBox")
    test.compare('y', combobox.currentText)
    type(combobox, "<Cancel>")
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Close_QPushButton"))

    # Quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))
