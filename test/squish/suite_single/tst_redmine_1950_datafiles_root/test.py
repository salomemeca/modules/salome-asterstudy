import re
from os.path import dirname

def main():
    source(findFile('scripts', 'common.py'))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    file1 = dirname(__file__) + "/file1.txt"
    file2 = dirname(__file__) + "/file2.txt"
    file3 = dirname(__file__) + "/file3.txt"
    file4 = dirname(__file__) + "/file4.txt"
    file5 = dirname(__file__) + "/file5.txt"
    
    # Create new study
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    
    # Stage 1
    openContextMenu(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 77, 12, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Add Stage"))
    for i, filename in enumerate((file1, file2, file3), start=-1):
        mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 43, 14, 0, Qt.LeftButton)
        mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 40, 13, 0, Qt.LeftButton)
        doubleClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase.Stage\\_1.Mesh.mesh" + (str(i) if i>=0 else "")), 42, 15, 0, Qt.LeftButton)
        clickButton(waitForObject(":UNITE_QPushButton"))
        type(waitForObject(":fileNameEdit_QLineEdit"), filename)
        type(waitForObject(":fileNameEdit_QLineEdit"), "<Return>")
        clickButton(waitForObject(":OK_QPushButton"))
    
    # Stage 2
    openContextMenu(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 77, 12, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Add Stage"))
    for i, filename in enumerate((file4, file5), start=2):
        mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 43, 14, 0, Qt.LeftButton)
        mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 40, 13, 0, Qt.LeftButton)
        doubleClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase.Stage\\_2.Mesh.mesh" + str(i)), 42, 15, 0, Qt.LeftButton)
        clickButton(waitForObject(":UNITE_QPushButton"))
        type(waitForObject(":fileNameEdit_QLineEdit"), filename)
        type(waitForObject(":fileNameEdit_QLineEdit"), "<Return>")
        clickButton(waitForObject(":OK_QPushButton"))
    
    # Click different items in DataSettings, check what we see in DataFiles 
    settingsView = waitForObject(":Data Settings_SettingsView")
    
    clickItem(settingsView, "CurrentCase", 79, 7, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for CurrentCase")
    
    clickItem(settingsView, "CurrentCase.Stage\\_1", 23, 5, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for Stage 1")
    
    clickItem(settingsView, "CurrentCase.Stage\\_1.Mesh", 43, 14, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for category in Stage 1")

    clickItem(settingsView, "CurrentCase.Stage\\_1.Mesh.mesh", 39, 4, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for mesh")

    clickItem(settingsView, "CurrentCase.Stage\\_1.Mesh.mesh0", 39, 4, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for mesh0")

    clickItem(settingsView, "CurrentCase.Stage\\_1.Mesh.mesh1", 39, 4, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for mesh1")
    
    clickItem(settingsView, "CurrentCase.Stage\\_2", 23, 5, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for Stage 2")
    
    clickItem(settingsView, "CurrentCase.Stage\\_2.Mesh", 43, 14, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for category in Stage 2")

    clickItem(settingsView, "CurrentCase.Stage\\_2.Mesh.mesh2", 39, 4, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for mesh2")

    clickItem(settingsView, "CurrentCase.Stage\\_2.Mesh.mesh3", 39, 4, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()
    test.vp("DataFiles for mesh3")