#------------------------------------------------------------------------------
# Issue #1606 - Remove item in Edition panel does not work for single item

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def check_value(value):
    activateOBContextMenuItem("CurrentCase.Stage_1.Other.cmd", "Copy")
    snippet = getClipboardText().strip()
    str_value = ', '.join([str(i) for i in value])
    if len(value) == 1:
        str_value += ', '
    expected = \
'''
cmd = COMB_FOURIER(ANGLE=({}),
                   NOM_CHAM=())
'''
    test.verify(check_text_eq(expected.format(str_value), snippet))

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add empty stage in current case
    waitForActivateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add COMB_FOURIER command
    setClipboardText("cmd = COMB_FOURIER()")
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Paste")

    #--------------------------------------------------------------------------
    # open COMB_FOURIER command for editing, add one ANGLE item, check
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Other.cmd", "Edit")
    clickButton(waitForObject(tr('ANGLE_add', 'QToolButton')))
    type(waitForObject(tr('0', 'QLineEdit')), "1")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    check_value((1.0, ))

    #--------------------------------------------------------------------------
    # open COMB_FOURIER command for editing, add another ANGLE item, check
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Other.cmd", "Edit")
    clickButton(waitForObject(tr('ANGLE_add', 'QToolButton')))
    type(waitForObject(tr('1', 'QLineEdit')), "2")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    check_value((1.0, 2.0))

    #--------------------------------------------------------------------------
    # open COMB_FOURIER command for editing, remove first ANGLE item, check
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Other.cmd", "Edit")
    clickButton(waitForObject(tr('ANGLE_expand', 'QToolButton')))
    clickButton(waitForObject(tr('Remove_Item', 'QToolButton')))
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    check_value((2.0,))

    #--------------------------------------------------------------------------
    # open COMB_FOURIER command for editing, remove last ANGLE item, check
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Other.cmd", "Edit")
    clickButton(waitForObject(tr('ANGLE_expand', 'QToolButton')))
    clickButton(waitForObject(tr('Remove_Item', 'QToolButton')))
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    check_value(())

    #--------------------------------------------------------------------------
    # exit
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
