#------------------------------------------------------------------------------
# Issue #921 - Fix Command order in Categories
#  Case 2
#
#   table = INFO_EXEC_ASTER(...)
#   recu1 = RECU_FONCTION(TABLE=table)
#   table2 = INFO_EXEC_ASTER(...)
#   recu2 = RECU_FONCTION(TABLE=table, FILTRE=_F(NOM_PARA="X"))
#
#  Here, the order in OB is not
#
#  (table, recu1, table2, recu2)
#
#
#  but
#
#  (table, recu1, recu2, table2)
#
#
#  i.e. in accordance with specified categories sorting

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    text1 = \
"""
table = INFO_EXEC_ASTER(LISTE_INFO=('UNITE_LIBRE', ))
recu1 = RECU_FONCTION(TABLE=table, PARA_X='X', PARA_Y='Y')
table2 = INFO_EXEC_ASTER(LISTE_INFO=('UNITE_LIBRE', ))
recu2 = RECU_FONCTION(TABLE=table, PARA_X='X', PARA_Y='Y', FILTRE=_F(NOM_PARA='X', VALE=0))
"""
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ). clear()
    waitForObject( text_editor ). appendPlainText( text1 )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    #--------------------------------------------------------------------------

    showOBItem( "CurrentCase", "Stage_1", "Other", "table" )
    showOBItem( "CurrentCase", "Stage_1", "Output", "recu1" )
    showOBItem( "CurrentCase", "Stage_1", "Output", "recu2" )
    selectItem( "Stage_1:2", "Other:-3", "table2:8" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
