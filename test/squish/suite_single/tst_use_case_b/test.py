def main():
    source(findFile("scripts", "common.py"))
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    activateMenuItem("Operations", "Add Stage")
    checkOBItem( "CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    selectObjectBrowserItem("CurrentCase.Stage_1")
    activateMenuItem("Commands", "Material", "DEFI_MATERIAU")
    showOBItem( "CurrentCase", "Stage_1", "Material", "DEFI_MATERIAU")

    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Material.DEFI_MATERIAU", "Edit")
    waitForObject(":qt_splithandle__EditionPanel")
    test.vp("VP1")
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    activateMenuItem("Operations", "Add Stage")
    checkOBItem( "CurrentCase", "Stage_2")

    #--------------------------------------------------------------------------
    selectObjectBrowserItem("CurrentCase.Stage_2")
    mouseClick(waitForObject(":AsterStudy Analysis_ComboBox"), 56, 11, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":AsterStudy Analysis_ComboBox", "DYNA\\_VIBRA"), 55, 9, 0, Qt.LeftButton)
    showOBItem( "CurrentCase", "Stage_2", "Analysis", "DYNA_VIBRA")
    for _ in range(10):
        nativeType("<F4>")
        snooze(0.5)
        if object.exists(":qt_splithandle__EditionPanel"):
            break
    else:
        # workaround
        test.warning("F4 shortcut to edit a command doesn't work.", "'Edit' item from context menu is used instead of.")
        waitForActivateOBContextMenuItem("CurrentCase.Stage_2.Analysis.DYNA_VIBRA", "Edit")
    waitForObject(":qt_splithandle__EditionPanel")

    symbol = tr( 'BASE_CALCUL', 'QComboBox' )
    setParameterValue( symbol, 'PHYS' )

    symbol = tr( 'TYPE_CALCUL', 'QComboBox' )
    setParameterValue( symbol, 'HARM' )

    test.vp("VP2")
    clickButton(waitForObject(":Close_QPushButton"))

    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    file_path = saveFile("tst_use_case_b.ajs")

    #--------------------------------------------------------------------------
    activateMenuItem("File", "Close")
    test.compare(findObject(":AsterStudy.Close_QToolButton").enabled, False)
