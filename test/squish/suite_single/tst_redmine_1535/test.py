#------------------------------------------------------------------------------
# Issue #1535 - Trouble when editing the command COMB_SISM_MODAL

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add empty stage in current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add COMB_SISM_MODAL command
    setClipboardText("cmd = COMB_SISM_MODAL()")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Paste")

    #--------------------------------------------------------------------------
    # open COMB_SISM_MODAL command for editing
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Analysis.cmd", "Edit")

    #--------------------------------------------------------------------------
    # add one EXCIT and edit it
    clickButton(waitForObject(tr('EXCIT_add', 'QToolButton')))
    clickButton(waitForObject(":0_ParameterButton"))

    #--------------------------------------------------------------------------
    # switch ON TRI_SPEC radiobutton, check presence of ECHELLE and TRI_SPEC keywords
    clickButton(waitForObject(tr('TRI_SPEC', 'QRadioButton')))
    waitForObject(tr('ECHELLE', 'QCheckBox'))
    waitForObject(tr('SPEC_OSCI_expand', 'QToolButton'))

    #--------------------------------------------------------------------------
    # switch ON TRI_AXE radiobutton, check presence of ECHELLE and TRI_SPEC keywords
    clickButton(waitForObject(tr('TRI_AXE', 'QRadioButton')))
    waitForObject(tr('ECHELLE', 'QCheckBox'))
    waitForObject(tr('SPEC_OSCI', 'QComboBox'))

    #--------------------------------------------------------------------------
    # switch ON AXE radiobutton, check presence of ECHELLE and TRI_SPEC keywords
    clickButton(waitForObject(tr('AXE', 'QRadioButton')))
    waitForObject(tr('ECHELLE', 'QCheckBox'))
    waitForObject(tr('SPEC_OSCI', 'QComboBox'))

    #--------------------------------------------------------------------------
    # close Edition panel
    clickButton(waitForObject(":Abort_QPushButton"))
    clickButton(waitForObject(":Abort.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    # exit
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
