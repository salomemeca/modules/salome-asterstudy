#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populate_MAIL_Q( *args ):
    #--------------------------------------------------------------------------
    name = args[0]
    path = args[1:]

    #--------------------------------------------------------------------------
    addCommandByDialog( "LIRE_MAILLAGE", "Mesh" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    #[step] Click '...' browse button
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Open 'Mesh_recette.med' file
    med_file_path = os.path.join( casedir(), findFile( "testdata", "zzzz121b.med" ) )
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    #[check] 'Mesh_recette.med' file name was added to combobox and activated
    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(med_file_path))

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def parameterSymbol( name, type ):
    #--------------------------------------------------------------------------
    conatainer = ":qt_tabwidget_stackedwidget_QWidget"
    pattern = "{container='%s' name='%s' type='%s' visible='1'}"

    #--------------------------------------------------------------------------
    return pattern % ( conatainer, name, type )

#------------------------------------------------------------------------------
def check_MAIL_Q( item, name ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    symbol = parameterSymbol( 'INFO', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, False )

    #--------------------------------------------------------------------------
    symbol = parameterSymbol( 'FORMAT', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, False )

    #--------------------------------------------------------------------------
    symbol = parameterSymbol( 'UNITE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).enabled, True )

    #--------------------------------------------------------------------------
    symbol = parameterSymbol( 'VERI_MAIL', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, False )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    testSettings.waitForObjectTimeout = 50000
    importStage( good_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    name = "MAIL_Q"
    item = selectItem( "zzzz289f", "Mesh", name )
    check_MAIL_Q( item, name = name )

    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    item = selectItem( "zzzz289f", "Mesh", name )
    check_MAIL_Q( item, name = name )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    name = "MAIL_Q1"
    populate_MAIL_Q( name, "Stage_1", "Mesh", "LIRE_MAILLAGE" )

    item = selectItem( "Stage_1", "Mesh", name )
    check_MAIL_Q( item, name = name )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")

    item = selectItem( "Stage_1", "Mesh", name )
    check_MAIL_Q( item, name = name )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
