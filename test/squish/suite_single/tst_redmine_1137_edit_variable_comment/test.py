#------------------------------------------------------------------------------
# Issue #1137 - Manage comments
# Issue #1222 - Problem with edition variable comment
#
# For more details look at
# - http://salome.redmine.opencascade.com/issues/1137
# - http://salome.redmine.opencascade.com/issues/1222

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    text = \
"""
# comment
a = 10
"""
    setClipboardText(text)
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )
    waitForObject( ":_QTreeWidget" ).expandAll()
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "comment" )
    selectItem( "Stage_1", "Variables", "a" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "comment" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "comment" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("variable")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable" )
    selectItem( "Stage_1", "Variables", "a" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "a" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "variable" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("variable a")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "a" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":Comment_QPlainTextEdit_2").toPlainText() == "variable a" )
    waitForObject(":Comment_QPlainTextEdit_2").setPlainText("comment for variable a")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "comment for variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    checkOBItemNotExists( "CurrentCase", "Stage_1", "Variables", "variable a" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "a" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":Comment_QPlainTextEdit_2").toPlainText() == "comment for variable a" )
    waitForObject(":Comment_QPlainTextEdit_2").setPlainText("")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "a" )
    checkOBItemNotExists( "CurrentCase", "Stage_1", "Variables", "comment for variable a" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
