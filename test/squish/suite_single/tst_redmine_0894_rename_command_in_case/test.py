#------------------------------------------------------------------------------
# Issue #894 - Rename Command in use from a Stage

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    item = "CurrentCase.Stage\\_1"

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    text1 = \
"""
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'), DEBUG=_F(SDVERI='OUI'))

MAIL_Q = LIRE_MAILLAGE(UNITE=20)

M = DEFI_MATERIAU(
    ECRO_LINE=_F(D_SIGM_EPSI=-1950.0, SY=3.0),
    ELAS=_F(E=30000.0, NU=0.2, RHO=2764.0)
)

CHM = AFFE_MATERIAU(AFFE=_F(MATER=M, TOUT='OUI'), MAILLAGE=MAIL_Q)
"""
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    waitForObject( text_editor ).appendPlainText( text1 )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "Stage_1", "Mesh", "MAIL_Q" )
    showOBItem( "CurrentCase", "Stage_1", "Material", "M" )
    showOBItem( "CurrentCase", "Stage_1", "Material", "CHM" )
    showOBItem( "CurrentCase", "Stage_1", "Deprecated" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_2" )

    #openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    #activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    activateOBContextMenuItem("CurrentCase.Stage_2", "Text Mode")

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    text2 = \
"""
MODELUPG = AFFE_MODELE(
    AFFE=_F(MODELISATION='AXIS_INCO_UPG', PHENOMENE='MECANIQUE', TOUT='OUI'),
    MAILLAGE=MAIL_Q
)

MATR = CALC_MATR_ELEM(CHAM_MATER=CHM, MODELE=MODELUPG, OPTION='MASS_MECA')

FIN()
"""
    waitForObject( text_editor ).clear()
    waitForObject( text_editor ).appendPlainText( text2 )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "Stage_2", "Model Definition", "MODELUPG" )
    showOBItem( "CurrentCase", "Stage_2", "Other" )
    checkValidity( True, "CurrentCase", "Stage_2", "Other", "MATR" )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_1", "Material", "CHM" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( ":Name_QLineEdit", "affemate" )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase", "Stage_2", "Other", "MATR" )
    checkValidity( True, "CurrentCase", "Stage_2", "Other" )
    checkValidity( True, "CurrentCase", "Stage_2" )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_2" )

    #openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    #activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    activateOBContextMenuItem("CurrentCase.Stage_2", "Text Mode")

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase", "Stage_2" )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )
    #--------------------------------------------------------------------------
    pass
