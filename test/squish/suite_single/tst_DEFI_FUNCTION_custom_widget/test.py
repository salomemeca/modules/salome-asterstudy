def main():
    source( findFile( "scripts", "common.py" ) )
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "DEFI_FONCTION", "Functions and Lists" )
    showOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "DEFI_FONCTION" )

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1.Functions and Lists.DEFI\\_FONCTION", 50, 9, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    #--------------------------------------------------------------------------
    # read 2-columns file into 1-column table
    #--------------------------------------------------------------------------
    symbol = tr( 'ABSCISSE', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'ABSCISSE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Import table_QToolButton" ) )
    test.compare( waitForObject( ":QFileDialog.fileTypeCombo_QComboBox" ).currentText,
                  "Text files (*.txt *.csv *.dat)" )
    data_file_path = os.path.join( casedir(), 'szlz106a.dat' )
    setParameterValue( ":fileNameEdit_QLineEdit", data_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    clickButton(waitForObject(":No_QPushButton"))

    setParameterValue( ":fileNameEdit_QLineEdit", data_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    clickButton(waitForObject(":Yes_QPushButton"))

    data = [ [1.0, 3.125e+11], [2.0, 9765625000.0], [5.0, 100000000.0], [25.0, 32000.0] ]

    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")

    for row in range(len(data)):
        for column in range(tableWidget.columnCount):
            tableItem = tableWidget.item(row, column)
            text = str(tableItem.text())
            value = float(text)
            test.compare(value, data[row][column], "(%d, %d)" % (row, column))

    #--------------------------------------------------------------------------
    nb_rows = tableWidget.rowCount
    waitForObjectItem(":_FunctionTable", "2/0")
    clickItem(":_FunctionTable", "2/0", 52, 17, 0, Qt.LeftButton)

    clickButton(waitForObject(":Remove rows_QToolButton"))

    test.compare(nb_rows - 1, tableWidget.rowCount)

    tableItem = tableWidget.item(2, 0)
    text = str(tableItem.text())
    value = float(text)
    test.compare(value, data[3][0], "(2, 0)")

    #--------------------------------------------------------------------------
    nb_rows = tableWidget.rowCount
    clickButton(waitForObject(":Append row_QToolButton"))
    test.compare(nb_rows + 1, tableWidget.rowCount)
    tableWidget.scrollToBottom()

    waitForObjectItem(":_FunctionTable", "38/0")
    doubleClickItem(":_FunctionTable", "38/0", 39, 13, 0, Qt.LeftButton)
    cell_38_0 = "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    type(waitForObject(cell_38_0), "205")
    type(waitForObject(cell_38_0), "<Tab>")
    test.compare(nb_rows + 2, tableWidget.rowCount)

    cell_39_0 = "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    type(waitForObject(cell_39_0), "<Return>")

    type(waitForObject(":_FunctionTable"), "<Tab>")
    test.compare(nb_rows + 3, tableWidget.rowCount)

    cell_40_0 = "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    doubleClickItem(":_FunctionTable", "40/0", 39, 13, 0, Qt.LeftButton)
    type(waitForObject(cell_40_0), "215")
    type(waitForObject(cell_40_0), "<Return>")

    waitForObjectItem(":_FunctionTable", "40/0")
    clickItem(":_FunctionTable", "40/0", 53, 15, 0, Qt.LeftButton)

    clickButton(waitForObject(":Remove rows_QToolButton"))
    test.compare(nb_rows + 2, tableWidget.rowCount)

    clickButton(waitForObject(":OK_QPushButton"))
    # cell was selected because it is empty and can not be validated
    doubleClickItem(":_FunctionTable", "39/0", 39, 13, 0, Qt.LeftButton)
    type(waitForObject(cell_39_0), "210")
    type(waitForObject(cell_39_0), "<Return>")

    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    symbol = tr( 'ABSCISSE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")

    tableItem = tableWidget.item(2, 0)
    text = str(tableItem.text())
    value = float(text)
    test.compare(value, data[3][0])

    tableItem = tableWidget.item(38, 0)
    text = str(tableItem.text())
    value = float(text)
    test.compare(value, 205)

    tableItem = tableWidget.item(39, 0)
    text = str(tableItem.text())
    value = float(text)
    test.compare(value, 210)

    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # read 2-columns file into 2-columns table
    #--------------------------------------------------------------------------
    symbol = tr( 'VALE', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Import table_QToolButton" ) )
    data_file_path = os.path.join( casedir(), 'szlz106a.dat' )
    setParameterValue( ":fileNameEdit_QLineEdit", data_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    # read 2-columns file into 3-columns table
    #--------------------------------------------------------------------------
    symbol = tr( 'VALE_C', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'VALE_C', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Import table_QToolButton" ) )
    data_file_path = os.path.join( casedir(), 'szlz106a.dat' )
    setParameterValue( ":fileNameEdit_QLineEdit", data_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    clickButton(waitForObject(":No_QPushButton"))

    setParameterValue( ":fileNameEdit_QLineEdit", data_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    clickButton(waitForObject(":Yes_QPushButton"))

    data = [ [1.0, 3.125e+11, ""], [2.0, 9765625000.0, ""], [5.0, 100000000.0, ""], [25.0, 32000.0, ""] ]

    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")

    for row in range(len(data)):
        for column in range(tableWidget.columnCount):
            tableItem = tableWidget.item(row, column)
            text = str(tableItem.text())
            if text == "":
                test.compare("", text, "(%d, %d)" % (row, column))
            else:
                value = float(text)
                test.compare(value, data[row][column], "(%d, %d)" % (row, column))

    clickButton( waitForObject( ":OK_QPushButton" ) )

    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    clickButton(waitForObject(":Close.Yes_QPushButton"))

    symbol = tr( 'VALE_C', 'QRadioButton' )
    waitForObject( symbol )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    clickButton(waitForObject(":Close.Yes_QPushButton"))
