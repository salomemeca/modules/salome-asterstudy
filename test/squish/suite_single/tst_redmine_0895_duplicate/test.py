#------------------------------------------------------------------------------
# Issue #895 - Operations: Duplicate

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def checkContens():
    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "zzzz289f" )
    showOBItem( "CurrentCase", "zzzz289f", "Deprecated" )
    showOBItem( "CurrentCase", "zzzz289f", "Deprecated", "DEBUT" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "MATER" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    selectObjectBrowserItem( "CurrentCase" )

    #--------------------------------------------------------------------------
    import os
    comm_file = os.path.join( casedir(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % comm_file )

    #--------------------------------------------------------------------------
    importStage( comm_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    checkContens()

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Duplicate" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Duplicate" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    checkContens()

    #--------------------------------------------------------------------------
    import os
    new_file = os.path.join( testdir(), 'zzzz289f.new' )
    test.log( "Save stage into '%s' file" % new_file )

    item = showOBItem( "CurrentCase", "zzzz289f" )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Export Command File" ) )

    setParameterValue( ":fileNameEdit_QLineEdit", new_file )
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    #--------------------------------------------------------------------------
    import os
    modified_file = os.path.join( casedir(), 'zzzz289f.mod' )
    test.log( "Compare with '%s' file" % modified_file )

    import filecmp
    waitUntilWaitCursor()
    test.verify( filecmp.cmp( modified_file, new_file, shallow = False ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
