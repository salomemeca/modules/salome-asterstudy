#------------------------------------------------------------------------------
# Issue #782 - Deleting "parent" stage should not remove a "child" one
# - Changed 10/10/2018: deleting of single empty stage is possible now possible

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # Use case 1: delete single empty stage
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    # Create new study
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    # Create two stages
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_1" )
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_2" )

    #--------------------------------------------------------------------------
    # Remove first stage ("parent" one)
    activateOBContextMenuItem("CurrentCase.Stage_1", "Delete")
    clickButton( waitForObject(":Delete.Yes_QPushButton" ) )
    # No confirmation to remove child stages is needed!

    #--------------------------------------------------------------------------
    # Check that first stage ("parent" one) was deleted
    checkOBItemNotExists("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    # Check that second stage ("child" one) was NOT deleted
    checkOBItem("CurrentCase", "Stage_2")

    #--------------------------------------------------------------------------
    # Close study
    waitForActivateMenuItem("File", "Close")
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    # Use case 2: delete non-empty stage
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    # Create two stages
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_1" )
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_2" )

    #--------------------------------------------------------------------------
    # Create a command in the first stage
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    showOBItem( "CurrentCase", "Stage_1", "Mesh", "mesh" )

    #--------------------------------------------------------------------------
    # Remove first stage ("parent" one)
    activateOBContextMenuItem("CurrentCase.Stage_1", "Delete")
    clickButton( waitForObject(":Delete.Yes_QPushButton" ) )
    # Check that additional confirmation to remove child stages is needed
    clickButton( waitForObject(":Delete.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    # Check that first stage ("parent" one) was deleted
    checkOBItemNotExists("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    # Check that second stage ("child" one) was deleted as well
    checkOBItemNotExists("CurrentCase", "Stage_2")

    #--------------------------------------------------------------------------
    # Exit application
    waitForActivateMenuItem("File", "Exit")
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    pass
