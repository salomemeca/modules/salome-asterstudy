#------------------------------------------------------------------------------
# Issue #881 - Operations: Import Stage

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def checkContens():
    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "zzzz289f" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "MATER" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    showOBItem( "CurrentCase" )
    test.compare( waitForObjectExists( ":CurrentCase_QModelIndex" ).text, "CurrentCase" )

    test.compare( waitForObjectExists( ":AsterStudy *.Undo_QToolButton" ).enabled, False )
    test.compare( waitForObjectExists( ":AsterStudy *.Redo_QToolButton" ).enabled, False )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":AsterStudy *.Undo_QToolButton" ).enabled, True )
    test.compare( waitForObjectExists( ":AsterStudy *.Redo_QToolButton" ).enabled, False )

    test.verify( object.exists( ":CurrentCase.zzzz289f_QModelIndex" ) )
    test.verify( object.exists( ":CurrentCase_QModelIndex" ) )

    checkContens()

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ),
                         "CurrentCase.zzzz289f", 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    clickButton( waitForObject( ":AsterStudy *.Undo_QToolButton" ) )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":AsterStudy *.Undo_QToolButton" ).enabled, True )
    test.compare( waitForObjectExists( ":AsterStudy *.Redo_QToolButton" ).enabled, True )

    test.verify( object.exists( ":CurrentCase.zzzz289f_QModelIndex" ) )
    test.verify( object.exists( ":CurrentCase_QModelIndex" ) )

    checkContens()

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
