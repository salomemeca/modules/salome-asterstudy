#------------------------------------------------------------------------------
# Issue #1148 - Data Files view: show related concepts

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(nativenames=False, basicnaming=False)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # [section] Create New study
    # [step] Menu File - New
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # [section] Create empty stage in current case
    # [step] Switch to "Case View" tab
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    # [step] Select "CurrentCase" object in "Data Settings" panel, invoke "Add Stage" item from popup menu
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    # [section] Add command to the stage
    # [step] Select "Stage_1" object in "Data Settings" panel, invoke popup menu, choose "Mesh" / "Read a mesh" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "Read a mesh")
    # [step] Select "mesh" object in "Data Settings" panel, invoke popup menu, choose "Edit..." item
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")
    # [step] Press "..." button, select a file and press "Open" button
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    def_file_path = os.path.join( casedir(), 'default.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", def_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    # [step] Press "OK" button in "Edit command" dialog
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    # [section] Check "Data Files" view
    # [step] Switch to CurrentCase in Data Settings
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    # [step] Check that "mesh" item is shown under the file's item
    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp("DataFiles1")

    #--------------------------------------------------------------------------
    # Check behavior of command item in "Data Files" view
    # [step] Double-click on "mesh" item
    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.default\\.file.mesh")
    doubleClickItem(":DataFiles_QTreeView", "Stage\\_1.default\\.file.mesh", 35, 7, 0, Qt.LeftButton)
    # [step] Check that "Data Settings" view is activated and focus moves to "mesh" item
    test.vp("DataSettings")

    #--------------------------------------------------------------------------
    # [section] Switch "Show related concepts" feature OFF
    # [step] Menu Edit - Preferences
    activateMenuItem("File", "Preferences...")
    # [step] Switch OFF "Show related concepts in Data Files panel" check box
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_show_related_concepts_QCheckBox"))
    # [step] Preferences dialog: press OK button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Check "Data Files" view
    # [step] Switch to CurrentCase in Data Settings
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    # [step] Check that "mesh" item is not shown in the view
    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp("DataFiles2")

    #--------------------------------------------------------------------------
    # [section] Switch "Show related concepts" feature ON
    # [step] Menu Edit - Preferences
    activateMenuItem("File", "Preferences...")
    # [step] Switch OFF "Show related concepts in Data Files panel" check box
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_show_related_concepts_QCheckBox"))
    # [step] Preferences dialog: press OK button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Check "Data Files" view
    # [step] Check that "mesh" item is not shown in the view
    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp("DataFiles1")

    #--------------------------------------------------------------------------
    # [section] Quit application
    # [step] Menu File - Exit
    activateMenuItem("File", "Exit")
    # [step] Press "Discard" button in confirmation dialog
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
