def main():
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    # start-up
    #--------------------------------------------------------------------------

    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    # create 3 stages
    #--------------------------------------------------------------------------

    activateOBContextMenuItem("CurrentCase", "Add Stage")
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # save study
    #--------------------------------------------------------------------------

    saveFile(os.path.join(testdir(), "Noname.ajs"))

    #--------------------------------------------------------------------------
    # try to re-run, check that case was not launched.
    #--------------------------------------------------------------------------

    activateOBContextMenuItem("CurrentCase", "Re-run")

    #--------------------------------------------------------------------------
    # run case 1: execute case manually:
    #             Stage_1: Execute with Results
    #             Stage_2: Execute w/o Results
    #             Stage_3: Execute with Results
    #--------------------------------------------------------------------------
    
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 5, 5, 0, Qt.LeftButton)

    use_option( 'Stage_1', Execute )
    use_option( 'Stage_2', Execute )
    use_option( 'Stage_3', Execute )
    keep_results( 'Stage_1', True )
    keep_results( 'Stage_2', False )

    run_case( 'RunCase_1' )
    test.vp("VP1")

    #--------------------------------------------------------------------------
    # run case 2: re-execute case with reusing options from previous run
    #             Stage_1: Execute with Results
    #             Stage_2: Execute w/o Results
    #             Stage_3: Execute with Results
    #--------------------------------------------------------------------------
    
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateOBContextMenuItem("CurrentCase", "Re-run")

    wait_for_case( 'RunCase_2' )
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # run case 3: execute case manually:
    #             Stage_1: Reuse
    #             Stage_2: Execute with Results
    #             Stage_3: Execute with Results
    #--------------------------------------------------------------------------
    
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 5, 5, 0, Qt.LeftButton)

    use_option( 'Stage_1', Reuse )
    use_option( 'Stage_2', Execute )
    use_option( 'Stage_3', Execute )
    keep_results( 'Stage_2', True )

    run_case( 'RunCase_3' )
    test.vp("VP3")

    #--------------------------------------------------------------------------
    # run case 4: re-execute case with reusing options from previous run 
    #             Stage_1: Reuse
    #             Stage_2: Execute with Results
    #             Stage_3: Execute with Results
    #--------------------------------------------------------------------------
    
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateOBContextMenuItem("CurrentCase", "Re-run")

    wait_for_case( 'RunCase_4' )
    test.vp("VP4")

    #--------------------------------------------------------------------------
    # delete last stage
    #--------------------------------------------------------------------------

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateOBContextMenuItem("CurrentCase.Stage_3", "Delete")
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    # try to re-run, check that case was not launched.
    #--------------------------------------------------------------------------
    
    activateOBContextMenuItem("CurrentCase", "Re-run")
    test.vp("VP4")

    #--------------------------------------------------------------------------
    # add another stage
    #--------------------------------------------------------------------------

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # run case 5: re-execute case with reusing options from previous run 
    #             Stage_1: Reuse
    #             Stage_2: Execute with Results
    #             Stage_3: Execute with Results
    #--------------------------------------------------------------------------
    
    activateOBContextMenuItem("CurrentCase", "Re-run")

    wait_for_case( 'RunCase_5' )
    test.vp("VP5")
    
    #--------------------------------------------------------------------------
    # Run_Case_6: execute case manually
    #             Stage_1: Reuse
    #             Stage_2: Reuse
    #             Stage_3: Execute with Results
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 5, 5, 0, Qt.LeftButton)

    use_option( 'Stage_1', Reuse )
    use_option( 'Stage_2', Reuse )
    use_option( 'Stage_3', Execute )

    run_case( 'RunCase_6' )
    test.vp("VP6")

    #--------------------------------------------------------------------------
    # modify third stage
    #--------------------------------------------------------------------------
    
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateOBContextMenuItem("CurrentCase.Stage_3", "Mesh", "LIRE_MAILLAGE")

    #--------------------------------------------------------------------------
    # Run_Case_7: re-execute case with reusing options from previous run 
    #             Stage_1: Reuse
    #             Stage_2: Reuse
    #             Stage_3: Execute with Results
    #--------------------------------------------------------------------------
    
    activateOBContextMenuItem("CurrentCase", "Re-run")

    wait_for_case( 'RunCase_7' )
    test.vp("VP7")

    #--------------------------------------------------------------------------
    # modify second stage (reused in last run)
    #--------------------------------------------------------------------------
    
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateOBContextMenuItem("CurrentCase.Stage_2", "Mesh", "LIRE_MAILLAGE")

    #--------------------------------------------------------------------------
    # try to re-run, check that case was not launched.
    #--------------------------------------------------------------------------

    activateOBContextMenuItem("CurrentCase", "Re-run")
    test.vp("VP7")
