def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem( "Operations", "Add Stage" )

    # prepare variables
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.2"), 21, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Create Variable"))
    type(waitForObject(":Name_QLineEdit_2"), "a")
    type(waitForObject(":Expression_QLineEdit"), "'a'")
    clickButton(waitForObject(":OK_QPushButton"))
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.2"), 21, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Create Variable"))
    type(waitForObject(":Name_QLineEdit_2"), "b")
    type(waitForObject(":Expression_QLineEdit"), "'b'")
    clickButton(waitForObject(":OK_QPushButton"))

    # prepare table
    for i, fname in enumerate(["/tmp/blablabla", "/tmp/toto"]):
        openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 69, 15, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Output"))
        activateItem(waitForObjectItem(":Output_QMenu", "IMPR\\_TABLE"))
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Output.IMPR\\_TABLE_"+str(i+1)), 82, 12, 0, Qt.LeftButton)
        mouseClick(waitForObject(":TABLE_ComboBox"), 26, 18, 0, Qt.LeftButton)
        sendEvent("QMouseEvent", waitForObject(":TABLE_ComboBox"), QEvent.MouseButtonPress, 121, 54, Qt.LeftButton, 1, 0)
        mouseClick(waitForObject(":UNITE_QComboBox"), 121, 21, 0, Qt.LeftButton)
        clickButton(waitForObject(":UNITE_QPushButton"))
        type(waitForObject(":fileNameEdit_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":fileNameEdit_QLineEdit"), fname)
        type(waitForObject(":fileNameEdit_QLineEdit"), "<Return>")
        clickButton(waitForObject(":FORMAT_QCheckBox"))
        mouseClick(waitForObject(":FORMAT_ComboBox"), 155, 8, 0, Qt.LeftButton)
        mouseClick(waitForObjectItem(":FORMAT_ComboBox", "NUMPY"), 126, 10, 0, Qt.LeftButton)
        clickButton(waitForObject(":NOM_PARA_QCheckBox"))
        clickButton(waitForObject(":NOM_PARA_add_QToolButton"))
        type(waitForObject(":0_QLineEdit"), "x")
        clickButton(waitForObject(":NOM_PARA_add_QToolButton"))
        type(waitForObject(":1_QLineEdit"), "y")
        clickButton(waitForObject(":OK_QPushButton"))
        clickButton(waitForObject(":Yes_QPushButton"))

    # open export panel, leave it empty, close
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase"), 93, 14, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Export As Parametric"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.a"), 64, 9, 0, Qt.LeftButton)

    # open export panel, select variables, try closing
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase"), 93, 14, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Export As Parametric"))
    mouseClick(waitForObjectItem(":variables_QListWidget", "a"), 8, 7, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.a"), 64, 9, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))

    # open export panel, select output, try closing
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase"), 93, 14, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Export As Parametric"))
    setParameterValue(":output_cmd_QComboBox", "blablabla")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.a"), 64, 9, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))

    # open export panel, select output and variable, save
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase"), 93, 14, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Export As Parametric"))
    setParameterValue(":output_cmd_QComboBox", "blablabla")
    mouseClick(waitForObjectItem(":variables_QListWidget", "a"), 8, 7, 0, Qt.LeftButton)
    clickButton(waitForObject(":OK_QPushButton"))

    # open export panel, make no changes, close
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase"), 93, 14, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Export As Parametric"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.a"), 64, 9, 0, Qt.LeftButton)

    # open export panel, select variables, try closing
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase"), 93, 14, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Export As Parametric"))
    mouseClick(waitForObjectItem(":variables_QListWidget", "a"), 8, 7, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.a"), 64, 9, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))

    # open export panel, select output, try closing
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase"), 93, 14, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Export As Parametric"))
    setParameterValue(":output_cmd_QComboBox", "toto")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.a"), 64, 9, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
