#------------------------------------------------------------------------------
# Issue #1153 - Edition panel: management of macro keywords (CCTP 2.3.3)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1153

#------------------------------------------------------------------------------
def handleMessageBox(messageBox):
    #--------------------------------------------------------------------------
    test.log("MessageBox opened: '%s' - '%s'" % (messageBox.windowTitle, messageBox.text))
    messageBox.done(messageBox.Yes)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    installEventHandler("MessageBoxOpened", "handleMessageBox")
    activateMenuItem( "Operations", "Add Stage" )

    #--------------------------------------------------------------------------
    snippet = \
"""
mesh = LIRE_MAILLAGE(UNITE=20)

MACR_ADAP_MAIL(ADAPTATION='RAFFINEMENT_UNIFORME',
               MAILLAGE_N=mesh,
               MAILLAGE_NP1=CO('meshout'))

mesh2 = DEFI_GROUP(DETR_GROUP_MA=_F(NOM='group'),
                   MAILLAGE=meshout)
"""
    setClipboardText( snippet )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )

    #--------------------------------------------------------------------------
    isValid( True, "Stage_1", "Mesh", "mesh:6" )
    isValid( True, "Stage_1", "Post Processing", "MACR_ADAP_MAIL:7" )
    isValid( True, "Stage_1", "Post Processing", "MACR_ADAP_MAIL:7", "meshout:8" )
    isValid( True, "Stage_1", "Mesh:-3", "mesh2:9" )
    isValid( True, "Stage_1" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Post Processing", "MACR_ADAP_MAIL:7" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObject( tr( 'MAILLAGE_NP1', 'QLineEdit' ) ).text ), "meshout" )
#    test.verify( object.exists( trex( 'MAILLAGE_NP1-.*macro.*', 'QToolButton' ) ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Mesh:-3", "mesh2:9" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObject( tr( 'MAILLAGE', 'QComboBox' ) ).currentText ), "meshout" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_1" )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )

    actual = str( waitForObject( text_editor ).toPlainText() )
    test.verify( check_text_eq( snippet, actual ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )

    #--------------------------------------------------------------------------
    pass
