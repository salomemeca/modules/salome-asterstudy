#------------------------------------------------------------------------------
# Issue #1017 - Can't edit second text stage
#  - Check that "Edit" menu item is available for each text stage in a case

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def checkContens():
    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "Stage 1" )
    showOBItem( "CurrentCase", "Stage 1", "Deprecated" )
    showOBItem( "CurrentCase", "Stage 1", "Deprecated", "DEBUT" )
    showOBItem( "CurrentCase", "Stage 2" )
    showOBItem( "CurrentCase", "Stage 2", "Mesh" )
    showOBItem( "CurrentCase", "Stage 2", "Mesh", "mesh" )
    pass

def runCase():
    #--------------------------------------------------------------------------
    import os
    script = os.path.join( os.getcwd(), 'redmine_1017.py' )
    test.log( "Execute '%s' script" % script )

    #--------------------------------------------------------------------------
    activateMenuItem( "File", "Load Script..." )
    setParameterValue( ":fileNameEdit_QLineEdit", script )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    checkOBItem( "CurrentCase", "Stage 1" )
    selectObjectBrowserItem( "CurrentCase.Stage 1" )
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage 1", 43, 9, 0)
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    #--------------------------------------------------------------------------
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    text = "DEBUT()"
    waitForObject( text_editor ).appendPlainText( text )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkOBItem( "CurrentCase", "Stage 2" )
    selectObjectBrowserItem( "CurrentCase.Stage 2" )
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage 2", 43, 9, 0)
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    #--------------------------------------------------------------------------
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    text = "mesh = LIRE_MAILLAGE(UNITE=20)"
    waitForObject( text_editor ).appendPlainText( text )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage 1", 43, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode"))
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage 2", 43, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode"))
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    checkContens()

    #--------------------------------------------------------------------------
    pass
