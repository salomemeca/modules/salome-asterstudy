#------------------------------------------------------------------------------
# Issue #1273: Improper work of embed/unembed file functionality

import os

def main():
    source(findFile("scripts", "common.py"))
    global_start()


def cleanup():
    global_cleanup()


def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add empty stage in current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add command referencing undefined file
    setClipboardText("m = LIRE_MAILLAGE(UNITE=20)")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Paste")

    #--------------------------------------------------------------------------
    # select CurrentCase in Data Settings
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    test.vp("VP1")

    #--------------------------------------------------------------------------
    # select undefined file item, open 'Edit File' panel
    clickItem(":DataFiles_QTreeView", "Stage\\_1.20", 10, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":DataFilesBase.Edit_QToolButton"))

    #--------------------------------------------------------------------------
    # save study
    activateMenuItem("File", "Save")
    study = os.path.join(testdir(), "study_1273.ajs")
    setParameterValue(":fileNameEdit_QLineEdit", study)
    clickButton(waitForObject(":QFileDialog.Save_QPushButton"))

    #--------------------------------------------------------------------------
    # select existing file
    clickButton(waitForObject(":Filename.Filename_QToolButton"))
    file_path = os.path.join(casedir(), 'default.file')
    setParameterValue(":fileNameEdit_QLineEdit", file_path)
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))

    #--------------------------------------------------------------------------
    # embed file: switch ON 'Embedded' check box
    clickButton(waitForObject(":Embedded_QCheckBox"))

    #--------------------------------------------------------------------------
    # close 'Edit file' panel by pressing 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    test.vp("VP2")

    #--------------------------------------------------------------------------
    # exit application
    activateItem(waitForObjectItem(":AsterStudy_QMenuBar", "File"))
    activateItem(waitForObjectItem(":File_QMenu", "Exit"))
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
