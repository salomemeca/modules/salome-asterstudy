def main():
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )


def cleanup():
    global_cleanup()


def runCase():
    #--------------------------------------------------------------------------
    createNewStudy()
    test.verify( waitForObjectExists( "{column='0' container=':CasesTreeView_QTreeView' text='CurrentCase' type='QModelIndex'}" ).selected )

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    test.verify( waitForObjectExists( "{column='0' container=':Data Settings_SettingsView' text='CurrentCase' type='QModelIndex'}" ).selected )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    saveFile( os.path.join( testdir(), "test_1258.ajs" ) )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    # run one stage
    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )

    run_case( "RunCase_1" )

    waitForObjectItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1" )
    clickItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 39, 8, 0, Qt.LeftButton )

    #--------------------------------------------------------------------------
    # activate "Output" window
    #--------------------------------------------------------------------------
    clickTab(waitForObject(":Data Files_TabWidget"), "Output")

    # check that Console was updated after activation according to the selected RunCase
    t1 = "Output messages for stage Stage_1"
    t2 = str( waitForObject( ":_QTextEdit" ).toPlainText() )
    test.verify( t1 in t2 )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    # run two stages
    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Reuse )
    use_option( 'Stage_2', Execute )

#    run_case( "RunCase_2" )
    open_run_dialog()
    execute_case()

    waitForObjectItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_2" )
    clickItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_2", 39, 8, 0, Qt.LeftButton )

    clickTab(waitForObject(":Data Files_TabWidget"), "Output")

    t1 = "Output messages for stage Stage_2"
    s = 0
    while s < 10:
        snooze(0.5)
        t2 = str( waitForObject( ":_QTextEdit" ).toPlainText() )
        if t1 in t2:
            break
        s += 0.5

    # check that Console was updated after run
    test.verify( t1 in t2 )
    t1 = "Output messages for stage Stage_1"
    test.verify( t1 not in t2 )

    waitForObjectItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1" )
    clickItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 39, 8, 0, Qt.LeftButton )

    # check that Console was updated after selection another RunCase
    t1 = "Output messages for stage Stage_1"
    t2 = str( waitForObject( ":_QTextEdit" ).toPlainText() )
    test.verify( t1 in t2 )
    t1 = "Output messages for stage Stage_2"
    test.verify( t1 not in t2 )
