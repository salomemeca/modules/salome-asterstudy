#------------------------------------------------------------------------------
# Bug #991 - DataFiles panel should not keep temporary file descriptors

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1" )
    addCommandByDialog( "PRE_GMSH", "Other" )
    test.compare( isItemExists( "Stage_1", "Other", "PRE_GMSH" ), True )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Other", "PRE_GMSH" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------

    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    stage = waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    model = stage.model()
    test.compare( model.rowCount( stage ), 0 )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_GMSH', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    def_file_path = os.path.join( casedir(), 'default.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", def_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( def_file_path ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #--------------------------------------------------------------------------

    stage = waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    model = stage.model()
    test.compare( model.rowCount( stage ), 0 )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
