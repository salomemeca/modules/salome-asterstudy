def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem( "Operations", "Add Stage" )
    
    activateOBContextMenuItem("CurrentCase.Stage_1", "Functions and Lists", "DEFI_FONCTION")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Functions and Lists", "DEFI_FONCTION")
    
    # leave unmodified, close
    mouseClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 80, 7, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Text_QToolButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func0"), 63, 12, 0, Qt.LeftButton)
    
    # edit text, try closing
    mouseClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 80, 7, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Text_QToolButton"))
    type(waitForObject(":text_editor_PyEditor_Editor"), "<Ctrl+A>")
    type(waitForObject(":text_editor_PyEditor_Editor"), "func = DEFI_FONCTION(NOM_PARA='X', VALE=(1.0, 2.0))")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func0"), 63, 12, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
    
    # edit text, save, close
    mouseClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 80, 7, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Text_QToolButton"))
    type(waitForObject(":text_editor_PyEditor_Editor"), "<Ctrl+A>")
    type(waitForObject(":text_editor_PyEditor_Editor"), "func = DEFI_FONCTION(NOM_PARA='X', VALE=(1.0, 2.0))")
    clickButton(waitForObject(":Apply_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func0"), 63, 12, 0, Qt.LeftButton)

    # one more action, to be sure we are not seeing a dialog now
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 63, 12, 0, Qt.LeftButton)