def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()


def add_and_edit_command():
    addCommandByDialog( "DEFI_LIST_REEL", "Functions and Lists" )

    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "DEFI_LIST_REEL" )

    #--------------------------------------------------------------------------
    # modify the command
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Functions and Lists.DEFI_LIST_REEL", "Edit")
    waitForObject(":qt_splithandle__EditionPanel")

    symbol = tr( 'VALE', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    doubleClickItem(":_FunctionTable", "0/0", 5, 5, 0, Qt.LeftButton)
    cell_0_0 = "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    type(waitForObject(cell_0_0), "0.5")
    type(waitForObject(cell_0_0), "<Return>")

def save_check_delete():
    # save modification
    clickButton( waitForObject( ":OK_QPushButton" ) )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    # check that modification has been saved
    selectObjectBrowserItem( "CurrentCase.Stage_1.Functions and Lists.DEFI_LIST_REEL" )
    expected = \
"""
DEFI_LIST_REEL = DEFI_LIST_REEL(
  VALE=(0.5, )
)
"""
    actual = str(waitForObject(tr('information_view', 'QTextEdit')).plainText)
    test.verify(check_text_eq(expected, actual))

    activateOBContextMenuItem( "CurrentCase.Stage_1.Functions and Lists.DEFI_LIST_REEL", "Delete" )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    selectObjectBrowserItem( "CurrentCase.Stage_1" )


def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget"), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )

    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Stage_1" ) )

    treeWidget = waitForObject(":_QTreeWidget")

    #--------------------------------------------------------------------------
    oper_list = [
                ("File", "New"),
                ("File", "Open..."),
                ("File", "Close"),
                ("Edit", "Undo"),
                ("Edit", "Edit"),
                ("Edit", "Duplicate"),
                ("Edit", "Delete"),
                ("Operations", "Back Up"),
                ("Operations", "Add Stage"),
                ("Operations", "Add Stage from File"),
                ("Commands", "Create Variable"),
                ("Commands", "Show All"),
                ("Commands", "Edit Comment")
                ]

    for menu, oper in oper_list:
        # create a new command and start edition of it
        add_and_edit_command()

        # try to start another operation
        waitForActivateMenuItem( menu, oper )

        # cancel edition interruption
        clickButton( waitForObject( ":Operation performed.No_QPushButton" ) )

        # save modification, check it and delete command
        save_check_delete()

    #--------------------------------------------------------------------------
    # create a new command and start edition of it
    add_and_edit_command()

    # try to add a new command
    mouseClick( waitForObject( ":AsterStudy *_ShrinkingComboBox" ), 59, 15, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( ":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE" ), 51, 11, 0, Qt.LeftButton )

    # cancel edition interruption
    clickButton( waitForObject( ":Operation performed.No_QPushButton" ) )

    # save modification, check it and delete command
    save_check_delete()

    #--------------------------------------------------------------------------
    # just create a new command without edition
    addCommandByDialog( "LIRE_MAILLAGE", "Mesh" )

    # create a new command and start edition of it
    add_and_edit_command()

    # try to modify another command
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.LIRE_MAILLAGE", "Edit")

    # cancel edition interruption
    clickButton( waitForObject( ":Operation performed.No_QPushButton" ) )

    # save modification, check it and delete command
    save_check_delete()

    #--------------------------------------------------------------------------
    # export stage
    file_name = 'tst_redmine_1314_command_edit_interruption.ajs'
    file_path = os.path.join( squishinfo.resultDir, file_name )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    clickButton( waitForObject( ":AsterStudy *.Export Command File_QToolButton" ) )
    setParameterValue( ":fileNameEdit_QLineEdit", file_path )
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )
    waitUntilObjectExists( ":QFileDialog.Save_QPushButton" )

    # check that export does not block execution
    clickButton( waitForObject( ":AsterStudy *.Show All_QToolButton" ) )
    test.verify( waitForObject( ":_QLineEdit" ) )
