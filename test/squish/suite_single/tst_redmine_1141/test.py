#------------------------------------------------------------------------------
# Issue #1141 - Data Settings view: show catalogue name of command

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(nativenames=False, basicnaming=False)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # [section] Create New study
    # [step] Menu File - New
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # [section] Create empty stage in current case
    # [step] Switch to current case
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    # [step] Select CurrentCase object in Data Settings panel, invoke "Add Stage" item from popup menu
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    # [section] Add two commands to the stage
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Mesh" / "Read a mesh" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "Read a mesh")
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Output" / "Results output" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Output", "Set output results")

    #--------------------------------------------------------------------------
    # [section] Check Data Settings panel
    # [step] Check that the item "CurrentCase/Stage_1/Mesh/mesh" is present in view
    checkItemColumns("CurrentCase", "Stage_1", "Mesh", "mesh", "Read a mesh")
    # [step] Check that the item "CurrentCase/Stage_1/Output/Results output" is present in view
    checkItemColumns("CurrentCase", "Stage_1", "Output", "Set output results", "Set output results")

    #--------------------------------------------------------------------------
    # [section] Show second column in Data Settings panel
    # [step] Menu Edit - Preferences
    activateMenuItem("File", "Preferences...")
    # [step] Switch ON "Show catalogue name in Data Settings panel" check box
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_QCheckBox"))
    # [step] Preferences dialog: press OK button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Check Data Settings panel
    # [step] Check that the item "CurrentCase/Stage_1/Mesh/mesh" is present in view
    checkItemColumns("CurrentCase", "Stage_1", "Mesh", "mesh", "Read a mesh")
    # [step] Check that the item "CurrentCase/Stage_1/Output/[noname]" is present in view
    checkItemColumns("CurrentCase", "Stage_1", "Output", "[noname]", "Set output results")

    #--------------------------------------------------------------------------
    # [section] Quit application
    # [step] Menu File - Exit
    activateMenuItem("File", "Exit")
    # [step] Press "Discard" button in confirmation dialog
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass

#--------------------------------------------------------------------------
def checkItemColumns(*path):
    item_type = path[-1]
    path = path[:-1]
    item = checkOBItem(*path)
    sibling = item.sibling(item.row, item.column+1)
    test.compare(sibling.text, item_type)
