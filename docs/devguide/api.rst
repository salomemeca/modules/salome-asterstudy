.. _devguide-api:

*************************************
AsterStudy's API for external modules
*************************************

Class Diagram
=============

.. inheritance-diagram::
    asterstudy.api.execution
    asterstudy.api.parametric
   :parts: 1

Details
=======

.. automodule:: asterstudy.api.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.api.execution
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.api.parametric
   :show-inheritance:
   :members:
   :special-members: __init__
