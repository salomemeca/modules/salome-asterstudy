# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2021 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# person_in_charge: j-pierre.lefebvre at edf.fr

from ..Commons import *
from ..Language.DataStructure import *
from ..Language.Syntax import *

FIN = FIN_PROC(nom="FIN",
               op=9999,
               repetable='n',
               fr=tr("Fin d'une étude, fin du travail engagé par une des commandes DEBUT ou POURSUITE"),

        # FIN est appelé prématurément en cas d'exception ("SIGUSR1", ArretCPUError,
        # NonConvergenceError..., erreurs <S> ou erreurs <F> récupérées).
        # En cas d'ArretCPUError, on limite au maximum le travail à faire dans FIN.
        RETASSAGE=SIMP(
            fr=tr("retassage de la base GLOBALE"),
            statut='f', typ='TXM', defaut="NON", into=("OUI", "NON",)),
        INFO_RESU=SIMP(
            fr=tr("impression des informations sur les structures de données résultats"),
            statut='f', typ='TXM', defaut="NON", into=("OUI", "NON",)),

        PROC0=SIMP(statut='f',typ='TXM',defaut="OUI",into=("OUI","NON") ),
)
